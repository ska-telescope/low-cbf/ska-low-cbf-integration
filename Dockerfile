ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.4.2"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-pytango-runtime:9.4.2"

FROM $BUILD_IMAGE as buildenv
FROM $BASE_IMAGE

USER root

RUN apt update && apt install -y git && rm -rf /var/lib/apt/lists/*

RUN poetry config virtualenvs.create false
RUN poetry install
# this nasty hack is needed as our container does not have the 'netbase' package installed
RUN sed -i.bak 's/\\udp/  17/' /usr/local/lib/python3.10/dist-packages/spead2/_spead2.cpython-310-x86_64-linux-gnu.so

USER tango

