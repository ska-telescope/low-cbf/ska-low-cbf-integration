Welcome to ska-low-cbf-integration's documentation!
===================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   api/index

.. toctree::
   :maxdepth: 2
   :caption: Device Proxy JSON Wrapper

   json_wrapper

.. toctree::
   :maxdepth: 2
   :caption: SPS Analyser

   SPS

.. toctree::
   :maxdepth: 2
   :caption: Visibility Analyser

   Visibility

.. toctree::
   :maxdepth: 2
   :caption: Integration Testing Pipeline

   ci_pipeline.rst

.. toctree::
   :maxdepth: 2
   :caption: Taranta

   taranta.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
