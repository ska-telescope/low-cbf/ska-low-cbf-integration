Using Taranta in Low PSI
========================

Some pieces of Taranta infrastructure are shared for all Low PSI users (authentication,
dashboard storage) - see the ``taranta`` Kubernetes namespace. We shouldn't try to
deploy those pieces within Low PSI again.

You can deploy taranta in the Low PSI (without duplicating the shared pieces) like this:

.. code-block:: bash

    PSI_SERVER=psi-perentie2 TARANTA=true TARANTA_AUTH=false TARANTA_DASH=false make k8s-install-chart
