Integration Testing CI Pipeline
===============================

Integration tests are run in the Low PSI, via GitLab job ``psi-low-test``.

These tests are performed at the Tango control system level, using multiple Low CBF sub-components.

Special Types of Test
---------------------

Long Tests
^^^^^^^^^^

Apply the mark ``@pytest.mark,long_test`` to any tests that take an excessively long
time. To control whether these long tests will be executed, set ``PYTEST_MARKS``.

e.g. to run tests that use real hardware but not the long tests:
``PYTEST_MARKS='hardware_present and not long_test'``

Large Capture
^^^^^^^^^^^^^

By default, we use u280 FPGAs to run our CNICs, meaning we can capture ~4GB. To go
beyond this, apply the mark ``@pytest.mark.large_capture`` to your test. Large capture
mode is activated by setting the ``LARGE_CAPTURE`` environment variable to a non-empty
value.

Version Specification
---------------------

The CI tests are designed to be able to test a particular version of any sub component(s). Versions can be requested by
upstream pipelines using these environment variables:

- ``CNIC_FW_VERSION``: `CNIC Firmware <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic>`_
- ``CORR_FW_VERSION``: `Correlator Firmware <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-corr>`_
- ``PSS_FW_VERSION``: `PSS Firmware <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pss>`_
- ``PST_FW_VERSION``: `PST Firmware <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst>`_
- ``SKA_LOW_CBF_VERSION``: `Low CBF Monitoring & Control <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf>`_
- ``SKA_LOW_CBF_PROC_VERSION``: `Processor Tango Device <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc>`_
- ``SKA_LOW_CBF_TANGO_CNIC_VERSION``: `CNIC Tango Device <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-tango-cnic>`_

By default, software components use their latest release, from the
`SKAO Central Artefact Repository (CAR) <https://artefact.skao.int>`_.
If a software version contains ``dev``, the corresponding Helm chart &
container image will be sourced from GitLab instead (using a GitLab project ID specified
in ``Makefile``).

FPGA firmware defaults to the latest build from the main branch. A specific
version can be requested via the relevant ``<name>_FW_VERSION`` variable. These are
always sourced from GitLab.

CNIC Tests
----------

``test_cnic_p4.py`` uses a CNIC in duplex mode & the shared PSI P4 switch to verify CNIC operation.

Processor Tests
---------------

``test_processor.py`` shows an example of testing a Low CBF signal processing FPGA firmware image.

The example test function is ``test_correlator``, which performs multiple Correlator tests using a requested firmware
version.

The generic test routine (configure control system, transmit from CNIC, capture SUT output) is performed by the
``cnic_processor`` function.

The ``cnic_processor`` routine makes use of fixtures:

- ``ready_subarray``, which returns a Subarray device proxy that is, you guessed it, ready to configure.
- ``cnic_and_processor``, which returns device proxies for all CNICs and Processors.
  It configures network routing (using the Low PSI shared Connector device) between the
  first CNIC & first Processor. The routes are cleaned after use.

A data class ``ProcessorTestConfig`` is used to collate the various test parameters.

FPGA Register Logging
^^^^^^^^^^^^^^^^^^^^^

If environment variable ``FPGA_LOG_REGISTERS`` is set to either ``1`` or ``true``, the ``cnic_processor`` function will
ask the Processor to log register activity to ``/test-data/<short commit hash> <timestamp> <test name>.log``.

Manually Running the CI Pipeline
--------------------------------

When developing a new test, you might want to avoid the round-trip delays of pushing to GitLab and waiting for the whole
pipeline to run. Instead, you can use the `run-psi-test.sh` script to replicate the Low PSI test portion of the CI
pipeline from the comfort of your own laptop. It accepts the same environment variables, and tries to make sensible
guesses about what you'd like to do (e.g. using the latest integration container from your branch, falling back to the
main branch if your branch doesn't have one).

Extra Environment Variables for Manual Use
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- ``DEBUG_DIRTY``: inherited from SKA Makefiles, this lets you run the tests when you have local uncommitted changes.
  Beware that if you have changed code in ``src`` (as opposed to ``tests``), you need to push this to GitLab to get a new
  package built! If you know you have only changed tests, supply ``DEBUG_DIRTY=1`` to run the script.
- ``PYTEST_FILTER``: to filter tests. For example, to run PTCs 23 and 25
  ``PYTEST_FILTER='ptc23 or ptc25'``
- ``PYTEST_MARKS``: to filter tests using pytest marks. For example
  ``PYTEST_MARKS='large_capture and hardware_present'``
- ``EXTRA_PYTEST_ARGS``: as you might guess from the name, adds extra arguments to
  ``pytest``. For example if you want to only collect tests ``EXTRA_PYTEST_ARGS=--co``.
