Device Proxy JSON Wrapper
=========================

:py:class:`~ska_low_cbf_integration.tango.DeviceProxyJson` is a convenience wrapper that transparently encodes/decodes
`JSON <https://en.wikipedia.org/wiki/JSON>`_ strings on their way to/from a :py:class:`tango.DeviceProxy`.

Usage
-----

The intent is that operation is transparent and things work as per a normal :py:class:`~tango.DeviceProxy`,
except you're now free of the drudgery of typing ``json.loads`` and ``json.dumps`` all the time.

Instantiation
^^^^^^^^^^^^^

You can instantiate via an existing :py:class:`~tango.DeviceProxy` object, or just pass a device name and
one will be created for you:

.. code-block:: python

    # Using a DeviceProxy
    cnic_proxy = tango.DeviceProxy("low-cbf/cnic/1")
    cnic = DeviceProxyJson(cnic_proxy)

    # Or, straight from the name
    cnic = DeviceProxyJson("low-cbf/cnic/1")


Attributes & Commands using JSON are automatically detected if their description includes "JSON".
Optional parameters can add Attributes & Commands that are not picked up by the automatic detection logic:

.. code-block:: python

    cnic = DeviceProxyJson("low-cbf/cnic/1", json_commands=["CallMethod"], json_attributes=["vd__configuration"])

You can also exclude Attributes & Commands that are incorrectly detected:

.. code-block:: python

    # Perhaps MyCommand's doc_in says "Use CSV, never JSON!"
    # and notJsonAttr's description is "This is a colon-separated list, NOT JSON!"
    # These would normally be automatically detected as using JSON by DeviceProxyJson!
    DeviceProxyJson("my/device/name", not_json_commands=["MyCommand"], not_json_attributes=["notJsonAttr"])

These parameters can be combined as you see fit. If you try to include & exclude the same thing, exclusion wins.

Reading a JSON Attribute
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    cnic = DeviceProxyJson("low-cbf/cnic/1", json_attributes=["vd__configuration"])
    # We can loop over a list of dicts
    for cfg in cnic.vd__configuration:  # look Ma, no json.loads !
        print("Beam:", cfg["beam"], "Station:", cfg["station"], cfg["substation"])

.. code-block::

    Beam: 1 Station: 345 1
    Beam: 2 Station: 346 1
    Beam: 3 Station: 347 1
    Beam: 4 Station: 348 1
    Beam: 5 Station: 349 1

Sending a JSON Command
^^^^^^^^^^^^^^^^^^^^^^

If the Command is expecting a ``dict``, you can use either keyword arguments, or pass a ``dict`` directly, your choice.
Note that :py:class:`~ska_low_cbf_integration.tango.DeviceProxyJson` has no knowledge of the underlying Tango Device's
JSON schema, so you're on your own in terms of what data structure or keywords to supply.

.. code-block:: python

    cnic = DeviceProxyJson(cnic_proxy, json_commands=["CallMethod"])

    # Keyword Arguments
    cnic.CallMethod(method="stop_receive")  # Command explicitly configured as JSON above

    # Any data type that can be processed by json.dumps can be used directly
    request = {"version": "0.1.9", "source": "nexus"}
    cnic.SelectPersonality(request)  # Command auto-detected, as its doc_in contains "JSON"

API
---

.. autoclass:: ska_low_cbf_integration.tango.DeviceProxyJson
   :members:
   :special-members: __init__, __setattr__, __getattr__
