"""Configuration file for Sphinx."""
import os
import sys

sys.path.insert(0, os.path.abspath("../../src"))
sys.path.insert(0, os.path.abspath("../../tests"))

project = "Low CBF Integration "
copyright = "2020-2023, CSIRO"
author = "Perentie Team"

# The short X.Y version
version = "0.0.1"
# The full version, including alpha/beta/rc tags
release = "0.0.1"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_autodoc_typehints",
    "sphinx.ext.autosummary",
]

exclude_patterns = []

html_css_files = [
    "css/custom.css",
]

html_theme = "ska_ser_sphinx_theme"
html_theme_options = {}

autodoc_mock_imports = ["tango"]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3.10", None),
    "pytango": ("https://pytango.readthedocs.io/en/stable/", None),
}

nitpicky = True

# nitpick_ignore = [
# ]
