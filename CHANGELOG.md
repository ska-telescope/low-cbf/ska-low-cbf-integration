# Version History

## unreleased
* New version of the visibility Analyser compliant to latest ICD
* Bootstrap repo with ska-cookiecutter-pypackage
* PTC 11 test added
* Add SQL functions; update EDA configuration file
* In Helm charts, rename Alveo resource name (in k8s cluster) from `xilinx.com/fpga-xilinx_u55c_gen3x16_xdma_base_3-0` to `amd.com/xilinx_u55c_gen3x16_xdma_base_3-0`
  Use development versions of CNIC-VD and PROC until next release
* Add ska-tmc-low and ska-csp-lmc-low charts
* 7-May-2024 add Jupyter notebooks where subarrays are TMC controlled
  - Visibilities-Signal_Chain_TMC_Controlled.ipynb
  - Signal_Chain_TMC_Controlled_with_pulse.ipynb
  - updated .make directory
* 24-May-2024 SQL functions to fetch PST beam poly delays from data base
* 23-Jul-2024 update EDA configuration `eda_ska_low_cbf.yaml` file
  - add processor's hardware/functional/processing health Tango attributes
  - remove obsolete attributes
* 7-Aug-2024 added wireshark dissector for PSR
* 8-Aug-2024 PSR dissector to show PacketDest field as a string

