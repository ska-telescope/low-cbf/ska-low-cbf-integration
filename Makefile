PROJECT = ska-low-cbf-integration
PYTHON_LINE_LENGTH = 88

# GitLab Project IDs of ska-low-cbf sub components
SKA_LOW_CBF_GITLAB_PROJECT_ID ?= 15856638
SKA_LOW_CBF_CONN_GITLAB_PROJECT_ID ?= 33186816
SKA_LOW_CBF_PROC_GITLAB_PROJECT_ID ?= 19426925
SKA_LOW_CBF_TANGO_CNIC_GITLAB_PROJECT_ID ?= 47111964
# Optional Extra pytest arguments (e.g. if you want to filter tests)
EXTRA_PYTEST_ARGS ?=

# Tango/Kubernetes Settings
KUBE_NAMESPACE ?= $(PROJECT)-$(shell whoami)
KUBE_APP = ska-low-cbf
CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test
XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
# Deploy CSP LMC?
CSP ?= false
# Deploy TMC?
TMC ?= false
TARANTA ?= false# Enable Taranta
TARANTA_AUTH ?= $(TARANTA) # Enable taranta authentication pod?
TARANTA_DASH ?= $(TARANTA) # Enable taranta dashboard pod & PVC?
MINIKUBE ?= true ## Minikube or not
EXPOSE_All_DS ?= true ## Expose All Tango Services to the external network (enable Loadbalancer service)
N_PROCESSORS ?= 1 ## Number of Processor devices to deploy

# HTTP Proxy required for Low PSI (used in .make/k8s.mk)
PROXY_VALUES ?= --env=http_proxy=${http_proxy} --env=https_proxy=${https_proxy}

# SKA standard make includes (adds `make make`, `make help`, etc)
include .make/base.mk
include .make/helm.mk
include .make/k8s.mk
include .make/oci.mk
include .make/python.mk

DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build
### LOW PSI LOGIC

# option used to enable settings for use at PSI Low site
# if user specifies a PSI_SERVER, then we infer they want to use Low PSI!
ifeq ($(origin PSI_SERVER),undefined)
    PSI_LOW ?=
else
    # PSI_SERVER value was supplied
    PSI_LOW ?= true
endif

# default to perentie1 for running CI jobs
PSI_SERVER ?= perentie1
# remove "psi-" prefix (in case user specifies actual hostname, e.g. 'psi-perentie1')
# this will give us a path like 'charts/psi-low-perentie1.values.yaml'
PSI_SERVER_VALUES_FILE ?= charts/psi-low-$(PSI_SERVER:psi-%=%).values.yaml
TEST_RUNNER_OVERRIDES_FILE ?= charts/test-runner-$(PSI_SERVER:psi-%=%)-overrides.json

ifneq ($(PSI_LOW),)
# PSI_LOW is active
# settings for psi-low-test CI job
    PYTHON_VARS_BEFORE_PYTEST := export TANGO_HOST=$(TANGO_HOST); \
        export LARGE_CAPTURE=$(LARGE_CAPTURE); \
        export CNIC_FW_VERSION=$(CNIC_FW_VERSION); \
        export CORR_FW_VERSION=$(CORR_FW_VERSION); \
        export PSS_FW_VERSION=$(PSS_FW_VERSION); \
        export PST_FW_VERSION=$(PST_FW_VERSION); \
        export CI_COMMIT_SHORT_SHA=$(CI_COMMIT_SHORT_SHA); \
        export FPGA_LOG_REGISTERS=$(FPGA_LOG_REGISTERS);
    PYTHON_VARS_AFTER_PYTEST := --disable-pytest-warnings \
        --count=1 --timeout=300 --forked --true-context --verbose
    PYTEST_MARKS ?= hardware_present
    K8S_TEST_RUNNER_ADD_ARGS := --override-type="strategic" \
        --overrides="$$(cat $(TEST_RUNNER_OVERRIDES_FILE) | sed 's/test-runner-local/$(K8S_TEST_RUNNER)/g')"
    # PSI Low common values
    SCRIPT_VALUES := --values charts/psi-low.values.yaml
    # Server-specific values
    SCRIPT_VALUES += --values $(PSI_SERVER_VALUES_FILE)

# Settings to use PSI Low shared Taranta (user to set TARANTA=true as well)
    TARANTA_AUTH=false
    TARANTA_DASH=false
    MINIKUBE=false
else ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# PSI_LOW is not active, we are running k8s-test (presumably in Minikube)
    $(error k8s-test without PSI_SERVER/LOW_PSI set is not supported)
else ifeq ($(strip $(firstword $(MAKECMDGOALS))),python-test)
# PSI_LOW is not active
# settings for python-test CI job - only tests that don't need hardware
    PYTEST_MARKS ?= not hardware_present
endif

# Deploy more processor instances if requested (subject to limits)
MAX_PROCESSORS ?= 6
ifeq ($(shell [ ${N_PROCESSORS} -le ${MAX_PROCESSORS} ] && echo true),true)
    # 1 processor is the default and there is no 'processor-instance-1.yaml' file
    # so use the values file only if N_PROCESSORS > 1
    ifeq ($(shell [ ${N_PROCESSORS} -gt 1 ] && echo true),true)
        SCRIPT_VALUES += --values charts/processor-instances-$(strip ${N_PROCESSORS}).yaml
    endif
else
    # if greater than MAX_PROCESSORS processors requested, throw an error
    $(error N_PROCESSORS=${N_PROCESSORS} is not supported; max: ${MAX_PROCESSORS})
endif

ifneq ($(LARGE_CAPTURE),)
    # Use u55c for CNIC if we need to capture large files
    SCRIPT_VALUES += --values charts/cnic-u55c.yaml
    # Note: activating LARGE_CAPTURE does not deselect small capture tests, but we
    # could do that if we wanted to by adding 'large_capture' to PYTEST_MARKS
else
    ifeq ($(findstring not large_capture,$(PYTEST_MARKS)),)
        # if "not large_capture" is not already specified in PYTEST_MARKS
        $(info LARGE_CAPTURE is not set, de-selecting tests that need it)
        ifneq ($(PYTEST_MARKS),)
            PYTEST_MARKS += and
        endif
        PYTEST_MARKS += not large_capture
    endif
endif

ifneq ($(PYTEST_MARKS),)
    PYTEST_MARKS := '$(PYTEST_MARKS)'
    PYTHON_VARS_AFTER_PYTEST += -m $(PYTEST_MARKS)
    $(info pytest marks: $(PYTEST_MARKS))
endif
ifneq ($(PYTEST_FILTER),)
    PYTEST_FILTER := '$(PYTEST_FILTER)'
    PYTHON_VARS_AFTER_PYTEST += -k $(PYTEST_FILTER)
    $(info pytest filter: $(PYTEST_FILTER))
endif

# Add user-supplied pytest args last
PYTHON_VARS_AFTER_PYTEST += $(EXTRA_PYTEST_ARGS)

# Add the ability to specify a VALUES_FILE at runtime
ifneq ($(VALUES_FILE),)
    USER_VALUES := --values $(VALUES_FILE)
endif

ifneq ($(CI_REGISTRY),)
    K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/low-cbf/$(PROJECT)/$(PROJECT):$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
    K8S_TEST_IMAGE_TO_TEST=artefact.skao.int/$(PROJECT):$(VERSION)
endif

DEPLOY_PARAMS = --set ska-tmc-low.enabled=$(TMC) \
                --set ska-csp-lmc-low.enabled=$(CSP)

TARANTA_PARAMS = --set ska-taranta.enabled=$(TARANTA) \
                 --set ska-taranta-auth.enabled=$(TARANTA_AUTH) \
                 --set ska-dashboard-repo.enabled=$(TARANTA_DASH)

# default value to match that provided by GitLab when running in CI, used in image registry URLs
CI_REGISTRY ?= registry.gitlab.com

# Override image tag when user requests a specific version,
# also override image registry for dev versions
# Note: Helm chart versions & repositories are modified in k8s-pre-install-chart below
ifneq ($(SKA_LOW_CBF_VERSION),)
    IMAGE_OVERRIDES += --set ska-low-cbf.cbf.image.tag=$(SKA_LOW_CBF_VERSION)
    ifneq ($(findstring dev,$(SKA_LOW_CBF_VERSION)),)
        # dev version requested
        IMAGE_OVERRIDES += --set ska-low-cbf.cbf.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf
    endif
endif
ifneq ($(SKA_LOW_CBF_PROC_VERSION),)
    ifneq ($(findstring dev,$(SKA_LOW_CBF_PROC_VERSION)),)
        # dev version requested
        IMAGE_OVERRIDES += --set ska-low-cbf-proc.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-proc
    endif
endif
ifneq ($(SKA_LOW_CBF_TANGO_CNIC_VERSION),)
    ifneq ($(findstring dev,$(SKA_LOW_CBF_TANGO_CNIC_VERSION)),)
        # dev version requested
        IMAGE_OVERRIDES += --set ska-low-cbf-tango-cnic.cnic.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-tango-cnic
    endif
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
    --set global.exposeAllDS=$(EXPOSE_All_DS) \
    --set global.tango_host=$(TANGO_HOST) \
    --set global.cluster_domain=$(CLUSTER_DOMAIN) \
    --set global.device_server_port=$(TANGO_SERVER_PORT) \
    --set ska-tango-base.display=$(DISPLAY) \
    --set ska-tango-base.xauthority=$(XAUTHORITY) \
    --set ska-tango-base.jive.enabled=$(JIVE) \
    --set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
    $(IMAGE_OVERRIDES) \
    $(TARANTA_PARAMS) \
    $(DEPLOY_PARAMS) \
    ${SCRIPT_VALUES} \
    ${USER_VALUES}  # user-supplied VALUES_FILE as ultimate source of truth

k8s-pre-test:
ifneq ($(PSI_LOW),)
##	Enable PVC creation for psi-low-test
	kubectl apply -n $(KUBE_NAMESPACE) -f charts/perentie-test-data-pvc.yaml
	@echo -e "Using \033[1;33mCNIC firmware\033[0m version \033[1;36m${CNIC_FW_VERSION}\033[0m from \033[1;36m${CNIC_FW_SOURCE}\033[0m"
	@echo -e "Using \033[1;33mCORR firmware\033[0m version \033[1;36m${CORR_FW_VERSION}\033[0m from \033[1;36m${CORR_FW_SOURCE}\033[0m"
	@echo -e "Using \033[1;33mPST firmware\033[0m version \033[1;36m${PST_FW_VERSION}\033[0m from \033[1;36m${PST_FW_SOURCE}\033[0m"
	@echo -e "\033[1;33mFPGA_LOG_REGISTERS\033[0m = \033[1;36m${FPGA_LOG_REGISTERS}\033[0m"
else
endif

k8s-pre-install-chart:
	# Modify Helm sub-chart versions (and repositories) as per env vars
	# script copies Chart.original.yaml to Chart.yaml then modifies it
	# (no changes other than sub-chart version/repository are made)
	poetry install  # to make chart_dep_mod python script available
	SKA_LOW_CBF_GITLAB_PROJECT_ID=$(SKA_LOW_CBF_GITLAB_PROJECT_ID) \
	SKA_LOW_CBF_CONN_GITLAB_PROJECT_ID=$(SKA_LOW_CBF_CONN_GITLAB_PROJECT_ID) \
	SKA_LOW_CBF_PROC_GITLAB_PROJECT_ID=$(SKA_LOW_CBF_PROC_GITLAB_PROJECT_ID) \
	SKA_LOW_CBF_TANGO_CNIC_GITLAB_PROJECT_ID=$(SKA_LOW_CBF_TANGO_CNIC_GITLAB_PROJECT_ID) \
	poetry run scripts/mod_all_charts.sh
