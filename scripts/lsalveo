#!/usr/bin/env python3

"""Discover processor pods with Alveo cards in Kubernetes cluster and
show their details (BDF, serial number P4 switch port).
Normally exectued on psi-head host (unless KUBECONFIG is tweaked).

Depends on 'kubernetes' 3rd party library:
  $ pip install --user kubernetes
"""


import re

from kubernetes import client, config
from kubernetes.client.rest import ApiException
from kubernetes.stream import stream

# TODO use Google Doc API to fetch these from the Hardware Register spreadsheet
bdf_to_sn_port = {
    # PCIe bus enumeration can be the same across two hosts:
    'psi-perentie1':
    {
        "0000:53:00.1": {"s/n": "XFL14SLO1LIF", "port": 19},
        "0000:57:00.1": {"s/n": "XFL1HOOQ1Y44", "port": 23},
        "0000:56:00.1": {"s/n": "XFL1DKXBEVG2", "port": 21},
        "0000:52:00.1": {"s/n": "XFL1LHN4TXO2", "port": 25},
    },
    'psi-perentie2':
    {
        "0000:53:00.1": {"s/n": "XFL1RCFEG244", "port": 17},
        "0000:ce:00.1": {"s/n": "XFL1XCRTUC22", "port": 13},
        "0000:d1:00.1": {"s/n": "XFL10NIYKVEU", "port": 11},
        "0000:d2:00.1": {"s/n": "XFL1E35JVJTQ", "port": 15},
        "0000:d5:00.1": {"s/n": "XFL1TJCHM3ON", "port":  7},
        "0000:d6:00.1": {"s/n": "XFL1VCYSXCL0", "port":  9},
    }
}
"Map BDF to cards' serial number and P4 switch port"

class KubeAlveo:
    """Process Alveo cards in PSI low Kubernetes cluster."""

    PROC_IMAGE_NAME = 'ska-low-cbf-proc[-:]'
    """Name (regex) of processor's container image."""
    # NOTE processors got '-xrt' suffix (~ Feb 2024)
    CNIC_IMAGE_NAME = 'ska-low-cbf-tango-cnic:'
    """Name of CNIC's container image."""
    IMAGE_NAMES = (PROC_IMAGE_NAME, CNIC_IMAGE_NAME)
    """All the images that use FPGAs"""

    def __init__( self ):
        "Initialise API."
        config.load_kube_config()
        self.api = client.CoreV1Api()

    def collect_alveos(self):
        """Build a mapping of {namespace: [pods..]} where pods contain Alveo
        cards (i.e. image name matches IMAGE_NAME_TOKEN).
        """
        allocated_processors = {}
        ret = self.api.list_pod_for_all_namespaces(watch=False)
        for i in ret.items:
            ns, pod_name = i.metadata.namespace, i.metadata.name
            try:
                spec = self.api.read_namespaced_pod( name=pod_name, namespace=ns).spec
                node = spec.node_name
            except Exception as ex:
                print( f'OOPS {ex}' )
                continue
            for cont in spec.containers:
                # skip over container images we don't care about
                fpga_container = any( re.search( image_name, cont.image ) is not None for image_name in self.IMAGE_NAMES)
                if not fpga_container:
                    continue
                if ns not in allocated_processors:
                    allocated_processors[ns] = []
                allocated_processors[ns].append( (node, pod_name) )
        return allocated_processors

    def show_alveos(self):
        """Present Alveo details in a format suitable to copy-n-paste as a table in
        Jupyter notebook."""
        all_alveos = self.collect_alveos()
        if all_alveos == {}:
            print('No Alveos found')
            return
        # command to run in each pod to extract (PCI) BDF number
        cmd = ["/opt/xilinx/xrt/bin/xbutil", "examine"]
        # show individual Alveo details in each K8S namespace
        for ns, alveos in all_alveos.items():
            max_len = max( len(pod) for _, pod in alveos )
            print( f"Alveo cards in namespace {ns}:" )
            print( f"  %-{max_len}s |    BDF       |     S/N      | port | host" % 'device' )
            print( "  " + ('-'*max_len) +" | ------------ | ------------ | ---- | -------------")
            for item in alveos:
                node, pod = item
                try:
                    output = stream(
                        self.api.connect_get_namespaced_pod_exec,
                        pod,
                        ns,
                        command=cmd,
                        stderr=True,
                        stdin=True,
                        stdout=True,
                        tty=False,
                    )
                except ApiException as e:
                    print( f'Exception: {e}', end='' )
                    continue
                try:
                    host = bdf_to_sn_port[node]
                except KeyError as e:
                    print( f'\nEXCEPTION: key error, {pod} deployed on {e}' )
                    continue
                # capture BDF number in command's output
                for line in output.split("\n"):
                    m = re.match(r"^\[(0000:.{2,}:00.1).+xilinx", line)
                    if m:
                        bdf = m.group(1)
                        print(
                            f'  {pod:{max_len}} | {bdf} | {host[bdf]["s/n"]} | {host[bdf]["port"]:2}   | {node}'
                        )
            print()


if __name__ == "__main__":
    kube = KubeAlveo()
    kube.show_alveos()
