#!/bin/bash
# Copy a reference Helm Chart.yaml file, then modify it to use the requested versions.
# Desired versions (and GitLab Project IDs, for dev versions) are read from environment
# variables with names like:
#  SKA_LOW_CBF_VERSION, SKA_LOW_CBF_GITLAB_PROJECT_ID
#  i.e. chart name in capitals, dashes replaced by underscores, with a suffix.

# TODO - arguments to this script? Use SKA env var?
SOURCE_UMBRELLA_CHART_FILE=charts/ska-low-cbf-integration/Chart.original.yaml
UMBRELLA_CHART_FILE=charts/ska-low-cbf-integration/Chart.yaml
cp "${SOURCE_UMBRELLA_CHART_FILE}" "${UMBRELLA_CHART_FILE}"

# ideally this list would be extracted from Chart.yaml, but this is easier for now
INHERITED_CHARTS=("ska-low-cbf" "ska-low-cbf-conn" "ska-low-cbf-proc" "ska-low-cbf-tango-cnic")


# Colours (from https://colors.sh/)
NO_FORMAT="\033[0m"
C_AQUA="\033[38;5;14m"
C_RED="\033[38;5;9m"
C_YELLOW="\033[38;5;11m"

for chart in "${INHERITED_CHARTS[@]}"; do
    echo -e "Checking chart: ${C_AQUA}${chart}${NO_FORMAT}"
    # replace (/) all (/) dashes (-) with (/) underscores (_)
    chart_env_var_prefix=${chart//-/_}
    # convert to uppercase
    chart_env_var_prefix=${chart_env_var_prefix^^}
    version_var=${chart_env_var_prefix}_VERSION
    project_var=${chart_env_var_prefix}_GITLAB_PROJECT_ID
    version=${!version_var}
    if [[ -n ${version} ]]; then
        # version override requested
        echo -e "\t${C_YELLOW}${version_var}: ${version}${NO_FORMAT}"
        chart_dep_mod "${UMBRELLA_CHART_FILE}" -i "${chart}" "version:${version}"
        if [[ $version =~ dev ]]; then
            # dev version requested, get it from GitLab instead of CAR
            project=${!project_var}
            if [[ -z $project ]]; then
                echo -e "${C_RED}Error: ${project_var} must be set when requesting a dev version of ${chart}!${NO_FORMAT}"
                exit 1
            fi
            repo_url=https://gitlab.com/api/v4/projects/${project}/packages/helm/dev
            echo -e "\t${C_YELLOW}${project_var}: ${project}${NO_FORMAT}"
            chart_dep_mod "${UMBRELLA_CHART_FILE}" -i "${chart}" "repository:${repo_url}"
        fi
    else
        echo -e "\tNo override requested."
    fi
done
