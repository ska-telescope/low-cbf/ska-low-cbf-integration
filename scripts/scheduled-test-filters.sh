#!/bin/bash

# Show scheduled test filters and their results.
#
# This script serves as a way of documenting the test filters,
# as GitLab's scheduled pipeline configuration is awkward to use.

# create empty arrays, so all real items can use the same '+=' operator
group_filt=("")
group_mark=("")
group_name=("")

group_filt+=("subarray and not corr and not pss and not pst")
group_mark+=("hardware_present")
group_name+=("Subarray")

group_filt+=("corr and not ptc23")
group_mark+=("hardware_present and not long_test and not large_capture")
group_name+=("Correlator - Short and Small")

group_filt+=("corr and not ptc23")
group_mark+=("hardware_present and (long_test or large_capture)")
group_name+=("Correlator - Long or Large")

group_filt+=("pst")
group_mark+=("hardware_present")
group_name+=("Pulsar Timing")

group_filt+=("pss")
group_mark+=("hardware_present")
group_name+=("Pulsar Search")

group_filt+=("")
group_mark+=("hardware_present")
group_name+=("All Integration Tests (for reference)")

show_pytest_selection () {
    # Arguments:
    # 1. filter expression
    # 2. mark expression

    # pytest: --co: collect only, -k: filter, -m: mark
    # grep: -o: only matching part, -P: perl-style regex
    #  prints everything between "<Function " and ">"
    # sort: -V: version number sorting, works well with PTC names
    pytest --co -k "$1" -m "$2" | grep -oP '\<Function \K[^>]+' | sort -V
}

# ensure the three arrays are the same size
if [[ "${#group_filt[@]}" != "${#group_name[@]}" ]] ||
    [[ "${#group_filt[@]}" != "${#group_mark[@]}" ]]; then
    >&2 echo "Error: script internal arrays are not the same size"
    >&2 echo "group_filt: ${#group_filt[@]}"
    >&2 echo "group_mark: ${#group_mark[@]}"
    >&2 echo "group_name: ${#group_name[@]}"
    exit 2
fi

n_things=$((${#group_filt[@]} - 1))
echo "${n_things} groups configured in this script"

for i in $(seq 1 ${n_things}); do
    echo "======"
    echo -e "${i}.\t${group_name[i]}\n"
    echo -e "\t filter: ${group_filt[i]}"
    echo -e "\t  marks: ${group_mark[i]}"
    echo ""
    show_pytest_selection "${group_filt[i]}" "${group_mark[i]}"
done
