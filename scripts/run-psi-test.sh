#!/bin/bash
# bail out on errors
set -e

#
# Ensure deployed CNIC SW version is the same as the one test needs.
# Can be useful when running a different test on existing deployement
#
ensure_cnic_sw_version_match()
{
    # bail out if helper utilities are not available & good luck
    if ! type -P kubectl > /dev/null
    then
        clear
        echo "Didn't find kubectl; can't determine CNIC SW version, or do much else..."
        sleep 1
        read -t 4 -p "Press ENTER to continue regardless or Ctrl-C to quit "
        return
    fi
    if [ -n "${SKA_LOW_CBF_TANGO_CNIC_VERSION}" ]
    then
        actual_cnic_version=$(kubectl get pod cnic-1-0 -n ${KUBE_NAMESPACE} -o jsonpath='{.spec.containers[0].image}' | grep -oP '(?<=:).*')
        if [ "${SKA_LOW_CBF_TANGO_CNIC_VERSION}" != "${actual_cnic_version}" ]
        then
            echo
            echo -e "${RED}ERROR:${NC} requested ${SKA_LOW_CBF_TANGO_CNIC_VERSION} CNIC Tango version, have ${actual_cnic_version} deployed"
            sleep 5
            read -rp "       press ENTER to continue " # exit here?
        fi
    fi
}

# colours - https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
RED='\033[0;31m'
L_CY='\033[1;36m'  # Light Cyan
YEL='\033[1;33m'  # Yellow
NC='\033[0m' # No Colour

# Need to use proxy inside PSI but not locally
proxy=http://delphoenix.atnf.csiro.au:8888
export PROXY_VALUES="--env=http_proxy=${proxy} --env=https_proxy=${proxy}"
# define namespace unless user already has one (allows running multiple deployments)
if [ -z ${KUBE_NAMESPACE} ]
then
    export KUBE_NAMESPACE="ska-low-cbf-integration-${USER}"
fi
export PSI_SERVER="${PSI_SERVER:-perentie2}"
# enable itango by default for interactive tinkering
export ITANGO_ENABLED="${ITANGO_ENABLED:-true}"
# A major use-case for this script is trying test mods before committing...
export DEBUG_DIRTY=true

if [[ -z "$CI_COMMIT_SHORT_SHA" ]]; then
    # No commit hash specified, auto-detect the latest
    # use most recent CI build for test runner container
    # git remote is /probably/ "origin"
    git_remote=$(git remote)
    git_branch=$(git branch --show-current)
    git_ref=${git_remote}/${git_branch}

    # check if there is a matching remote branch
    if git rev-parse --short=8 "${git_ref}" > /dev/null 2> /dev/null; then
        echo "Using latest commit from ${git_ref}"
    else
        echo -e "${RED}Couldn't find ${L_CY}${git_ref}${NC} - falling back to main branch.\n"
        git_branch=main
        git_ref=${git_remote}/${git_branch}
    fi
    CI_COMMIT_SHORT_SHA=$(git rev-parse --short=8 "${git_ref}") || exit 1
    export CI_COMMIT_SHORT_SHA

    # check if MR is open, because then we can't rely on our local commit hash
    merge_requests=$(curl -k "https://gitlab.com/api/v4/projects/48248401/merge_requests?scope=all&state=opened&source_branch=${git_branch}" 2> /dev/null)
    # primitive test, doesn't actually parse returned string, just checks if empty or not
    if [[ "[]" != "$merge_requests" ]]; then
        echo "There is a merge request on branch ${git_branch}"
        echo "Set CI_COMMIT_SHORT_SHA to the commit hash of the latest merged results pipeline"
        exit 1
    fi
else
    # commit hash specified
    git_ref="(unknown branch)"  # used for display only
fi
echo -e "Using ${YEL}Integration Container${NC} commit ${L_CY}${CI_COMMIT_SHORT_SHA}${NC} from ${L_CY}${git_ref}${NC}"
echo ""
export CI_REGISTRY=registry.gitlab.com

# defaults copied from .gitlab-ci.yml because I don't want to do battle with yq today
export CNIC_FW_VERSION="${CNIC_FW_VERSION}"
export CORR_FW_VERSION="${CORR_FW_VERSION}"
export PSS_FW_VERSION="${PSS_FW_VERSION}"
export PST_FW_VERSION="${PST_FW_VERSION}"
export SKA_LOW_CBF_VERSION="${SKA_LOW_CBF_VERSION}"
export SKA_LOW_CBF_PROC_VERSION="${SKA_LOW_CBF_PROC_VERSION}"
export SKA_LOW_CBF_TANGO_CNIC_VERSION="${SKA_LOW_CBF_TANGO_CNIC_VERSION}"

if  kubectl get pods -n "${KUBE_NAMESPACE}" | egrep -q '^(cnic|processor)' ; then
    echo -e "\nNamespace ${L_CY}${KUBE_NAMESPACE}${NC} exists. Reinstall chart?"
    echo -e "Press ${YEL}y${NC} or ${YEL}r${NC} to re-deploy, otherwise existing deployment will be used"
    echo -n "Waiting 10s for input... "
    set +e  # temporarily disable exit on error as 'read' timeout below can throw a tantrum
    read -n 1 -t 10
    set -e  # re-enable exit on error
    echo  # newline after read
    if [[ $REPLY =~ ^[YyRr]$ ]]; then
        echo "Reinstalling chart"
        make k8s-reinstall-chart
    else
        echo "Using existing deployment"
    fi
else
    # Namespace does not exist, use install command
    make k8s-install-chart
fi
make k8s-wait
ensure_cnic_sw_version_match
make k8s-test

echo -e "${L_CY}You might like to monitor activity with:${NC}"
echo -e "    k9s -n \"${KUBE_NAMESPACE}\""
echo -e ""
echo -e "${YEL}When you are finished, please clean up by running:${NC}"
echo -e "    KUBE_NAMESPACE=\"${KUBE_NAMESPACE}\" make k8s-delete-namespace"

# archive build directory to protect it from being wiped next run.
# TODO - logrotate?
if [ -d build ]
then
    tar caf "build_$(date +%Y%m%d_%H%M).tar.xz" build
fi
