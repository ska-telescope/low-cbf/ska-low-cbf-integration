# ska-low-cbf-integration
Integration Testing Functions/Utilities for Low CBF

## Purpose

This repository/project serves two different (but related) purposes:

1. **A collection of integration testing utilities** for use in Low CBF testing and debugging.
    - Use these by installing the `ska-low-cbf-integration` Python package, and
use them in your jupyter notebooks, when manually debugging, etc.
    - The source code for these utilities is in `src/ska_low_cbf_integration`
    - See "Integration Testing Utilities" for more details.
2. **An automated integration testing pipeline** for the Low CBF subsystem.
    - The integration test code is in `tests/integration` (unsurprisingly)
    - These tests make heavy use of the integration testing utilities.
    - See "CI Pipeline" for more details.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-integration/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-integration/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-integration documentation](https://developer.skatelescope.org/projects/ska-low-cbf-integration/en/latest/index.html "SKA Developer Portal: ska-low-cbf-integration documentation")

## Project Avatar (Repository Icon)
[Quality control icons created by Eucalyp - Flaticon](https://www.flaticon.com/free-icons/quality-control "quality control icons")

# Integration Testing Utilities

* Visibilities Analyser class that allows for the user to analyse correlator output stored in a pcap file
* `pcap_to_sim` - file conversion utility that takes a PCAP(NG) file and converts it for use in FPGA simulation tools
* `sim_to_pcap` - inverse of `pcap_to_sim`
* `JsonWrapper` - translate JSON for Tango device proxies (see documentation)

## compare\_pcap
Find differences between two pcap files, checking only packets with a specified
destination port. Exits with non-zero status if differences found.

```console
compare_pcap [-h] [--packets PACKETS] [--ignore-fields IGNORE_FIELDS [...]] input input
```

## pst\_timestamps
Analyse PST timestamps, using a pcap file that contains both SPS and PST data.

```console
pst_timestamps [-h] -f FILE [-b BEAM] [-s SUBARRAY]
```

# CI Pipeline

Integration tests are run in the Low PSI, generally with a manual trigger (to minimise resource contention).

The CI pipeline here is triggered by other projects to perform regression testing when modifying components.

Further details of the CI pipeline, including environment variables used to control it, are in the documentation.

# Developer's Guide

* The Makefiles here are inherited from
  [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).
  * Refer to docs at that repo, or use `make help` for details.
  * This link is via a git submodule, so use the `--recursive` flag when cloning
this repository, or run `git submodule update --init --recursive` afterwards.
* A pre-commit config is provided to try and format your code before it gets
rejected by the CI pipeline lint checkers.
  * Install [pre-commit](https://pre-commit.com/) - `pip3 install pre-commit`
  * Activate the pre-commit checks for the repo - `pre-commit install`
  * From now on your commits will be formatted automatically. Note that you
have to add & commit again any files changed by pre-commit.
* A git hook is provided that may help comply with SKA commit message rules.
You can install the hook with `cp -s "$(pwd)/resources/git-hooks"/* .git/hooks`.
Once installed, the hook will insert commit messages to match the JIRA ticket
from the branch name.
e.g. On branch `perentie-1350-new-base-classes`:
```console
ska-low-cbf$ git commit -m "Add git hook note to README"
Branch perentie-1350-new-base-classes
Inserting PERENTIE-1350 prefix
[perentie-1350-new-base-classes 3886657] PERENTIE-1350 Add git hook note to README
 1 file changed, 7 insertions(+)
```
You can see the modified message above, and confirming via the git log:
```console
ska-low-cbf$ git log -n 1 --oneline
3886657 (HEAD -> perentie-1350-new-base-classes) PERENTIE-1350 Add git hook note to README
```

## Perentie Test Case (PTC) Naming Convention

### Why?
Naming PTCs consistently facilitates (de)selection of tests for both automated pipelines
and manual execution of tests.

Putting some details like firmware image and a short description in the name helps people
to understand what part of the system a test is exercising.

PTC numbers provide unambiguous linking to
[the spreadsheet where they are defined](
https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E/edit?gid=0#gid=0
), and from there to the underlying SKAO requirements.

### Convention
Integration tests implementing PTCs are named according to the convention:

`test_ptc<n>[_<part>]_<SUT>_<test title>`

Using lower case with words separated by underscores (aka `snake_case`), as per the
[Python convention for naming functions](
https://peps.python.org/pep-0008/#function-and-variable-names).

Where:
* `n` is the PTC number (no leading zeros to pad)
  * This allows selection of a specific test using a pytest filter, e.g. `-k 'ptc3_'` will
  select PTC 3 without also selecting PTCs 30–9
* `part` is used when a single PTC is divided into multiple pieces for implementation
  * E.g. if part of the test has different hardware requirements, or is testing a
  different aspect of the system's performance.
* `SUT` is the system under test
  * Usually the short name of one firmware image: `corr`, `pss`, `pst`.
  * One test uses `subarray`.
  * **Future: What about tests that use multiple different firmware?**
  * **Option:** `multi_corr_pst` (additional "multi" keyword for easy de-duplication
  when breaking tests into chunks for scheduling, listing FW names
  allows all tests of a firmware to be selected if desired e.g. when testing a change)
  * **Option:** `multi` (does not facilitate selecting all tests that involve a particular
  type of firmware)
  * **Option:** `corr_pst` (test de-duplication in scheduled pipeline configs via
  filters like `-k 'corr and not pst and not pss and not subarray'`)
* `test title` is the test's name per the PTC spreadsheet or a short paraphrase thereof

Examples:

| PTC Title from Spreadsheet                         | Python test function name(s)                                                    |
|:---------------------------------------------------|:--------------------------------------------------------------------------------|
| PTC#1 Exercising the Observing state machine       | `test_ptc1_a_subarray_obsstate`, `test_ptc1_b_subarray_restart_stops_processor` |
| PTC#15 Visibility signal to noise ratio            | `test_ptc15_corr_visibility_snr`                                                |
| PTC#29 PST Beam Metadata                           | `test_ptc29_pst_beam_metadata`                                                  |
| PTC#42 PSS Beam packets flagged without valid poly | `test_ptc42_pss_validity_flags`                                                 |


## Beam ID / Routing Conventions

To reduce the risk of routing rule collisions between concurrent tests, these conventions
should be followed when implementing integration tests (aka PTCs):

* Single Processor, Basic Routing (i.e. most tests): may use station beam 1
* Multiple Processors, or Multicast Routing: must use a unique station beam ID

Beam IDs used by various tests are controlled in `tests/integration/station_beam_ids.py`.

## Deploying to Low PSI

Makefile variables `PSI_SERVER` and `PSI_LOW` can be used to activate one of the Low
PSI values files (there is one per FPGA host server).

e.g. to use `psi-perentie2`:
```console
$ PSI_SERVER=perentie2 make k8s-install-chart
```

The server name can be specified either with or without the `psi-` prefix - i.e.
`psi-perentie2` and `perentie2` are equivalent.

Setting a value for `PSI_SERVER` implies that `PSI_LOW` should be active. If you
set `PSI_LOW` without specifying a server, it will default to `perentie1`.

### Using more than one Processor device

By default, only one Processor instance is deployed. More can be requested with the
`N_PROCESSORS` variable.

e.g. for two Processors:
```console
$ PSI_SERVER=perentie2 N_PROCESSORS=2 make k8s-install-chart
```

## Deploying with TMC and CSP LMC

Makefile variables `TMC` and `CSP` can be used to deploy the higher level portions of
the control system. These are not deployed by default.

e.g.
```console
$ TMC=true CSP=true make k8s-install-chart
```
