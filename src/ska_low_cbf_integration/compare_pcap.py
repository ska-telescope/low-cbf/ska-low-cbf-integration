# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""PCAP file comparison tool."""
import argparse
import sys
from typing import List, Optional

import dpkt
from dpkt.pcap import DLT_EN10MB, DLT_LINUX_SLL, UniversalReader


def eth_from_sll(sll_bytes: bytes) -> bytes:
    """Convert an SLL ("cooked") PCAP packet into an Ethernet one."""
    # dpkt uses dynamic attr magic that pylint can't see
    # pylint: disable=no-member
    sll = dpkt.sll.SLL(sll_bytes)
    eth = dpkt.ethernet.Ethernet()
    eth.data = sll.data.pack()
    if sll.hlen == 6:  # Ethernet MAC address is 6 bytes
        eth.src = sll.hdr[: sll.hlen]
    return eth.pack()


def zero_fields(packet: bytes, fields: List[str]) -> bytes:
    """Zero out specified fields in a packet."""
    eth = dpkt.ethernet.Ethernet(packet)
    ip = eth.data
    if "ip.ttl" in fields:
        ip.ttl = 0
    if "ip.checksum" in fields:
        ip.sum = 0
    return eth.pack()


def compare_packets(
    packets_a: dpkt.pcap.Reader | dpkt.pcapng.Reader,
    packets_b: dpkt.pcap.Reader | dpkt.pcapng.Reader,
    max_packets: Optional[int] = None,
    enforce_same_length: bool = True,
    ignore_fields: Optional[List[str]] = None,
) -> (List[int], int):
    """
    Compare packets.

    :param packets_a: one set of packets
    :param packets_b: other set of packets
    :param max_packets: maximum number of packets to compare (None => compare all)
    :param enforce_same_length: require input files to be same length?
    :param ignore_fields: list of fields to ignore in comparison, available options are
      "ip.ttl" and "ip.checksum"
    :return: differing packet indices (1 based, for consistency with WireShark),
      number of packets compared
    :raises ValueError: if enforce_same_length is True and files differ in length
    """
    index = 0
    differences = []
    link_layer_a = _check_link_layer(packets_a)
    link_layer_b = _check_link_layer(packets_b)

    # timestamps not used
    for index, ((_, packet_a), (_, packet_b)) in enumerate(
        zip(packets_a, packets_b, strict=enforce_same_length), start=1
    ):
        if link_layer_a == DLT_LINUX_SLL:
            packet_a = eth_from_sll(packet_a)
        if link_layer_b == DLT_LINUX_SLL:
            packet_b = eth_from_sll(packet_b)

        if ignore_fields:
            packet_a = zero_fields(packet_a, ignore_fields)
            packet_b = zero_fields(packet_b, ignore_fields)

        if packet_b != packet_a:
            differences.append(index)
        if max_packets is not None and index >= max_packets:
            break
    return differences, index


def _check_link_layer(reader: dpkt.pcap.Reader | dpkt.pcapng.Reader) -> int:
    """Read Link Layer type, ensure it is OK."""
    link_layer = reader.datalink()
    if link_layer not in [DLT_EN10MB, DLT_LINUX_SLL]:
        raise NotImplementedError(f"Link-Layer Header {link_layer} not supported!")
    return link_layer


def same_packets(
    packets_a: dpkt.pcap.Reader | dpkt.pcapng.Reader | str,
    packets_b: dpkt.pcap.Reader | dpkt.pcapng.Reader | str,
    max_packets: Optional[int] = None,
) -> bool:
    """
    Test if two files contain the same packet data.

    :param packets_a: one set of packets - reader or filename
    :param packets_b: other set of packets - reader or filename
    :param max_packets: maximum number of packets to compare (None => compare all)
    :return: True if all packet data is the same
    """
    try:
        # Don't think it's feasible to use 'with' here?
        # pylint: disable=consider-using-with
        if isinstance(packets_a, str):
            packets_a = UniversalReader(open(packets_a, "rb"))
        if isinstance(packets_b, str):
            packets_b = UniversalReader(open(packets_b, "rb"))
        differences, _ = compare_packets(
            packets_a, packets_b, max_packets, enforce_same_length=True
        )
    except ValueError as value_error:
        if "zip() argument" in str(value_error) and "shorter than" in str(value_error):
            return False
        raise value_error
    return len(differences) == 0


def main():
    """PCAP comparison utility main function (CLI)."""
    argparser = argparse.ArgumentParser(description="Perentie PCAP payload comparator.")
    argparser.add_argument(
        "input",
        type=argparse.FileType("rb"),
        nargs=2,
        help="Input pcap files.",
    )
    argparser.add_argument(
        "--packets",
        type=int,
        help="Number of packets to compare. Default: all",
    )
    argparser.add_argument(
        "--ignore-fields", type=str, nargs="+", help="Fields to ignore (e.g. ip.ttl)"
    )

    args = argparser.parse_args()

    try:
        differences, n_comp = compare_packets(
            UniversalReader(args.input[0]),
            UniversalReader(args.input[1]),
            args.packets,
            ignore_fields=args.ignore_fields,
        )
    except StopIteration:
        print(f"{args.input[1].name} does not have enough packets to finish comparison")
        sys.exit(2)

    if n_comp > 0 and len(differences) == 0:
        print(f"Two files contain same packets ({n_comp} packets compared)")
    elif n_comp == 0:
        print("No packets compared!")
        sys.exit(3)
    else:
        print(
            (
                f"Files are different.\n"
                f"{n_comp} packets compared, {len(differences)} differences.\n"
                f"Mismatched packet indices: {differences}"
            )
        )
        sys.exit(1)


if __name__ == "__main__":
    main()
