# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Integration Testing project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""Pulsar (PSR) packet decoder with filters."""
import math
import struct
import warnings
from dataclasses import dataclass
from functools import partial
from typing import BinaryIO, Callable, Generator, Tuple

import numpy as np

from ska_low_cbf_integration.pcap import payloads

N_POL = 2
N_VALS_PER_COMPLEX_SAMPLE = 2
WEIGHT_VAL_BYTES = 2

PSS_CHANS_PER_PACKET = 54
PSS_BYTES_PER_VAL = 1
PST_CHANS_PER_PACKET = 24
PST_BYTES_PER_VAL = 2

# byte offsets in payload
seq_no_slice = slice(8, 16)
t_num_slice = slice(16, 18)
t_den_slice = slice(18, 20)
scale1_slice = slice(32, 36)
first_chan_slice = slice(48, 52)
n_chan_slice = slice(52, 54)
n_samp_slice = slice(56, 58)
beam_slice = slice(58, 60)
SAMP_PER_W_POS = 67
VALIDITY_POS = 68
METADATA_LEN = 96
"""PSR Metadata Length in Bytes"""
WEIGHTS_OFFSET = 96  # multiple of 16bytes=128 bits
"""Offset for start of weights in Bytes. Measured from start of UDP payload."""

METADATA_FORMAT = "QQHHIQffffIHHHHIBBBBBBHQffff"


# Validty field in PSR packet is 8 bits
# 0 - beamPoly_ok(0)
# 1 - beamPoly_ok(1)
# 2 - beamJonesStatus(0)
# 3 - beamJonesStatus(1)
# 4 - beamJonesStatus(2) (PSS ONLY)
# 5 - beamJonesStatus(3) (PSS ONLY)
# 6 - reserved
# 7 - reserved

# beamJonesStatus
# Jones bits 1:0 = beam jones status (PSS) || beam specific station jones (PST)
# Jones bits 3:2 = station jones status (PSS ONLY)
# For both types of Jones status,  bit 0 = Used default Jones matrices
#                                  bit 1 = used valid jones matrices


@dataclass(frozen=True)
class PssValidityFlags:
    """PSS Validity Flags."""

    station_delay_poly: bool
    """Station Delay Polynomial OK"""
    beam_delay_poly: bool
    """Beam Delay Polynomial OK"""
    beam_default_jones: bool
    """Default Beam Jones Matrix Used"""
    beam_valid_jones: bool
    """Valid Beam Jones Matrix Used"""
    station_default_jones: bool
    """Default Station Jones Matrix Used"""
    station_valid_jones: bool
    """Valid Station Jones Matrix Used"""

    @classmethod
    def from_int(cls, flags: int):
        """Decode an integer validity value into the various bit flags."""
        return cls(
            station_delay_poly=bool(flags & 0b0000_0001),
            beam_delay_poly=bool(flags & 0b0000_0010),
            beam_default_jones=bool(flags & 0b0000_0100),
            beam_valid_jones=bool(flags & 0b0000_1000),
            station_default_jones=bool(flags & 0b0001_0000),
            station_valid_jones=bool(flags & 0b0010_0000),
        )


@dataclass(frozen=True)
class PstValidityFlags:
    """PST Validity Flags."""

    station_delay_poly: bool
    """Station Delay Polynomial OK"""
    beam_delay_poly: bool
    """Beam Delay Polynomial OK"""
    station_beam_default_jones: bool
    """Default Beam-specific Station Jones Matrix Used"""
    station_beam_valid_jones: bool
    """Valid Beam-specific Station Jones Matrix Used"""

    @classmethod
    def from_int(cls, flags: int):
        """Decode an integer validity value into the various bit flags."""
        return cls(
            station_delay_poly=bool(flags & 0b0000_0001),
            beam_delay_poly=bool(flags & 0b0000_0010),
            station_beam_default_jones=bool(flags & 0b0000_0100),
            station_beam_valid_jones=bool(flags & 0b0000_1000),
        )


@dataclass
class PsrMetadata:  # pylint: disable=too-many-instance-attributes
    """Pulsar (PSR) packet metadata (headers) only."""

    # beware: the order of parameters here must be the same as in the packet
    sequence_number: int
    samples_since_epoch: int
    numerator: int
    denominator: int
    separation: int
    first_channel_freq: int
    scale1: float
    scale2: float
    scale3: float
    scale4: float
    first_channel: int
    n_channels: int
    valid_channels: int
    n_samples: int
    """Number of time samples"""
    beam_id: int
    magic: int
    pkt_dst: int
    data_precs: int
    """Data precision in bits (8 or 16)."""
    n_power_avg: int
    samples_per_weight: int
    validity: int
    _reserved: int
    version: int
    scan_id: int
    offset1: float  # added to Stokes params before scaling
    offset2: float
    offset3: float
    offset4: float

    @classmethod
    def from_bytes(cls, payload: bytes) -> "PsrMetadata":
        """
        Create a PsrMetadata object from bytes.

        :param payload: UDP packet payload.
        """
        return cls(*struct.unpack(METADATA_FORMAT, payload[:METADATA_LEN]))

    def tobytes(self) -> bytes:
        """Pack into bytes."""
        return struct.pack(
            METADATA_FORMAT,
            self.sequence_number,
            self.samples_since_epoch,
            self.numerator,
            self.denominator,
            self.separation,
            self.first_channel_freq,
            self.scale1,
            self.scale2,
            self.scale3,
            self.scale4,
            self.first_channel,
            self.n_channels,
            self.valid_channels,
            self.n_samples,
            self.beam_id,
            self.magic,
            self.pkt_dst,
            self.data_precs,
            self.n_power_avg,
            self.samples_per_weight,
            self.validity,
            self._reserved,
            self.version,
            self.scan_id,
            self.offset1,
            self.offset2,
            self.offset3,
            self.offset4,
        )

    @property
    def sample_period_us(self) -> float:
        """Get the sample period (μs)."""
        return self.numerator / self.denominator

    @property
    def ska_timestamp_us(self) -> float:
        """Get packet timestamp, derived from samples since SKA Epoch (μs)."""
        return self.samples_since_epoch * self.sample_period_us

    @property
    def seq_timestamp_us(self) -> float:
        """Get packet timestamp, derived from sequence number (μs)."""
        return self.sequence_number * self.sample_period_us

    @property
    def sample_offset(self) -> int:
        """Get offset of sample data in Bytes, from start of UDP payload."""
        n_weight_bytes = (
            self.n_samples
            / self.samples_per_weight
            * WEIGHT_VAL_BYTES
            * self.n_channels
        )
        # weights padded to multiple of 128 bits = 16 bytes
        return WEIGHTS_OFFSET + math.ceil(n_weight_bytes / 16) * 16

    @property
    def validity_flags(self) -> PssValidityFlags | PstValidityFlags:
        """Get validity flags, decoded into appropriate dataclass."""
        if self.data_precs == 8:
            return PssValidityFlags.from_int(self.validity)
        if self.data_precs == 16:
            return PstValidityFlags.from_int(self.validity)
        raise NotImplementedError(f"Unknown data precision {self.data_precs}")


# there may be some benefit (mainly in testing) in using a separate "PsrData" class?
@dataclass
class PsrPacket(PsrMetadata):
    """Pulsar (PSR) protocol packet - metadata & data."""

    _data: bytes

    @classmethod
    def from_bytes(cls, payload: bytes) -> "PsrPacket":
        """
        Create a PsrMetadata object from bytes.

        :param payload: UDP packet payload.
        """
        metadata = struct.unpack(METADATA_FORMAT, payload[:METADATA_LEN])
        return cls(
            *metadata,
            _data=payload[METADATA_LEN:],
        )

    def __post_init__(self):
        """Initialise derived values."""
        self._data_sample_offset = self.sample_offset - METADATA_LEN
        self._val_dtype = f"i{self.data_precs // 8}"

    @property
    def sample_bytes(self) -> bytes:
        """
        Get raw sample data bytes.

        data is in order:
        Byte  Content
        0     ch 0, pol a, t=0, I
        1     ch 0, pol a, t=0, Q
        2     ch 0, pol a, t=1, I
        3     ch 0, pol a, t=1, Q
        repeat for n_samples
        repeat for pol b
        repeat for n_channels
        """
        return self._data[self._data_sample_offset :]

    @property
    def complex_samples(self) -> np.ndarray:
        """Get Complex scaled sample values, indexed by: Channel, Polarisation, Time."""
        iq_samples = (
            np.frombuffer(self.sample_bytes, dtype=self._val_dtype).reshape(
                (self.n_channels, N_POL, self.n_samples, N_VALS_PER_COMPLEX_SAMPLE)
            )
            / self.scale1
        )
        # the divide above will change the dtype from int to float
        return iq_samples[:, :, :, ::2] + 1j * iq_samples[:, :, :, 1::2]

    @property
    def weight_bytes(self) -> bytes:
        """Get raw weight data bytes."""
        # TODO: might need to chop off some padding here?
        return self._data[: self._data_sample_offset]

    def average_channel_power(self) -> np.ndarray:
        """Average Power per Sample, for each Channel."""
        power = np.empty((self.n_channels,))
        scale = self.scale1

        # we don't need to split into polarisations or I/Q parts to sum power
        raw_samples = np.frombuffer(self.sample_bytes, dtype=self._val_dtype).reshape(
            (self.n_channels, self.n_samples * N_VALS_PER_COMPLEX_SAMPLE * N_POL)
        )
        for n in range(self.n_channels):
            power[n] = np.sum(np.square(raw_samples[n] / scale)) / self.n_samples
        return power


def beam(payload: bytes) -> int:
    """
    Get Beam ID.

    :param payload: UDP payload
    """
    return int.from_bytes(payload[beam_slice], byteorder="little")


def n_chan(payload: bytes) -> int:
    """
    Get Channel Number.

    :param payload: UDP payload
    """
    return int.from_bytes(payload[n_chan_slice], byteorder="little")


def first_chan(payload: bytes) -> int:
    """
    Get First Channel Number.

    :param payload: UDP payload
    """
    return int.from_bytes(payload[first_chan_slice], byteorder="little")


def channel_samples(
    payload: bytes, channel: int
) -> Generator[Tuple[complex, complex], None, None]:
    """
    Get complex dual-polarisation samples for a given channel, one at a time.

    WARNING: The payload is assumed to hold the channel!

    :param payload: UDP payload
    :param channel: channel id of interest
    :returns: Complex sample values (pol. a, pol. b)
    """
    psr = PsrPacket.from_bytes(payload)
    channel -= psr.first_channel
    samples = psr.complex_samples
    for n_sample in range(psr.n_samples):
        yield samples[channel, 0, n_sample], samples[channel, 1, n_sample]


def _sample_data_details(payload) -> (int, int, float):
    """
    Get sample data details.

    :param payload: UDP payload
    :returns: sample data byte offset, number of samples, scale factor 1
    """
    # potential for a small speed gain here by doing one big struct.unpack
    #  (if we happen to be using mostly sequential values)
    n_channel = n_chan(payload)
    n_samples = int.from_bytes(payload[n_samp_slice], "little")
    sample_per_weight = payload[SAMP_PER_W_POS]
    [scale1] = struct.unpack("f", payload[scale1_slice])
    n_weight_bytes = n_samples / sample_per_weight * WEIGHT_VAL_BYTES * n_channel
    # weights padded to multiple of 128 bits = 16 bytes
    data_offset = WEIGHTS_OFFSET + math.ceil(n_weight_bytes / 16) * 16
    return data_offset, n_samples, scale1


def contains_channel(payload: bytes, channel_id: int) -> bool:
    """
    Check if payload contains this channel.

    :param payload: UDP payload
    :param channel_id: channel id of interest
    """
    first = first_chan(payload)
    return first <= channel_id <= first + n_chan(payload)


def is_beam(payload: bytes, beam_id: int) -> bool:
    """
    Check if payload contains this beam.

    :param payload: UDP payload
    :param beam_id: beam id of interest
    """
    return beam(payload) == beam_id


def beam_chan_selector(beam_id, channel_id) -> Callable[[bytes], bool]:
    """
    Create a beam and channel selection filter function.

    Note: if you want different filtering, you might prefer to use a lambda, e.g.:

    .. code:: python

        lambda x: contains_channel(x, 108) and len(x) < 128

    :param beam_id: Beam id to select
    :param channel_id: Channel id to select
    :return: A function that returns True when payload contains beam_id & channel_id
    """

    def filter_(payload):
        """Beam & Channel check."""
        # there is very little to be gained by inlining these calls,
        # so let's retain some readability instead
        return is_beam(payload, beam_id) and contains_channel(payload, channel_id)

    return filter_


def sample_period_us(payload: bytes) -> float:
    """Get sample period (μs)."""
    numerator = int.from_bytes(payload[t_num_slice], byteorder="little")
    denominator = int.from_bytes(payload[t_den_slice], byteorder="little")
    return numerator / denominator


def ts_samples_beam_channel(
    file: BinaryIO,
    beam_id: int,
    channel_id: int,
    max_len: int = 1_000_000,
) -> np.recarray:
    """
    Get all timestamps & samples (complex, dual-polarisation) for one beam & channel.

    :param file: PCAP(NG) file object
    :param beam_id: Beam of interest
    :param channel_id: Channel of interest
    :param val_bytes: DEPRECATED - now detected from packet.
    :param max_len: Max number of samples to process (per polarisation)
    :returns: Numpy Record Array with column names:
      - ``t``: Timestamps
      - ``x``: X-polarisation samples
      - ``y``: Y-polarisation samples
    """
    function = beam_chan_selector(beam_id, channel_id)
    total_samples = 0
    t_sample_us = 0
    ts_samples = np.rec.fromarrays(
        (
            np.empty(max_len),
            np.empty(max_len, dtype=np.complex64),
            np.empty(max_len, dtype=np.complex64),
        ),
        names=("t", "x", "y"),
    )
    n_packet = 0
    for n_packet, payload in enumerate(filter(function, payloads(file))):
        if n_packet == 0:
            # we can assume the sample period remains the same for each packet,
            # the FPGA has no means of changing it
            t_sample_us = sample_period_us(payload)
        sequence = int.from_bytes(payload[seq_no_slice], byteorder="little")
        t_packet_us = sequence * t_sample_us
        for n_sample, samples in enumerate(channel_samples(payload, channel_id)):
            ts_samples.x[total_samples] = samples[0]
            ts_samples.y[total_samples] = samples[1]
            ts_samples.t[total_samples] = t_packet_us + t_sample_us * n_sample
            total_samples += 1
        if total_samples >= max_len:
            warnings.warn(
                f"More samples than max length {max_len}, data will be discarded."
            )
            break
    print(f"{n_packet} packets decoded, {total_samples} complex dual-pol. samples")
    return ts_samples[:total_samples]


def channel_power_beam(
    file: BinaryIO, beam_id: int, n_channels: int, first_channel: int = 0
) -> np.ndarray:
    """
    Return the average power per channel per packet for one beam in a file.

    :param file: PCAP(NG) file object
    :param beam_id: Beam of interest
    :param n_channels: Number of PSR channels in file
    :param first_channel: First PSR channel used by beam
    """
    ch_pow = np.zeros(n_channels)
    n_packets = np.zeros(n_channels)
    my_beam = partial(is_beam, beam_id=beam_id)

    for payload in filter(my_beam, payloads(file)):
        psr = PsrPacket.from_bytes(payload)
        ch_slice = slice(
            psr.first_channel - first_channel,
            psr.first_channel + psr.n_channels - first_channel,
        )
        ch_pow[ch_slice] += psr.average_channel_power()
        n_packets[ch_slice] += 1

    return ch_pow / n_packets
