# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low CBF Connector Tango device helper functions."""

from dataclasses import dataclass
from time import strftime
from typing import Dict, Iterable

import tango

from ska_low_cbf_integration.tango import DeviceProxyJson


def connector_port_active(connector: DeviceProxyJson, port: str) -> bool:
    """
    Given a port, check if it's currently active.

    :param connector: Connector Tango device to interrogate (w/ JSON wrapper)
    :param port: The port name to check, e.g. "29/0"
    :returns: Port active (True) or not (False)
    """
    port_statuses = connector.portStatus["Ports_Status"]
    for port_status in port_statuses:
        if port_status["$PORT_NAME"] == port:
            return True

    return False


def load_ports(connector: DeviceProxyJson, ports: Iterable[str]):
    """
    Call Connector's LoadPorts command for multiple ports.

    :param connector: Connector device proxy (w/ JSON wrapper)
    :param ports: one or more ports, e.g. ["29/0"]
    """
    print(f"Loading ports in Connector: {ports}")
    for port in ports:
        # FIXME: This guard can cause exceptions...
        # if not connector_port_active(connector, port):

        # We need to send one port at a time to ensure they're all acted upon
        # See Connector issue #2:
        # https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-conn/-/issues/2
        connector.LoadPorts({"Physical": [_create_port_config(port)]})


def _create_port_config(port: str) -> dict:
    """
    Construct a configuration item for Connector's LoadPorts command.

    :param port: port name, e.g. "29/0"
    :returns: dictionary containing the items required by LoadPorts, wrap it in a dict
        using the key "Physical" before sending to command.
    """
    return {"port": port, "autoneg": "disable", "mac": "84:c7:8f:03:79:b2"}


@dataclass
class PortCounts:
    """Data class to collect Tx & Rx counters together."""

    tx: int
    rx: int


def get_port_packet_counts(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> Dict[str, PortCounts]:
    """
    Return packet counts for the given P4 ports.

    :param connector: the connector tango device (w/ JSON wrapper)
    :param ports: one or more ports, e.g. ["29/0"]
    :returns: dict mapping port to counters, e.g.
        {"29/0": PortCounts(tx=123, rx=456)}
    """
    counts = {}
    ports_status = connector.portStatus["Ports_Status"]
    for port_status in ports_status:
        port_name = port_status["$PORT_NAME"].strip()
        if port_name in ports:
            counts[port_name] = PortCounts(
                tx=int(port_status["packets_sent"]),
                rx=int(port_status["packets_received"]),
            )
    return counts


def print_routing_tables(connector: DeviceProxyJson) -> None:
    """
    Print all routing tables (attributes with "RoutingTable" in their name).

    :param connector: Connector device
    """
    route_table_attrs = [attr for attr in dir(connector) if "RoutingTable" in attr]
    for attr in route_table_attrs:
        print(attr)
        print(getattr(connector, attr))
        print()


def _remove_basic_routes_to_ports(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> None:
    """Remove all Basic routes to given destination ports."""
    # Connector attribute format:
    # {"Basic": [{"ingress port": "23/0", "port": "21/0"}, ...]}
    routes = connector.basicRoutingTable["Basic"]
    connector.RemoveBasicEntry(
        {
            "basic": [
                {"src": {"port": route["ingress port"]}}
                for route in routes
                if route["port"] in ports
            ]
        }
    )


def _remove_spead_unicast_routes_to_ports(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> None:
    """Remove all SPEAD Unicast routes to given destination ports."""
    # Connector attribute format:
    # {"Spead": [{"Frequency": "140", "Beam": "1", "Sub_array": "4", "port": "11/0"},
    #               ... ]}
    routes = connector.speadUnicastRoutingTable["Spead"]
    connector.RemoveSpeadUnicastEntry(
        {
            "spead": [
                {
                    "src": {
                        "frequency": route["Frequency"],
                        "beam": route["Beam"],
                        "sub_array": route["Sub_array"],
                    }
                }
                for route in routes
                if route["port"] in ports
            ]
        }
    )


def _remove_spead_multiplier_routes_to_ports(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> None:
    """Remove all SPEAD Multiplier routes to given destination ports."""
    # Connector attribute format:
    # {"multicast_routing_table": {"1": ["17/0", "7/0"], "2": ["7/0", "9/0"], ...}}
    port_sessions = {
        int(session)
        for session, session_ports in connector.multicastSessions[
            "multicast_routing_table"
        ].items()
        if set(session_ports) & set(ports)  # if any port is in the session's ports
    }

    # Connector attribute format:
    # {"Spead": [{"Frequency": 0, "Beam": 0, "Sub_array": 0, "session": 0}]}
    routes = connector.speadMultiplierRoutingTable["Spead"]
    connector.RemoveSpeadMultiplierEntry(
        {
            "spead": [
                {
                    "src": {
                        "frequency": route["Frequency"],
                        "beam": route["Beam"],
                        "sub_array": route["Sub_array"],
                    }
                }
                for route in routes
                if route["session"] in port_sessions
            ]
        }
    )


def _remove_sdp_routes_to_ports(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> None:
    """Remove all SDP IP routes to given destination ports."""
    # Connector attribute format:
    # {"SDP_IP": [{"IP_Address": "192.168.14.14", "port": "13/0"}]}
    routes = connector.sdpIpRoutingTable["SDP_IP"]
    connector.RemoveSDPIPEntry(
        {
            "sdp_ip": [
                {"src": {"ip": route["IP_Address"]}}
                for route in routes
                if route["port"] in ports
            ]
        }
    )


def _remove_psr_routes_to_ports(
    connector: DeviceProxyJson, ports: Iterable[str]
) -> None:
    """Remove all Pulsar (PSS,PST) routes to a given destination port."""
    # Connector attribute format:
    # {"PSR": [{"Beam": "17", "port": "15/0"}, ... ]}
    routes = connector.psrRoutingTable["PSR"]
    connector.RemovePSREntry(
        {
            "psr": [
                {"src": {"beam": route["Beam"]}}
                for route in routes
                if route["port"] in ports
            ]
        }
    )


def clear_routes_to_ports(connector: DeviceProxyJson, ports: Iterable[str]) -> None:
    """
    Clear all routes with destinations set to the given ports.

    :param connector: Connector device
    :param ports: ports to clear, e.g. ["7/0", "11/0"]
    """
    _remove_basic_routes_to_ports(connector, ports)
    _remove_spead_unicast_routes_to_ports(connector, ports)
    _remove_spead_multiplier_routes_to_ports(connector, ports)
    _remove_sdp_routes_to_ports(connector, ports)
    _remove_psr_routes_to_ports(connector, ports)


def assert_packet_counts_zero(connector: DeviceProxyJson, ports: Iterable[str]):
    """
    Check P4 counters are zero.

    :raises: AssertionError if any device's port has non-zero Tx or Rx count.
    """
    print(
        f"{strftime('%H:%M:%S')} Port Packet Counts:",
        get_port_packet_counts(connector, ports),
    )
    assert all(
        count.rx == 0 and count.tx == 0
        for count in get_port_packet_counts(connector, ports).values()
    )


def add_basic_route(connector, src_port, dst_port):
    """Add a basic route to the Connector."""
    try:
        connector.UpdateBasicEntry(
            {
                "basic": [
                    {"src": {"port": src_port}, "dst": {"port": dst_port}},
                ]
            }
        )
    except tango.DevFailed as dev_failed:
        print(f"Exception when updating routing rules: {dev_failed}")
        print("Going to keep forging ahead regardless...")
