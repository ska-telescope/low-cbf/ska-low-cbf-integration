# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""
Analysis of SPS and PST packets from a PCAP file.

Determine timing difference between pulses in SPS data and pulses in PST beams.

KB 13 Sep 2024
"""

import argparse
import math
import os.path
import struct
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np

from ska_low_cbf_integration.pcap import (
    get_timestamps_udp_payload_bytes,
    get_udp_payload_bytes,
)


@dataclass
class SpsPkt:  # pylint: disable=too-many-instance-attributes
    """Structure to save decoded data from spead SPS packets."""

    subarray_id: int
    stn_bm_id: int
    chan_id: int
    stn_id: int
    sstn_id: int
    pkt_no: int
    data: np.ndarray
    capture_no: int


@dataclass
class PstPkt:
    """Structure to save decoded data from PST packets."""

    seq_no: int
    sample_no: int
    first_chan: int
    power_sum: np.ndarray
    capture_no: int


def get_pst_and_sps_pkts(payloads: list[bytes]):  # pylint: disable=too-many-locals
    """Extract lists of PST packets, subs, and SPS packets from packet payloads."""
    num_spead = 0
    num_pst = 0
    non_spd_non_pst_pkt = 0
    spead_pkt_list = []
    pst_pkt_list = []
    subs = {}
    for cap_no, payload in enumerate(payloads):
        # unused fields in next line: ipw, haw, zer
        magic, ver, _, _, _, nitm = struct.unpack(">BBBBHH", payload[0:8])
        pst_magic = struct.unpack("<I", payload[60:64])[0]
        if (magic == 0x53) and (ver == 0x4) and (nitm == 6):
            num_spead += 1
            spead = extract_sps_info(payload, cap_no)
            spead_pkt_list.append(spead)

            if spead.subarray_id not in subs:
                subs[spead.subarray_id] = {"stns": set(), "beams": {}}
            stn_repr = f"{spead.stn_id}.{spead.sstn_id}"
            subs[spead.subarray_id]["stns"].add(stn_repr)
            if spead.stn_bm_id not in subs[spead.subarray_id]["beams"]:
                subs[spead.subarray_id]["beams"][spead.stn_bm_id] = set()
            subs[spead.subarray_id]["beams"][spead.stn_bm_id].add(spead.chan_id)
        elif pst_magic == 0xBEADFEED:
            num_pst += 1
            pst = extract_pst_info(payload, cap_no)
            pst_pkt_list.append(pst)
        else:
            non_spd_non_pst_pkt += 1

    print(f"{num_spead} SPEAD packets, {num_pst} PST packets")
    if non_spd_non_pst_pkt != 0:
        print(f"{non_spd_non_pst_pkt} other packets")

    # convert subarray sets to sorted lists
    for sub in subs.values():
        sub["stns"] = sorted(list(sub["stns"]))
        for bm_id in sub["beams"]:
            sub["beams"][bm_id] = sorted(list(sub["beams"][bm_id]))

    return spead_pkt_list, subs, pst_pkt_list


def extract_sps_info(payload, capture_no):
    """
    Get SPS fields from packet.

    :param payload: SPS packet payload bytes
    :param capture_no: packet number within the original pcap file
    """
    # unused fields in next line: magic, ver, ipw, haw, zer
    _, _, _, _, _, nitm = struct.unpack(">BBBBHH", payload[0:8])
    sub = beam = chan = stn = sstn = -1
    for hdr_no in range(0, nitm):
        ofst = (hdr_no + 1) * 8
        hdr_itm = struct.unpack(">H", payload[ofst : ofst + 2])[0]
        if hdr_itm == 0x8001:
            pktnum = struct.unpack(">Q", payload[ofst : ofst + 8])[0] & 0x0FFFFFFFFFF
            continue
        if hdr_itm == 0x8004:
            data_len = (
                struct.unpack(">Q", payload[ofst : ofst + 8])[0] & 0x0FFFFFFFFFFFF
            )
        if hdr_itm == 0xB000:
            beam, chan = struct.unpack(">HH", payload[ofst + 4 : ofst + 8])
            continue
        if hdr_itm == 0xB001:
            sstn, sub, stn = struct.unpack(">BBH", payload[ofst + 2 : ofst + 6])
    ofst = (nitm + 1) * 8
    # ensure we end up with 8-bit data in a larger type so we can multiply safely
    data = np.frombuffer(payload[ofst : ofst + data_len], dtype=np.int8).astype(int)
    return SpsPkt(sub, beam, chan, stn, sstn, pktnum, data, capture_no)


N_POL = 2
N_VALS_PER_CPLX = 2
N_BYES_PER_VAL = 2
N_BYTES_PER_SAMPLE = N_POL * N_VALS_PER_CPLX * N_BYES_PER_VAL


def extract_pst_info(payload, capture_no):  # pylint: disable=too-many-locals
    """
    Get PST fields from PST packet.

    :param payload: PST packet payload bytes
    :param capture_no: packet number within the original pcap file
    """
    seq_no = struct.unpack("Q", payload[0:8])[0]
    sampl_no = struct.unpack("Q", payload[8:16])[0]
    scale1 = struct.unpack("f", payload[32:36])[0]
    first_chan = struct.unpack("I", payload[48:52])[0]
    num_chan = struct.unpack("H", payload[52:54])[0]
    valid_chan = struct.unpack("H", payload[54:56])[0]
    num_sample = struct.unpack("H", payload[56:58])[0]
    # beam_id = struct.unpack("H", payload[58:60])[0]
    sample_per_weight = struct.unpack("B", payload[67:68])[0]

    weights_offset = 96  # multiple of 16bytes=128 bits
    n_weight_bytes = num_sample / sample_per_weight * 2 * num_chan
    # weights padded to multiple of 128 bits = 16 bytes
    data_offset = weights_offset + math.ceil(n_weight_bytes / 16) * 16

    pwr_sum = np.zeros(num_sample)
    for chan in range(0, valid_chan):
        ch_offset = data_offset + (
            chan * num_sample * N_BYES_PER_VAL * N_VALS_PER_CPLX * N_POL
        )
        # sum sample power for both polarisations
        for pol in range(0, 2):
            pol_base = ch_offset + pol * num_sample * N_BYES_PER_VAL * N_VALS_PER_CPLX
            for sample_idx in range(0, num_sample):
                loc_x = pol_base + sample_idx * 4
                x_i, x_q = struct.unpack("hh", payload[loc_x : loc_x + 4])
                sample_pwr = x_i * x_i + x_q * x_q
                pwr_sum[sample_idx] += sample_pwr

    # avg pwr per sample per channel
    pwr_sum = pwr_sum / (scale1 * scale1) / num_sample / valid_chan

    return PstPkt(seq_no, sampl_no, first_chan, pwr_sum, capture_no)


def find_first_nonzero_sps(spd_pkts: list[SpsPkt]) -> int:
    """
    Find first SPS SPEAD packet that has non-zero power.

    :param spd_pkts: list of SPS SPEAD packet objects
    :return: Index of packet in original capture list, or -1
    """
    for pkt in spd_pkts:
        apol_re = pkt.data[0::4]
        apol_im = pkt.data[1::4]
        bpol_re = pkt.data[2::4]
        bpol_im = pkt.data[3::4]

        pwr = (
            np.multiply(apol_re, apol_re)
            + np.multiply(apol_im, apol_im)
            + np.multiply(bpol_re, bpol_re)
            + np.multiply(bpol_im, bpol_im)
        )

        if np.count_nonzero(pwr) > 0:
            return pkt.capture_no
    return -1


def find_first_nonzero_pst(pst_pkts: list[PstPkt]) -> int:
    """
    Find first PST packet that has non-zero power.

    :param pst_pkts: llist of pst packet objects
    :return: index of packet in original capture list, or -1
    """
    for pkt in pst_pkts:
        if np.count_nonzero(pkt.power_sum) != 0:
            return pkt.capture_no
    return -1


FOLD_SAMP = 960 - 1  # this must match the CNIC pulsar mode period
PST_SPS_SAMP = 192  # SPS samples per PST sample period
POINTS_IN_FOLD = FOLD_SAMP // math.gcd(FOLD_SAMP, PST_SPS_SAMP)


def spead_power(  # pylint: disable=too-many-locals
    spd_pkts: list[SpsPkt], subs
) -> tuple[np.ndarray, np.ndarray]:
    """
    Get average SPS sample power and timestamp.

    Normalied to 1 channel and 1 station if multiple are present

    :return: (sample_time_since_ska_epoch, sample_power)
    """
    # Get list of stations in the station-beam (so we can get pwr for each)
    sa_bm_stn = []
    for sa_id, sub in subs.items():
        for bm_id in sub["beams"]:
            for stn_id in sub["stns"]:
                sa_bm_stn.append(f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}")

    # Get list of packet numbers (pkt number is packet since SKA epoch)
    pkt_nos = set()
    for pkt in spd_pkts:
        pkt_nos.add(pkt.pkt_no)
    pkt_nos = sorted(list(pkt_nos))

    # Calculate sample number since SKA epoch for samples in each packet
    sample_nos = np.zeros(len(pkt_nos) * 2048, dtype=int)  # 2048 samples/SPS pkt
    for pkt_idx, pkt_no in enumerate(pkt_nos):
        start_sampl = 2048 * pkt_no
        sample_nos[pkt_idx * 2048 : (pkt_idx + 1) * 2048] = np.asarray(
            list(range(start_sampl, start_sampl + 2048)), dtype=int
        )

    # =============== STATION BEAM POWER ==============
    # Numpy vector to hold power for each station of each subarray-beam created all-zero
    stn_powers = [np.zeros(len(pkt_nos) * 2048, dtype=float) for _ in sa_bm_stn]
    # Numpy vector to hold numbr of chans that were summed
    stn_chans = [np.zeros(len(pkt_nos), dtype=int) for _ in sa_bm_stn]
    # Get power for each packet's samples and put in stn_powers list
    for pkt in spd_pkts:
        sa_id = pkt.subarray_id
        bm_id = pkt.stn_bm_id
        stn_id = pkt.stn_id
        sstn_id = pkt.sstn_id
        sa_bm_stn_idx = sa_bm_stn.index(
            f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}.{sstn_id}"
        )

        pkt_no_idx = pkt_nos.index(pkt.pkt_no)
        pkt_start_idx = pkt_no_idx * 2048
        apol_re = pkt.data[0::4]
        apol_im = pkt.data[1::4]
        bpol_re = pkt.data[2::4]
        bpol_im = pkt.data[3::4]

        pwr = (
            np.multiply(apol_re, apol_re)
            + np.multiply(apol_im, apol_im)
            + np.multiply(bpol_re, bpol_re)
            + np.multiply(bpol_im, bpol_im)
        )

        stn_powers[sa_bm_stn_idx][pkt_start_idx : pkt_start_idx + 2048] += pwr
        stn_chans[sa_bm_stn_idx][pkt_no_idx] += 1
    # normalise station power: divide by number of channels accumulated
    for stn_pwr_data in stn_powers:
        for pkt_no_idx in range(0, len(pkt_nos)):
            if stn_pwr_data[pkt_no_idx] != 0:
                pkt_start_idx = pkt_no_idx * 2048
                if stn_chans[sa_bm_stn_idx][pkt_no_idx] == 0:
                    continue
                stn_pwr_data[pkt_start_idx : pkt_start_idx + 2048] /= stn_chans[
                    sa_bm_stn_idx
                ][pkt_no_idx]

    # accumulate power in SPS packets across all stations
    all_stns_pwr = np.zeros((len(stn_powers[0]),), dtype=float)
    for stn_pwr in stn_powers:
        all_stns_pwr += stn_pwr
    all_stns_pwr /= len(stn_powers)

    return sample_nos, all_stns_pwr


def pst_power(  # pylint: disable=too-many-locals
    pst_pkts: list[SpsPkt],
) -> tuple[np.ndarray, np.ndarray]:
    """
    Get average PST sample power and timestamp.

    Normalised to a single channel

    :return: (sample_time_since_ska_epoch, sample_power)
    """
    # From PST sample number, calculate PST sample times
    pst_sample_nos = set()
    pst_first_chans = set()
    for pkt in pst_pkts:
        pst_sample_nos.add(pkt.sample_no)
        pst_first_chans.add(pkt.first_chan)
    pst_sample_nos = sorted(list(pst_sample_nos))
    pst_first_chans = sorted(list(pst_first_chans))

    # Calculate PST sample times
    pst_smp_idxs = [0] * len(pst_sample_nos) * 32
    for cnt, sample_no in enumerate(pst_sample_nos):
        idx = cnt * 32
        pst_smp_idxs[idx : idx + 32] = [(i + sample_no) for i in range(0, 32)]
    pst_sample_numbers = np.asarray(pst_smp_idxs, dtype=int)

    # get power for each PST packet's samples
    # Numpy vector holding power for each group of pst channels, created all-zero
    pst_powers = [
        np.zeros(len(pst_sample_nos) * 32, dtype=float) for _ in pst_first_chans
    ]
    print(f"{len(pst_pkts)} pst packets")
    last_sample_no = None
    num_bad_pst_timestamps = 0
    for pkt in pst_pkts:
        if last_sample_no is None:
            last_sample_no = pkt.sample_no
        if pkt.sample_no not in (last_sample_no, last_sample_no + 32):
            num_bad_pst_timestamps += 1
        last_sample_no = pkt.sample_no

        frq_idx = pst_first_chans.index(pkt.first_chan)
        pkt_idx = pst_sample_nos.index(pkt.sample_no)
        pkt_start_idx = 32 * pkt_idx
        pst_powers[frq_idx][pkt_start_idx : pkt_start_idx + 32] = pkt.power_sum / 16
    # Accumulate PST beam power across all PST channels
    all_chans_pwr = np.zeros(pst_powers[0].shape, dtype=float)
    for pst_pwr in pst_powers:
        all_chans_pwr += pst_pwr
    all_chans_pwr /= len(pst_powers)

    return pst_sample_numbers, all_chans_pwr


def analysis_main(
    pst_file,
    is_interactive=False,
    plot_dir: str = ".",
):  # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    """
    Analyse a PCAP file containing simultaneously captured PST/SPS packets.

    Determine PST timestamps relative to SPS timestamps
    (Difference between middle of PST pulse and peak of quadratic fit to beam power)
    Plot folded PST beam power, SPS station power, and quadratic fit
    """
    print("Check accuracy of PST timestamp (by pulse folding)")
    # From PCAP file get PCAP timestamps and UDP payloads for each packet
    payloads = get_udp_payload_bytes(pst_file, min_udp_data_size=56)
    print(f"{len(payloads)} packets")
    spd_pkts_all, subs, pst_pkts_all = get_pst_and_sps_pkts(payloads)

    # Check that capture file meets requirements for analysis
    assert len(subs) == 1, f"Test expects 1 subarray got {len(subs)}"
    for sub in subs.values():
        n_stn_beams = len(sub["beams"])
        assert n_stn_beams == 1, f"Test expects 1 stn-beam, got {n_stn_beams}"

    # Analyse packets which occur after pulses have started
    first_pwr = find_first_nonzero_sps(spd_pkts_all)
    spd_pkts = [pkt for pkt in spd_pkts_all if pkt.capture_no > first_pwr + 100]
    pst_pkts = [pkt for pkt in pst_pkts_all if pkt.capture_no > first_pwr + 100]

    # Get list of stations in the station-beam (so we can get pwr for each)
    sa_bm_stn = []
    for sa_id, sub in subs.items():
        for bm_id in sub["beams"]:
            for stn_id in sub["stns"]:
                sa_bm_stn.append(f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}")

    # From packet number, calculate SPS sample times modulo folding period
    pkt_nos = set()
    for pkt in spd_pkts:
        pkt_nos.add(pkt.pkt_no)
    pkt_nos = sorted(list(pkt_nos))
    # Numpy vector holding sample numbers (x-axis of plot)
    sample_nos = np.zeros(len(pkt_nos) * 2048, dtype=int)  # 2048 samples/SPS pkt
    for pkt_idx, pkt_no in enumerate(pkt_nos):
        start_sampl = 2048 * pkt_no
        sample_nos[pkt_idx * 2048 : (pkt_idx + 1) * 2048] = np.asarray(
            [i % FOLD_SAMP for i in range(start_sampl, start_sampl + 2048)], dtype=int
        )
    sample_times = sample_nos.astype(float) * 1080e-9
    unique_sample_times = sorted(list(set(sample_times.tolist())))
    print(f"{len(unique_sample_times)} samples of stn power after folding")

    # =============== STATION BEAM POWER ==============
    # Numpy vector to hold power for each station of each subarray-beam created all-zero
    stn_powers = [np.zeros(len(pkt_nos) * 2048, dtype=float) for _ in sa_bm_stn]
    # Numpy vector to hold numbr of chans that were summed
    stn_chans = [np.zeros(len(pkt_nos), dtype=int) for _ in sa_bm_stn]
    # Get power for each packet's samples and put in stn_powers list
    for pkt in spd_pkts:
        sa_id = pkt.subarray_id
        bm_id = pkt.stn_bm_id
        stn_id = pkt.stn_id
        sstn_id = pkt.sstn_id
        sa_bm_stn_idx = sa_bm_stn.index(
            f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}.{sstn_id}"
        )

        pkt_no_idx = pkt_nos.index(pkt.pkt_no)
        pkt_start_idx = pkt_no_idx * 2048
        apol_re = pkt.data[0::4]
        apol_im = pkt.data[1::4]
        bpol_re = pkt.data[2::4]
        bpol_im = pkt.data[3::4]

        pwr = (
            np.multiply(apol_re, apol_re)
            + np.multiply(apol_im, apol_im)
            + np.multiply(bpol_re, bpol_re)
            + np.multiply(bpol_im, bpol_im)
        )

        stn_powers[sa_bm_stn_idx][pkt_start_idx : pkt_start_idx + 2048] += pwr
        stn_chans[sa_bm_stn_idx][pkt_no_idx] += 1
    # normalise station power: divide by number of channels accumulated
    for stn_pwr_data in stn_powers:
        for pkt_no_idx in range(0, len(pkt_nos)):
            if stn_pwr_data[pkt_no_idx] != 0:
                pkt_start_idx = pkt_no_idx * 2048
                if stn_chans[sa_bm_stn_idx][pkt_no_idx] == 0:
                    continue
                stn_pwr_data[pkt_start_idx : pkt_start_idx + 2048] /= stn_chans[
                    sa_bm_stn_idx
                ][pkt_no_idx]

    # From PST sample number, calculate PST sample times
    pst_sample_nos = set()
    pst_first_chans = set()
    for pkt in pst_pkts:
        pst_sample_nos.add(pkt.sample_no)
        pst_first_chans.add(pkt.first_chan)
    pst_sample_nos = sorted(list(pst_sample_nos))
    pst_first_chans = sorted(list(pst_first_chans))

    # accumulate power in SPS packets across all stations
    all_stns_pwr = np.zeros((len(stn_powers[0]),), dtype=float)
    for stn_pwr in stn_powers:
        all_stns_pwr += stn_pwr
    all_stns_pwr /= len(stn_powers)

    # first_nonzero_stn_pwr_idx = None
    # for idx, pwr in enumerate(all_stns_pwr.aslist()):
    #     if pwr > 0.0:
    #         first_nonzero_stn_pwr_idx = idx
    #         break

    # Fold station power
    folded_stn_pwr = [0.0] * len(unique_sample_times)
    stns_added = [0] * len(unique_sample_times)
    for pkt, s_time in zip(all_stns_pwr.tolist(), sample_times):
        idx = unique_sample_times.index(s_time)
        folded_stn_pwr[idx] += pkt
        stns_added[idx] += 1
    # normalise bynumber of samples accumulated
    for idx, fpwr in enumerate(folded_stn_pwr):
        if stns_added[idx] == 0:
            continue
        folded_stn_pwr[idx] = fpwr / stns_added[idx]

    # find the SPS pulse start/end/middle in the folded data
    for idx, fpwr in enumerate(folded_stn_pwr):
        if fpwr != 0.0:
            first_secs = unique_sample_times[idx]
            # print(f"First non-zero stn power at: t={first_secs}")
            break
    for idx in range(0, len(folded_stn_pwr)):
        rev_idx = len(folded_stn_pwr) - 1 - idx
        if folded_stn_pwr[rev_idx] != 0.0:
            last_secs = unique_sample_times[rev_idx]
            # print(f"Last non-zero stn power at : t={last_secs}")
            break
    middle_secs = (first_secs + last_secs) / 2.0
    print(f"Mid station pulse at     : t={middle_secs}")

    # ==================== PST BEAM PoWER ===========================

    # Numpy vector holding power for each group of pst channels, created all-zero
    pst_powers = [
        np.zeros(len(pst_sample_nos) * 32, dtype=float) for _ in pst_first_chans
    ]

    # Calculate PST sample times, folded every "FOLD_SAMP" SPS samples
    pst_smp_indicies = [0] * len(pst_sample_nos) * 32
    for cnt, sample_no in enumerate(pst_sample_nos):
        idx = cnt * 32
        pst_smp_indicies[idx : idx + 32] = [
            ((i + sample_no) * PST_SPS_SAMP) % FOLD_SAMP for i in range(0, 32)
        ]
    pst_sample_times = np.asarray(pst_smp_indicies, dtype=float) * 1080e-9

    # get power for each PST packet's samples
    print(f"{len(pst_pkts)} pst packets")
    last_sample_no = None
    num_bad_pst_timestamps = 0
    for pkt in pst_pkts:
        if last_sample_no is None:
            last_sample_no = pkt.sample_no
        if pkt.sample_no not in (last_sample_no, last_sample_no + 32):
            num_bad_pst_timestamps += 1
        last_sample_no = pkt.sample_no

        frq_idx = pst_first_chans.index(pkt.first_chan)
        pkt_idx = pst_sample_nos.index(pkt.sample_no)
        pkt_start_idx = 32 * pkt_idx
        pst_powers[frq_idx][pkt_start_idx : pkt_start_idx + 32] = pkt.power_sum / 16

    # Accumulate PST beam power across all PST channels
    all_chans_pwr = np.zeros(pst_powers[0].shape, dtype=float)
    for pst_pwr in pst_powers:
        all_chans_pwr += pst_pwr
    all_chans_pwr /= len(pst_powers)

    # Fold PST beam power
    unique_timestamps = sorted(list(set(pst_sample_times.tolist())))
    print(f"{len(unique_timestamps)} unique PST timestamps")
    folded_pwr = [0.0] * len(unique_timestamps)
    num_folded = [0] * len(unique_timestamps)
    for s_time, pkt in zip(pst_sample_times.tolist(), all_chans_pwr.tolist()):
        idx = unique_timestamps.index(s_time)
        folded_pwr[idx] += pkt
        num_folded[idx] += 1
    for idx, fpwr in enumerate(folded_pwr):
        folded_pwr[idx] = fpwr / num_folded[idx]

    # ===================== compare peak of beam power to midpoint of pulse

    max_idx = folded_pwr.index(max(folded_pwr))
    max_time = unique_timestamps[max_idx]
    print(f"max {folded_pwr[max_idx]} at {max_time}")
    adjust = (1.5 * FOLD_SAMP * 1080e-9) - max_time

    # stn pwr plot adjust time to put peak near middle
    for idx, ust in enumerate(unique_sample_times):
        unique_sample_times[idx] = (ust + adjust) % (FOLD_SAMP * 1080e-9)
    # beam pwr plot adjust time to put peak near middle
    for idx, ust in enumerate(unique_timestamps):
        unique_timestamps[idx] = (ust + adjust) % (FOLD_SAMP * 1080e-9)

    # Get points that are near the middle of the pulse
    np_ut = np.asarray(unique_timestamps)
    centre_idxes = (np_ut > (0.4 * FOLD_SAMP * 1080e-9)) & (
        np_ut < (0.6 * FOLD_SAMP * 1080e-9)
    )

    # fit polynomial to the middle
    fit_pwr = np.asarray(folded_pwr)[centre_idxes]
    fit_time = np_ut[centre_idxes]
    poly = np.polyfit(fit_time, fit_pwr, 2)
    poly_pk_t = -poly[1] / (2.0 * poly[0])
    poly_vals = np.polyval(poly, fit_time)

    stn_pulse_min_time = FOLD_SAMP * 1080e-9
    stn_pulse_max_time = 0.0
    for s_time, pkt in zip(unique_sample_times, folded_stn_pwr):
        if pkt > 0.0:
            if s_time > stn_pulse_max_time:
                stn_pulse_max_time = s_time
            if s_time < stn_pulse_min_time:
                stn_pulse_min_time = s_time

    stn_pulse_middle_time = (stn_pulse_min_time + stn_pulse_max_time) / 2.0
    print(f"Middle folded station pulse: {stn_pulse_middle_time * 1e6:.3f} usec")
    print(f"PST polynomial fit max at  : {poly_pk_t * 1e6:.3f} usec")

    # ================ PLOTTING ================
    sps_ts_usec = np.asarray(unique_sample_times, dtype=float) * 1e6
    pst_ts_usec = np.asarray(unique_timestamps, dtype=float) * 1e6

    # Plot time-domain power for each station of each subarray-beam
    _, axis = plt.subplots(figsize=(10, 6))  # fig == _ is unused
    axis.scatter(
        sps_ts_usec,
        folded_stn_pwr,
        label=f"Average station power [midpoint@ {stn_pulse_middle_time * 1e6:.1f} μs]",
        color="red",
        marker=".",
    )
    axis.scatter(pst_ts_usec, folded_pwr, label="Average PST beam power", marker=".")

    axis.scatter(
        fit_time * 1e6,
        poly_vals,
        label=f"beam power poly fit [peak@ {poly_pk_t * 1e6:.1f} usec]",
        marker=".",
    )

    axis.legend()  # show the legend
    title_txt = "PST beam power and SPS station power folded on pulse period"
    axis.set_title(title_txt + f" (of {FOLD_SAMP} SPS samples)")
    axis.set_xlabel("Time (microseconds)")
    axis.set_ylabel("Power")
    plt.grid()

    pst_timing_error_sec = poly_pk_t - stn_pulse_middle_time

    plt.savefig(os.path.join(plot_dir, "ptc30_folded_pulses.png"))
    if is_interactive:
        plt.show()
    print(f"measured PST timestamp error {pst_timing_error_sec * 1e6:.3e} usec")
    print(f"{num_bad_pst_timestamps} bad timestamps in PST packets")

    return pst_timing_error_sec, num_bad_pst_timestamps


def check_first_pulses(
    pst_file: str,
    is_interactive=False,
    plot_dir: str = ".",
) -> (float, float):
    """
    Analyse a PCAP file containing simultaneously captured PST/SPS packets.

    Check that the first PST-beam pulse occurs at the same timestamp as the
    first SPS station-beam pulse.

    :returns: offset (s), latency (s)
    """
    # pylint: disable=too-many-locals,too-many-statements
    print("Check first PST pulse out has same timestamp as first SPS pulse in")
    # From PCAP file get PCAP timestamps and UDP payloads for each packet
    tstamps, payloads = get_timestamps_udp_payload_bytes(pst_file, min_udp_data_size=56)
    spd_pkts_all, subs, pst_pkts_all = get_pst_and_sps_pkts(payloads)

    # latency between packet with first SPS pulse and packet with first PST pulse
    first_sps = find_first_nonzero_sps(spd_pkts_all)
    first_pst = find_first_nonzero_pst(pst_pkts_all)
    first_sps_ts = tstamps[first_sps]
    first_pst_ts = tstamps[first_pst]
    pkt_latency_sec = first_pst_ts - first_sps_ts

    # get averaged PST and SPS power samples and time
    sps_samp_no, sps_pwr = spead_power(spd_pkts_all, subs)
    sps_ts = sps_samp_no.astype(float) * 1080e-9
    pst_samp_no, pst_pwr = pst_power(pst_pkts_all)
    pst_ts = pst_samp_no.astype(float) * (5184.0e-6 / 25.0)

    # select portion of SPS data (around first pulse) to plot
    sps_idx = np.argmax(sps_pwr != 0.0)
    assert sps_idx > 3000, "SPS data peaked earlier than expected"
    plt_sps_pwr = sps_pwr[sps_idx - 3000 : sps_idx + 4000]
    plt_sps_ts = sps_ts[sps_idx - 3000 : sps_idx + 4000]
    # select portion of PST data (around first pulse) to plot
    pst_idx = np.argmax(pst_pwr != 0.0)
    assert pst_idx > 10, "PST data peaked earlier than expected"
    plt_pst_pwr = pst_pwr[pst_idx - 10 : pst_idx + 30]
    plt_pst_ts = pst_ts[pst_idx - 10 : pst_idx + 30]

    zero_time = min(plt_sps_ts[0], plt_pst_ts[0])
    plt_pst_ts -= zero_time
    plt_sps_ts -= zero_time

    # find highest value in first PST pulse
    pk_threshold = 0.25 * np.max(plt_pst_pwr)
    over_thresh = False
    pk_idx = -1
    pk_pwr = 0
    for idx, pwr in enumerate(plt_pst_pwr.tolist()):
        if pwr > pk_threshold:
            over_thresh = True
        if not over_thresh:
            continue
        if pwr > pk_pwr:
            pk_pwr = pwr
            pk_idx = idx
        else:
            break
    # Fit quadratic through 3pts around highest value
    fit_pwr = plt_pst_pwr[pk_idx - 1 : pk_idx + 2]
    # avoid numerical precision probs by making t near zero
    fit_t0 = plt_pst_ts[pk_idx - 1]
    fit_time = plt_pst_ts[pk_idx - 1 : pk_idx + 2] - fit_t0
    poly = np.polyfit(fit_time, fit_pwr, 2)
    poly_pk_t = -poly[1] / (2.0 * poly[0]) + fit_t0
    # construct quadratic interpolation for plotting
    fit_time = np.linspace(fit_t0, plt_pst_ts[pk_idx + 1], 10) - fit_t0
    fit_vals = np.polyval(poly, fit_time)
    fit_time += fit_t0

    # find middle of first sps pulse
    start_idx = None
    end_idx = None
    for idx, pwr in enumerate(plt_sps_pwr):
        if start_idx is None:
            if pwr == 0:
                continue
            start_idx = idx
        else:
            if pwr != 0:
                continue
            end_idx = idx - 1
            break
    t_mid = (plt_sps_ts[start_idx] + plt_sps_ts[end_idx]) / 2.0
    # calculate how far first PST peak is from middle of first SPS pulse
    t_offset = poly_pk_t - t_mid
    print(f"Offset of first PST pulse from first SPS pulse = {t_offset * 1e6:.1f} usec")

    # Create plots
    _, axis = plt.subplots(figsize=(10, 6))  # fig == _ is unused
    axis.plot(
        plt_sps_ts * 1e3,  # to milliseconds
        plt_sps_pwr,
        label="Average station-beam power",
        color="red",
    )
    axis.scatter(
        plt_pst_ts * 1e3,  # to milliseconds
        plt_pst_pwr,
        label="Average PST-beam power",
        color="blue",
        marker=".",
    )
    axis.plot(
        fit_time * 1e3,  # to milliseconds
        fit_vals,
        label=f"PST power fit [peak@ {poly_pk_t * 1e3:.3f} msec]",
        color="orange",
    )
    axis.legend()  # show the legend
    title_txt = "First SPS and PST beam pulses"
    title_txt += f" [offset of PST from SPS = {t_offset * 1e6:.1f} usec]"
    axis.set_title(title_txt)
    axis.set_xlabel("Time since first plotted sample (milliseconds)")
    axis.set_ylabel("Power")
    plt.grid()
    plt.savefig(os.path.join(plot_dir, "ptc30_first_pulses.png"))
    if is_interactive:
        plt.show()

    return t_offset, pkt_latency_sec


def parse_args():
    """Parse arguments for manual runs of this code."""
    parser = argparse.ArgumentParser(description="pst packet capture analyser")
    parser.add_argument("-f", "--file", required=True, help="File to analyse")
    parser.add_argument(
        "-b", "--beam", required=False, type=int, default=None, help="beam_id"
    )
    parser.add_argument(
        "-s", "--subarray", required=False, type=int, default=None, help="subarray_id"
    )
    return parser.parse_args()


def main():
    """Command-line interface."""
    args = parse_args()
    pst_filename = args.file
    _, latency = check_first_pulses(args.file, True)
    print(f"SPS to PST latency {float(latency) * 1e3:.1f}msec")
    tim_err, bad_tstamps = analysis_main(pst_filename, True)
    print(f"Timestamp difference {tim_err * 1e6:.1f}usec")
    print(f"{bad_tstamps} bad timestamps")
    sys.exit(0)


if __name__ == "__main__":
    main()
