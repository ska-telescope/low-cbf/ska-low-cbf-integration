# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""PCAP file processing functions."""
import socket
from collections import defaultdict
from dataclasses import dataclass
from decimal import Decimal
from typing import BinaryIO, Generator, Iterable, List, Optional, Set

import dpkt
from dpkt.ethernet import Ethernet
from dpkt.pcap import UniversalReader

SPEAD_TIMESTAMP_POS = slice(64, 72)


@dataclass
class PcapStatistics:
    """A collection of PCAP file statistics."""

    packet_sizes: Set[int]
    """All the packet sizes that were seen"""
    n_packets: int
    """How many packets were analysed"""
    duration: float
    """Difference between last and first timestamp"""
    total_bytes: int
    """Total bytes (sum of all packets analysed)"""


def pcap_stats(file, start_packet: int = 0) -> PcapStatistics:
    """
    Gather basic statistics of a PCAP file.

    :param file: PCAP file object
    :param start_packet: packet number at which to start analysis
    :raises RuntimeError: if no packets, or start_packet is not in file
    """
    reader = dpkt.pcap.UniversalReader(file)
    first_ts = None
    last_ts = None
    total_bytes = 0
    packet_sizes = set()
    n = None
    for n, (timestamp, packet) in enumerate(reader):
        if n == start_packet:
            first_ts = timestamp
            total_bytes += len(packet)
            packet_sizes.add(len(packet))
        elif n > start_packet:
            last_ts = timestamp
            total_bytes += len(packet)
            packet_sizes.add(len(packet))

    if n is None:
        raise RuntimeError("no packets in PCAP file")
    if first_ts is None:
        raise RuntimeError("start_packet > packets in file")

    return PcapStatistics(
        n_packets=n + 1 - start_packet,
        packet_sizes=packet_sizes,
        duration=float(last_ts - first_ts),  # timestamps are Decimal
        total_bytes=total_bytes,
    )


def pcap_timestamps(pcap_file, start_packet=0) -> List[float] | List[Decimal]:
    """
    Extract timestamps from each packet.

    :param pcap_file: PCAP file object
    :param start_packet: packet number at which to start analysis
    """
    reader = dpkt.pcap.UniversalReader(pcap_file)
    timestamps = []
    for n, (timestamp, _) in enumerate(reader):
        if n < start_packet:
            continue
        timestamps.append(timestamp)

    return timestamps


def latency_stats(file, start_packet: int = 0, sdp_port: int = 20000) -> (dict, dict):
    """
    Gather latency statistics of a SPEAD SDP and SPS protocol PCAP file.

    Supports:
    - SPS version 3
    - Correlator to SDP, from firmware version 0.0.8+

    :param file: PCAP filename
    :param start_packet: packet number containing SPEAD Init packets
    :param sdp_port: starting port number for SDP Port
    :return: two dictionary containing timestamps of SPS and SDP packets
    """
    # pylint: disable=too-many-nested-blocks, too-many-locals, R0912, R1732
    file_to_open = open(file, "rb")
    pcap = dpkt.pcap.Reader(file_to_open)

    sps_timestamps = {}
    visibility_data = {}
    port_to_freq = {}
    for nb_packets, (pkt_ts, buf) in enumerate(pcap):
        if nb_packets > start_packet:
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            if ip.p == dpkt.ip.IP_PROTO_UDP:
                udp = ip.data
                if udp.dport == 4660:
                    # let's get time stamp for all samples number of
                    # one coarse channel
                    data = udp.data
                    cnt_sample = int.from_bytes(
                        data[12:16],
                        byteorder="big",
                        signed=False,
                    )
                    frequency = int.from_bytes(
                        data[54:56],
                        byteorder="big",
                        signed=False,
                    )
                    if frequency not in sps_timestamps:
                        sps_timestamps[frequency] = {}
                        print(f"FreqSPS {frequency}")
                    if (cnt_sample * 2048) not in sps_timestamps[frequency]:
                        for sps_sample in range(
                            cnt_sample * 2048, (cnt_sample * 2048) + 2048, 1
                        ):
                            sps_timestamps[frequency][sps_sample] = float(pkt_ts)
                else:
                    data = udp.data
                    if udp.dport in [
                        sdp_port,
                        (sdp_port + (start_packet + 1) / 2),
                        (sdp_port + start_packet),
                    ]:
                        # only interested in 3 visibilities, one per coarse channel
                        frequency = port_to_freq[udp.dport]
                        # timestamp at the beginning of data
                        timestamp = int.from_bytes(
                            data[SPEAD_TIMESTAMP_POS],
                            byteorder="little",
                            signed=False,
                        )
                        if frequency not in visibility_data:
                            visibility_data[frequency] = {}
                        if len(visibility_data[frequency]) == 0:
                            biggest_record = int(timestamp / 1080) - 384 * 2048
                        else:
                            biggest_record = sorted(visibility_data[frequency].keys())[
                                len(visibility_data[frequency].keys()) - 1
                            ]
                        if int(timestamp / 1080) not in visibility_data:
                            for vis_time in range(
                                biggest_record, int(timestamp / 1080) + 1, 1
                            ):
                                visibility_data[frequency][vis_time] = float(pkt_ts)
        else:
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            if ip.p == dpkt.ip.IP_PROTO_UDP:
                udp = ip.data
                if udp.dport in [
                    sdp_port,
                    (sdp_port + (start_packet + 1) / 2),
                    (sdp_port + start_packet),
                ]:
                    data = udp.data
                    freq = int.from_bytes(
                        data[33 * 8 - 4 : 33 * 8],
                        byteorder="little",
                        signed=False,
                    )
                    print(f"Freq {freq / 781250}")
                    port_to_freq[udp.dport] = round(
                        int.from_bytes(
                            data[33 * 8 - 4 : 33 * 8],
                            byteorder="little",
                            signed=False,
                        )
                        / 781250
                    )

    return sps_timestamps, visibility_data


def get_udp_payload_bytes(pcap_filename, min_udp_data_size: int = 0) -> List[bytes]:
    """
    Read UDP payload bytes from a .pcap(ng) file.

    :param pcap_filename: filename of .pcap(ng) file
    :param min_udp_data_size: minimum number of bytes required in UDP payload
      (e.g. for higher-level protocol headers)

    :return list with one entry per packet, entry containing udp payload
    """
    with open(pcap_filename, "rb") as file:
        pcap_read = dpkt.pcap.UniversalReader(file)  # yes this is iterable
        udp_payloads = []
        for _, buf in pcap_read:  # pylint: disable=not-an-iterable
            # skip packet if not long enough to contain IP+UDP headers + user data
            if len(buf) < (34 + 8 + min_udp_data_size):
                print(f"WARNING: Found packet that is too small {len(buf)}Bytes")
                continue
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            # skip non-UDP packets
            if ip.p != dpkt.ip.IP_PROTO_UDP:
                print(f"WARNING: Found packet that is not UDP {ip.p} type")
                continue
            # add the UDP payload data into the list of payloads
            udp = ip.data
            udp_payloads.append(udp.data)

    return udp_payloads


def get_timestamps_udp_payload_bytes(
    pcap_filename: str, min_udp_data_size: int = 0
) -> (List[float], List[bytes]):
    """
    Read timestamps and UDP payload bytes from a .pcap(ng) file.

    :param pcap_filename: filename of .pcap(ng) file
    :param min_udp_data_size: minimum number of bytes required in UDP payload
      (e.g. for higher-level protocol headers)

    :return: two lists each with one entry per packet. (timestamps, udp payloads)
    """
    with open(pcap_filename, "rb") as file:
        pcap_read = dpkt.pcap.UniversalReader(file)
        timestamps = []
        udp_payloads = []
        for t_stamp, buf in pcap_read:  # pylint: disable=not-an-iterable
            # skip packet if not long enough to contain IP+UDP+SPEAD hdrs
            if len(buf) < (34 + 8 + min_udp_data_size):
                print(f"WARNING: Found packet that is too small {len(buf)}Bytes")
                continue
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            # skip non-UDP packets
            if ip.p != dpkt.ip.IP_PROTO_UDP:
                print(f"WARNING: Found packet that is not UDP {ip.p} type")
                continue
            # add the UDP payload data into the list of payloads
            udp = ip.data
            udp_payloads.append(udp.data)
            timestamps.append(t_stamp)

    return timestamps, udp_payloads


def udp_data_from_eth_packet(packet: bytes) -> bytes:
    """
    Get UDP payload out of an Ethernet packet, which is assumed to be UDP.

    :param packet: Ethernet packet
    """
    return Ethernet(packet).data.data.data


def payloads(file: BinaryIO) -> Generator[bytes, None, None]:
    """
    Yield packet UDP payloads, one at a time.

    :param file: PCAP(NG) file object
    """
    reader = UniversalReader(file)
    for _, packet in reader:  # pylint: disable=not-an-iterable
        yield udp_data_from_eth_packet(packet)


def data_start_stop_per_dst(filename, ip_addresses: Optional[Iterable[str]] = None):
    """
    Find start/stop times for data flow in a PCAP file, per destination IP address.

    :param ip_addresses: IP addresses of interest
      (omit or use ``None`` for all IP addresses)
    :param filename: PCAP file name
    :returns: {ip address: (start time, stop time)}
    """
    with open(filename, "rb") as pcap_file:
        timestamps_lengths = timestamp_length_per_dst_address(pcap_file)

    if not ip_addresses:
        ip_addresses = timestamps_lengths.keys()

    t_start_stop = {}
    for ip_addr in ip_addresses:
        # note these are PCAP timestamps, not Correlator/Beamformer timestamps
        timestamps = [ts for ts, _ in timestamps_lengths[ip_addr]]
        t_start_stop[ip_addr] = (float(timestamps[0]), float(timestamps[-1]))

    return t_start_stop


def timestamp_length_per_dst_address(
    pcap_file, start_packet=0
) -> dict[str, list[(Decimal, int)]]:
    """
    Extract timestamp and length from each packet, group by destination IP address.

    :param pcap_file: PCAP file object
    :param start_packet: packet number at which to start analysis
    :returns: { destination IP address: [(timestamp, length), ...], ... }
    """
    reader = dpkt.pcap.UniversalReader(pcap_file)
    ip_dst_dict = defaultdict(list)
    for n, (timestamp, buf) in enumerate(reader):
        if n < start_packet:
            continue
        ip_packet = dpkt.ethernet.Ethernet(buf).data
        dst_ip_addr_str = socket.inet_ntoa(ip_packet.dst)
        ip_dst_dict[dst_ip_addr_str].append((timestamp, len(ip_packet.data)))

    return ip_dst_dict
