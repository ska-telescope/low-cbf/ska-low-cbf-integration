# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Tango helper functions."""
import json
import time
from typing import Any, Callable, Iterable, List, Union

from tango import ConnectionFailed, Database, DevFailed, DeviceProxy, DevString


def get_proxies_for_class(device_class: str) -> List[DeviceProxy]:
    """
    Get Tango device proxies for all instances of a given class.

    At least one instance is expected to exist in the Tango Database.
    A sleep/retry loop is used if no instances are found, because they may still be
    starting up.

    :raises RuntimeError: if no instances are found.
    """
    # k8s-wait should ensure our Tango devices are ready, but it doesn't always work.
    # So, we check here that the devices actually exist.
    for sleep_time in range(15):
        time.sleep(sleep_time)
        db_query = Database().get_device_exported_for_class(device_class)
        proxies = [DeviceProxy(fqdn) for fqdn in db_query.value_string]
        if len(proxies) > 0:
            return proxies
    raise RuntimeError(f"No instances of {device_class} found in database.")


class DeviceProxyJson:
    """Transparently translate JSON to/from a Tango DeviceProxy."""

    # pylint: disable=useless-suppression
    # For some reason pylint thinks that disabling 'too-many-instance-attributes'
    # is useless.
    # If it is enabled, it complains that there are too many!
    # pylint: disable=too-many-instance-attributes
    # pylint: enable=useless-suppression

    def __init__(  # pylint: disable=too-many-arguments
        self,
        target: Union[DeviceProxy, str],  # "|" syntax confuses Sphinx??
        json_commands: Iterable | str | None = None,
        not_json_commands: Iterable | str | None = None,
        json_attributes: Iterable | str | None = None,
        not_json_attributes: Iterable | str | None = None,
    ):
        """
        Create a transparent JSON wrapper.

        JSON conversion is automatically applied to Tango Attributes or Commands that
        contain the word "JSON" (case-insensitive) in their description (`doc_in` aka
        `in_type_desc` for Commands). Parameters are available to override detection
        behaviour.

        Attributes detected/configured as JSON will have their return value decoded.

        Commands detected/configured as JSON will have their input argument encoded.

        :param target: Tango DeviceProxy or device name
        :param json_commands: Commands that take JSON input (overrides auto-detection)
        :param not_json_commands: Commands that do not take JSON input (overrides
            auto-detection)
        :param json_attributes: Attributes that return JSON strings (overrides
            auto-detection)
        :param not_json_attributes: Attributes that do not return JSON strings
            (overrides auto-detection)
        """
        json_commands = self._make_set(json_commands)
        not_json_commands = self._make_set(not_json_commands)
        json_attributes = self._make_set(json_attributes)
        not_json_attributes = self._make_set(not_json_attributes)

        if isinstance(target, str):
            target = DeviceProxy(target)
        self._target = target

        # detect JSON-using Commands
        for command in target.get_command_list():
            info = target.get_command_config(command)
            if info.in_type == DevString and "JSON" in info.in_type_desc.upper():
                json_commands.add(command)
        self._json_commands = json_commands - not_json_commands

        # detect JSON-using Attributes
        for attr in target.get_attribute_list():
            info = target.get_attribute_config(attr)
            if info.data_type == DevString and "JSON" in info.description.upper():
                json_attributes.add(attr)
        self._json_attrs = json_attributes - not_json_attributes

        self._json_wrapper_init_complete = True

    def __dir__(self):
        """Get list of target's attributes (plus ours)."""
        return dir(self._target) + list(self.__dict__.keys())

    def __getattr__(self, attr_name):
        """Get a Python attribute from our target (note: may be a Tango Command)."""
        attr = getattr(self._target, attr_name)
        if attr_name in self._json_commands:
            return self.jsonify_input(attr)
        if attr_name in self._json_attrs:
            return json.loads(attr)

        return attr

    def __setattr__(self, key, value):
        """Delegate to target, once we are initialised."""
        if (
            "_json_wrapper_init_complete" in self.__dict__
            and self._json_wrapper_init_complete
        ):
            setattr(self._target, key, value)
        else:
            super().__setattr__(key, value)

    @staticmethod
    def jsonify_input(func: Callable) -> Callable:
        """Convert non-JSON input into JSON before passing to func."""

        def json_from_args(argin=None, **kwargs):
            """Convert input arguments to JSON."""
            if kwargs:
                return func(json.dumps(kwargs))
            if isinstance(argin, str):
                # if input is string, we assume user has already performed JSON encoding
                return func(argin)
            return func(json.dumps(argin))

        return json_from_args

    @staticmethod
    def _make_set(input_: str | Iterable | None):
        """Convert an input to a set object."""
        if input_ is None:
            input_ = set()
        if isinstance(input_, str):
            input_ = {input_}
        # this should hande iterable types (list being the main one we expect to see)
        input_ = set(input_)
        return input_


def wait_for_attr(  # pylint: disable=too-many-arguments
    device: Union[DeviceProxy, DeviceProxyJson],
    attribute: str,
    value=True,
    failure_message: str = "Timed out",
    max_duration: int = 120,
    sleep_per_loop: int = 2,
) -> None:
    """
    Wait until an attribute has a certain value.

    :param device: Tango device proxy with the attribute to check
    :param attribute: The name of the attribute
    :param value: Expected value (defaults to True)
    :param failure_message: Message for the exception on failure. Defaults to
        "Timed out". A note about duration is appended.
    :param max_duration: Approximate time-out period (in reality it could be longer due
        to delays waiting for each attribute read)
    :param sleep_per_loop: How long to wait between checks
    :raises RuntimeError: if expected value not seen before timing out
    """
    max_sleeps = max_duration // sleep_per_loop
    for _ in range(max_sleeps + 1):
        if getattr(device, attribute) == value:
            break
        time.sleep(sleep_per_loop)
    else:
        raise RuntimeError(f"{failure_message} after {max_duration}s")


def wait_for_attribute_value(
    device: Union[DeviceProxy, DeviceProxyJson],
    attribute: str,
    value: Any = True,
    failure_message: str = "Timed out waiting for attribute value",
    timeout_sec: int = 120,
) -> None:
    """
    Wait until a Tango device's attribute has a certain value.

    :param device: Tango device proxy with the attribute to check
    :param attribute: The name of the attribute
    :param value: Expected value (defaults to ``True``)
    :param failure_message: Message for the exception on failure.
      Defaults to "Timed out waiting for attribute value".
      A note about duration is appended.
    :param timeout_sec: Time-out period in seconds
    :raises RuntimeError: if expected value not seen before timing out
    """
    deadline = time.time() + timeout_sec
    poll_interval_seconds = 2
    while time.time() < deadline:
        try:
            if device.read_attribute(attribute).value == value:
                break
        except DevFailed as err_:
            raise AttributeError from err_

        time.sleep(poll_interval_seconds)
    else:
        raise RuntimeError(f"{failure_message} after {timeout_sec} sec")


def wait_for_device_response(
    device: Union[DeviceProxy, DeviceProxyJson],
    failure_message: str = "Timed out waiting for device to respond",
    timeout_sec: int = 120,
) -> None:
    """
    Wait until a Tango device responds.

    :param device: Tango device proxy to wait for
    :param failure_message: Message for the exception on failure.
      Defaults to "Timed out waiting for device to respond".
      A note about duration is appended.
    :param timeout_sec: Approximate time-out period in seconds
    :raises RuntimeError: if the device does not respond in time
    """
    deadline = time.time() + timeout_sec
    poll_interval_seconds = 2
    while time.time() < deadline:
        try:
            device.ping()
            return
        except ConnectionFailed:
            time.sleep(poll_interval_seconds)
    raise RuntimeError(f"{failure_message} after {timeout_sec} sec")
