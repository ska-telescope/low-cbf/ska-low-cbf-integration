# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""File for printing RMS data."""


import matplotlib.pyplot as plt
import numpy as np


def print_rms_std_dev(slice_size: int, index_rms: int, rms1: np.ndarray, case=""):
    """
    Print RMS standard dev graph for RMS evaluation.

    :param slice_size: the number of cross correlation baseline to
        group together slice = nb_baseline/sum_1 vector (power of 2)
    :param index_rms: if not using the full default 64k (2^16)
        vector, needs to indication the maximum power we are studying
    :param rms1: the vector of rms1 from rms_slice_total_preparation
        from Visibility Analyser
    :param case: case study for the various figures

    """
    rmss = []
    # https://docs.google.com/presentation/d/1-5gemzwfCb2xbR46V8kVgt6gbi8HY-b4tZcpT4zaQJY/edit#slide=id.g237a6a320ab_0_120
    # RMS calculation
    rmss.append(np.sqrt(rms1 / slice_size))
    for index in range(16):
        to_add = [
            np.sqrt((a**2 + b**2) / 2) for a, b in zip(*[iter(rmss[index])] * 2)
        ]
        rmss.append(to_add)
    x_points = [(slice_size * 0.849 * 2**x) / 3600 for x in range(17)]
    y_points = [np.std(rms_thingamajig) for rms_thingamajig in rmss]
    power_2 = [2**x for x in range(index_rms)]
    y_fit = [y_points[0] * x ** (-0.5) for x in power_2]
    y_fit_45 = [y_points[0] * x ** (-0.45) for x in power_2]
    y_fit_55 = [y_points[0] * x ** (-0.55) for x in power_2]
    plt.clf()
    plt.plot(
        x_points[:index_rms],
        y_points[:index_rms],
        "--bo",
        label="Std Dev of RMS",
    )
    plt.plot(x_points[:index_rms], y_fit, "--g", label="0.5 slope")
    plt.plot(x_points[:index_rms], y_fit_45, "--c", label="0.45 slope")
    plt.plot(x_points[:index_rms], y_fit_55, "--y", label="0.55 slope")
    plt.yscale("log")
    plt.xscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("RMS Standard Deviation")
    plt.legend()
    plt.grid()
    plt.savefig(f"{case}ptc_14_rms.png")


def print_rms_sum(slice_size: int, index_rms: int, sum_1: np.ndarray, case=""):
    """
    Print RMS Sum graph for RMS evaluation.

    :param slice_size: the number of cross correlation baseline to
        group together slice = nb_baseline/sum_1 vector (power of 2)
    :param index_rms: if not using the full default 64k (2^16)
        vector, needs to indication the maximum power we are studying
    :param sum_1: the vector of sum from rms_slice_total_preparation
        from Visibility Analyser
    :param case: case study for the various figures

    """
    sumss = []
    sumss.append(sum_1 / slice_size)
    for index in range(16):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(sumss[index])] * 2)]
        sumss.append(to_add)
    x_points = [(slice_size * 0.849 * 2**x) / 3600 for x in range(17)]
    y_points = [
        np.std(sumss[0]),
        np.std(sumss[1]),
        np.std(sumss[2]),
        np.std(sumss[3]),
        np.std(sumss[4]),
        np.std(sumss[5]),
        np.std(sumss[6]),
        np.std(sumss[7]),
        np.std(sumss[8]),
        np.std(sumss[9]),
        np.std(sumss[10]),
        np.std(sumss[11]),
        np.std(sumss[12]),
        np.std(sumss[13]),
        np.std(sumss[14]),
        np.std(sumss[15]),
        np.std(sumss[16]),
    ]
    power_2 = [2**x for x in range(index_rms)]
    y_fit = [y_points[0] * x ** (-0.5) for x in power_2]
    y_fit_45 = [y_points[0] * x ** (-0.45) for x in power_2]
    y_fit_55 = [y_points[0] * x ** (-0.55) for x in power_2]
    plt.clf()
    plt.plot(
        x_points[:index_rms],
        y_points[:index_rms],
        "--ro",
        label="Sum Std Dev",
    )
    plt.plot(x_points[:index_rms], y_fit[:index_rms], "--g", label="0.5 slope")
    plt.plot(
        x_points[:index_rms],
        y_fit_45[:index_rms],
        "--c",
        label="0.45 slope",
    )
    plt.plot(
        x_points[:index_rms],
        y_fit_55[:index_rms],
        "--y",
        label="0.55 slope",
    )
    plt.yscale("log")
    plt.xscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("sum_ Standard Deviation")
    plt.legend()
    plt.grid()
    plt.savefig(f"{case}ptc_14_sum.png")


def print_rms_sum_real(slice_size: int, index_rms: int, sum_1: np.ndarray, case=""):
    """
    Print RMS Real graph for RMS evaluation.

    :param slice_size: the number of cross correlation baseline to
        group together slice = nb_baseline/sum_1 vector (power of 2)
    :param index_rms: if not using the full default 64k (2^16)
        vector, needs to indication the maximum power we are studying
    :param sum_1: the vector of sum from rms_slice_total_preparation
        from Visibility Analyser
    :param case: case study for the various figures

    """
    real_sum = []
    real_sum.append(sum_1.real)
    for index in range(16):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(real_sum[index])] * 2)]
        real_sum.append(to_add)
    y_points = [real_sum[16][0]] * 17
    x_points = [(slice_size * 0.849 * 2**x) / 3600 for x in range(17)]
    e_points = [
        np.std(real_sum[0]),
        np.std(real_sum[1]),
        np.std(real_sum[2]),
        np.std(real_sum[3]),
        np.std(real_sum[4]),
        np.std(real_sum[5]),
        np.std(real_sum[6]),
        np.std(real_sum[7]),
        np.std(real_sum[8]),
        np.std(real_sum[9]),
        np.std(real_sum[10]),
        np.std(real_sum[11]),
        np.std(real_sum[12]),
        np.std(real_sum[13]),
        np.std(real_sum[14]),
        np.std(real_sum[15]),
        0,
    ]
    plt.clf()
    plt.errorbar(x_points, y_points, np.array(e_points), fmt="--ro")
    plt.xscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("Mean value of Real Cross Correlation")
    plt.savefig(f"{case}ptc_sum_real.png")
    plt.clf()
    power_2 = [2**x for x in range(index_rms)]
    y_fit = [e_points[0] * x ** (-0.5) for x in power_2]
    y_fit_45 = [e_points[0] * x ** (-0.45) for x in power_2]
    y_fit_55 = [e_points[0] * x ** (-0.55) for x in power_2]
    plt.plot(
        x_points[:index_rms],
        e_points[:index_rms],
        "--ro",
        label="Std Dev real component",
    )
    plt.xscale("log")
    plt.yscale("log")
    plt.plot(x_points[:index_rms], y_fit[:index_rms], "--g", label="0.5 slope")
    plt.plot(
        x_points[:index_rms],
        y_fit_45[:index_rms],
        "--c",
        label="0.45 slope",
    )
    plt.plot(
        x_points[:index_rms],
        y_fit_55[:index_rms],
        "--y",
        label="0.55 slope",
    )
    plt.xlabel("Number of hours")
    plt.ylabel("Standard Deviation of Real Cross Correlation")
    plt.legend()
    plt.grid()
    plt.savefig(f"{case}ptc_sum_dev_real.png")


def print_rms_sum_imag(slice_size: int, index_rms: int, sum_1: np.ndarray, case=""):
    """
    Print RMS Imaginary graph for RMS evaluation.

    :param slice_size: the number of cross correlation baseline to
        group together slice = nb_baseline/sum_1 vector (power of 2)
    :param index_rms: if not using the full default 64k (2^16)
        vector, needs to indication the maximum power we are studying
    :param sum_1: the vector of sum from rms_slice_total_preparation
        from Visibility Analyser
    :param case: case study for the various figures

    """
    img_sum = []
    img_sum.append(sum_1.imag)
    for index in range(16):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(img_sum[index])] * 2)]
        img_sum.append(to_add)
    y_points = [img_sum[16][0] for i in range(17)]
    e_points = [
        np.std(img_sum[0]),
        np.std(img_sum[1]),
        np.std(img_sum[2]),
        np.std(img_sum[3]),
        np.std(img_sum[4]),
        np.std(img_sum[5]),
        np.std(img_sum[6]),
        np.std(img_sum[7]),
        np.std(img_sum[8]),
        np.std(img_sum[9]),
        np.std(img_sum[10]),
        np.std(img_sum[11]),
        np.std(img_sum[12]),
        np.std(img_sum[13]),
        np.std(img_sum[14]),
        np.std(img_sum[15]),
        0,
    ]
    power_2 = [2**x for x in range(index_rms)]
    plt.clf()
    x_points = [(slice_size * 0.849 * 2**x) / 3600 for x in range(17)]
    plt.errorbar(x_points, y_points, np.array(e_points), fmt="--ro")
    plt.xscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("Mean value of Imaginary Cross Correlation")
    plt.savefig(f"{case}ptc_sum_imag.png")
    y_fit = [e_points[0] * x ** (-0.5) for x in power_2]
    y_fit_45 = [e_points[0] * x ** (-0.45) for x in power_2]
    y_fit_55 = [e_points[0] * x ** (-0.55) for x in power_2]
    plt.clf()
    plt.plot(
        x_points[:index_rms],
        e_points[:index_rms],
        "--ro",
        label="Std dev of imaginary component",
    )
    plt.plot(x_points[:index_rms], y_fit[:index_rms], "--g", label="0.5 slope")
    plt.plot(
        x_points[:index_rms],
        y_fit_45[:index_rms],
        "--c",
        label="0.45 slope",
    )
    plt.plot(
        x_points[:index_rms],
        y_fit_55[:index_rms],
        "--y",
        label="0.55 slope",
    )
    plt.xscale("log")
    plt.yscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("Standard Deviation of Imaginary Cross Correlation")
    plt.grid()
    plt.legend()
    plt.savefig(f"{case}ptc_sum_dev_imag.png")


def print_rms_final_graph(slice_size: int, index_rms: int, sum_1: np.ndarray, case=""):
    """
    Print final graph for RMS evaluation.

    :param slice_size: the number of cross correlation baseline to
        group together slice = nb_baseline/sum_1 vector (power of 2)
    :param index_rms: if not using the full default 64k (2^16)
        vector, needs to indication the maximum power we are studying
    :param sum_1: the vector of sum from rms_slice_total_preparation
        from Visibility Analyser
    :param case: case study for the various figures

    """
    # pylint: disable=too-many-locals
    sumss = []
    sumss.append(sum_1 / slice_size)
    for index in range(16):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(sumss[index])] * 2)]
        sumss.append(to_add)
    y_points = [
        np.sqrt(
            np.sum((np.abs(sumss[0]) ** 2)[: max(1, 2**index_rms)]) / 2**index_rms
        ),
        np.sqrt(
            np.sum((np.abs(sumss[1]) ** 2)[: max(1, 2**index_rms - 1)])
            / 2 ** (index_rms - 1)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[2]) ** 2)[: max(1, 2**index_rms - 2)])
            / 2 ** (index_rms - 2)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[3]) ** 2)[: max(1, 2**index_rms - 3)])
            / 2 ** (index_rms - 3)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[4]) ** 2)[: max(1, 2**index_rms - 4)])
            / 2 ** (index_rms - 4)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[5]) ** 2)[: max(1, 2**index_rms - 5)])
            / 2 ** (index_rms - 5)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[6]) ** 2)[: max(1, 2**index_rms - 6)])
            / 2 ** (index_rms - 6)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[7]) ** 2)[: max(1, 2**index_rms - 7)])
            / 2 ** (index_rms - 7)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[8]) ** 2)[: max(1, 2**index_rms - 8)])
            / 2 ** (index_rms - 8)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[9]) ** 2)[: max(1, 2**index_rms - 9)])
            / 2 ** (index_rms - 9)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[10]) ** 2)[: max(1, 2**index_rms - 10)])
            / 2 ** (index_rms - 10)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[11]) ** 2)[: max(1, 2**index_rms - 11)])
            / 2 ** (index_rms - 11)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[12]) ** 2)[: max(1, 2**index_rms - 12)])
            / 2 ** (index_rms - 12)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[13]) ** 2)[: max(1, 2**index_rms - 13)])
            / 2 ** (index_rms - 13)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[14]) ** 2)[: max(1, 2**index_rms - 14)])
            / 2 ** (index_rms - 14)
        ),
        np.sqrt(
            np.sum((np.abs(sumss[15]) ** 2)[: max(1, 2**index_rms - 15)])
            / 2 ** (index_rms - 15)
        ),
    ]
    power_2 = [2**x for x in range(index_rms)]
    plt.clf()
    x_points = [(slice_size * 0.849 * 2**x) / 3600 for x in range(17)]
    x_array = np.log10(x_points)
    y_array = np.log10(y_points)
    x_array = x_array.astype("float64")
    y_array = y_array.astype("float64")
    params = np.polyfit(x_array[0:14], y_array[0:14], 1)
    poly1d = np.poly1d(params)
    origin_point = 10 ** (poly1d(x_array)[0])
    y_fit = [origin_point * x ** (params[0]) for x in power_2]
    y_fit_50 = [origin_point * x ** (-0.50) for x in power_2]
    y_fit_45 = [origin_point * x ** (-0.45) for x in power_2]
    y_fit_55 = [origin_point * x ** (-0.55) for x in power_2]
    plt.plot(
        x_points[:index_rms],
        y_points[:index_rms],
        "--ro",
        label="Observed RMS",
    )
    plt.plot(
        x_points[:index_rms],
        y_fit[:index_rms],
        "--m",
        label=f"Data slope ({params[0]})",
    )
    plt.plot(x_points[:index_rms], y_fit_50[:index_rms], "--g", label="0.5 slope")
    plt.plot(
        x_points[:index_rms],
        y_fit_45[:index_rms],
        "--c",
        label="0.45 slope",
    )
    plt.plot(
        x_points[:index_rms],
        y_fit_55[:index_rms],
        "--y",
        label="0.55 slope",
    )
    plt.xscale("log")
    plt.yscale("log")
    plt.xlabel("Number of hours")
    plt.ylabel("RMS for given integration period")
    plt.grid()
    plt.legend()
    plt.savefig(f"{case}rms_ptc_14.png")
    return x_points, y_points, params[0]
