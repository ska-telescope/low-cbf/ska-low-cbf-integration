# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
SPS (Signal Processing Subsystem) constants / helper functions.

SPS provides CBF's input data - signals from the antennas.
SPS was formerly known as LFAA (Low Frequency Aperture Array).
"""

SAMPLES_PER_PACKET = 2048
SAMPLE_PERIOD = 1080e-9
"""SPS Sampling Period (seconds)"""
SPS_COARSE_SPACING_HZ = 781_250


def frequency_from_id(channel_id: int) -> int:
    """Get the frequency (Hz) of a given channel_id."""
    assert channel_id >= 0, "Channel ID must be positive"
    return channel_id * SPS_COARSE_SPACING_HZ


def id_from_frequency(frequency: int) -> int:
    """Get the channel_id of a given frequency (Hz)."""
    # TODO what to do if not valid?
    return frequency // SPS_COARSE_SPACING_HZ
