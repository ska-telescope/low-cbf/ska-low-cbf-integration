# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""Tools to print register values for debugging."""
import time
from functools import partial

from ska_low_cbf_integration.processor import debug_reg_read
from ska_low_cbf_integration.tango import DeviceProxyJson


def record_debug(cnic, processor, filename="log.txt") -> None:
    """
    Record register values to a file for debugging.

    Use this function as a callback in a test.
    Use functools.partial to supply a better filename.

    e.g.

    .. code:: python

        debug_log_ptc123 = partial(record_debug, filename="build/ptc123/log.txt")

        ProcessorVdTestConfig(
            during_scan_callback=debug_log_ptc123,
            # [...]
            )

    """
    packets_received = cnic.hbm_pktcontroller__rx_packet_count
    print(f"CNIC Packets Received: {packets_received}")
    print(f"Stats delay {processor.stats_delay}")
    # TODO - detect type of firmware in use, change class...
    pst_util = RegisterPrintPst(processor)
    number_of_seconds = 0
    with open(filename, "a", encoding="utf-8") as the_file:
        while not cnic.finished_receive:
            time.sleep(5)
            number_of_seconds += 5
            the_file.write("++++++++++++++++++++++++++++\n")
            the_file.write(f"Status after {number_of_seconds} seconds\n")
            the_file.write("++++++++++++++++++++++++++++\n")
            the_file.write(pst_util.all())
            the_file.write("----------------------------\n")
            the_file.write("----------------------------\n")


class RegisterPrintPst:
    """Register printer for PST firmware."""

    def __init__(self, pst_device_proxy: DeviceProxyJson):
        """Initialise the class."""
        self.debug_reg_read = partial(debug_reg_read, pst_device_proxy)

    def spead_sdp(self):
        """Show the IP/UDP information."""
        # TODO: not sure this is needed for PST
        ips = self.debug_reg_read("spead_sdp", "data", 0, 16)
        udp = self.debug_reg_read("spead_sdp", "data", 2048, 16)
        hea = self.debug_reg_read("spead_sdp", "data", 4096, 16)
        siz = self.debug_reg_read("spead_sdp", "data", 6144, 16)
        stp = self.debug_reg_read("spead_sdp", "data", 6272, 16)
        chn = self.debug_reg_read("spead_sdp", "data", 6400, 16)

        returning_txt = ""
        returning_txt += (
            "entry  ip_dest  udp_dest heap_ctr heap_siz blk_size channels\n"
        )
        for i, ip in range(0, len(ips)):
            txt = (
                f"{i:5d}: {ip:08x} {udp[i]:08x} {hea[i]:08x} "
                + f"{siz[i]:08x} {stp[i]:08x} {chn[i]:08x}"
            )
            returning_txt += txt + "\n"
        return returning_txt

    def ingest(self):
        """List the ingest registers."""
        periphs = ["lfaadecode100g", "lfaadecode100g_2", "lfaadecode100g_3"]
        regs = [
            "table_select",
            "total_stations",
            "total_coarse",
            "total_channels",
            "spead_packet_count",
            "nonspead_packet_count",
            "badethernetframes",
            "badipudpframes",
            "novirtualchannelcount",
            "spead_v1_packet_found",
            "spead_v2_packet_found",
            "spead_v3_packet_found",
            "non_spead_packet_found",
            "hbm_reset",
            "hbm_reset_status",
            "lfaa_decode_reset",
            "lfaa_tx_fsm_debug",
            "lfaa_stats_fsm_debug",
            "lfaa_rx_fsm_debug",
            "lfaa_lookup_fsm_debug",
        ]
        returning_txt = ""
        for reg in regs:
            txt = f"{reg:35}"
            for _, periph in enumerate(periphs):
                rslt = self.debug_reg_read(periph, reg)
                txt += f"  0x{rslt:08x}"
            returning_txt += txt + "\n"
        return returning_txt

    def ct1(self):
        """List the ct1 registers."""
        periphs = ["pst_ct1", "pst_ct1_2", "pst_ct1_3"]
        regs = [
            "full_reset",
            "status",
            "output_cycles",
            "framecount_start",
            "input_packets",
            ("frame_count_low", "  * 24 = pkt_cntr @ frame start"),
            "frame_count_high",
            "early_or_late_count",
            "pre_latch_on_count",
            "duplicates_count",
            "missing_count",
            "buffers_sent_count",
            "error_input_overflow",
            "error_read_overflow",
            "read_overflow_buffer_count",
            "max_frame_jump",
            "recent_clocks_between_readouts",
            "min_clocks_between_readouts",
            "buffer_count_at_min_readout_gap",
            "recent_packets_per_readout",
            "rfi_scale",
        ]
        returning_txt = ""
        for itm in regs:
            reg = itm
            descr = ""
            if isinstance(itm, tuple):
                reg = itm[0]
                descr = itm[1]
            txt = f"{reg:35}"
            for periph in periphs:
                rslt = self.debug_reg_read(periph, reg)
                txt += f"  0x{rslt:08x}"

            if len(descr) > 0:
                returning_txt += txt + descr + "\n"
            else:
                returning_txt += txt + "\n"
        return returning_txt

    def ct2(self):
        """List the ct2 registers."""
        periphs = ["ct2", "ct2_2", "ct2_3"]
        regs = [
            "bufferoverflowerror",
            "readouterror",
            "bufferenable",
            "scalefactor",
            "beamsenabled",
            "numberofbeams",
            "readinclocks",
            "readinallclocks",
            "readoutclocks",
            "hbmbuf0cornerturncount_low",  # *24 = pkt_cntr @ frame start
            "hbmbuf0cornerturncount_high",
            "hbmbuf1cornerturncount_low",
            "hbmbuf1cornerturncount_high",
            "jones_buffer0_valid_frame_low",
            "jones_buffer0_valid_frame_high",
            "jones_buffer0_valid_duration",
            "jones_buffer1_valid_frame_low",
            "jones_buffer1_valid_frame_high",
            "jones_buffer1_valid_duration",
            "poly_buffer0_info_valid",
            "poly_buffer0_valid_frame_low",
            "poly_buffer0_valid_frame_high",
            "poly_buffer0_valid_duration",
            "poly_buffer0_offset_ns",
            "poly_buffer1_info_valid",
            "poly_buffer1_valid_frame_low",
            "poly_buffer1_valid_frame_high",
            "poly_buffer1_valid_duration",
            "poly_buffer1_offset_ns",
        ]
        returning_txt = ""
        for reg in regs:
            txt = f"{reg:35}"
            for periph in periphs:
                rslt = self.debug_reg_read(periph, reg)
                txt += f"  0x{rslt:08x}"
            returning_txt += txt + "\n"
        return returning_txt

    def packetiser(self):
        """List the packetiser registers."""
        periphs = ["packetiser", "packetiser_2", "packetiser_3"]
        regs = [
            "control_vector",
            "stats_packets_rx_sig_proc_valid",
            "stats_packets_rx_sig_proc_invalid",
            "stats_packets_rx_sm_off",
            "stats_packets_rx_sig_proc",
        ]
        returning_txt = ""
        for reg in regs:
            txt = f"{reg:35}"
            for perip in periphs:
                rslt = self.debug_reg_read(perip, reg)
                txt += f"  0x{rslt:08x}"
            returning_txt += txt

        periph = "system"
        reg = "eth100g_tx_total_packets"
        rslt = self.debug_reg_read(periph, reg)
        returning_txt += f"\n{reg:35}  0x{rslt:08x}"
        return returning_txt

    def all(self):
        """List all the registers."""
        returning_txt = ""
        up_time = self.debug_reg_read("system", "time_uptime")
        returning_txt += f"system.time_uptime:                  0x{up_time:08x}\n"
        returning_txt += "        == PST ingest == \n"
        returning_txt += self.ingest()
        returning_txt += "        == first corner turn ==\n"
        returning_txt += self.ct1()
        returning_txt += "        == second corner turn ==\n"
        returning_txt += self.ct2()
        returning_txt += "        == packetisers == \n"
        returning_txt += self.packetiser() + "\n"
        return returning_txt
