# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low CBF Allocator Helpers."""
from collections import defaultdict
from typing import Dict, Set

from ska_low_cbf_integration.tango import DeviceProxyJson

ALLOCATOR_ADDR = "low-cbf/allocator/0"


def allocator_proxy(tango_address=ALLOCATOR_ADDR) -> DeviceProxyJson:
    """Get a Proxy to the Allocator.

    At time of writing, JSON translation will be applied for:

    Commands: ReserveCapabilities, InternalRegisterFsp

    Attributes: station_beams, pst_beams, alveo_firmware_image_names, resourceTableP4,
    internal_subarray, fsps, internal_alveo, p4_stn_routes, sdp_routes,
    longRunningCommandResult, resourceTableFSP
    """
    return DeviceProxyJson(tango_address)


def serials_per_subarray(allocator: DeviceProxyJson) -> Dict[int, Set[str]]:
    """Get the Alveo serial numbers assigned to each subarray."""
    return transpose_serials_subarrays(
        internal_alveo_serials_subarrays(allocator.internal_alveo)
    )


def internal_alveo_serials_subarrays(internal_alveo: dict) -> Dict[str, Set[int]]:
    """Get serial numbers & subarray assignments from `internal_alveo` structure."""
    # Example internal_alveo structure:
    # {
    #   "XFL1HOOQ1Y44": {
    #     "fw": "corr:0.1.0-main.1e88e61d:gitlab",
    #     "regs": [
    #       {
    #         "sa_id": 1,
    #         "stns": [ [1,1], [2,1], [3,1], [4,1], [5,1], [6,1] ],
    #         "sa_bm": [ [ 1, 100, 0, 3456, 192, 24, 0 ] ]
    #       }
    #     ]
    #   }
    # }

    return {
        serial: {reg["sa_id"] for reg in parameters["regs"]}
        for serial, parameters in internal_alveo.items()
    }


def transpose_serials_subarrays(serials_subarrays: dict) -> Dict[int, Set[str]]:
    """Transpose {serial: subarrays} to {subarray: serials}."""
    subarray_serials = defaultdict(set)
    for serial, subarrays in serials_subarrays.items():
        for subarray in subarrays:
            subarray_serials[subarray].add(serial)

    return subarray_serials
