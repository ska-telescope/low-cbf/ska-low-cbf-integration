# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Convert PCAP files to FPGA Simulation format."""
import argparse
from io import StringIO

import dpkt

BYTES_PER_LINE = 64  # 512 bits per line


def valid_bytes_str(n_valid_bytes: int) -> str:
    """
    Create byte validity string for simulation text format.

    One bit of validity per byte. e.g. If 46 bytes are valid, the expected string is:
    FFFF FFFF FFFC 0000 (without spaces).

    :param n_valid_bytes: Number of valid bytes.
    :raises AssertionError: if n_valid_bytes is too big
    """
    assert n_valid_bytes <= BYTES_PER_LINE
    binary_string = "1" * n_valid_bytes + "0" * (BYTES_PER_LINE - n_valid_bytes)
    return f"{int(binary_string, 2):016X}"


def write_packet_text(packet: bytes, file: StringIO):
    """
    Write one packet as text.

    :param packet: The packet data
    :param file: File-like object to print to
    """
    size = len(packet)
    for offset in range(0, size, BYTES_PER_LINE):
        end = min(offset + BYTES_PER_LINE, size)
        chunk = packet[offset:end]
        valid_bytes = len(chunk)
        if valid_bytes < BYTES_PER_LINE:
            chunk += bytes(BYTES_PER_LINE - valid_bytes)
        data = chunk.hex().upper()
        last = int(end == size)
        # two spaces between some fields, who knows why
        print(
            f"0000 1 {last}  {valid_bytes_str(valid_bytes)}  {data}",
            file=file,
        )


def main():
    """PCAP to FPGA simulation conversion utility main function (CLI)."""
    parser = argparse.ArgumentParser(
        description="Perentie PCAP to FPGA Simulation Text File Converter."
    )
    parser.add_argument(
        "input",
        type=argparse.FileType("rb"),
        help="Input pcap(ng) file.",
    )
    parser.add_argument(
        "output",
        type=argparse.FileType("w"),
        help="Output text file.",
    )
    args = parser.parse_args()

    reader = dpkt.pcap.UniversalReader(args.input)
    i = 0
    for i, (_, packet) in enumerate(reader, start=1):
        write_packet_text(packet, args.output)

    print(f"Converted {i} packets.")
