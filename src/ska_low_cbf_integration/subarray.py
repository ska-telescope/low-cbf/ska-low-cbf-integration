# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""Low CBF Subarray Tango device helper functions."""

from time import sleep

from ska_control_model import AdminMode, ObsState

from ska_low_cbf_integration.tango import DeviceProxyJson, wait_for_attr


def get_ready_subarray_proxy(subarray_id: int):
    """Get a Low CBF Subarray device that is ready to Configure."""
    subarray = DeviceProxyJson(
        f"low-cbf/subarray/{subarray_id:02d}",
        json_commands=[
            "AssignResources",
            "Configure",
            "ConfigureScan",
            "ReleaseResources",
            "Scan",
        ],
    )
    # the default Tango timeout (3") can be insufficient:
    subarray.set_timeout_millis(10_000)
    subarray.adminmode = AdminMode.ONLINE
    empty_subarray(subarray)  # in case previous test messed up
    subarray.AssignResources("{}")
    wait_for_attr(subarray, "obsState", ObsState.IDLE, "Assignment not finished")
    return subarray


def empty_subarray(subarray):
    """Take a subarray in any (?) ObsState and return it to EMPTY."""
    # a brief pause if caught in transient state:
    if subarray.obsState == ObsState.CONFIGURING:
        for _ in range(5):
            if subarray.obsState != ObsState.CONFIGURING:
                break
            sleep(1)
    if subarray.obsState == ObsState.FAULT:
        subarray.restart()
        wait_for_attr(subarray, "obsState", ObsState.EMPTY)
    if subarray.obsState == ObsState.SCANNING:
        subarray.EndScan()
        wait_for_attr(subarray, "obsState", ObsState.READY)
    if subarray.obsState == ObsState.READY:
        subarray.End()
        wait_for_attr(subarray, "obsState", ObsState.IDLE)
    if subarray.obsState == ObsState.IDLE:
        subarray.ReleaseAllResources()
        wait_for_attr(subarray, "obsState", ObsState.EMPTY)
    # debug, in case things go wrong: (throw exception?)
    print(
        f"\n\t** empty_subarray DONE; state: {subarray.obsState},"
        f" expecting {ObsState.EMPTY}"
    )
