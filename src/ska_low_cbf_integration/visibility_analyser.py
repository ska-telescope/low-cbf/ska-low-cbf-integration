# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=attribute-defined-outside-init,too-many-instance-attributes,too-many-nested-blocks,too-many-locals,too-many-arguments,too-many-lines
"""Class for analyasing visibility data."""

import json
import logging
import math
import os
import traceback
from collections import defaultdict
from typing import Dict, Optional

import matplotlib.pyplot as plt
import numpy as np
import spead2
import spead2.recv
from matplotlib import cm

POLARISATIONS = ("XX", "XY", "YX", "YY")

logging.basicConfig(level=logging.INFO)

COLOURS = "rgbcmykrgb"


class VisibilityAnalyser:
    """
    Implement a visibility analyser.

    This is used to check the results of the correlator
    """

    # pylint: disable=too-many-public-methods

    def __init__(self, configuration: str):
        """
        Initialiase the visibility analyser.

        :param configuration: the json configuration string, it contains
            the following for 2 stations and 2 channels:
            '{"coase_channels": [124, 125], "stations": [345, 350] }

        """
        self.configuration = json.loads(configuration)
        self.coarse_channels = self.configuration["coarse_channels"]
        self.stations: list = self.configuration["stations"]
        self.nb_station = len(self.stations)
        self.visu_type = np.dtype([("VIS", "<c8", (4,)), ("TCI", "|i1"), ("FD", "|u1")])
        self.visibilities = defaultdict(lambda: defaultdict(lambda: []))
        self.visibilities_flag = defaultdict(lambda: defaultdict(lambda: []))
        self.display_graph = False
        self.time_sequence = []
        self.corr_abs_matrixes_time = None
        self.corr_abs_matrixes_time_fine_channels = None
        self.corr_phase_matrixes_time = None
        self._average_correlation = None
        self._average_correlation_phase = None
        self._stddev_corr_phase = None
        self.vis_number = []
        for coarse_channel in sorted(self.coarse_channels):
            for vis_channel in range(144):
                self.vis_number.append(coarse_channel * 144 + vis_channel)
        self.vis_number = sorted(self.vis_number)

    def set_display_graph(self, display: bool = True):
        """
        Display figure instead of saving if display = True.

        :param display: change the display_graph variable

        """
        self.display_graph = display

    def extract_spead_data(
        self, pcap_file: str, verbose: bool = False, filter_: str = ""
    ):
        """
        Extract visibility from the spead packets.

        :param pcap_file: the file containing packet following pcap format
        :param verbose: boolean to have more information about extraction
        :param filter_: libpcap filter string - defaults to no filter,
          so we don't miss anything
        """
        # pylint: disable=too-many-branches
        self.n_baselines = -999
        spead_items_seen = set()
        first_heap_with_data = None
        heap_number = 0
        # Initialise spead2 library

        stream = spead2.recv.Stream(spead2.ThreadPool())

        stream.add_udp_pcap_file_reader(pcap_file, filter=filter_)
        item_group = spead2.ItemGroup()
        for heap_number, heap in enumerate(stream):
            items = item_group.update(heap)
            if verbose:
                # General information about heap
                print(
                    f"Heap {heap.cnt:#X} ("
                    f"SPEAD-{heap.flavour.item_pointer_bits}"
                    f"-{heap.flavour.heap_address_bits} "
                    f"v{heap.flavour.version})",
                    end="",
                )
            # don't process start/end packets any further
            if heap.is_start_of_stream():
                for key, item in items.items():
                    if key == "Basel":
                        self.n_baselines = item.value
                continue

            if first_heap_with_data is None:
                first_heap_with_data = (
                    heap_number,
                    heap.cnt,
                )  # our enumeration count, heap ID

            # Extract correlation data
            for key, item in items.items():
                if heap_number == first_heap_with_data[0] and key == "Basel":
                    self.n_baselines = item.value
                if heap_number == first_heap_with_data[0]:
                    spead_items_seen.add(key)

                # this loop is silly, but this doesn't work:
                # x = items["Corre"]  # why doesn't this work?!
                if key == "Corre":
                    # print(f"Found a correlation for heap {heap.cnt:#x}")
                    visibility_baseline_data = item.value
                    # print(f"CORRELATION VALUE: {item.value}")
                elif key == "tOffs":
                    time = item.value
                elif key == "VisFl":
                    visi_flag = item.value
            # heap_number_byte = heap.cnt.to_bytes(6, byteorder="big")
            # byte_array_heap_number_byte = bytearray(heap_number_byte)

            # chan = int.from_bytes(byte_array_heap_number_byte[2:4], "big")
            chan = int.from_bytes(
                bytearray(heap.cnt.to_bytes(6, byteorder="big"))[2:4], "big"
            )

            try:
                self.visibilities[chan][time] = visibility_baseline_data
                self.visibilities_flag[chan][time] = visi_flag
                # print(f"channel {chan} time {t} {visibility_baseline_data}")
                if time not in self.time_sequence:
                    self.time_sequence.append(time)
                # dirty way to stop old data carrying over (FIXME)
                del chan
                del time
                del visibility_baseline_data
            except NameError:
                traceback.print_exc()
                print(f"Something went wrong in heap number {heap_number}")

        stream.stop()
        self.time_sequence = sorted(self.time_sequence)
        print()
        print(f"Received {heap_number + 1} heaps")
        print(f"\t{self.n_baselines} baselines")
        print("\tSPEAD item names:", " ".join(spead_items_seen))

    def populate_correlation_matrix(self):
        """
        Populate the correlation matrix.

        This matrix is in fact a dictionary on the form
        corr_abs_matrixes_time = {
        time1: {
        channel1: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel1
        channel2: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel2
        },
        time2: {
        channel1: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel1
        channel2: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel2
        }
        }

        """
        self.corr_abs_matrixes_time = {}
        self.corr_abs_matrixes_time_fine_channels = {}
        self.corr_phase_matrixes_time = {}
        for time in self.time_sequence:
            corr_matrixes_coarse = {}
            corr_matrixes_fine = {}
            corr_matrixes_angle = {}
            for coarse_channel in self.coarse_channels:
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])

                for vis_channel in range(144):
                    index = 0
                    corr_matrix_fine = np.zeros(
                        [2 * self.nb_station, 2 * self.nb_station]
                    )
                    corr_matrix_phase = np.zeros(
                        [2 * self.nb_station, 2 * self.nb_station]
                    )
                    vis_index = self.vis_number.index(
                        coarse_channel * 144 + vis_channel
                    )
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            polarisation = 0
                            for polar_x in range(2):
                                for polar_y in range(2):
                                    vis = self.visibilities[vis_index][time]["VIS"][
                                        index
                                    ][polarisation]
                                    corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.abs(
                                        vis
                                    )
                                    corr_matrix_fine[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.abs(
                                        vis
                                    )
                                    corr_matrix_phase[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix_phase[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.angle(
                                        vis,
                                        deg=True,
                                    )
                                    polarisation = polarisation + 1
                            index = index + 1
                    corr_matrixes_fine[
                        coarse_channel * 144 + vis_channel
                    ] = corr_matrix_fine
                    corr_matrixes_angle[
                        coarse_channel * 144 + vis_channel
                    ] = corr_matrix_phase
                corr_matrixes_coarse[coarse_channel] = corr_matrix
            self.corr_abs_matrixes_time[time] = corr_matrixes_coarse
            self.corr_abs_matrixes_time_fine_channels[time] = corr_matrixes_fine
            self.corr_phase_matrixes_time[time] = corr_matrixes_angle

    def _average_correlation_matrices(
        self, time_channel_data: Dict[int, Dict[int, np.ndarray]]
    ) -> np.ndarray:
        """
        Average correlation data across all time samples.

        :param time_channel_data: {time: {channel: [data], ...}, ...} data structure,
         probably one populated by ``populate_correlation_matrix``
        """
        total = np.zeros([2 * self.nb_station, 2 * self.nb_station])
        n = 0
        for _, vis in time_channel_data.items():
            for matrix in vis.values():
                total += matrix
                n += 1
        return total / n

    def _stddev_corr_matrices(
        self, time_channel_data: Dict[int, Dict[int, np.ndarray]]
    ) -> np.ndarray:
        """
        Calculate standard deviation for phase across all time samples.

        :param time_channel_data: {time: {channel: [data], ...}, ...} data structure,
         probably one populated by ``populate_correlation_matrix``
        """
        col_sz = 2 * self.nb_station
        row_sz = 2 * self.nb_station
        z_axis_sz = len(time_channel_data)
        all_data = np.zeros([col_sz, row_sz, z_axis_sz])

        for z, vis in enumerate(time_channel_data.values()):
            for matrix in vis.values():
                all_data[:, :, z] = matrix

        return all_data.std(axis=2)

    @property
    def average_correlation(self) -> np.ndarray:
        """
        The average correlation across all time samples.
        """
        if self._average_correlation is None:
            if self.corr_abs_matrixes_time is None:
                # populate all correlation/time matrices
                self.populate_correlation_matrix()
            self._average_correlation = self._average_correlation_matrices(
                self.corr_abs_matrixes_time
            )
        return self._average_correlation

    @property
    def average_correlation_phase(self) -> np.ndarray:
        """
        The average correlation phase across all time samples.
        """
        if self._average_correlation_phase is None:
            if self.corr_phase_matrixes_time is None:
                # populate all correlation/time matrices
                self.populate_correlation_matrix()
            self._average_correlation_phase = self._average_correlation_matrices(
                self.corr_phase_matrixes_time
            )
        return self._average_correlation_phase

    @property
    def stddev_correlation_phase(self) -> np.ndarray:
        """
        The correlation phase standard deviation across all time samples.
        """
        if self._stddev_corr_phase:
            return self._stddev_corr_phase

        if self.corr_phase_matrixes_time is None:
            # populate all correlation/time matrices
            self.populate_correlation_matrix()
        self._stddev_corr_phase = self._stddev_corr_matrices(
            self.corr_phase_matrixes_time
        )
        return self._stddev_corr_phase

    def save_phase_heatmap_to_disk(
        self, mode="average_all", case="", directory=".", label_format=".0f"
    ):
        """Save the phase heatmap to disk."""
        os.makedirs(directory, exist_ok=True)
        if mode == "average_all":
            self.render_heatmap(
                self.average_correlation_phase,
                f"{case}phase_heat_map.png",
                "Phase Heatmap",
                directory,
                label_format=label_format,
            )
        else:
            raise NotImplementedError(f"Mode {mode} not implemented")

    def save_phase_stddev_to_disk(self, directory=".", case="", label_format=".0f"):
        """Save the phase standard deviation heatmap to disk."""
        os.makedirs(directory, exist_ok=True)
        self.render_heatmap(
            self.stddev_correlation_phase,
            f"{case}phase_stddev_heat_map.png",
            "Phase standard deviation",
            directory,
            label_format=label_format,
        )

    def save_heatmap_to_disk(
        self, mode, frequency_to_check=None, case="", directory="."
    ) -> None:
        """
        Save in PNG the heat maps of correlation.

        :param mode: 3 different mode so far:
            average_all: heat map of the sum of correlation across time
            for all coarse channels
            average_frequencies: produces 1 figure per integration time
            across all frequencies
            average_times: produces 1 figure per channel across all
            integration times
            average_times_fine: produces 1 figure per visibility channel
            for frequency_to_check coarse channel
        :param frequency_to_check: the coarse channel to further analyse
            when using the average_times_fine mode
        :param case: case study for the various figures
        :param directory: where to save the heatmap image
        """
        os.makedirs(directory, exist_ok=True)
        if mode == "average_all":
            self.render_heatmap(
                self.average_correlation,
                f"{case}heat_map.png",
                "Correlation Heatmap",
                directory,
                label_format=None,
            )
        elif mode == "average_frequencies":
            for time, vis in self.corr_abs_matrixes_time.items():
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for matrix in vis.values():
                    corr_matrix = corr_matrix + matrix
                self.render_heatmap(
                    corr_matrix,
                    f"{case}heat_map_time_{time}.png",
                    f"Correlation matrix at time {time}",
                    directory,
                    label_format=None,
                )
        elif mode == "average_times":
            for freq in self.coarse_channels:
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for time, vis in self.corr_abs_matrixes_time.items():
                    corr_matrix = corr_matrix + vis[freq]
                self.render_heatmap(
                    corr_matrix,
                    f"{case}heat_map_freq{freq}.png",
                    f"Correlation matrix for frequency {freq}",
                    directory,
                    label_format=None,
                )
        elif mode == "average_times_fine":
            for freq in range(frequency_to_check * 144, frequency_to_check * 144 + 144):
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for (
                    time,
                    vis,
                ) in self.corr_abs_matrixes_time_fine_channels.items():
                    corr_matrix = corr_matrix + vis[freq]
                self.render_heatmap(
                    corr_matrix,
                    f"{case}heat_map_fine_freq{freq-72}.png",
                    f"Correlation matrix for fine frequency {freq-72}",
                    directory,
                    label_format=None,
                )
        else:
            raise NotImplementedError(f"Mode {mode} not implemented")

    def render_heatmap(
        self,
        data: np.ndarray,
        filename: str = "heatmap.png",
        title: str = "Heatmap",
        directory: str = ".",
        label_format: Optional[str] = None,
    ) -> None:
        """
        Render a Heatmap to disk or screen, according to ``self.display_graph``.

        :param data: 2D data array, (number of stations * 2) in each dimension
        :param filename: Name of file to write (if ``display_graph`` is ``False``)
        :param directory: Directory to save in
        :param title: Heading for the graph
        :param label_format: Format specifier for data values on heatmap. Defaults to
          ``None``, which will supress labels.
        """
        figure, axes = plt.subplots()
        plt.imshow(data)
        # Label Axes
        axis_labels = []
        n = len(self.stations)
        if n <= 6:  # label X & Y polarisations for all stations
            for station in self.stations:
                axis_labels.extend([f"{station} X", f"{station} Y"])
        elif n <= 32:  # label stations, not polarisations
            for station in self.stations:
                axis_labels.extend([f"{station}", ""])
        else:  # too many, only label first and last
            axis_labels = [self.stations[0]] + [""] * (n * 2 - 2) + [self.stations[-1]]
        axes.set_xticks(np.arange(self.nb_station * 2), labels=axis_labels)
        axes.set_yticks(np.arange(self.nb_station * 2), labels=axis_labels)
        plt.setp(
            axes.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor"
        )

        # Annotate with values
        if label_format:
            for i in range(data.shape[0]):
                for j in range(data.shape[1]):
                    if data[j, i] == 0:
                        continue
                    axes.text(
                        i,
                        j,
                        f"{data[j,i]:{label_format}}",
                        ha="center",
                        va="center",
                        color="k",
                    )

        plt.colorbar()
        axes.set_title(title)
        figure.tight_layout()
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(os.path.join(directory, filename))
        plt.close()

    def angle_analysis(
        self,
        pair_of_station: (int, int),
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        starting_period: int = 0,
        filename: str = None,
    ) -> np.ndarray:
        """
        Generate the angle analysis figure.

        :param pair_of_station: the pair of stations to generate
            angle against
        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param starting_period: the staring period for analysis
        :param coarse_channels: the list of coarse channels to plot
        :param filename: If not provided, will be named according to `pair_of_station`,
            `polarisation`. If `nb_time_period` > 1, time period count will be appended.
            e.g. "myfile.png" -> "myfile_t0.png", "myfile_t1.png", ...
        :returns: line of best fit, from numpy.polyfit
        :raises: ValueError if `polarisation` or `pair_of_station` are invalid
        """
        # pylint: disable=too-many-branches
        if polarisation not in POLARISATIONS:
            raise ValueError("Polarisations should be on the form XX, XY, YX, YY")
        if (
            pair_of_station[0] not in self.stations
            or pair_of_station[1] not in self.stations
        ):
            raise ValueError("Stations unknown")
        index_to_find = 0
        index = 0
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                if self.stations.index(pair_of_station[0]) > self.stations.index(
                    pair_of_station[1]
                ):
                    if (
                        self.stations.index(pair_of_station[0]) == i
                        and self.stations.index(pair_of_station[1]) == j
                    ):
                        index_to_find = index_to_find + index
                else:
                    if (
                        self.stations.index(pair_of_station[1]) == i
                        and self.stations.index(pair_of_station[0]) == j
                    ):
                        index_to_find = index_to_find + index
                index = index + 1
        for time in range(starting_period, starting_period + nb_time_period):
            plt.clf()
            angle_to_fit = []
            channel_to_plot = []
            for coarse_channel in coarse_channels:
                for vis_channel in range(144):
                    vis_index = self.vis_number.index(
                        coarse_channel * 144 + vis_channel
                    )
                    angle_to_fit.append(
                        np.angle(
                            self.visibilities[vis_index][
                                int(list(self.time_sequence)[time])
                            ]["VIS"][index_to_find][POLARISATIONS.index(polarisation)],
                            deg=True,
                        )
                    )
                    channel_to_plot.append(
                        (coarse_channel * 144 + vis_channel - 72) / 144
                    )
            params = np.polyfit(channel_to_plot, angle_to_fit, 1)
            poly1d = np.poly1d(params)
            x_space = np.linspace(channel_to_plot[0], channel_to_plot[-1], 10000000)

            plt.plot(
                channel_to_plot,
                angle_to_fit,
                COLOURS[time % 10] + ".-",
                label=f"Integration {time}",
            )
            plt.plot(
                x_space,
                poly1d(x_space),
                COLOURS[(time + 1) % 10] + "-",
                label=f"Angle = {params[1]} + {params[0]}x",
            )
            plt.xlabel("station channel")
            plt.ylabel("phase in degrees")
            plt.grid()
            plt.title(f"{pair_of_station[0]}x{pair_of_station[1]}_{polarisation}")
            plt.legend(loc=2, prop={"size": 6})
            if self.display_graph:
                plt.show()
            else:
                filename = filename or (
                    f"{pair_of_station[0]}x{pair_of_station[1]}_"
                    + f"{polarisation}_angle.png"
                )
                if nb_time_period > 1:
                    # inject time count into filename
                    filename = f"_t{time}".join(os.path.splitext(filename))

                plt.savefig(filename)
        return params

    def amplitude_time_analysis(
        self,
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        sps_correction: bool,
    ):
        """
        Generate the amplitude vs time analysis figure.

        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot

        """

        correction_factor = [1] * 144
        if sps_correction:
            with open("fbscale_linear.txt", "r", encoding="utf8") as coefficient_file:
                correction_factor = coefficient_file.readlines()

        if polarisation not in POLARISATIONS:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return
        list_of_correlation_name = []
        list_of_correlation = []
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                list_of_correlation_name.append(f"{i + 1}x{j + 1}")
                list_of_correlation.append([])
        plt.clf()
        for time in range(nb_time_period):
            for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
                absolute_value = 0
                for coarse_channel in coarse_channels:
                    for vis_channel in range(144):
                        vis_index = self.vis_number.index(
                            coarse_channel * 144 + vis_channel
                        )
                        absolute_value = absolute_value + float(
                            correction_factor[vis_channel]
                        ) * np.abs(
                            self.visibilities[vis_index][
                                int(sorted(list(self.time_sequence))[time])
                            ]["VIS"][i][POLARISATIONS.index(polarisation)]
                        )

                list_of_correlation[i].append(
                    absolute_value / (len(coarse_channels) * 144)
                )
        for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
            plt.plot(
                range(nb_time_period),
                list_of_correlation[i],
                COLOURS[i % 10] + "-",
                label=list_of_correlation_name[i],
            )
        plt.xlabel("integration number")
        plt.ylabel("absolute value")
        plt.title(f"amplitude_vs_time_polarisation_{polarisation}")
        plt.legend(loc=2, prop={"size": 6})
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"amplitude_vs_time_polarisation_{polarisation}.svg",
                format="svg",
                dpi=300,
            )

    def amplitude_freq_analysis(
        self,
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        correction_sps: bool,
    ):
        """
        Generate the amplitude vs freq analysis figure.

        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot
        :param correction_sps: a boolean if we apply correction
        :return: the raw data of the figures

        """
        correction_factor = [1] * 144
        if correction_sps:
            with open("fbscale_linear.txt", "r", encoding="utf8") as coefficient_file:
                correction_factor = coefficient_file.readlines()

        print(correction_factor)
        if polarisation not in POLARISATIONS:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return correction_factor
        list_of_correlation_name = []
        list_of_correlation = []
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                list_of_correlation_name.append(f"{i + 1}x{j + 1}")
                list_of_correlation.append([])
        plt.clf()
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
                    vis_index = self.vis_number.index(
                        coarse_channel * 144 + vis_channel
                    )
                    absolute_value = 0
                    for time in range(nb_time_period):
                        absolute_value = absolute_value + float(
                            correction_factor[vis_channel]
                        ) * np.abs(
                            self.visibilities[vis_index][
                                int(sorted(list(self.time_sequence))[time])
                            ]["VIS"][i][POLARISATIONS.index(polarisation)]
                        )

                    list_of_correlation[i].append(absolute_value / nb_time_period)
        for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
            plt.plot(
                range(
                    coarse_channels[0] * 144 - 72,
                    coarse_channels[0] * 144 + len(coarse_channels) * 144 - 72,
                ),
                list_of_correlation[i],
                COLOURS[i % 10] + "-",
                label=list_of_correlation_name[i],
            )
        plt.xlabel("visibility channel number")
        plt.ylabel("absolute value")
        plt.title(f"amplitude_vs_freq_polarisation_{polarisation}")
        plt.legend(loc=2, prop={"size": 6})
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"amplitude_vs_freq_polarisation_{polarisation}.png",
                format="png",
                dpi=300,
            )
        return list_of_correlation

    def auto_station_analysis(
        self,
        station: int,
        nb_time_period: int,
        polarisation: str,
        coarse_channels,
    ):
        """
        Generate the 3D auto amplitude-freq-time analysis figure for a station.

        :param station: the station number.
        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot

        """

        if polarisation not in POLARISATIONS:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return

        real_index = self.get_index_in_low_triangle((station - 1, station - 1))
        amplitudes = np.zeros(
            (nb_time_period, len(coarse_channels) * 144), dtype=np.int32
        )

        time_scale = np.linspace(
            coarse_channels[0] * 144 - 72,
            coarse_channels[0] * 144 + 144 * len(coarse_channels) - 72,
            len(coarse_channels) * 144,
        )
        freq_scale = np.linspace(1, nb_time_period, nb_time_period)
        plt.clf()
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                vis_index = self.vis_number.index(coarse_channel * 144 + vis_channel)
                for time in range(nb_time_period):
                    amplitudes[time][
                        coarse_channels.index(coarse_channel) * 144 + vis_channel
                    ] = np.abs(
                        self.visibilities[vis_index][
                            int(sorted(list(self.time_sequence))[time])
                        ]["VIS"][real_index][POLARISATIONS.index(polarisation)]
                    )

        time_scale, freq_scale = np.meshgrid(time_scale, freq_scale)

        # Set up plot
        fig, axis = plt.subplots(subplot_kw={"projection": "3d"})

        surface = axis.plot_surface(
            time_scale,
            freq_scale,
            amplitudes,
            rstride=1,
            cstride=1,
            cmap=cm.get_cmap("Spectral"),
            linewidth=0,
            antialiased=False,
            shade=False,
        )
        plt.xlabel("visibility channels")
        plt.ylabel("integration period")
        plt.title(f"3D_mesh_station_{station}_{polarisation}")
        fig.colorbar(surface)
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"3D_mesh_station_{station}_{polarisation}.svg", format="svg", dpi=300
            )

    def rms_total(self):
        """
        Calculate RMS Total.

        For the entire dataset calculate its RMS - square, sum, average
        and take square root. ONE RMS number comes out.
        Effectively the average cross correlation power over all outputs
        :return: a single RMS value for each of auto/cross (auto, cross)

        """
        rms_total_values = [0, 0]
        complex_cross_total = np.complex256()
        number_of_samples = [0, 0]
        complex_auto_total = np.complex256()
        for time in self.time_sequence:
            for coarse_channel in self.coarse_channels:
                for vis_channel in range(144):
                    index = 0
                    vis_index = self.vis_number.index(
                        coarse_channel * 144 + vis_channel
                    )
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            if i != j:
                                for polarisation in range(4):
                                    rms_total_values[0] = (
                                        rms_total_values[0]
                                        + np.abs(
                                            self.visibilities[vis_index][time]["VIS"][
                                                index
                                            ][polarisation]
                                        )
                                        ** 2
                                    )
                                    complex_cross_total = (
                                        complex_cross_total
                                        + self.visibilities[vis_index][time]["VIS"][
                                            index
                                        ][polarisation]
                                    )
                                    number_of_samples[0] = number_of_samples[0] + 1
                            else:
                                polarisation = 0
                                for polar_x in range(2):
                                    for polar_y in range(2):
                                        if polar_x == polar_y:
                                            rms_total_values[1] = (
                                                rms_total_values[1]
                                                + np.abs(
                                                    self.visibilities[vis_index][time][
                                                        "VIS"
                                                    ][index][polarisation]
                                                )
                                                ** 2
                                            )
                                            complex_auto_total = (
                                                complex_auto_total
                                                + self.visibilities[vis_index][time][
                                                    "VIS"
                                                ][index][polarisation]
                                            )
                                            number_of_samples[1] = number_of_samples[1]
                                        polarisation = polarisation + 1
                            index = index + 1

        print(
            f"RMS_Tot Cross {rms_total_values[0]}, Nb_samples Cross {number_of_samples}"
        )
        rms_totals = (
            math.sqrt(rms_total_values[0] / number_of_samples[0]),
            math.sqrt(rms_total_values[1] / number_of_samples[1]),
        )
        print(f"RMS_Tot Auto {rms_total_values[1]}, Nb_samples {number_of_samples[1]}")

        print(f"Ratio Auto/Cross {rms_total_values[1] / rms_total_values[0]}")
        print(f"Complex Cross Total {complex_cross_total / number_of_samples[0]}")
        print(f"Complex Auto Total {complex_auto_total / number_of_samples[1]}")
        return (rms_totals[0], rms_totals[1])

    # flake8: noqa: C901
    def rms_slice_total_preparation(self, slice_size: int):
        """
        Calculater Slice of RMS.

        For the entire dataset calculate two giant vectors sum_1 and rms1.
        From these two vectors we can generate various figures including
        the main figure of PTC 14.

        :param slice_size: the number of cross correlation baseline to
            group together slice = nb_baseline/sum_1 vector (power of 2)
        :return: (rms, sum) the two vectors of RMS and SUM

        """
        number_of_samples = 0
        sum_1 = np.zeros(64 * 1024, dtype=np.complex256)
        rms1 = np.zeros(64 * 1024, dtype=np.int64)
        for time in self.time_sequence:
            for coarse_channel in self.coarse_channels:
                for vis_channel in range(144):
                    index = 0
                    vis_index = self.vis_number.index(
                        coarse_channel * 144 + vis_channel
                    )
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            if i != j:
                                for polarisation in range(4):
                                    rms1[math.floor(number_of_samples / slice_size)] = (
                                        rms1[math.floor(number_of_samples / slice_size)]
                                        + np.abs(
                                            self.visibilities[vis_index][time]["VIS"][
                                                index
                                            ][polarisation]
                                        )
                                        ** 2
                                    )
                                    sum_1[
                                        math.floor(number_of_samples / slice_size)
                                    ] = (
                                        sum_1[
                                            math.floor(number_of_samples / slice_size)
                                        ]
                                        + self.visibilities[vis_index][time]["VIS"][
                                            index
                                        ][polarisation]
                                    )
                                    # polarisation = polarisation + 1
                                    number_of_samples = number_of_samples + 1
                            index = index + 1
        return (rms1, sum_1)

    def phase_and_amplitude_closure(
        self, stations_closure, coarse_channels, polarisation, integration_period
    ):
        """
        Calculate the phase and amplitude closure.

        This is following documentation from
        https://www.atnf.csiro.au/computing/software/miriad/doc/closure.html
        in addition to corrections using formula 6 from
        https://iopscience.iop.org/article/10.3847/1538-4357/aab6a8/
        We then need a list of at least 4 stations to perform as follows:
        * Closure (triple) phase:    arg( V12*V23*conjg(V13) )
        * Closure (quad) amplitude:  abs( (V12*V34)/(V14*V23) )
        * Triple amplitude:          abs( V12*V23*conjg(V13) )**0.3333
        * Quad phase:                arg( (V12*V34)/(V14*V23) )
        The closure phase, quad phase and closure amplitude should be
        independent of antenna-based errors, and for a point source
        should have values of zero phase or unit amplitude.  The triple
        amplitude is independent of antenna-based phase errors (but not
        amplitude errors), and for a point source is a measure of the
        flux density.

        The task works by averaging the quantites that are the argument
        of the abs or arg function.  These are always averaged over the
        selected frequency channels and over the parallel-hand
        polarizations. Optionally the averaging can also be done over
        time and over the different closure paths.

        CLOSURE also prints (and optionally plots) the theoretical error
        (as a result of thermal noise) of the quantities.  Note this
        assumes a point source model, and that the signal-to-noise ratio
        is appreciable (at least 10) in each averaged product.

        :param stations: the list of station to goes through in circle
        :param coarse_channels: the list of coarse channels to plot
        :return: (Closure (triple) phase, Closure (quad) amplitude)

        """
        # pair_index = (index_12, index_23, index_13, index_34, index_14)
        pair_index = [
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[1])),
            self.get_index_in_low_triangle((stations_closure[1], stations_closure[2])),
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[2])),
            self.get_index_in_low_triangle((stations_closure[2], stations_closure[3])),
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[3])),
        ]
        # print(pair_index)
        closure_phase = []
        closure_amplitude = []
        # triple_amplitude = []
        # quad_phase = []
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                closure_phase.append(
                    self.calculate_angle_closure(
                        [pair_index[0], pair_index[1], pair_index[2]],
                        self.vis_number.index(coarse_channel * 144 + vis_channel),
                        polarisation,
                        integration_period,
                    )
                )
                closure_amplitude.append(
                    self.calculate_amplitude_closure(
                        [pair_index[0], pair_index[3], pair_index[4], pair_index[1]],
                        self.vis_number.index(coarse_channel * 144 + vis_channel),
                        polarisation,
                        integration_period,
                    )
                )
                # triple_amplitude.append()
                # quad_phase.append()

        # re_ordered_stations = sorted(stations_closure)
        # matched_stations = [
        #     self.stations.index(stn) for stn in re_ordered_stations
        # ]
        # indexes = {}
        return (closure_phase, closure_amplitude)

    def get_index_in_low_triangle(self, station_pair):
        """
        Return the index from the list of visibilities.

        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        (station_j, station_i) = tuple(sorted(station_pair))

        index = 0
        for i in range(0, self.nb_station):
            for j in range(0, i + 1):
                if i == station_i and j == station_j:
                    return index
                index = index + 1
        return index

    def calculate_angle_closure(
        self, indexes, vis_freq, polarisation, integration_period
    ):
        """
        Return the index from the list of visibilities.

        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        voltage_12 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[0]][POLARISATIONS.index(polarisation)]
        voltage_13 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[1]][POLARISATIONS.index(polarisation)]
        voltage_23 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[2]][POLARISATIONS.index(polarisation)]
        return (
            np.angle(
                voltage_12,
                deg=True,
            )
            + np.angle(
                voltage_23,
                deg=True,
            )
            - np.angle(
                voltage_13,
                deg=True,
            )
        )

    def calculate_amplitude_closure(
        self, indexes, vis_freq, polarisation, integration_period
    ):
        """
        Return the index from the list of visibilities.

        Closure (quad) amplitude:  abs( (V12*V34)/(V14*V23) )
        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        voltage_12 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[0]][POLARISATIONS.index(polarisation)]
        voltage_34 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[1]][POLARISATIONS.index(polarisation)]
        voltage_14 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[2]][POLARISATIONS.index(polarisation)]
        voltage_23 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[3]][POLARISATIONS.index(polarisation)]

        return np.abs((voltage_12 * voltage_34) / (voltage_14 * voltage_23))

    def remove_visibilities(self, nb_integrations=1):
        """Remove the visibilities at startup."""
        for index, time in enumerate(self.time_sequence):
            if index == nb_integrations:
                break
            for channel_index, _ in enumerate(self.coarse_channels):
                for vis_channel in range(144):
                    del self.visibilities[channel_index * 144 + vis_channel][time]

        del self.time_sequence[:nb_integrations]

    def save(self, filename: str):
        """
        Save visibilities to multiple numpy data files.

        :param filename: Base filename. Channel and time references will be inserted.
        e.g. "base.npy" -> "base_ch1_t1.npy". One file per channel per time.
        """
        for channel, chan_data in self.visibilities.items():
            for time, time_data in chan_data.items():
                mod_filename = f"_ch{channel}_t{time}".join(os.path.splitext(filename))
                np.save(mod_filename, time_data)
