# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Correlator FPGA personality helper functions."""

from math import ceil

from ska_low_cbf_integration.sps import SAMPLE_PERIOD

integration_periods = {849: 0.84934656, 283: 0.28311552}
"""Maps control system (approx.) integration period (ms) to exact value (s)."""


def samples_in_integration_period(period: int | float) -> int:
    """
    Get the number of SPEAD samples (note: distinct from packets) in an integration.

    :param period: The integration period. Integer milliseconds or float seconds.
    :return: Number of SPEAD samples in the integration period.
    """
    if isinstance(period, int):
        period = integration_periods[period]

    return round(period / SAMPLE_PERIOD)


MAX_PACKETISER_READ = 8192
FIRST_SPEAD_PACKET_HEADER = 114
OTHER_SPEAD_PACKET_HEADER = 106


def matrix_bytes(n_stations: int) -> int:
    """
    Calculate correlation matrix size.

    :param n_stations: Number of stations correlated
    :returns: Size in bytes
    """
    return n_stations * (n_stations + 1) * 17


def packets_per_correlation(n_stations: int) -> int:
    """
    Calculate how many SDP packets are sent per correlation.

    :param n_stations: Number of stations correlated
    :return: Packets per correlation
    """
    return ceil(matrix_bytes(n_stations) / MAX_PACKETISER_READ)


def expectation(nb_station, nb_channel, nb_integration) -> (set, int, float, float):
    """Compute expected packet sizes, number of packets, data rate, and duration."""
    matrix_size = matrix_bytes(nb_station)
    nb_packet_per_correlation = packets_per_correlation(nb_station)
    block_of_packets = 144 * nb_channel * nb_integration
    exp_nb_packet = block_of_packets * nb_packet_per_correlation
    packet_sizes = []
    if nb_packet_per_correlation == 1:
        packet_sizes.append(FIRST_SPEAD_PACKET_HEADER + matrix_size)
    elif nb_packet_per_correlation == 2:
        packet_sizes.append(FIRST_SPEAD_PACKET_HEADER + MAX_PACKETISER_READ)
        packet_sizes.append(
            OTHER_SPEAD_PACKET_HEADER + matrix_size % MAX_PACKETISER_READ
        )
    elif nb_packet_per_correlation > 2:
        # first packet
        packet_sizes.append(FIRST_SPEAD_PACKET_HEADER + MAX_PACKETISER_READ)
        other_packets_size = OTHER_SPEAD_PACKET_HEADER + MAX_PACKETISER_READ
        packet_sizes += [other_packets_size] * (nb_packet_per_correlation - 2)
        # last packet
        packet_sizes.append(
            OTHER_SPEAD_PACKET_HEADER + matrix_size % MAX_PACKETISER_READ
        )

    expected_time = (nb_integration - 1) * integration_periods[849] + (
        sum(packet_sizes) * 8 + len(packet_sizes) * 24 * 8  # 24 = preamble + cvs + IPG
    ) / 100_000_000_000
    exp_rate = block_of_packets * sum(packet_sizes) * 8 / expected_time
    return set(packet_sizes), exp_nb_packet, exp_rate, expected_time
