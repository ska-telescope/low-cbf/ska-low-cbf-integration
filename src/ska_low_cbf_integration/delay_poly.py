# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low CBF Delay Polynomial Tango device helper functions."""
import json
from time import sleep

import tango

from ska_low_cbf_integration.tango import DeviceProxyJson


def config_beam_delay(delay: tango.DeviceProxy, config: dict):
    """
    Configure a 'Beam' delay polynomial.

    :param delay: Low CBF Delay Polynomial source device proxy
    :param config: Configuration to apply
    """
    command = getattr(delay, "Beam" + _detect_command_type_suffix(config), None)
    command(json.dumps(config))


def config_source_delay(delay: tango.DeviceProxy, config: dict):
    """
    Configure a 'Source' delay polynomial.

    :param delay: Low CBF Delay Polynomial source device proxy
    :param config: Configuration to apply
    """
    command = getattr(delay, "Source" + _detect_command_type_suffix(config), None)
    command(json.dumps(config))


def config_pst_delay(delay: tango.DeviceProxy, config: dict):
    """
    Configure a 'PST' delay polynomial.

    :param delay: Low CBF Delay Polynomial source device proxy
    :param config: Configuration to apply
    """
    command = getattr(delay, "pst" + _detect_command_type_suffix(config), None)
    command(json.dumps(config))


def wait_for_delay_poly(
    delay: DeviceProxyJson,
    test_config: dict,
) -> None:
    """
    Wait for the delay polynomial to bootstrap.

    :param delay: The delay polynomial tango device
    :param test_config: Low CBF Subarray ConfigureScan message contents
    """
    sv0 = json.loads(
        getattr(
            delay,
            test_config["lowcbf"]["stations"]["stn_beams"][0]["delay_poly"].split("/")[
                -1
            ],
        )
    )["start_validity_sec"]
    while True:
        svsec = json.loads(
            getattr(
                delay,
                test_config["lowcbf"]["stations"]["stn_beams"][0]["delay_poly"].split(
                    "/"
                )[-1],
            )
        )["start_validity_sec"]
        if svsec not in (0, sv0):
            break
        print(f"  - Waiting until first valid katpoint ... start_validity={svsec}")
        sleep(1)


def _detect_command_type_suffix(config: dict) -> str:
    """
    Infer the Tango command suffix from the configuration structure.

    :returns: one of 'AzEl', 'Delay', or 'RaDec'
    :raises RuntimeError: if we can't figure it out
    """
    if "direction" in config:
        direction = config["direction"]
        if isinstance(direction, list):
            direction = direction[0]
        if "az" in direction:
            return "AzEl"
        if "ra" in direction:
            return "RaDec"
    elif "delay" in config:
        return "Delay"

    raise RuntimeError(f"Can't figure out what delay poly command for {config}")
