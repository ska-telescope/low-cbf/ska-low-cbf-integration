# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2025 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""Things to work with generic SPEAD files."""

import socket
from dataclasses import dataclass
from enum import Enum
from time import perf_counter
from typing import BinaryIO, Generator

import spead2
import spead2.recv
import spead2.send
from dpkt.ethernet import Ethernet
from dpkt.pcap import UniversalReader


class PacketType(Enum):
    """SPEAD packet types."""

    INIT = "init"
    DATA = "data"
    END = "end"


@dataclass
class PacketInfo:
    """SPEAD packet - minimal details for plotting packet flows."""

    time: float
    "Timestamp from PCAP file."
    dst: str
    """Destination IP address."""
    type: PacketType
    """SPEAD packet type."""


def packet_infos(pcap: BinaryIO) -> Generator[PacketInfo, None, None]:
    """
    Generate PacketInfo objects from a PCAP file.

    BEWARE: Multi-packet heaps will make this function freeze!

    :param pcap: File object to read from
    """
    # We want to be told about end packets
    stream_config = spead2.recv.StreamConfig(stop_on_stop_item=False)
    stream = spead2.recv.Stream(spead2.ThreadPool(), config=stream_config)

    queue = spead2.InprocQueue()
    stream.add_inproc_reader(queue)

    t_0 = perf_counter()
    reader = UniversalReader(pcap)
    for n, (timestamp, packet) in enumerate(reader):
        if n % 100_000 == 0 and n > 0:
            print(f"packet_infos - {n} packets t={perf_counter()-t_0:.3f} s")
        ip_packet = Ethernet(packet).data
        dst_ip_addr_str = socket.inet_ntoa(ip_packet.dst)
        udp_payload = ip_packet.data.data
        queue.add_packet(udp_payload)
        heap = stream.get()  # will stall here if packet does not contain full heap

        # TODO - we need to do something to handle multi-packet heaps!
        # using get_nowait sounds good, but doesn't work well
        # - runs very slowly and makes mistakes
        # for i in range(5):
        #     try:
        #         heap = stream.get_nowait()
        #         break
        #     except spead2.Empty:
        #         time.sleep(0.001 * i)
        # else:
        #     # queue still empty after retries ran out
        #     print(f"Packet {n} - incomplete heap? No? Increase sleep!")

        packet_type = PacketType.DATA
        if heap.is_start_of_stream():
            packet_type = PacketType.INIT
        elif heap.is_end_of_stream():
            packet_type = PacketType.END

        yield PacketInfo(time=timestamp, type=packet_type, dst=dst_ip_addr_str)
