# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low CBF PST utility functions."""

from typing import Iterable

import numpy as np

from ska_low_cbf_integration.psr_analyser import PulsarPacket


# TODO this is used in multiple places AND is very similar to its PST equivalent
#      consider consolidating it
def freq_power_per_beam(
    payloads: Iterable[bytes], pss_beam_ids: Iterable[int]
) -> (list, list):
    """
    Get frequencies and power for the given beams.

    :returns: two lists, each with one entry per beam. first list contains lists
        of frequency channels, second list contains lists of average power per sample
        in each of those frequency channels
    """
    # pylint: disable=too-many-locals
    total_freqs = {}
    total_pwr = {}
    for beam_number in pss_beam_ids:
        # accumulate per-pss-channel power with first-chan as key
        np_chan_pwr = {}  # numpy power data by sequence num and first chan
        chan_pkts = {}
        # get sample by sample power from packet payloads
        for pkt_payload in payloads:
            psr_packet = PulsarPacket(pkt_payload)
            if psr_packet.beam_id != beam_number:
                continue
            first_chan = psr_packet.first_channel
            if first_chan not in np_chan_pwr:
                size = psr_packet.channel_power().size
                np_chan_pwr[first_chan] = np.zeros(size)
                chan_pkts[first_chan] = 0
            np_chan_pwr[first_chan] += psr_packet.channel_power()
            chan_pkts[first_chan] += 1

        # get average power-per-sample numbers by dividing by number added
        for first_chan_key in np_chan_pwr:
            np_chan_pwr[first_chan_key] = (
                np_chan_pwr[first_chan_key] / chan_pkts[first_chan_key]
            )

        freqs = []
        pwr = []
        ordered_chans = sorted(np_chan_pwr.keys())
        for channel in ordered_chans:
            # there are 54 consecutive pss freqs in each packet/block
            frequencies = [channel + i for i in range(0, 54)]
            freqs.extend(frequencies)
            pwr.extend(np_chan_pwr[channel].tolist())
        total_freqs[beam_number] = freqs
        total_pwr[beam_number] = pwr

    return total_freqs, total_pwr
