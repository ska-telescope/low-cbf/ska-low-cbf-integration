# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Integration Testing project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""Modify a Helm chart to use different versions of dependencies."""
import argparse
import sys
from typing import List

import yaml


def key_colon_value(raw: str) -> list:
    """
    Split a key:value string into its key and value.

    :param raw: key:value - value may contain colons
    :return: [key, value]
    """
    n_colons = raw.count(":")
    if n_colons >= 1:
        return raw.split(":", maxsplit=1)
    raise argparse.ArgumentTypeError(
        f"Expected one colon in key:value string. Found {n_colons} in '{raw}'"
    )


def find_dep_in_list(dependencies: List[dict], name: str) -> dict:
    """
    Given a list of dependency specs, find one by its name.

    :param dependencies: The 'dependencies' portion of Chart.yaml (parsed)
    :param name: search key
    :return: the dependency spec with the given name, if it exists
    :raises ValueError: if name not found
    """
    for dependency in dependencies:
        if dependency["name"] == name:
            return dependency
    raise ValueError(f"Name '{name}' not found")


def main():
    """Perform the requested dependency modifications."""
    parser = argparse.ArgumentParser(description="Helm chart dependency modifier")
    parser.add_argument(
        "input",
        help="Source Helm chart YAML file.",
        type=argparse.FileType(mode="r"),
    )
    parser.add_argument(
        "-i",
        "--in-place",
        help="Modify file in place. WARNING: Comments will be erased!",
        action="store_true",
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Output file name, defaults to printing to stdout.",
        type=argparse.FileType(mode="w"),
        default=sys.stdout,
    )
    parser.add_argument("dependency", help="Name of dependency to modify.", type=str)
    parser.add_argument(
        "modifications",
        help="Things to modify. Specify as 'key:value'.",
        nargs="+",
        type=key_colon_value,
    )

    args = parser.parse_args()
    chart = yaml.safe_load(args.input)
    dependencies = chart["dependencies"]

    if args.in_place:
        print("Modifying in place.", file=sys.stderr)
    print(f"Modifying dependency {args.dependency}", file=sys.stderr)
    dependency = find_dep_in_list(dependencies, args.dependency)
    for key, value in args.modifications:
        print(f"\t{key} {value}", file=sys.stderr)
        dependency[key] = value

    if args.in_place:
        with open(args.input.name, "w", encoding="utf-8") as output:
            yaml.dump(chart, output)
    else:
        yaml.dump(chart, args.output)


if __name__ == "__main__":
    main()
