# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Processor device helpers."""
import json
from typing import List

from tango import Database

from ska_low_cbf_integration.allocator import allocator_proxy
from ska_low_cbf_integration.tango import DeviceProxyJson


def get_json_processor_proxies() -> List[DeviceProxyJson]:
    """
    Get proxies to all Processor devices, with JSON codec configured.

    :returns: List in Allocator order.
    """
    # checking if this is the cause of occasional CNIC failures to finish receiving
    assert len(allocator_proxy().stats_alveo) == len(
        Database().get_device_exported_for_class("LowCbfProcessor")
    ), "Not all Processors registered with Allocator."

    return [
        DeviceProxyJson(
            alveo["tango_dev"],
            json_commands=["DebugRegRead", "DebugRegWrite"],
            json_attributes=["stats_delay", "stats_io", "stats_mode"],
        )
        for alveo in allocator_proxy().stats_alveo
    ]


def debug_reg_read(proxy: DeviceProxyJson, peripheral, register, offset=0, length=1):
    """
    Access the DebugRegRead Command from the processor.

    :returns: single value if length=1, list of values if length>1
    """
    values = proxy.DebugRegRead(
        {"name": f"{peripheral}.{register}", "offset": offset, "length": length}
    )
    if isinstance(values, str):
        values = json.loads(values)  # proxy only translates command input at present
    if length == 1:
        return values[0]
    return values
