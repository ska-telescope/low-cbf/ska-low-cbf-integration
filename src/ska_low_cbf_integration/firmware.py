# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low CBF Firmware Utilities."""
import os
from dataclasses import dataclass, field
from enum import Enum
from typing import Dict, Optional
from urllib.parse import quote

import requests

HTTP_TIMEOUT = 30


class Personality(Enum):
    """Low CBF FPGA Firmware Personalities."""

    CNIC = -1
    CORR = 0
    PST = 1
    PSS = 2


scan_config_keys = {
    Personality.CORR: "vis",
    Personality.PSS: "search_beams",
    Personality.PST: "timing_beams",
}
"""
Keys used inside Subarray Configure message schema for the firmware-specific portion.

N.B. These will occur under top level "lowcbf" key.
"""

gitlab_projects = {
    Personality.CNIC: "ska-telescope/low-cbf/ska-low-cbf-fw-cnic",
    Personality.CORR: "ska-telescope/low-cbf/ska-low-cbf-fw-corr",
    Personality.PSS: "ska-telescope/low-cbf/ska-low-cbf-fw-pss",
    Personality.PST: "ska-telescope/low-cbf/ska-low-cbf-fw-pst",
}
"""GitLab project path for each Low CBF Personality."""


class BuildType(Enum):
    """Types of FPGA firmware build."""

    RELEASE = 0
    MAIN = 1
    DEV = 2


@dataclass
class FirmwareReference:
    """Firmware download URL & details about what's inside it."""

    personality: str
    version: str
    platform: str  # includes FPGA type
    url: str
    fpga_type: str = field(init=False)

    @classmethod
    def from_filename_url(cls, filename: str, url: str) -> "FirmwareReference":
        """Create from filename & URL strings."""
        chunks = _remove_tarfile_extension(filename).split("_")
        personality = chunks[0]
        version = _remove_build_metadata(chunks[-1])
        platform = "_".join(chunks[1:-1])
        return cls(personality, version, platform, url)

    def __post_init__(self):
        """Populate derived fields."""
        self.fpga_type = self.platform.split("_")[0]


def _remove_tarfile_extension(filename: str) -> str:
    """Remove tar file extension(s)."""
    # handle long .tar.<compression type> style
    if ".tar" in filename:
        return filename.split(".tar")[0]
    # handle short .t<compression name> style
    if ".t" in filename:
        return filename.split(".t")[0]
    raise ValueError("No .tar or .t extension in filename.")


def _remove_build_metadata(version: str) -> str:
    """Remove build metadata from filename."""
    version = version.split("+")[0]
    # As of 2024-10-01, many Low CBF firmware names use a minus sign to denote build
    # metadata, because for one day (~2024-05-28) GitLab had a bug handling + signs.
    version = version.split("-vitis")[0]
    return version


class NotFoundError(Exception):
    """Exception class for firmware not found."""


def find_latest_gitlab(
    project: str,
    search_mode: BuildType = BuildType.RELEASE,
    require: Optional[Dict[str, str]] = None,
) -> FirmwareReference:
    """
    Find latest firmware package.

    :param project: project path, e.g. "ska-telescope/low-cbf/ska-low-cbf-fw-corr"
    :param search_mode: type of build to look for, see :py:class:`BuildType`
    :param require: requirements, use :py:class:`FirmwareReference` field names as keys.
      e.g. `{"fpga_type": "u55c"}`.
    """
    package_url = (
        os.environ.get("CI_API_V4_URL", "https://gitlab.com/api/v4")
        + "/projects/"
        + quote(project, safe="")
        + "/packages"
    )
    package_ids_checked = []

    for package_id in _gitlab_package_ids(package_url, search_mode):
        package_ids_checked.append(package_id)
        # try to find the right file in the package
        response = requests.get(
            package_url + f"/{package_id}/package_files", timeout=HTTP_TIMEOUT
        )
        for file in response.json():
            url = f"https://gitlab.com/{project}/-/package_files/{file['id']}/download"
            ref = FirmwareReference.from_filename_url(file["file_name"], url)
            if require:
                # check if this file meets all requirements
                if not all((require[key] == getattr(ref, key) for key in require)):
                    continue

            return ref

    raise NotFoundError(
        f"Didn't find {require} in any packages from {package_url} ."
        f"Checked package ids: {package_ids_checked}"
    )


def _gitlab_package_ids(
    package_url: str, search_mode: BuildType, max_requests: int = 100
):
    """Get GitLab package IDs from a 'packages' API URL."""
    query = {"order_by": "created_at", "sort": "desc"}
    response = requests.get(package_url, params=query, timeout=HTTP_TIMEOUT)
    for _ in range(max_requests):
        assert response.status_code == 200, f"Oh no! HTTP status {response.status_code}"
        for package in response.json():
            version = package["version"]
            if search_mode == BuildType.RELEASE:
                # should be 3 numbers separated by dots
                if all(str.isnumeric(n) for n in version.split(".")):
                    yield package["id"]
            elif search_mode == BuildType.MAIN:
                if "main" in version:
                    yield package["id"]
            elif search_mode == BuildType.DEV:
                if "dev" in version:
                    yield package["id"]
        # look at next page of results
        if "next" not in response.links:
            raise NotFoundError(
                "Searched all API result pages, "
                f"didn't find any {search_mode.name} package."
            )
        response = requests.get(response.links["next"]["url"], timeout=HTTP_TIMEOUT)

    raise NotFoundError(
        f"Hit max_requests limit ({max_requests}), "
        f"didn't find any {search_mode.name} package."
    )


def version_under_test(
    fw_type: Personality,
    fpga_type: str = "u55c",
) -> str:
    """
    Get firmware version that should be tested.

    Defaults to latest main branch build. Specific versions can be requested via
    environment variables.

    :return: e.g. "1.2.3-main.12345678"
    """
    fw_ver_env_var = {
        Personality.CNIC: "CNIC_FW_VERSION",
        Personality.CORR: "CORR_FW_VERSION",
        Personality.PSS: "PSS_FW_VERSION",
        Personality.PST: "PST_FW_VERSION",
    }
    # ignores empty string or unset env var
    if version := os.getenv(fw_ver_env_var[fw_type]):
        # specifically requested firmware version
        return version

    # default to latest main branch build
    return find_latest_gitlab(
        project=gitlab_projects[fw_type],
        search_mode=BuildType.MAIN,
        require={"fpga_type": fpga_type},
    ).version
