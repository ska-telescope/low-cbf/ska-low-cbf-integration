# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""CNIC (Customisable Network Interface Card) FPGA Firmware helper functions."""
import time

import tango

from ska_low_cbf_integration.firmware import Personality, version_under_test
from ska_low_cbf_integration.tango import DeviceProxyJson

VD_FINE_FREQ_HZ = (1 / 1080e-9) * (1 / 32768)
"""One unit of fine_frequency, ~28.257 Hz"""
VD_NOISE_SCALE = 65536 / 209.03
"""For CNIC-VD noise source, this scale gives noise with std dev 1."""

fpga_type_config = {
    "u280": {"memory": "2Gs:2Gs:2Gs:2Gs", "platform": 1},
    "u55c": {"memory": "4095Ms:4095Ms:4095Ms:4095Ms", "platform": 3},
}
"""map of FPGA type to CNIC SelectPersonality options"""


def _cnic_fw_loaded(cnic: DeviceProxyJson, version: str) -> bool:
    """
    Check if CNIC firmware is already loaded.

    :param cnic: CNIC device proxy
    :param version: Desired version, e.g. "1.2.3"
    :returns: True if already loaded, False otherwise.
    """
    if cnic.activePersonality is None:
        return False

    if (
        getattr(cnic, "fw_personality", None) == "CNIC"
        and getattr(cnic, "fw_version", None) == version
    ):
        return True

    return False


def load_firmware(cnic: DeviceProxyJson, cnic_fw_version=None) -> None:
    """
    Load CNIC firmware, using version from environment variables.

    Note: if the requested version of firmware is already loaded, it will not be
    reloaded and the FPGA will continue doing whatever it is doing.

    :param cnic: CNIC device proxy
    """
    fpga_type = None
    cnic.set_timeout_millis(240_000)
    if hasattr(cnic, "platform"):
        # reading platform takes a strangely long time on u280 FPGAs
        platform = cnic.platform  # e.g. 'xilinx_u280_gen3x16_xdma_base_1'
        for fpga in fpga_type_config:
            if fpga in platform:
                # we could extract the platform version here,
                # but we don't need to today...
                fpga_type = fpga
                break
        if fpga_type is None:
            raise NotImplementedError(f"Unrecognised FPGA type in {platform}")
    else:
        fpga_type = "u55c"

    if cnic_fw_version is None:
        cnic_fw_version = version_under_test(Personality.CNIC, fpga_type=fpga_type)
    if not _cnic_fw_loaded(cnic, cnic_fw_version):
        personality_options = {
            "version": cnic_fw_version,
            "source": "gitlab",
            "fpga_type": fpga_type,
        }
        personality_options.update(fpga_type_config[fpga_type])
        cnic.SelectPersonality(personality_options)
        print(f"{cnic.name()} Download complete! (version {cnic_fw_version})")
    else:
        print(f"{cnic.name()} already running version {cnic_fw_version}")


def load_firmware_and_reset(cnic: DeviceProxyJson, cnic_fw_version=None) -> None:
    """
    Load CNIC firmware and reset card to initial state.

    :param cnic: CNIC device proxy
    """
    if cnic_fw_version is not None:
        load_firmware(cnic, cnic_fw_version)
    else:
        load_firmware(cnic)
    cnic.CallMethod(method="reset")


def wait_until_fpga_ready(cnic: tango.DeviceProxy | DeviceProxyJson):
    """
    Wait until CNIC FPGA is ready to use.

    :param cnic: CNIC device proxy
    """
    print("Checking FPGA attributes exist (i.e. polling is active)\n")
    max_attempts = 10
    for i in range(max_attempts + 1):
        if "system__time_uptime" in cnic.get_attribute_list():
            break
        if i == max_attempts:
            raise RuntimeError('"system__time_uptime" not found in attribute list')
        time.sleep(2)
    time.sleep(0.5)
    print(
        f"Confirming FPGA operation - {cnic.name()} FPGA Uptime: "
        f"{cnic.system__time_uptime}s"
    )
