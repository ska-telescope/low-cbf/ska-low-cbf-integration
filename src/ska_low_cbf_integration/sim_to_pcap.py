# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Convert FPGA Simulation format text files to PCAP."""
import argparse
import os
import typing

import dpkt
from parse import parse

from ska_low_cbf_integration.pcap_to_sim import BYTES_PER_LINE

LINE_FORMAT = "{zeros:4d} {valid:1d} {last:1d}  {valid_bytes:16x}  {data:128x}"


def mask_data(data: bytes, mask: int) -> bytes:
    """
    Apply a one bit per byte mask to data bytes.

    :param data: 64 Bytes of data
    :param mask: 64 bits of mask
    :return: valid data bytes, concatenated
    """
    # short-circuit the easy case
    if mask == 0xFFFF_FFFF_FFFF_FFFF:
        return data

    result = bytes()
    for i in range(BYTES_PER_LINE):
        if (1 << (64 - i - 1)) & mask:
            # we slice i:i+1 to get bytes rather than int
            result += data[i : i + 1]
    return result


def get_writer(
    file: typing.BinaryIO,
    packet_size: int = 9000,
) -> typing.Union[dpkt.pcap.Writer, dpkt.pcapng.Writer]:
    """
    Create a writer for a PCAP(NG) file.

    :param file: file object to write to
    :param packet_size: packet size (Bytes)
    """
    if os.path.splitext(file.name)[1] == ".pcapng":
        writer_class = dpkt.pcapng.Writer
    else:
        writer_class = dpkt.pcap.Writer

    return writer_class(file, snaplen=packet_size, linktype=dpkt.pcap.DLT_LINUX_SLL)


def main():
    """FPGA simulation to PCAP conversion utility main function (CLI)."""
    parser = argparse.ArgumentParser(
        description="Perentie FPGA Simulation Text File to PCAP Converter."
    )
    parser.add_argument(
        "input",
        type=argparse.FileType("r"),
        help="Input text file.",
    )
    parser.add_argument(
        "output",
        type=argparse.FileType("wb"),
        help="Output pcap(ng) file.",
    )
    args = parser.parse_args()

    writer = get_writer(args.output)
    n_packets = 0
    packet = bytes()
    for raw_line in args.input.readlines():
        # Concatenate data fields (per validity mask) until we hit the "last" flag
        line = parse(LINE_FORMAT, raw_line.strip())
        assert line["zeros"] == 0
        if not line["valid"]:
            continue
        raw_data = line["data"].to_bytes(64, "big")
        data = mask_data(raw_data, line["valid_bytes"])
        packet += data
        if line["last"]:
            # Collected a whole packet, write it to disk
            writer.writepkt(packet)
            n_packets += 1
            packet = bytes()

    print(f"Converted {n_packets} packets.")


if __name__ == "__main__":
    main()
