# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Low PSI (Prototype System Integration) definitions."""
from typing import Iterable, List

import tango

from ska_low_cbf_integration.tango import DeviceProxyJson

P4_SWITCH_ADDRESS = "202.9.15.139:50052"

serial_port = {
    # psi-perentie1 - u55c
    "XFL14SLO1LIF": "19/0",
    "XFL1DKXBEVG2": "21/0",
    "XFL1HOOQ1Y44": "23/0",
    "XFL1LHN4TXO2": "25/0",
    "XFL1AO0DDB0M": "27/0",
    # - u280
    "217702027008": "6/0",
    "217702027005": "8/0",
    "217702027009": "10/0",
    "217702027002": "12/0",
    "21770202700Y": "14/0",
    # psi-perentie2 - u55c
    "XFL1TJCHM3ON": "7/0",
    "XFL1VCYSXCL0": "9/0",
    "XFL10NIYKVEU": "11/0",
    "XFL1XCRTUC22": "13/0",
    "XFL1E35JVJTQ": "15/0",
    "XFL1RCFEG244": "17/0",
    # - u280
    "21770202700T": "18/0",
    "21770202700D": "20/0",
    "217702027010": "22/0",
    "21770202700N": "24/0",
}
"""look-up table mapping serial numbers to P4 port numbers"""

serial_spead_hwid = {
    # psi-perentie1
    "XFL14SLO1LIF": 0x35_0B_1A_80,
    "XFL1DKXBEVG2": 0x35_0B_1A_B0,
    "XFL1HOOQ1Y44": 0x35_0B_19_C0,
    "XFL1LHN4TXO2": 0x35_0B_1A_88,
    "XFL1AO0DDB0M": 0x35_08_8E_35,
    # psi-perentie2
    "XFL1TJCHM3ON": 0x35_08_8D_45,
    "XFL1VCYSXCL0": 0x35_0B_18_E0,
    "XFL10NIYKVEU": 0x35_0B_19_88,
    "XFL1XCRTUC22": 0x35_0B_19_10,
    "XFL1E35JVJTQ": 0x35_0B_1A_08,
    "XFL1RCFEG244": 0x35_0B_19_D0,
}
"""look-up table mapping serial numbers to SPEAD hardware ID ('Hardw' field)"""


def get_connector_proxy() -> DeviceProxyJson:
    """Get a Tango device proxy to the Low PSI shared Connector device."""
    db_port_number = 10000
    db_service = "tango-databaseds"
    db_namespace = "ska-low-cbf-conn"
    db_host = f"{db_service}.{db_namespace}:{db_port_number}"
    connector = DeviceProxyJson(
        tango.DeviceProxy(f"{db_host}/low-cbf/connector/0"),
        json_attributes=["portStatus"],
    )
    connector.ConnectToSwitch({"Switch": P4_SWITCH_ADDRESS})
    return connector


def device_ports(devices: Iterable[tango.DeviceProxy]) -> List[str]:
    """
    Get P4 ports associated with Low CBF FPGA devices.

    :param devices:
    :return: list of ports, e.g. ["29/0"]
    """
    return [serial_port[device.serialNumber] for device in devices]
