# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test JSON Wrapper."""
import json
from dataclasses import dataclass

import pytest
from tango import DevString

from ska_low_cbf_integration.tango import DeviceProxyJson


@dataclass
class FakeInfo:
    """Fake info class for DummyProxy's Commands & Attributes."""

    in_type_desc: str  # Command
    description: str  # Attr
    in_type: int  # Command
    data_type: int  # Attr


class DummyProxy:
    """A minimal Tango DeviceProxy-ish object using JSON."""

    def __init__(self):
        """Create DummyProxy."""
        self._commands = {
            "String": "This is a normal string Command",
            "Json1": "This Command takes JSON in, should be auto-detected",
            "Json2": "This takes Jay eSs Oh eN in, should NOT be auto-detected!",
        }
        self._attrs = {
            "string_attr": "This is a normal string Attribute",
            "json_1": "This Attribute returns JSON, should be auto-detected",
            "json_2": "This returns Jay eSs Oh eN, should NOT be auto-detected!",
        }
        self._string = "Hello, World!"

    def get_command_list(self):
        """Get Command list."""
        return list(self._commands.keys())

    def get_command_config(self, cmd):
        """Get (fake) Command configuration."""
        desc = self._commands[cmd]
        return FakeInfo(
            in_type=DevString, data_type=DevString, in_type_desc=desc, description=desc
        )

    def get_attribute_list(self):
        """Get Attribute List."""
        return list(self._attrs.keys())

    def get_attribute_config(self, attr):
        """Get (fake) Attribute configuration."""
        desc = self._attrs[attr]
        return FakeInfo(
            in_type=DevString, data_type=DevString, in_type_desc=desc, description=desc
        )

    @property
    def json_1(self):
        """JSON Attribute 1 (Auto-Detected)."""
        return json.dumps({"hello": "world"})

    @property
    def json_2(self):
        """JSON Attribute 2 (NOT Auto-Detected)."""
        return json.dumps({"hello": "world"})

    @property
    def string_attr(self):
        """String Attribute (returns UPPERCASE of written value)."""
        return self._string.upper()

    @string_attr.setter
    def string_attr(self, string):
        """Set String Attribute."""
        self._string = string

    def String(self, string: str):  # pylint: disable=invalid-name
        """Do String Command."""
        assert isinstance(string, str)

    def Json1(self, string: str):  # pylint: disable=invalid-name
        """Do JSON Command 1 (Auto-Detected)."""
        return json.loads(string)

    def Json2(self, string: str):  # pylint: disable=invalid-name
        """Do JSON Command 2 (NOT Auto-Detected)."""
        json.loads(string)


non_json_obj = ["One", "Two", {"Three": 3}]
json_string = json.dumps(non_json_obj)


class TestJsonWrapper:
    """Tests for DeviceProxyJson."""

    def test_init(self):
        """Check that creating our object works."""
        DeviceProxyJson(DummyProxy())

    def test_attr_detection(self):
        """Check JSON-using Attribute detection."""
        j = DeviceProxyJson(DummyProxy())
        assert isinstance(j.json_1, dict)
        assert isinstance(j.json_2, str)  # shouldn't be auto-detected
        assert isinstance(j.string_attr, str)

    def test_attr_specification(self):
        """Check ability to specify which Attributes use JSON."""
        j = DeviceProxyJson(
            DummyProxy(), json_attributes=["json_2"], not_json_attributes="json_1"
        )
        assert isinstance(j.json_1, str)  # specifically excluded from auto-detect
        assert isinstance(j.json_2, dict)  # specified for conversion
        assert isinstance(j.string_attr, str)

    def test_cmd_detection(self):
        """Check JSON-using Command detection."""
        j = DeviceProxyJson(DummyProxy())
        j.Json1(non_json_obj)
        with pytest.raises(TypeError):
            j.Json2(non_json_obj)  # shouldn't be auto-detected
        j.String(json_string)

    def test_cmd_specification(self):
        """Check ability to specify which Commands use JSON."""
        j = DeviceProxyJson(
            DummyProxy(), json_commands=["Json2"], not_json_commands="Json1"
        )
        with pytest.raises(TypeError):
            j.Json1(non_json_obj)  # specifically excluded from auto-detect
        j.Json2(non_json_obj)  # specified for conversion
        j.String(json_string)

    def test_set_attr(self):
        """Ensure our JSON Wrapper lets writes go through to attributes."""
        test_str = "my test string"
        j = DeviceProxyJson(DummyProxy())
        j.string_attr = test_str
        assert j.string_attr == test_str.upper()  # would fail if wrapper broke setattr

    def test_str_passthrough(self):
        """Ensure already-JSON strings are passed through as-is to a JSON command."""
        j = DeviceProxyJson(DummyProxy())
        assert j.Json1(json_string) == non_json_obj
