# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info
"""Test the code in compare_pcap."""
from io import BytesIO

import dpkt
import pytest

from ska_low_cbf_integration.compare_pcap import (
    compare_packets,
    same_packets,
    zero_fields,
)


def test_same():
    """Check that no difference is found between files with same payloads."""
    temp1 = BytesIO()
    temp2 = BytesIO()
    writer1 = dpkt.pcap.Writer(temp1, nano=True)
    writer2 = dpkt.pcap.Writer(temp2, nano=True)
    n_packets = 100

    for writer in [writer1, writer2]:
        # writing separately ensures timestamps are different between files
        # (comparison must ignore timestamps to be useful for us!)
        for _ in range(n_packets):
            data = b"0123456789abcdef"
            writer.writepkt(data)

    temp1.seek(0)
    temp2.seek(0)
    differences, n_comp = compare_packets(
        dpkt.pcap.UniversalReader(temp1), dpkt.pcap.UniversalReader(temp2)
    )
    assert len(differences) == 0
    assert n_comp == n_packets

    temp1.seek(0)
    temp2.seek(0)
    assert same_packets(
        dpkt.pcap.UniversalReader(temp1), dpkt.pcap.UniversalReader(temp2)
    )


def test_one_diff():
    """Check that one difference is detected."""
    temp1 = BytesIO()
    temp2 = BytesIO()
    writer1 = dpkt.pcap.Writer(temp1)
    writer2 = dpkt.pcap.Writer(temp2)
    n_packets = 100
    different_packet_index = 42

    for i in range(1, n_packets + 1):  # compare function uses 1-based counting
        data = b"0123456789abcdef"
        writer1.writepkt(data)
        if i == different_packet_index:
            data = b"fedcba9876543210"
        writer2.writepkt(data)

    temp1.seek(0)
    temp2.seek(0)
    differences, n_comp = compare_packets(
        dpkt.pcap.UniversalReader(temp1), dpkt.pcap.UniversalReader(temp2)
    )
    assert len(differences) == 1
    assert different_packet_index in differences
    assert n_comp == n_packets

    temp1.seek(0)
    temp2.seek(0)
    assert not same_packets(
        dpkt.pcap.UniversalReader(temp1), dpkt.pcap.UniversalReader(temp2)
    )


def test_not_enough():
    """Check behaviour when one file is shorter than the other."""
    temp1 = BytesIO()
    temp2 = BytesIO()
    writer1 = dpkt.pcap.Writer(temp1)
    writer2 = dpkt.pcap.Writer(temp2)
    n_packets = 100

    for i in range(n_packets):
        data = b"0123456789abcdef"
        writer1.writepkt(data)
        if i != 0:
            # skip one packet
            writer2.writepkt(data)

    temp1.seek(0)
    temp2.seek(0)
    with pytest.raises(Exception):
        _, _ = compare_packets(
            dpkt.pcap.UniversalReader(temp1),
            dpkt.pcap.UniversalReader(temp2),
            enforce_same_length=True,
        )

    # ensure no exception when enforcement disabled
    temp1.seek(0)
    temp2.seek(0)
    _, _ = compare_packets(
        dpkt.pcap.UniversalReader(temp1),
        dpkt.pcap.UniversalReader(temp2),
        enforce_same_length=False,
    )

    temp1.seek(0)
    temp2.seek(0)
    assert not same_packets(
        dpkt.pcap.UniversalReader(temp1), dpkt.pcap.UniversalReader(temp2)
    )


def test_different_link_layers():
    """Check that differently-encoded PCAP files with same data compare as equal."""
    assert same_packets(
        "tests/pcap_files/10_multi_length_en10mb.pcap",
        "tests/pcap_files/10_multi_length_sll.pcap",
    )


def test_ignore_ttl_and_checksum():
    """Check that we can ignore the IP TTL & Checksum fields when comparing."""
    packet_a = (
        b"$\x8a\x07F;^b\x00\n\x05\n\x02\x08\x00E\x00 dv\x87\x00\x00\x7f\x11|\x97\n\x05"
        b"\n\x02\n\x00\nd\xf0\xd0\x124 PL"
    )
    packet_b = (
        b"$\x8a\x07F;^b\x00\n\x05\n\x02\x08\x00E\x00 dv\x87\x00\x00\x80\x11{\x97\n\x05"
        b"\n\x02\n\x00\nd\xf0\xd0\x124 PL"
    )
    assert packet_a != packet_b
    # both TTL and checksum differ, so ignoring only one should show a difference
    assert zero_fields(packet_a, ["ip.ttl"]) != zero_fields(packet_b, ["ip.ttl"])
    assert zero_fields(packet_a, ["ip.checksum"]) != zero_fields(
        packet_b, ["ip.checksum"]
    )
    # ignore both, should match
    ignore_fields = ["ip.ttl", "ip.checksum"]
    assert zero_fields(packet_a, ignore_fields) == zero_fields(packet_b, ignore_fields)
