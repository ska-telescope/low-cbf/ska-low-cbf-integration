# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test Conversion of PCAP files to FPGA Simulation format."""
import io

import pytest

from ska_low_cbf_integration.pcap_to_sim import (
    BYTES_PER_LINE,
    valid_bytes_str,
    write_packet_text,
)


@pytest.mark.parametrize(
    "n_valid, str_", [(64, "F" * 16), (46, "FFFFFFFFFFFC0000"), (0, "0" * 16)]
)
def test_valid_bytes_str(n_valid, str_):
    """Test valid_bytes_str function."""
    assert valid_bytes_str(n_valid) == str_


def test_write_packet_text():
    """Test write_packet_text function."""
    # Simple packet of 6 bytes: 0, 1, 2, 3, 4, 5
    packet = bytes(range(6))
    output = io.StringIO()
    write_packet_text(packet, output)
    output.seek(0)
    assert (
        output.read().strip()
        == "0000 1 1  FC00000000000000  000102030405" + "00" * (BYTES_PER_LINE - 6)
    )
