# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test Conversion of FPGA Simulation format text files to PCAP."""

from ska_low_cbf_integration.sim_to_pcap import mask_data


def test_mask():
    """Check data byte masking operation."""
    mask = 0xFFFF_FFFF_FFFC_0000
    data_str = (
        "29295D2C2027666F727472616E5F6F72646572273A2046616C73652C20277368"
        "617065273A202832302C2031297D000000000000000000000000000000000000"
    )
    data_int = int(data_str, 16)
    data_bytes = data_int.to_bytes(64, "big")
    assert (
        mask_data(data_bytes, mask) == b"))], 'fortran_order': False, 'shape': (20, 1)}"
    )
