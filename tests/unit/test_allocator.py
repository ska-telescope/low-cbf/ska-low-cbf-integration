# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test Allocator Helpers."""

from ska_low_cbf_integration.allocator import (
    internal_alveo_serials_subarrays,
    transpose_serials_subarrays,
)


def test_internal_alveo_serials_subarrays():
    """Test the internal_alveo_serials_subarrays function."""
    x = {
        "XFL1HOOQ1Y44": {
            "fw": "corr:0.1.0-main.1e88e61d:gitlab",
            "regs": [
                {
                    "sa_id": 1,
                    "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                    "sa_bm": [[1, 100, 0, 3456, 192, 24, 0]],
                }
            ],
        }
    }
    assert internal_alveo_serials_subarrays(x) == {"XFL1HOOQ1Y44": {1}}


def test_transpose_serials_subarrays():
    """Test the transpose_serials_subarrays function."""
    serials_subarrays = {"FOO": {1, 2}, "BAR": {2, 3}}
    assert transpose_serials_subarrays(serials_subarrays) == {
        1: {"FOO"},
        2: {"FOO", "BAR"},
        3: {"BAR"},
    }
