# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Tests for pulsar_protocol.py code."""
import random
from collections import defaultdict

import numpy as np
import pytest

from ska_low_cbf_integration.pulsar_protocol import (
    METADATA_LEN,
    PSS_BYTES_PER_VAL,
    PST_BYTES_PER_VAL,
    WEIGHT_VAL_BYTES,
    PsrMetadata,
    PsrPacket,
)


@pytest.mark.parametrize(
    "val_size",
    (
        pytest.param(PSS_BYTES_PER_VAL, id="PSS"),
        pytest.param(PST_BYTES_PER_VAL, id="PST"),
    ),
)
def test_complex_samples(val_size):
    """Check complex sample values are extracted correctly."""
    # pylint: disable=too-many-locals
    channels = (1, 2, 3, 4)
    polarisations = ("a", "b")
    components = ("I", "Q")
    times = list(range(64))
    scale = 2  # use a scale factor to make sure it gets applied in conversion
    test_pattern = np.empty(
        len(channels) * len(polarisations) * len(components) * len(times),
        dtype=f"i{val_size}",
    )

    value_lookup = defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))
    )
    """Lookup by channel, pol., time, i/q"""

    random.seed(1337)
    i = 0
    for channel in channels:
        for polarisation in polarisations:
            for t in times:
                for component in components:
                    value = random.randint(-50, 50)
                    test_pattern[i] = value * scale
                    value_lookup[channel][polarisation][t][component] = value
                    i += 1

    meta_bytes = np.ones(METADATA_LEN, dtype=np.uint8).tobytes()
    meta = PsrMetadata.from_bytes(meta_bytes)
    meta.first_channel = channels[0]
    meta.n_channels = len(channels)
    meta.n_samples = len(times)
    meta.samples_per_weight = len(times)
    meta.data_precs = val_size * 8
    meta.scale1 = scale
    # 1 because n_samples == samples_per_weight above
    weight_n_bytes = 1 * len(channels) * WEIGHT_VAL_BYTES
    pad_weight_n_bytes = weight_n_bytes + (16 - (weight_n_bytes % 16))
    weight_bytes = np.zeros(pad_weight_n_bytes, dtype=np.uint8).tobytes()
    data_bytes = test_pattern.tobytes()
    psr = PsrPacket.from_bytes(
        meta.tobytes() + weight_bytes + data_bytes,
    )

    for channel in channels:
        for polarisation in polarisations:
            for t in times:
                n_pol = 0 if polarisation == "a" else 1
                n_ch = channel - meta.first_channel

                assert (
                    psr.complex_samples[n_ch, n_pol, t]
                    == value_lookup[channel][polarisation][t]["I"]
                    + 1j * value_lookup[channel][polarisation][t]["Q"]
                )
