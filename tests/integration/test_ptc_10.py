# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 10 - Correlator filterbank leakage.


- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import math
import os
import shutil
import time
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.correlator import packets_per_correlation
from ska_low_cbf_integration.firmware import Personality

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc10"
HBM_FILE = "dump_hbm_02.bin"

N_STATIONS = 256
SPS_CHAN = 64
SUBARRAY_ID = 1
STN_BEAM_ID = 1
NUM_INTEGRATIONS = 10  # enough to get good time-domain data in CT2 HBM

# CNIC params for this test
TONE_AMPLITUDE = 450  # approx maximum possible tone amplitude
FINE_TONE_FREQ = 1 + 10 * 8  # 8 step per fine chan, 192 step per integ chan
NOISE_AMPLITUDE = 140  # approx minimum dither required to avoid spurii
# Filterbank scale factors for this test
SCALES = [16, 22, 13]


@pytest.mark.timeout(1500)
@pytest.mark.hardware_present
def test_ptc10_corr_filterbank_leakage(
    cnic_and_processor, ready_subarray
):  # noqa # pylint: disable=R0914
    """Implement PTC 10 - Correlator filterbank leakage."""
    time_start = time.time()
    print(datetime.now(), "Starting Test")
    os.makedirs(DATA_DIR, exist_ok=True)

    stations = [stn + 1 for stn in range(0, N_STATIONS)]
    channels = [SPS_CHAN]
    cnic_cfg = _ptc10_cnic_config(
        SUBARRAY_ID,
        [STN_BEAM_ID],
        stations,
        [channels],
        NOISE_AMPLITUDE,
        TONE_AMPLITUDE,
        FINE_TONE_FREQ,
    )
    subarray_cfg = _ptc10_subarray_cfg(
        SUBARRAY_ID,
        [STN_BEAM_ID],
        stations,
        [channels],
    )
    # minus 500k usec gives true zero delay in CNIC
    fixed_dlys = [{"stn": x, "nsec": -500_000.0} for x in stations]
    source_delay_config = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STN_BEAM_ID,
        "delay": [fixed_dlys] * 4,  # 4 delays for 4 hardware sources
    }
    beam_delay_config = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STN_BEAM_ID,
        "delay": fixed_dlys,
    }

    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # n batches of visibility data per stream, plus SPEAD init packets
        cnic_capture_packets=144
        * (NUM_INTEGRATIONS * packets_per_correlation(N_STATIONS) + 1),
        scan_config=subarray_cfg,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_cfg,
        during_scan_callback=None,
        source_delay_config=[source_delay_config],
        beam_delay_config=[beam_delay_config],
    )

    # Run test with all scale factors, capture filterbank output in HBM
    for count, scale in enumerate(SCALES):
        proc_dev = cnic_and_processor[1][0]
        # Need to change processor mem config if we're to dump HBM
        # shared_mem_cfg = "3Gi:3Gs:3Gi:512Mi:512Mi:4Gi"  # latest ver fails
        shared_mem_cfg = "3Gi:3Gs:3Gi:512Mi:512Mi"  # earlier version works
        proc_dev.debugsetmemconfig(shared_mem_cfg)
        # first run is a dummy to get FPGA firmware loaded
        if count != 0:
            proc_dev.debugregwrite(
                json.dumps({"name": "filterbanks.scaling", "offset": 0, "value": scale})
            )
        # Run test
        open_file = cnic_processor(cnic_and_processor, ready_subarray, test_config)
        open_file.close()  # don't care about pcap result
        dump_hbm(proc_dev, scale)
    # put scale back to defaut 16 in case later tests use same device
    proc_dev.debugregwrite(
        json.dumps({"name": "filterbanks.scaling", "offset": 0, "value": 16})
    )
    # put memory conf back to default
    default_mem_cfg = ""
    proc_dev.debugsetmemconfig(default_mem_cfg)
    time_capture_done = time.time()

    leak_db = ptc10_analysis(DATA_DIR, SCALES, HBM_FILE)

    # update overleaf report
    ptc10_report(test_config.scan_config, leak_db)
    time_end = time.time()
    print(f"Capture HBM dump files: {time_capture_done-time_start:.1f} seconds")
    print(f"Data analysis: {time_end-time_capture_done:.1f} seconds")

    assert leak_db <= -60.0, f"leakage {leak_db:.1f}dB but should be below -60dB"


def dump_hbm(proc_dev, scale_value: int) -> None:
    """Dump CT2 HBM contents to file."""
    hbm_path = os.path.join(DATA_DIR, HBM_FILE)
    # remove old file so waiting will work
    try:
        os.remove(hbm_path)
    except OSError:
        pass

    # initiate HBM dumping
    proc_dev.debugdumphbm(json.dumps({"hbm": 2, "dir": DATA_DIR}))

    # Wait while file is written out
    last_size = 0
    this_size = 0
    iter_count = 0
    while this_size == 0 or this_size != last_size:
        last_size = this_size
        time.sleep(0.5)  # wait while file is written
        try:
            fstat = os.stat(hbm_path)
            this_size = fstat.st_size
        except FileNotFoundError:
            this_size = 0
        iter_count += 1
        assert iter_count <= 40, "HBM dumping took too long"

    # give filename a prefix based on the scale used
    new_path = os.path.join(DATA_DIR, f"scale{scale_value:02d}_" + HBM_FILE)
    os.rename(hbm_path, new_path)


def _ptc10_cnic_config(  # pylint: disable=R0913,R0914
    sub_id,
    stn_bm_ids,
    stn_ids,
    beam_freq_ids,
    nscale=4000,
    tscale=10,
    steps=1,
):
    """
    Create cnic configuration for test.

    Single tone with small amount of dither=noise
    """
    seed = 32109
    config = {
        "sps_packet_version": 3,
        "stream_configs": [],  # one entry per SPEAD stream
    }

    sc_list = []
    for beam_idx, beam_id in enumerate(stn_bm_ids):
        for stn_id in stn_ids:
            for freq_id in beam_freq_ids[beam_idx]:
                scfg = {
                    "scan": 0,  # this is in packet but unused, dropped by FPGA
                    "subarray": sub_id,
                    "station": stn_id,
                    "substation": 1,
                    "frequency": freq_id,
                    "beam": beam_id,
                    "sources": {},  # set up sources later
                }
                sc_list.append(scfg)
    config["stream_configs"] = sc_list

    # Note 192 fine_freq steps per COR chan = 32768 * 27/32 / (144 cor_ch/coarse)
    # set up same tone + small uncorrelated noise for each station
    for stream in config["stream_configs"]:
        beam_id = stream["beam"]
        # chan = stream["frequency"]
        stn = stream["station"]
        stream["sources"] = {
            "x": [
                {"tone": False, "seed": seed + 2 * stn, "scale": nscale},
                {"tone": True, "fine_frequency": steps, "scale": tscale},
            ],
            "y": [
                {"tone": False, "seed": seed + 2 * stn + 1, "scale": nscale},
                # {"tone": True, "fine_frequency": steps , "scale": tscale},
            ],
        }
    return config


def _ptc10_subarray_cfg(sub_id, bm_ids, stn_ids, freq_ids):
    """Subarray configuration for test."""
    attr_stem = "low-cbf/delaypoly/0/delay_s"
    cfg = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": [[s, 1] for s in stn_ids],
                "stn_beams": [
                    {
                        "beam_id": bm_id,
                        "freq_ids": freq_ids[idx],
                        "delay_poly": attr_stem + f"{sub_id:02d}_b{bm_id:02d}",
                    }
                    for idx, bm_id in enumerate(bm_ids)
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": bm_id,
                        "host": [[0, "192.168.10.10"]],
                        "port": [[0, 9000, 1]],
                        "mac": [[0, "22-33-44-0a-0b-0c"]],
                        "integration_ms": 849,
                    }
                    for idx, bm_id in enumerate(bm_ids)
                ],
            },
        },
    }
    return cfg


# ---------------------- Analysis code
POLS = 2
N_CHAN = 3456
BYTES_PER_STN = 128
SAMPL_PER_STN = 32
STN_PER_GRP = 4
BYTES_PER_GRP = STN_PER_GRP * BYTES_PER_STN
STN_GRPS = math.ceil(N_STATIONS / STN_PER_GRP)

N_SAMP = BYTES_PER_STN // 4  # 4bytes = 8-bit, dual-pol, RE+IM (A_re, A_im, B_re, Bim)
BYTES_PER_CHAN = BYTES_PER_GRP * STN_GRPS  # all the stations for one freq channel


def ptc10_analysis(data_dir, scales, hbm_file) -> float:  # pylint: disable=R0914,R0915
    """
    Analyse HBM dumpfiles for tone SNR.

    :param data_dir: directory path for the dumpfiles
    :param scales: list of scale registere values (first elt is dummy)
    :param hbm_file: name of the original dumped hbm files (no scale prefix)
    """
    filepaths = []
    for scale in scales:
        path = os.path.join(data_dir, f"scale{scale:02d}_" + hbm_file)
        filepaths.append(path)

    pwrs = []
    apol_tds = []
    bpol_tds = []
    for file in filepaths:
        pwr, apol_max_ch_td, bpol_max_ch_td = analyse_file(file)
        pwrs.append(pwr)
        apol_tds.append(apol_max_ch_td)
        bpol_tds.append(bpol_max_ch_td)

    max_scale_idx = scales.index(max(scales))
    assert max_scale_idx != 0, "first scale is default, shouldn't be largest!"
    min_scale_idx = scales.index(min(scales))
    assert min_scale_idx != 0, "first scale is default, shouldn't be smallest!"

    # find tone bin for the run with largest filterbank scale
    max_chan_idx = np.argmax(pwrs[max_scale_idx])
    max_pwr = pwrs[max_scale_idx][max_chan_idx]
    # scale the maximum so it can replace saturated chan in the min scale run
    # scales are bit-shifts so factor of 2^N in voltage
    voltage_scaling = 2 ** (scales[max_scale_idx] - scales[min_scale_idx])
    max_pwr = max_pwr * (voltage_scaling**2)
    # replace the saturated value in the min power run with the scaled max
    pwrs[min_scale_idx][max_chan_idx] = max_pwr

    # normalise channel powers so the tone-bin power is unity
    chan_pwr = pwrs[min_scale_idx] / max_pwr
    # create a version with tone and two adjacent channels zeroed
    masked_pwr = np.copy(chan_pwr)
    masked_pwr[max_chan_idx - 1 : max_chan_idx + 2] = 0
    # accumulate power across the channels (with 3 zeroed)
    cum_mask_pwr = np.cumsum(masked_pwr)
    cum_leakage_db = 10.0 * math.log10(cum_mask_pwr[-1])

    chan_nums = np.arange(0, len(cum_mask_pwr))
    masked_chans = chan_nums[max_chan_idx - 1 : max_chan_idx + 2]
    masked_vals = chan_pwr[max_chan_idx - 1 : max_chan_idx + 2]

    # Plot channel power, cumulative power, & points excluded from pwr sum
    _, ax = plt.subplots(1, 1, figsize=(12, 8))
    ax.scatter(
        chan_nums,
        10.0 * np.log10(chan_pwr),
        color="blue",
        label="Channel power",
        marker="+",
    )
    ax.scatter(
        masked_chans,
        10.0 * np.log10(masked_vals),
        color="red",
        label="Masked chans",
        marker="x",
    )
    ax.plot(
        chan_nums, 10 * np.log10(cum_mask_pwr), color="green", label="Cumulative power"
    )
    ax.grid()
    ax.legend()
    ax.text(0, -10, f"Leakage: {cum_leakage_db:.1f} dB")
    ax.set_xlabel("Correlator filterbank fine channel")
    ax.set_ylabel("Power relative to tone (dB)")
    ax.set_title("PTC10 filterbank leakage")
    plt.savefig(os.path.join(data_dir, "ptc10_fig1.png"), format="png")

    # close-up version of the plot for John Bunton
    plt.axis([1600, 1900, -110, -60])
    ax.set_title("PTC10 filterbank leakage - zoomed")
    plt.savefig(os.path.join(data_dir, "ptc10_fig1_zoom.png"), format="png")

    # plot average time-domain waveform for tone
    _, ax = plt.subplots(1, 1)
    sample_no = np.arange(0, len(apol_tds[max_scale_idx]))
    ax.plot(
        sample_no,
        apol_tds[max_scale_idx].real,
        color="r",
        label="real",
        marker=".",
        linestyle="dotted",
    )
    ax.plot(
        sample_no,
        apol_tds[max_scale_idx].imag,
        color="b",
        label="imag",
        marker=".",
        linestyle="dotted",
    )
    ax.grid()
    ax.legend()
    ax.set_xlabel("Sample number")
    ax.set_ylabel("Average of 8-bit sample values")
    ax.set_title(f"Averaged filterbank tone waveform ({N_STATIONS } stations)")
    plt.savefig(os.path.join(data_dir, "ptc10_tone_time_domain.png"), format="png")

    return cum_leakage_db


def analyse_file(filename):  # pylint: disable=R0914
    """
    Analyse a HBM dump file for tone levels.

    This analysis expects a single large tone was sent by the CNIC
    """
    x = np.fromfile(filename, dtype=np.int8)

    tdsum = np.zeros((N_CHAN, POLS, N_SAMP), dtype=complex)

    chan_pwr = []
    for frq in range(0, N_CHAN):
        # coherently sum stations (tone sums coherently, noise incoherently)
        apol = np.zeros(SAMPL_PER_STN, dtype=complex)
        bpol = np.zeros(SAMPL_PER_STN, dtype=complex)
        chan_base = frq * BYTES_PER_CHAN
        # print(f"{chan_base}")
        for stn_grp in range(0, STN_GRPS):
            # Get data for the 4 stations in a group
            start = chan_base + stn_grp * BYTES_PER_GRP
            end = start + BYTES_PER_GRP
            grp_bytes = x[start:end].astype(float)

            for stn in range(0, STN_PER_GRP):
                apol += grp_bytes[4 * stn + 0 :: 16] + 1j * grp_bytes[4 * stn + 1 :: 16]
                bpol += grp_bytes[4 * stn + 2 :: 16] + 1j * grp_bytes[4 * stn + 3 :: 16]

        tdsum[frq, 0, :] = apol
        tdsum[frq, 1, :] = bpol

        pwr = np.dot(apol, np.conj(apol)) + np.dot(bpol, np.conj(bpol))
        chan_pwr.append(pwr.real)  # discard zero imaginary part

    max_chan_pwr = max(chan_pwr)
    big = [i for i, v in enumerate(chan_pwr) if v > (max_chan_pwr / 2)]
    max_idx = big[0]  # lowest channel with reasonable power
    print(f"{filename}: max_pwr={max(chan_pwr):.1f} @ idx={max_idx}")

    apol_max_ch_td = tdsum[max_idx, 0, :] / N_STATIONS
    bpol_max_ch_td = tdsum[max_idx, 1, :] / N_STATIONS

    return chan_pwr, apol_max_ch_td, bpol_max_ch_td


def ptc10_report(scan_config, leak_db):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6747a5ed0d8bbf2f65fe915f"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    replace_word_in_report(
        file_result_to_replace,
        "LEAKED_DB",
        str(leak_db),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQPTC47",
        str(leak_db <= -60.0),
    )
    shutil.copy(
        os.path.join(DATA_DIR, "ptc10_fig1.png"),
        local_directory + new_repo_name + "/ptc10_fig1.png",
    )
    shutil.copy(
        os.path.join(DATA_DIR, "ptc10_fig1_zoom.png"),
        local_directory + new_repo_name + "/ptc10_fig1_zoom.png",
    )
    shutil.copy(
        os.path.join(DATA_DIR, "ptc10_tone_time_domain.png"),
        local_directory + new_repo_name + "/ptc10_tone_time_domain.png",
    )

    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc10_fig1.png")
    repo.git.add(new_repo_name + "/ptc10_fig1_zoom.png")
    repo.git.add(new_repo_name + "/ptc10_tone_time_domain.png")
    repo.index.commit("Test with python")
    repo.git.push()
