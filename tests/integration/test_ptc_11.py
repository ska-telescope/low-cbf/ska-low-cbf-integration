# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 11 - Visibility delay tracking accuracy.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #11 Presentation <https://docs.google.com/presentation/d/1MfESRHFKRMVol5tIm33d4UD9-kpZmhTX_KnK19OETcY>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
import shutil
from copy import deepcopy
from itertools import combinations
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.tango import get_proxies_for_class
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from . import corr_configs, delay_configs
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

stations_closure = [0, 1, 2, 3, 4, 5]

combinations_of_three = list(combinations(stations_closure, 3))
combinations_of_four = list(combinations(stations_closure, 4))

TEST_TIMEOUT_SEC = 2500
TEST_TIMEOUT_TRACKING_SEC = 900
DATA_DIR = "/test-data/PTC/ptc11/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH = DATA_DIR + "PTC11_corr.pcap"
"""PCAP file name"""

source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}


# @pytest.mark.long_test when we enable the multiple RA time
@pytest.mark.timeout(TEST_TIMEOUT_TRACKING_SEC)  # default of 300s is not long enough
@pytest.mark.parametrize("ra_time", ["00"])  # , "20", "22", "02", "04"])
@pytest.mark.parametrize("sps_packet_version", [3])  # no longer need to test version 2
@pytest.mark.hardware_present
def test_ptc11_corr_delay_tracking_precision(
    cnic_and_processor, ready_subarray, ra_time, sps_packet_version  # noqa
):
    """Implement delay tracking precision and phase closure analysis part of PTC 11."""
    # pylint: disable = too-many-locals
    beam_configuration = deepcopy(beam_delay_config)
    beam_configuration["direction"]["ra"] = f"{ra_time}h00m00.0s"
    source_configuration = deepcopy(source_delay_config)
    vd_config = {
        "stream_configs": cnic_config_ptc11_tracking,
        "sps_packet_version": sps_packet_version,
    }
    for direction in range(4):
        source_configuration["direction"][direction]["ra"] = f"{ra_time}h00m00.0s"
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=80,
        cnic_capture_packets=72 * 144,
        scan_config=corr_configs.scan_6stn_1sa_1bm_ch448_delay_tracking,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=vd_config,
        during_scan_callback=None,
        source_delay_config=[source_configuration],
        beam_delay_config=[beam_configuration],
    )
    correlator_output = cnic_processor(
        cnic_and_processor, ready_subarray, test_config, PCAP_PATH
    )
    analyser = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_ptc11,
                "stations": sorted(stations),
            }
        )
    )
    analyser.extract_spead_data(correlator_output.name)
    analyser.populate_correlation_matrix()
    analyser.save_heatmap_to_disk(mode="average_all")
    analyser.save_phase_heatmap_to_disk(mode="average_all")
    (
        phase_integrated,
        sumss_phase,
        amplitude_integrated,
        sumss_amplitude,
    ) = _delay_tracking_phase_analysis(analyser)
    # Baseline of interest to print for illustration
    analyser.angle_analysis((243, 255), "XX", 1, [448])
    analyser.angle_analysis((243, 261), "XX", 1, [448])
    analyser.angle_analysis((255, 315), "XX", 1, [448])
    analyser.angle_analysis((255, 393), "XX", 1, [448])
    analyser.angle_analysis((243, 405), "XX", 1, [448])
    rms_phase, rms_amp = _ptc11_tracking_report(
        test_config,
        phase_integrated,
        sumss_phase,
        amplitude_integrated,
        sumss_amplitude,
    )
    corr_matrix = np.zeros([2 * analyser.nb_station, 2 * analyser.nb_station])
    number_of_phase = 0
    for _, vis in analyser.corr_phase_matrixes_time.items():
        for matrix in vis.values():
            corr_matrix = corr_matrix + matrix
            number_of_phase = number_of_phase + 1
    corr_matrix = corr_matrix / number_of_phase
    assert corr_matrix.max() < 0.01
    assert corr_matrix.min() > -0.01
    assert float(rms_phase[6]) < 0.0017
    assert float(rms_amp[6]) < 0.0017


@pytest.mark.long_test
@pytest.mark.large_capture
@pytest.mark.timeout(TEST_TIMEOUT_SEC)  # default of 300s is not long enough
@pytest.mark.hardware_present
@pytest.mark.parametrize("sps_packet_version", [3])
def test_ptc11_corr_static_tracking_precision(
    cnic_and_processor, ready_subarray, sps_packet_version
):  # noqa
    """Implement Static tracking precision part of PTC 11."""
    initial_beam_delay = deepcopy(delay_configs.constant_beam_delay)
    initial_beam_delay["delay"][0]["nsec"] = 0.0
    initial_beam_delay["delay"][1]["nsec"] = 276480.0
    vd_config = {
        "stream_configs": cnic_config_ptc11_static,
        "sps_packet_version": sps_packet_version,
    }

    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=80,
        cnic_capture_packets=45 * 144 * 32,
        scan_config=corr_configs.scan_6stn_1sa_1bm_ch448_delay,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=vd_config,
        during_scan_callback=_ptc_11_delay_steps_correlator,
        source_delay_config=[delay_configs.source_constant_delay],
        beam_delay_config=[initial_beam_delay],
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    analyser = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_ptc11,
                "stations": list(range(1, 7)),  # match source_constant_delay
            }
        )
    )
    analyser.extract_spead_data(correlator_output.name)
    analyser.populate_correlation_matrix()
    ordered_time = analyser.corr_abs_matrixes_time.keys()
    cross_of_interest = []

    for time in sorted(ordered_time):
        for _, array in analyser.corr_abs_matrixes_time[time].items():
            cross_of_interest.append(
                (array[2][0] + array[2][1] + array[3][0] + array[3][1]) / 4
            )
    plt.clf()
    plt.plot(cross_of_interest, ".-")
    plt.ylabel("Amplitude")
    plt.xlabel("Integration")
    plt.savefig("/app/build/ptc_cross.png")
    parameters = ptc_11_plot_static(cross_of_interest)
    ptc_11_generate_report(parameters)
    _ptc11_static_report(test_config, parameters)


def _delay_tracking_phase_analysis(analyser):
    """Analyse delay tracking results."""
    # pylint: disable=too-many-locals, consider-using-enumerate
    closure_angle_total = []
    for time in range(64):
        for combination in combinations_of_three:
            for polarisation in ["XX", "XY", "YX", "YY"]:
                closure_angle, _ = analyser.phase_and_amplitude_closure(
                    combination + (3,), [448], polarisation, time
                )
                closure_angle_total.extend(closure_angle)
    closure_amplitude_total = []
    for time in range(64):
        for combination in combinations_of_four:
            for polarisation in ["XX", "XY", "YX", "YY"]:
                _, closure_amp = analyser.phase_and_amplitude_closure(
                    combination, [448], polarisation, time
                )
                closure_amplitude_total.extend(closure_amp)
    phase_integrated = [0] * 64
    print(np.mean(closure_angle_total))

    closure_angle_total_wout_average = (
        np.array(closure_angle_total) - 0.0
    )  # np.mean(closure_angle_total)
    for index, _ in enumerate(phase_integrated):
        phase_integrated[index] = np.sum(
            closure_angle_total_wout_average[index * 144 * 80 : (index + 1) * 144 * 80]
        ) / (
            144 * 80
        )  # not sum but RMS

    sumss_phase = []
    sumss_phase.append(phase_integrated)
    for index in range(6):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(sumss_phase[index])] * 2)]
        sumss_phase.append(to_add)

    amplitude_integrated = [0] * 64
    print(np.mean(closure_amplitude_total))
    closure_amplitude_total_wout_mean = np.array(closure_amplitude_total) - 1.0
    samples = 144 * 56  # wrong name, I don't know what it should be!
    for index in range(len(amplitude_integrated)):
        # not sum but RMS (is it? looks more like a mean to me)
        amplitude_integrated[index] = np.sum(
            closure_amplitude_total_wout_mean[index * samples : (index + 1) * samples]
        ) / (samples)

    sumss_amplitude = []
    sumss_amplitude.append(amplitude_integrated)
    for index in range(6):
        to_add = [(a + b) / 2 for a, b in zip(*[iter(sumss_amplitude[index])] * 2)]
        sumss_amplitude.append(to_add)

    return phase_integrated, sumss_phase, amplitude_integrated, sumss_amplitude


def _print_phase_analysis(
    directory, phase_integrated, sumss_phase, amplitude_integrated, sumss_amplitude
):
    """Print the result of phase analysis."""
    x_points = [(144 * 80 * 0.849 * 2**x) / 3600 for x in range(8)]
    y_points = [
        np.sqrt(np.sum((np.abs(sumss_phase[0]) ** 2)) / 2**6),
        np.sqrt(np.sum((np.abs(sumss_phase[1]) ** 2)) / 2**5),
        np.sqrt(np.sum((np.abs(sumss_phase[2]) ** 2)) / 2**4),
        np.sqrt(np.sum((np.abs(sumss_phase[3]) ** 2)) / 2**3),
        np.sqrt(np.sum((np.abs(sumss_phase[4]) ** 2)) / 2**2),
        np.sqrt(np.sum((np.abs(sumss_phase[5]) ** 2)) / 2**1),
        np.sqrt(np.sum((np.abs(sumss_phase[6]) ** 2)) / 2**0),
    ]

    plt.clf()
    plt.plot(
        x_points[:6],
        y_points[:6],
        "--ro",
        label="Standard Deviation of phase closure",
    )
    plt.ylabel("RMS of phase closure")
    plt.xlabel("Number of hours")
    plt.xscale("log")
    plt.yscale("log")
    plt.savefig(f"{directory}/rms_phase_closure.png")

    plt.clf()
    plt.plot(phase_integrated, "b-")
    plt.ylabel("phase closure")
    plt.xlabel("integration number")
    plt.savefig(f"{directory}/phase_closure_integration.png")

    y_points_amp = [
        np.sqrt(np.sum((np.abs(sumss_amplitude[0]) ** 2)) / 2**6),
        np.sqrt(np.sum((np.abs(sumss_amplitude[1]) ** 2)) / 2**5),
        np.sqrt(np.sum((np.abs(sumss_amplitude[2]) ** 2)) / 2**4),
        np.sqrt(np.sum((np.abs(sumss_amplitude[3]) ** 2)) / 2**3),
        np.sqrt(np.sum((np.abs(sumss_amplitude[4]) ** 2)) / 2**2),
        np.sqrt(np.sum((np.abs(sumss_amplitude[5]) ** 2)) / 2**1),
        np.sqrt(np.sum((np.abs(sumss_amplitude[6]) ** 2)) / 2**0),
    ]

    plt.clf()
    plt.plot(
        x_points[:7],
        y_points_amp[:7],
        "--ro",
        label="Standard Deviation of phase closure",
    )
    plt.ylabel("RMS of amplitude closure")
    plt.xlabel("Number of hours")
    plt.xscale("log")
    plt.yscale("log")
    plt.savefig(f"{directory}/rms_amplitude_closure.png")

    plt.clf()
    plt.plot(amplitude_integrated[:50], ".-")
    plt.ylabel("closure amplitude")
    plt.xlabel("integration number")
    plt.savefig(f"{directory}/amplitude_closure_integration.png")

    return y_points, y_points_amp


def _ptc11_tracking_report(
    test_config, phase_integrated, sumss_phase, amplitude_integrated, sumss_amplitude
):
    """Generate Test Report for PTC11."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6566a86cd623938d75f26927"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    rms_phase, rms_amplitude = _print_phase_analysis(
        local_directory + new_repo_name,
        phase_integrated,
        sumss_phase,
        amplitude_integrated,
        sumss_amplitude,
    )
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RMSPHCLOSURE",
        str(sumss_phase[6]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RMSAMPCLOSURE",
        str(sumss_amplitude[6]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RMSPHREQ",
        str(float(rms_phase[6]) < 0.0017),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RMSAMPREQ",
        str(float(rms_amplitude[6]) < 0.0017),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "heat_map.png",
        local_directory + new_repo_name + "/heat_map.png",
    )
    shutil.copy(
        "phase_heat_map.png",
        local_directory + new_repo_name + "/phase_heat_map.png",
    )
    shutil.copy(
        "/app/243x255_XX_angle.png",
        local_directory + new_repo_name + "/243x255_XX_angle_time_0.png",
    )
    shutil.copy(
        "/app/243x261_XX_angle.png",
        local_directory + new_repo_name + "/243x261_XX_angle_time_0.png",
    )
    shutil.copy(
        "/app/255x315_XX_angle.png",
        local_directory + new_repo_name + "/255x315_XX_angle_time_0.png",
    )
    shutil.copy(
        "/app/255x393_XX_angle.png",
        local_directory + new_repo_name + "/255x393_XX_angle_time_0.png",
    )
    shutil.copy(
        "/app/243x405_XX_angle.png",
        local_directory + new_repo_name + "/243x405_XX_angle_time_0.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/heat_map.png")
    repo.git.add(new_repo_name + "/phase_heat_map.png")
    repo.git.add(new_repo_name + "/rms_phase_closure.png")
    repo.git.add(new_repo_name + "/phase_closure_integration.png")
    repo.git.add(new_repo_name + "/rms_amplitude_closure.png")
    repo.git.add(new_repo_name + "/amplitude_closure_integration.png")
    repo.git.add(new_repo_name + "/243x255_XX_angle_time_0.png")
    repo.git.add(new_repo_name + "/243x261_XX_angle_time_0.png")
    repo.git.add(new_repo_name + "/255x315_XX_angle_time_0.png")
    repo.git.add(new_repo_name + "/255x393_XX_angle_time_0.png")
    repo.git.add(new_repo_name + "/243x405_XX_angle_time_0.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return rms_phase, rms_amplitude


def _ptc11_static_report(test_config, params):
    """Generate Test Report for PTC11."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66fca6fbd61873ef415fd8d4"
    )
    maximum_y = params[2] - (params[1] ** 2 / (4 * params[0]))
    maximum_x = -1 * params[1] / (2 * params[0])
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXIMUMX",
        str(maximum_x),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXIMUMY",
        str(maximum_y),
    )
    replace_word_in_report(
        file_result_to_replace,
        "COEF1",
        str(params[0]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "COEF2",
        str(params[1]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "COEF3",
        str(params[2]),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "/app/build/ptc_cross.png",
        local_directory + new_repo_name + "/ptc_cross.png",
    )
    shutil.copy(
        "/app/build/ptc_11_all.png",
        local_directory + new_repo_name + "/ptc_11_all.png",
    )
    shutil.copy(
        "/app/build/ptc_11_zoom_in.png",
        local_directory + new_repo_name + "/ptc_11_zoom_in.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc_cross.png")
    repo.git.add(new_repo_name + "/ptc_11_all.png")
    repo.git.add(new_repo_name + "/ptc_11_zoom_in.png")
    repo.index.commit("Test with python")
    repo.git.push()


def _ptc_11_delay_steps_correlator(cnic, processor):
    """Increment delay in steps."""
    nb_iteration = 33
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
    delays = get_proxies_for_class("DelayDevice")
    delay = delays[0]
    try:
        os.remove("/app/build/update_time.txt")
    except OSError:
        pass
    for iteration in range(nb_iteration):
        correlator_delay_to_apply = deepcopy(delay_configs.constant_beam_delay)
        correlator_delay_to_apply["delay"][0]["nsec"] = 17280.0 * iteration
        correlator_delay_to_apply["delay"][1]["nsec"] = 276480.0
        delay.beamdelay(json.dumps(correlator_delay_to_apply))
        n_sleeps = 0
        while len(processor.stats_delay) == 0:
            n_sleeps += 1
            sleep(1)
            if n_sleeps > 60:  # this is killing the productivity
                raise RuntimeError(
                    f"Iteration {iteration} - did not receive "
                    "packets in reasonable time."
                )
        n_sleeps = 0
        while (
            processor.stats_delay[0]["beams"][0]["stn_delay_ns"][0]["ns"]
            != 17280.0 * iteration
        ):
            if n_sleeps % 5 == 0:
                print(
                    f"Delay Configuration: {processor.stats_delay}"
                    f" vs {correlator_delay_to_apply}"
                )
            sleep(1)
            n_sleeps += 1
            if n_sleeps > 60:  # this is killing the productivity
                raise RuntimeError(
                    f"Iteration {iteration} - did not receive "
                    "packets in reasonable time."
                )
        packets_received = cnic.hbm_pktcontroller__rx_packet_count
        with open("/app/build/update_time.txt", "a", encoding="UTF8") as file:
            file.write(
                f"iteration: {iteration}, packets_received: {packets_received}\n"
            )
        n_sleeps = 0
        expected_packets = packets_received + 15 * 144
        while packets_received < expected_packets:
            if n_sleeps % 5 == 0:
                print(f"Packets Received: {packets_received} Iteration {iteration}")
            sleep(1)
            n_sleeps += 1
            packets_received = cnic.hbm_pktcontroller__rx_packet_count
            if n_sleeps > 30:  # should take 15s
                raise RuntimeError(
                    f"Iteration {iteration} - did not receive {expected_packets} "
                    "packets in reasonable time."
                )


def _apply_delay(cnic: tango.DeviceProxy, iteration_number: int):
    """Apply Delay to Station 2."""
    # [256 samples, 16 samples, 0, 0, 0, 0]
    delays = [276480, 17280, 0.0, 0.0, 0.0, 0.0, 0.0]

    for station_idx in range(len(stations)):
        # sources start from zero 8 sources per SPEAD stream
        # for channel 0 then 8 again for channel 1
        source_x = station_idx * 8
        source_y = source_x + 4  # Y is 4 after X
        if station_idx == 1:
            polynomial = [iteration_number * delays[station_idx], 0, 0, 0, 0, 0]
        else:
            polynomial = [delays[station_idx], 0, 0, 0, 0, 0]

        delay_config_x = {
            "peripheral": "vd_datagen",
            "method": "set_delay_polynomial",
            "arguments": {"source": source_x, "polynomial": polynomial, "buffer": 0},
        }
        delay_config_y = deepcopy(delay_config_x)
        delay_config_y["arguments"]["source"] = source_y

        cnic.CallMethod(json.dumps(delay_config_x))
        cnic.CallMethod(json.dumps(delay_config_y))


def ptc_11_plot_static(cross_of_interest):
    """Plot two figures of delay vs amplitude."""
    interesting_values = range(3, 13)
    print(len(interesting_values))
    average_amp = [0] * 34
    with open("/app/build/update_time.txt", "r", encoding="UTF8") as file:
        lines = file.readlines()
    plateaux_start = [0]
    for line in lines:
        if len(line.split(":")) > 0:
            plateaux_start.append(int(int(line.split(":")[2]) / 144))

    for index, border in enumerate(plateaux_start):
        if index > 0:
            if index < len(plateaux_start) - 1:
                if len(cross_of_interest) < plateaux_start[index + 1]:
                    average_amp[index] = np.mean(cross_of_interest[border + 2 :])
                    break
                average_amp[index] = np.mean(
                    cross_of_interest[border + 2 : plateaux_start[index + 1] - 2]
                )
            else:
                average_amp[index] = np.mean(cross_of_interest[border + 2 :])
    average_amp[0] = average_amp[1]
    delays = [i * 16 for i in range(32)]
    plt.clf()
    plt.plot(delays, average_amp[2:], ".-")
    plt.ylabel("Amplitude")
    plt.xlabel("delays (in samples)")
    plt.savefig("/app/build/ptc_11_all.png")
    params = np.polyfit(delays[10:23], average_amp[11:24], 2)
    poly1d = np.poly1d(params)
    x_space = np.linspace(156, 356, 10000000)
    plt.clf()
    plt.plot(delays[10:23], average_amp[11:24], ".-")
    plt.plot(x_space, poly1d(x_space), "r-")
    plt.grid()
    plt.ylabel("Amplitude")
    plt.xlabel("delays (in samples)")
    plt.savefig("/app/build/ptc_11_zoom_in.png")
    return params


def ptc_11_generate_report(params):
    """Generate file containing results."""
    # for quadratic functions of the form ax^2 + bx + c
    # max_y = c - (b^2/4a) for x = -b/2a
    maximum_y = params[2] - (params[1] ** 2 / (4 * params[0]))
    maximum_x = -1 * params[1] / (2 * params[0])
    report = f"""
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>PTC 11 Report Card </title>
    </head>
    <body>
    <h1> PTC 11 Report Card </h1>

    <h2> Raw result </h2>

    <p> Here we plot the raw results </p>
    <p>
    <img src="ptc_cross.png" alt="Raw data">
    </p>
    <h2> Quadratic fitting </h2>

    <p>Now we can focus on the middle 10 points of each plateau from
    above. Average those and plot in function of the delay.</p>
    <p>
    <img src="ptc_11_all.png" alt="PTC 11 result">
    </p>

    <p>Next we can zoom in and fit a quadratic function.</p>
    <p>
    <img src="ptc_11_zoom_in.png" alt="PTC 11 result zoomed in">
    </p>


    <p> We have finished the PTC 11 tests. Overall the between 96 and 352
    we can approximate the amplitude as </p>
    <p>
    <math>
    amplitude = {params[0]}s^2 + {params[1]}s + {params[2]}
    </math>
    </p>
    <p>where s is the number of delay in sample.</p>
    <p>This gives MaxY: {maximum_y} for x = {maximum_x}</p>
    </body>
    </html>
    """
    with open("build/report_ptc11.html", "w", encoding="UTF8") as file:
        file.write(report)


MAX_CHANNEL_PTC11 = 1
channels_ptc11 = list(range(448, 448 + MAX_CHANNEL_PTC11))
seed_per_channel_ptc11 = {
    405: 1981,
    393: 1981,
    315: 1981,
    261: 1981,
    255: 1982,
    243: 1982,
}
stations = [405, 393, 315, 261, 255, 243]
cnic_config_ptc11 = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.SINGLE_BASIC,
        "sources": {
            "x": [
                {"tone": False, "seed": seed_per_channel_ptc11[station], "scale": 4000},
            ],
            "y": [
                {"tone": False, "seed": seed_per_channel_ptc11[station], "scale": 4000},
            ],
        },
    }
    for station in stations
    for channel in channels_ptc11
]
cnic_config_ptc11_static = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.SINGLE_BASIC,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 4000},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 4000},
            ],
        },
    }
    for station in range(1, 7)  # to match delay_configs.source_constant_delay
    for channel in channels_ptc11
]
cnic_config_ptc11_tracking = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.SINGLE_BASIC,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 4000},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 4000},
            ],
        },
    }
    for station in stations
    for channel in channels_ptc11
]
