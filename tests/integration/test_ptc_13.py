# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 13 - Visibility spectral characteristics.

Test:
    Check that the visibility channel power follows the expected pattern
    Check that power decreases to -60dB at edge of channel


More information:
- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #13 Presentation <https://docs.google.com/presentation/d/1OTnAaR_fR1FdHuZYjt-Y4Gfw561r_ZnH4BAgdpr9tDg>`_
- `PTC #13 Overleaf <https://www.overleaf.com/read/fjsgqcvxvdzt#c2222e>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import shutil
from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from . import corr_configs
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 7200

source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}

theoretical_response = [
    -0.0002,
    -0.0004,
    -0.0007,
    0,
    -0.0016,
    -0.0236,
    -0.2181,
    -1.0142,
    -3.0104,
    -6.8165,
    -13.1065,
    -22.7333,
    -36.4958,
    -54.5417,
    -75.00,
]
x_axis_model = [
    184,
    184.5,
    185,
    185.5,
    186,
    186.5,
    187,
    187.5,
    188,
    188.5,
    189,
    189.5,
    190,
    190.5,
    191,
]


@pytest.mark.timeout(TEST_TIMEOUT_SEC)  # default of 300s is not long enough
@pytest.mark.hardware_present
def test_ptc13_corr_visibility_spectral(
    cnic_and_processor, ready_subarray
):  # noqa:  # noqa
    """Implement 1 beam 6 stations - 1 channel part of PTC 13."""
    # pylint: disable = too-many-locals
    # basic settings
    n_stations = 6
    station_ids = list(range(1, 1 + n_stations))
    stations = [[station, 1] for station in station_ids]
    channels = [333]
    # tweak the stock standard scan configuration
    scan_config = deepcopy(corr_configs.scan_6stn_1sa_1bm_1ch)
    stn = scan_config["lowcbf"]["stations"]
    stn["stn_beams"][0]["freq_ids"] = channels
    stn["stns"] = stations

    # output packet related constants
    channel_count = len(channels)
    n_vis = 11  # 10 integrations and 1 init per visibility
    rx_packets = 144 * n_vis * channel_count
    rx_size = 80
    # for analysis
    values_around_center = {}
    value_72 = []

    for position in range(184, 193):
        vd_config = {
            "stream_configs": prepare_stream_cnic_config(position),
            "sps_packet_version": 3,
        }
        test_config = ProcessorVdTestConfig(
            cnic_capture_min_size=rx_size,
            # n batches of visibility data per stream, plus SPEAD init packets
            cnic_capture_packets=rx_packets,
            scan_config=scan_config,
            firmware_type=Personality.CORR,
            input_packet_count=None,
            start_scan_packet_count=None,
            before_end_scan_packet_count=None,
            output_packet_count=None,  # proc packets after end of scan
            vd_config=vd_config,
            during_scan_callback=None,
        )
        pcap_file = f"/test-data/ci-rx-ptc13-{position}-position.pcap"
        _ = cnic_processor(
            cnic_and_processor, ready_subarray, test_config, pcap_file_name=pcap_file
        )
        visibilities = VisibilityAnalyser(
            json.dumps({"coarse_channels": channels, "stations": station_ids})
        )
        visibilities.extract_spead_data(pcap_file)
        amp_9 = visibilities.amplitude_freq_analysis("XX", 5, channels, False)
        value_72.append(sum_channel(visibilities))
        values_around_center[position] = amp_9[0][70:75]

    value_72 = np.abs(value_72)
    print_amplitude_vs_freq(values_around_center, value_72)
    minimum_amplitude = 10 * np.log10(value_72[-1]) - (np.max(10 * np.log10(value_72)))
    half_amplitude = 10 * np.log10(value_72[4]) - (np.max(10 * np.log10(value_72)))
    ptc_report(scan_config, minimum_amplitude, half_amplitude)
    assert minimum_amplitude < -60


def print_amplitude_vs_freq(values_around_center, value_72):
    """Print the amplitude."""
    response_norm = [
        np.max(value_72) * 10 ** (resp / 10) for resp in theoretical_response
    ]
    x_axis_model_hz = [axis * 28.257 for axis in x_axis_model]
    x_axis = list(values_around_center.keys())
    x_axis_hz = [axis * 28.257 for axis in x_axis]
    plt.clf()
    plt.plot(
        x_axis_hz[0:12],
        10 * np.log10(value_72) - (np.max(10 * np.log10(value_72))),
        "r-x",
        label="experiment",
    )
    plt.plot(
        x_axis_model_hz,
        10 * np.log10(response_norm) - (np.max(10 * np.log10(value_72))),
        "g-x",
        label="model",
    )
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Power (dB)")
    # plt.yscale("log")
    plt.title("Amplitude vs Frequency")
    plt.legend(loc=1, prop={"size": 12})
    plt.savefig("/app/ptc13_amp_freq.png")


def sum_channel(visibil):
    """Sum channel 72 (excluding auto-correlations)."""
    channel_of_interest = 0
    for time in range(len(visibil.time_sequence)):
        for i in range(int((visibil.nb_station * (visibil.nb_station + 1)) / 2)):
            # exclude auto-correlations
            if i not in [0, 2, 5, 9, 14, 20]:
                for polarisation in [0, 3]:
                    channel_of_interest += visibil.visibilities[72][
                        int(sorted(list(visibil.time_sequence))[time])
                    ]["VIS"][i][polarisation]
    return channel_of_interest


def prepare_stream_cnic_config(tone_position):
    """Prepare the CNIC config."""
    tmp_stream_config_beam_9 = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": 1,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": station, "scale": 5},
                    {"tone": True, "fine_frequency": int(tone_position), "scale": 6},
                ],
                "y": [
                    {"tone": False, "seed": station, "scale": 5},
                    {"tone": True, "fine_frequency": int(tone_position), "scale": 6},
                ],
            },
        }
        for station in range(1, 7)
        for channel in range(333, 334)
    ]
    return tmp_stream_config_beam_9


def ptc_report(scan_config, minimum_amplitude, half_amplitude):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66f0ff0c0d0b491dd29c048f"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "/app/ptc13_amp_freq.png",
        local_directory + new_repo_name + "/ptc13_amp_freq.png",
    )
    replace_word_in_report(
        file_result_to_replace,
        "RESULT_AMPLITUDE",
        f"{minimum_amplitude:.2f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "HALF_AMPLITUDE",
        f"{half_amplitude:.2f}",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc13_amp_freq.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()
