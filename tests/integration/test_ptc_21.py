# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 21 - PST General Functionality.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #21 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from math import ceil

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.pst_utils import freq_power_per_beam

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 600


station_ids = [
    345,
    350,
    352,
    355,
    431,
    434,
    2,
    12,
    131,
    150,
    55,
    44,
    98,
    123,
    22,
    25,
    107,
    147,
    75,
    60,
    17,
]

beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
src1_radec = {"ra": "2h30m00.00s", "dec": "-28d49m50.0s"}
src2_radec = {"ra": "2h30m00.00s", "dec": "-30d49m50.0s"}
src3_radec = {"ra": "2h30m00.00s", "dec": "-32d49m50.0s"}
src4_radec = {"ra": "2h30m00.00s", "dec": "-34d49m50.0s"}

pst_radecs_beam_noise = [beam_radec, src1_radec, src1_radec, src1_radec]
pst_radecs_beam_beam = [src1_radec, src2_radec, src3_radec, src4_radec]
source_delay_config_noise = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": ([src1_radec] * 4),
}
source_delay_config_beam = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pst_radecs_beam_beam,
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}
pst_delay_config_beam = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pst_radecs_beam_beam,
}
pst_delay_config_noise = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pst_radecs_beam_noise,
}
station_weights_noise = {
    11: [1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    12: [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
    13: [0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
    14: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
}

station_weights_beam = {
    11: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    12: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    13: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    14: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc21_pst_general_functionality(cnic_and_processor, ready_subarray):
    """
    Implement PTC 21.

    In this test we aim to show that:
    * PST beams can be steered in a different direction to the station beam
    * the FPGA can apply appropriate delays to steer pulsar beams
    """
    # pylint: disable=too-many-locals
    delay_sub = "low-cbf/delaypoly/0/delay_s01_b01"

    pst_sub = "low-cbf/delaypoly/0/pst_s01_b01_"
    print(datetime.now(), "Starting PTC#21")
    n_stations = 6
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 1
    first_channel = ceil(230 - n_channels / 2)
    channels = list(range(first_channel, first_channel + n_channels))
    pst_beam_id = [11, 12, 13, 14]
    pst_svr_ip = "192.168.55.55"
    n_stations_beam = 6
    stations_beam = [[station, 1] for station in station_ids[:n_stations_beam]]
    # Generate CNIC-VD Configuration
    cnic_config_noise = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_noise = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "timing_beams": {
                "beams": [
                    {
                        "pst_beam_id": pst_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pst_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pst_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": station_weights_noise[pst_beam],
                    }
                    for station_idx, pst_beam in enumerate(pst_beam_id)
                ],
            },
        },
    }
    cnic_config_beam = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": True, "fine_frequency": 0, "scale": 10},
                    {"tone": True, "fine_frequency": -8192, "scale": 10},
                    {"tone": True, "fine_frequency": 8192, "scale": 10},
                    {"tone": True, "fine_frequency": 12288, "scale": 10},
                ],
                "y": [
                    {"tone": True, "fine_frequency": 0, "scale": 10},
                    {"tone": True, "fine_frequency": -8192, "scale": 10},
                    {"tone": True, "fine_frequency": 8192, "scale": 10},
                    {"tone": True, "fine_frequency": 12288, "scale": 10},
                ],
            },
        }
        for station, substation in stations_beam
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_beam = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations_beam,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "timing_beams": {
                "beams": [
                    {
                        "pst_beam_id": pst_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pst_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pst_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": station_weights_beam[pst_beam],
                    }
                    for station_idx, pst_beam in enumerate(pst_beam_id)
                ],
            },
        },
    }
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=792 * 5 * n_channels,
        scan_config=scan_config_noise,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_noise],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_noise,
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    print(datetime.now(), "Captured PTC#21 output, beginning analysis")
    payloads = get_udp_payload_bytes(correlator_output.name, 64)
    total_freqs, total_pwr = freq_power_per_beam(payloads, pst_beam_id)

    test_config_beam = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=792 * 5 * n_channels,
        scan_config=scan_config_beam,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_beam,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_beam,
    )
    correlator_output_beam = cnic_processor(
        cnic_and_processor, ready_subarray, test_config_beam
    )
    payloads_beam = get_udp_payload_bytes(correlator_output_beam.name, 64)
    total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads_beam, pst_beam_id)
    _ptc21_report(
        test_config,
        total_freqs,
        total_pwr,
        total_freqs_beam,
        total_pwr_beam,
        test_config_beam,
    )
    # Beam 14 has 1.0 weight for each of the 6 stations, 13 has only a single station
    relative_power = np.mean(total_pwr[14]) / np.mean(total_pwr[13])
    assert 36 * 0.99 < relative_power < 36 * 1.01
    assert total_pwr_beam[11].index(max(total_pwr_beam[11])) == 108
    assert total_pwr_beam[12].index(max(total_pwr_beam[12])) == 108 - 64
    assert total_pwr_beam[13].index(max(total_pwr_beam[13])) == 108 + 64
    assert total_pwr_beam[14].index(max(total_pwr_beam[14])) == 108 + 96
    print(datetime.now(), "End of PTC#21")


def _ptc21_report(
    test_config,
    total_freqs,
    total_pwr,
    total_freqs_beam,
    total_pwr_beam,
    test_config_beam,
):
    """Generate Test Report for PTC21."""
    # pylint: disable=too-many-arguments
    average_pwr = np.mean(total_pwr[14]) / np.mean(total_pwr[13])
    requirements_average_pwr = 36 * 0.99 < average_pwr < 36 * 1.01
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6646b11fd040ef29de70b59f"
    )
    if not print_figures_for_ptc21_report(
        total_pwr,
        total_freqs,
        total_pwr_beam,
        total_freqs_beam,
        local_directory,
        new_repo_name,
    ):
        print("error generating figures ")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_BEAM_SCAN",
        json.dumps(test_config_beam.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RATIO6O1",
        str(average_pwr),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ6O1",
        str(requirements_average_pwr),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ1",
        str(f"{total_pwr_beam[11][108 - 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ2",
        str(f"{total_pwr_beam[11][108]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ3",
        str(f"{total_pwr_beam[11][108 + 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ4",
        str(f"{total_pwr_beam[11][108 + 96]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1REQ",
        str(total_pwr_beam[11].index(max(total_pwr_beam[11])) == 108),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ1",
        str(f"{total_pwr_beam[12][108 - 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ2",
        str(f"{total_pwr_beam[12][108]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ3",
        str(f"{total_pwr_beam[12][108 + 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ4",
        str(f"{total_pwr_beam[12][108 + 96]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2REQ",
        str(total_pwr_beam[12].index(max(total_pwr_beam[12])) == 108 - 64),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ1",
        str(f"{total_pwr_beam[13][108 - 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ2",
        str(f"{total_pwr_beam[13][108]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ3",
        str(f"{total_pwr_beam[13][108 + 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ4",
        str(f"{total_pwr_beam[13][108 + 96]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3REQ",
        str(total_pwr_beam[13].index(max(total_pwr_beam[13])) == 108 + 64),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ1",
        str(f"{total_pwr_beam[14][108 - 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ2",
        str(f"{total_pwr_beam[14][108]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ3",
        str(f"{total_pwr_beam[14][108 + 64]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ4",
        str(f"{total_pwr_beam[14][108 + 96]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4REQ",
        str(total_pwr_beam[14].index(max(total_pwr_beam[14])) == 108 + 96),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.pdf")
    repo.git.add(new_repo_name + "/beam11.pdf")
    repo.git.add(new_repo_name + "/beam12.pdf")
    repo.git.add(new_repo_name + "/beam13.pdf")
    repo.git.add(new_repo_name + "/beam14.pdf")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def print_figures_for_ptc21_report(
    total_pwr,
    total_freqs,
    total_pwr_beam,
    total_freqs_beam,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    # pylint: disable=too-many-arguments
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    pst_channel_base = total_freqs_beam[11][0]
    ratio = []
    for idx, pwr in enumerate(total_pwr[14]):
        ratio.append(pwr / total_pwr[13][idx])

    plt.plot(total_freqs[11], ratio, label="Ratio 6st/1st")
    plt.hlines(
        36, total_freqs[11][0], total_freqs[11][-1], "r", "--", label="Theoretical"
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[11], total_pwr_beam[11], "g", label="beam 11")
    plt.axvline(
        x=total_freqs_beam[11][0] + 108,
        color="c",
        ls="--",
        label="expected beam 11 spike",
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()

    # plt.axis([0,216,0,1e6])
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam11.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[12], total_pwr_beam[12], "c", label="beam 12")
    plt.axvline(
        x=pst_channel_base + 108 - 64,
        color="g",
        ls="--",
        label="expected beam 12 spike",
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power linear scale)")
    plt.legend()

    # plt.axis([0,216,0,1e6])
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam12.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[13], total_pwr_beam[13], "r", label="beam 13")
    plt.axvline(
        x=pst_channel_base + 108 + 64,
        color="b",
        ls="--",
        label="expected beam 13 spike",
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()

    # plt.axis([0,216,0,1e6])
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam13.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[14], total_pwr_beam[14], "b", label="beam 14")
    plt.axvline(
        x=pst_channel_base + 108 + 96,
        color="r",
        ls="--",
        label="expected beam 14 spike",
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam14.pdf", format="pdf")
    return True
