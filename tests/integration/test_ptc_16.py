# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 16 - Visibility same data for auto and cross.

Based on Jupyter notebook: PI23/FAT1/PTC16/ptc16.ipynb

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #16 <https://docs.google.com/presentation/d/1OTnAaR_fR1FdHuZYjt-Y4Gfw561r_ZnH4BAgdpr9tDg/edit#slide=id.g274ec4c9400_0_278>` _

1. Set up a SPS CNIC to generate a single complex sinusoid for the X polarisation for 2 stations,
    single SPS channel, sinusoid amplitude of 2, frequency = n*5.4kHz where n=-72 to 72 in steps of 1
    (so 145 data sets) where this frequency is relative to centre of the visibility channel closest to the
    centre of SPS channel (=m*781250Hz), add noise of amplitude 1, zero delay, 5 seconds data, identical
    data for both stations
2. For each frequency setup CNIC to transmit SPS packets to the correlator Alveo.
3. Configure the correlator subarray to have 0.849s integrations, with zero delays
4. Set up SDP CNIC to capture data for all 144 fine channels of the SPS channel, auto and cross correlation
    of the two stations
5. Run the correlator scan for 30 seconds for each tone frequency
6. Save the visibilities, inspect to ensure that a majority are zero (due to one polarisation being zero) and
    check if the auto and cross correlation products are equal
7. REQ-127 is achieved if the auto and cross correlations for the visibility channel containing the tone are
    the same to within 0.1dB for all frequencies for all values of n (across the SPS channel)
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import math
from collections import defaultdict
from copy import deepcopy

import pytest

from ska_low_cbf_integration.correlator import integration_periods
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_1ch
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

MAX_STD_DEVIATION = 0.1  # in dB see comment above

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 123  # needs to match scan id in scan_6stn_1sa_1bm_1ch
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
BEAM_ID = 1  # needs to match beam id in scan_6stn_1sa_1bm_1ch
REPORT = "/report.tex"
REPORT2017 = "report2017.tex"
# ---------------------------
# PCAP/debug file names
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc16/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH = DATA_DIR + "PTC16_correlator.pcap"
"""PCAP file name"""


def _cnic_vd_cfg(tone_position: float, stations: list, channels: list) -> dict:
    """Return CNIC-VD configuration for given stations, channels and frequency.

    :param tone_position: tone position
    :param stations: a list of (station, substation) tuples
    :param channels: a list of channels
    :return: a list of dictionaries for CNIC-VD configuration
    """
    fine_freq = int(tone_position)
    cfg = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": chan,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    {"tone": False, "seed": station, "scale": 40},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 4,
                    },
                ],
                "y": [
                    {"tone": False, "seed": station, "scale": 0},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 0,
                    },
                ],
            },
        }
        for station, substation in stations
        for chan in channels
    ]
    return {"sps_packet_version": 3, "stream_configs": cfg}


CHAN = 448
VIS_CHAN_COUNT = 144
CAPTURE_TIME_SEC = 30


@pytest.mark.timeout(CAPTURE_TIME_SEC + 300)
@pytest.mark.hardware_present
def test_ptc16_visibility_auto_cross(
    cnic_and_processor, ready_subarray
):  # pylint: disable=too-many-locals
    """Implement PTC 12: simulate station data, capture packages, run analysis."""
    # basic settings
    n_stations = 2
    station_ids = list(range(1, 1 + n_stations))
    stations = [[station, 1] for station in station_ids]
    channels = [CHAN]
    # tweak the stock standard scan configuration (see corr_configs.py)
    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels
    scan_config["lowcbf"]["stations"]["stns"] = stations
    integration_time_sec = integration_periods[849]

    # output packet related constants
    channel_count = len(channels)
    rx_size = 64
    n_vis = int(CAPTURE_TIME_SEC / integration_time_sec)
    rx_packets = VIS_CHAN_COUNT * n_vis * channel_count

    # Generate / capture packets
    # --------------------------
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=rx_size,
        cnic_capture_packets=rx_packets,
        firmware_type=Personality.CORR,
        scan_config=scan_config,
        vd_config=_cnic_vd_cfg(0, stations, channels),
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,  # proc packets after end of scan
        during_scan_callback=None,
    )
    f_handle = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH,
    )
    f_handle.close()  # Analyser will consume the file's contents

    # Analysis
    # --------
    analyser_cfg = {"coarse_channels": channels, "stations": station_ids}
    visibilities = VisibilityAnalyser(json.dumps(analyser_cfg))
    visibilities.extract_spead_data(PCAP_PATH)

    visibilities.populate_correlation_matrix()

    # put all visibilities across all polarisations inside a single vector
    # so that we can cut in slices of 20s
    flat_version = defaultdict(list)
    for timing in visibilities.time_sequence:
        for i in range(n_stations):
            for j in range(i + 1):
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i][2 * j]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i + 1][2 * j]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i][2 * j + 1]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i + 1][
                        2 * j + 1
                    ]
                )
    # we need to have for each timestamp:
    #   - correlations with Y polarisation  = 0
    #   - auto and cross XX polarisation all equal to same value
    general_check = True
    for _, values in flat_version.items():
        auto_val = []
        for index, val in enumerate(values):
            if index % 4 == 0 and general_check:
                general_check = val != 0.0
                auto_val.append(val)
            elif index % 4 != 0 and general_check:
                general_check = val == 0.0
        # print(set(auto_val))
        general_check = len(set(auto_val)) == 1 and general_check
    print(f"PTC 16 passes if General Check is True so far it is {general_check}")
    ptc16_report(test_config, flat_version, general_check)
    assert general_check


def ptc16_report(test_config, flat_version, general_check):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66f5df26425d1f0799e41804"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    for time_index, values in enumerate(flat_version.values()):
        for index, val in enumerate(values):
            baseline = math.floor(index / 4)
            corr = index % 4
            replace_word_in_report(
                file_result_to_replace, f"M{time_index:02d}B{baseline}C{corr}", f"{val}"
            )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    replace_word_in_report(
        file_result_to_replace,
        "GENERALCHECK",
        str(general_check),
    )
    # apply the above names throughout the function
    repo.git.add(REPORT2017)
    repo.git.add(new_repo_name + REPORT)
    repo.index.commit("Test with python")
    repo.git.push()
