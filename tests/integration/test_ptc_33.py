# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 33 - PST Beam packets flagged without valid poly.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #33 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime

import matplotlib.pyplot as plt
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.psr_analyser import PulsarPacket
from ska_low_cbf_integration.sps import SPS_COARSE_SPACING_HZ

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc33/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC33_beamformer.pcap"
"""PCAP file name"""


TEST_TIMEOUT_SEC = 2000
SAMPLE_RATE = (SPS_COARSE_SPACING_HZ / 216) * (4 / 3)
MAX_NUMBER_SPS_CHANNELS = 384
N_POL = 2
N_VALS_PER_CPLX = 2
N_BYES_PER_VAL = 2

station_ids = list(range(345, 357)) + list(range(429, 435))
PST_BEAM_ID = 11
station_weights = {
    PST_BEAM_ID: [1.0] * 5,
    12: [1.0] * 5,
}
CNIC_FINE_FREQ = 2
SUBARRAY_ID = 1
STATION_BEAM_ID = BeamId.SINGLE_BASIC
TONE_POSITION = 96 + 12
DELAY_EMULATOR_ADDR = "low-cbf/delaypoly/0"
END_EXPECTED_VALUE = 4


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": "-26d19m50.0s",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": "-26d19m50.0s",
    }
    src_radec_beam_3 = {"ra": "2h30m00.00s", "dec": "-26d19m50.0s"}
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": "-26d19m50.0s",
    }
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }
    pstdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }

    return beamdir, srcdir, pstdirs


# Generate Scan Configuration
NB_STATION = 5
stations = [[station, 1] for station in station_ids[:NB_STATION]]
channels = [64]
pst_beam_id = [11]
PST_SVR_IP = "192.168.55.55"
delay_sub = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}"
pst_sub = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}_"
scan_config_same_beam = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": STATION_BEAM_ID,
                    "freq_ids": channels,
                    "delay_poly": delay_sub,
                }
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": pst_beam,
                    "stn_beam_id": STATION_BEAM_ID,
                    "delay_poly": pst_sub + str(station_idx + 1),
                    "jones": "tbd",
                    "destinations": [
                        {
                            "data_host": PST_SVR_IP,
                            "data_port": 11001,
                            "start_channel": 0,
                            "num_channels": 144,
                        }
                    ],
                    "stn_weights": station_weights[pst_beam],
                }
                for station_idx, pst_beam in enumerate(pst_beam_id)
            ],
        },
    },
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc33_pst_validity_flags(cnic_and_processor, ready_subarray):
    """
    Implement PTC 33.

    In this test, we aim to show that the beam data is flagged correctly
    when delay polynomials are valid or not.
    """
    print(datetime.now(), "Starting PTC#33")

    # Generate CNIC-VD Configuration
    cnic_config_noise = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": STATION_BEAM_ID,
            "sources": {
                "x": [
                    {"tone": False, "seed": 2000 + channel, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 2000 + channel, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]

    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=1356 * len(channels) * len(pst_beam_id) * 180,
        scan_config=scan_config_same_beam,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=change_delay_poly_time,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
    )
    _ = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH_SAME_BEAM,
    )
    print(datetime.now(), "Captured PTC#33 output, beginning analysis")
    valid_flag = _ptc33_analysis()
    last_value_to_compare = _ptc33_report(scan_config_same_beam, valid_flag)
    assert last_value_to_compare == END_EXPECTED_VALUE
    print(datetime.now(), "End of PTC#33")


def change_delay_poly_time(cnic, processor):
    """Change delay time to mess with polynomials."""
    packets_received = cnic.hbm_pktcontroller__rx_packet_count
    print(f"Packets Received: {packets_received}")
    print(f"Stat delay {processor.stats_delay}")
    delay = tango.DeviceProxy(DELAY_EMULATOR_ADDR)
    delay.SetSecondsAfterEpoch((18 * 60 + 30) * 60)


def _ptc33_analysis(filename=PCAP_PATH_SAME_BEAM):
    payloads = get_udp_payload_bytes(filename, 64)
    valid_flag = valid_flag_analysis(payloads)
    return valid_flag


def valid_flag_analysis(payloads):
    """Analyse validity per packet."""
    valid_flag = {}
    for pkt_payload in payloads:
        psr_packet = PulsarPacket(pkt_payload)
        first_chan = psr_packet.first_channel
        if first_chan not in valid_flag:
            valid_flag[first_chan] = {}
        timestamp = psr_packet.samples * psr_packet.numerator / psr_packet.denominator
        valid_flag[first_chan][timestamp] = psr_packet.validity
    return valid_flag


def _ptc33_report(scan_config, valid_flag):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6722b7c76ab2464a5dbe614c"
    )

    if not print_figures_for_ptc33_report(
        valid_flag,
        local_directory,
        new_repo_name,
    ):
        print("ERROR when printing figures")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    last_value = END_EXPECTED_VALUE
    for validities in valid_flag.values():
        valid_flags = list(validities.values())
        last_value = max(int(valid_flags[-1]), last_value)
    replace_word_in_report(
        file_result_to_replace,
        "ENDFLAGVALUE",
        str(last_value),
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/validity.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return last_value


def print_figures_for_ptc33_report(
    valid_flag,
    local_directory: str,
    new_repo_name: str,
):
    """Print the figures for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    for first_chan, validities in valid_flag.items():
        x_axis = validities.keys()
        y_axis = validities.values()
        plt.plot(x_axis, y_axis, label=f"1st channel {first_chan}")

    plt.xlabel("Time (us)")
    plt.ylabel("Validity Flag ")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/validity.png", format="png")
    plt.close()

    return True
