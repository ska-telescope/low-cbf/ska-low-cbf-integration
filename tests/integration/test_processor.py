# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""Test FPGA Firmware via Processor Device."""
import copy
import datetime
import json
import os
from copy import deepcopy
from dataclasses import dataclass
from time import sleep, strftime
from typing import BinaryIO, Callable, Iterable, List, Optional, Tuple

import pytest
import tango
from ska_tango_base.control_model import ObsState

from ska_low_cbf_integration.connector import (
    assert_packet_counts_zero,
    get_port_packet_counts,
)
from ska_low_cbf_integration.delay_poly import (
    config_beam_delay,
    config_pst_delay,
    config_source_delay,
    wait_for_delay_poly,
)
from ska_low_cbf_integration.firmware import (
    Personality,
    scan_config_keys,
    version_under_test,
)
from ska_low_cbf_integration.low_psi import device_ports, get_connector_proxy
from ska_low_cbf_integration.subarray import empty_subarray
from ska_low_cbf_integration.tango import (
    DeviceProxyJson,
    get_proxies_for_class,
    wait_for_attr,
)

from .corr_configs import scan_6stn_1sa_1bm_1ch

TEST_TIMEOUT_SEC = 900
DELAY_EMULATOR_ADDR = "low-cbf/delaypoly/0"
CONN_TIMEOUT_MS = 20_000  # takes more than the default 3s to confgure numerous routes
DUMMY_VERSION = {
    "CNIC": "0.1.13-main.b1f004fd",
    Personality.CORR: "0.0.8",
    Personality.PSS: "0.0.4-main.64b811e8",
    Personality.PST: "1.0.2-main.67067e21",
}

LIMIT_NR_OF_VC = False  # debug: reduce # of virtual channels


def sanitise_filename(raw: str, substitute="-") -> str:
    """
    Make a string safe(r) to use as a filename.

    :param raw: Desired filename, potentially containing invalid characters.
    :param substitute: Substituted in place of bad characters.
    """
    allowed_extras = [" ", "_", "-", ":", ".", "[", "]"]
    sanitised = ""
    for char in raw:
        if char.isalnum() or char in allowed_extras:
            sanitised += char
        else:
            sanitised += substitute
    return sanitised


def log_registers(processor: tango.DeviceProxy):
    """
    Log a Processor device's FPGA register activity.

    Log will be created at:
    "/test-data/<short commit hash> <timestamp> <test name>.log"
    """
    # TODO - consider a context handler so we can turn off logging automatically?
    current_test = os.environ.get("PYTEST_CURRENT_TEST", "no-pytest-test")
    current_test = current_test.removesuffix("(call)").strip()
    commit = os.environ.get("CI_COMMIT_SHORT_SHA", "no-ci-commit")
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log_name = sanitise_filename(f"{commit} {timestamp} {current_test}.log")
    log_path = os.path.join("/test-data", log_name)
    print(f"Logging FPGA Registers to: {log_path}")
    processor.StartRegisterLog(log_path)


@dataclass(kw_only=True)
class ProcessorTestConfig:
    """Specification for a Processor Test Scenario."""

    # pylint: disable=too-many-instance-attributes

    input_packet_count: Optional[int] = None
    """Number of packets sent from CNIC to Processor."""
    start_scan_packet_count: Optional[int] = None
    """Expected packets sent from Processor at start of Scan."""
    before_end_scan_packet_count: Optional[int] = None
    """Expected packets sent from Processor before end of Scan."""
    output_packet_count: Optional[int] = None
    """Expected total packets sent from Processor (after end of Scan)."""
    cnic_capture_packets: int
    """Number of packets for CNIC to receive."""
    cnic_capture_min_size: int
    """Minimum packet size filter for CNIC"""
    scan_config: dict
    firmware_type: Personality
    nb_cnics: int = 1
    """Number of CNIC required"""
    nb_processors: int = 1
    """Number of Processors required"""
    do_fw_switcharoo: Optional[bool] = False
    """Force reload of firware before the test."""


@dataclass(kw_only=True)
class ProcessorPcapTestConfig(ProcessorTestConfig):
    """Test scenario using PCAP file as Processor input."""

    input_pcap: str
    input_rate: float  # Gbps


@dataclass(kw_only=True)
class ProcessorVdTestConfig(ProcessorTestConfig):
    """Test scenario using Virtual Digitiser as Processor input."""

    vd_config: dict | List[dict]
    """
    Virtual Digitiser configuration.
    New format - {"stream_configs": List[dict], [optional other params]}
    Old format - list of one dict per SPEAD stream
    """
    during_scan_callback: Optional[Callable] = None
    """Callback function to execute during Scan. Called with (cnic, processor)."""
    # TODO - it would probably be nicer to bundle up pulsar mode (and other bits of)
    #  configuration in a new vd_config scheme...
    #  see https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-tango-cnic/-/issues/8
    pulsar_config: Optional[Tuple[int, int, int]] = None
    """Optional tuple of pulsar mode parameters."""


@dataclass(kw_only=True)
class ProcessorVdStaticTrackingTestConfig(ProcessorVdTestConfig):
    """Test scenario using static delay in delay device."""

    source_delay_config: [dict]
    """Delay polynomial static configuration for the CNIC"""
    beam_delay_config: [dict]
    """Delay polynomial static configuration for the Correlator"""
    pst_delay_config: Optional[dict] = None
    """Delay polynomial pst configuration for the Beamformer"""


@dataclass(kw_only=True)
class ProcessorVdStaticMulticastTestConfig(ProcessorVdTestConfig):
    """Test scenario using Virtual Digitiser and multicast routing."""

    spead_multicast_routing: list
    """Connector multicast routing"""


@dataclass(kw_only=True)
class ProcessorVdStaticTrackingMulticastTestConfig(ProcessorVdStaticTrackingTestConfig):
    """Test scenario using Virtual Digitiser and multicast routing."""

    spead_multicast_routing: list
    """Connector multicast routing"""


def cnic_processor(
    cnic_processor_devices: (List[DeviceProxyJson], List[DeviceProxyJson]),
    subarray: DeviceProxyJson,
    test_config: ProcessorPcapTestConfig | ProcessorVdTestConfig,
    pcap_file_name: str = "/test-data/ci-rx.pcap",
) -> BinaryIO:
    """
    Run test scenario through Processor(s), CNIC providing input and capturing output.

    :param subarray: Subarray device
    :param cnic_processor_devices: CNIC & Processor devices
    :param test_config: Test configuration parameters
    :param pcap_file_name: where to write the received data
    :returns: file object of FPGA output packets
    """
    # pylint: disable=too-many-branches,too-many-statements,too-many-locals
    # we could split some parts into functions (start_cnic_tx ?),
    # but I'm not sure if it's worthwhile

    def limit_virtual_channels(n_vch: int = 900) -> None:
        """Debugging: limit the number of virtual channels."""
        fw_key = "vis"  # assume correlator ...
        # ... but override if it isn't
        if (fw_type := test_config.firmware_type.name.lower()) in ("pss", "pst"):
            fw_key = fw_type
        alloc = DeviceProxyJson("low-cbf/allocator/0")
        limits = json.loads(alloc.internalalveolimits)
        limits.update({fw_key: {"vch": n_vch}})
        limits_str = json.dumps(limits, indent=2)
        fname = f"{os.path.dirname(pcap_file_name)}/allocator.txt"
        with open(fname, "w+", encoding="ascii") as ofd:
            ofd.write(f"UPDATED allocator.internalAlveoLimits\n{limits_str}\n")
        alloc.internalalveolimits = limits_str

    cnics, processors = cnic_processor_devices
    assert len(cnics) >= test_config.nb_cnics
    assert len(processors) >= test_config.nb_processors, "Deploy more Processors!"
    assert len(processors) >= test_config.nb_processors
    # ignore processors that the test doesn't need to use
    processors = processors[: test_config.nb_processors]
    cnic = cnics[0]
    connector = get_connector_proxy()
    connector.set_timeout_millis(CONN_TIMEOUT_MS)
    cnic_port, processor_port = device_ports([cnic, processors[0]])
    print(f"First CNIC port: {cnic_port}, First Processor port: {processor_port}")
    try:
        cnic.StopSourceDelays()  # TODO - handle exception in CNIC Tango device instead
    except tango.DevFailed:
        pass

    configure_delays_for_test(test_config)
    if test_config.do_fw_switcharoo:
        temp_config = copy.deepcopy(test_config)
        configure_scan_for_test(subarray, temp_config, load_dummy=True)
        empty_subarray(subarray)
        # removing the CNIC flashing as not stable enough
        # for cnic in cnics:
        #     load_firmware_and_reset(cnic, DUMMY_VERSION["CNIC"])
        #     wait_until_fpga_ready(cnic)
        #     sleep(30)
        #     load_firmware_and_reset(cnic)
        #     wait_until_fpga_ready(cnic)
        #     cnic.hbm_pktcontroller__duplex = True

    configure_scan_for_test(subarray, test_config)
    check_processors_firmware(test_config, processors)

    if LIMIT_NR_OF_VC:
        # debug: scale back the number of virual channels
        limit_virtual_channels(n_vch=900)

    connector.ResetPortStatistics()

    spead_unicast_route_cfg = {}
    if test_config.nb_processors > 1:
        # use allocator for the routing
        spead_unicast_route_cfg = _add_spead_unicast_routing(connector, cnic_port)

    assert_packet_counts_zero(connector, device_ports(cnics + processors))

    for processor_ in processors:
        if is_fpga_log_enabled():
            log_registers(processor_)
        _reset_processor(processor_)
        _reset_ingress_counters(processor_)

    # CNIC Rx
    cnic.CallMethod(
        {
            "method": "receive_pcap",
            "arguments": {
                "out_filename": pcap_file_name,
                "packet_size": test_config.cnic_capture_min_size,
                "n_packets": test_config.cnic_capture_packets,
            },
        }
    )
    if isinstance(test_config, ProcessorVdStaticTrackingTestConfig):
        delay = tango.DeviceProxy(DELAY_EMULATOR_ADDR)
        wait_for_delay_poly(delay, test_config.scan_config)
    subarray.Scan({"id": test_config.scan_config["id"]})
    wait_for_attr(subarray, "obsState", ObsState.SCANNING, "Not Scanning")
    _check_packet_counts(
        connector, test_config.start_scan_packet_count, processor_port, cnic_port
    )

    if isinstance(test_config, ProcessorPcapTestConfig):
        _tx_pcap(cnic, test_config)
    elif isinstance(test_config, ProcessorVdTestConfig):
        cnic.ConfigureVirtualDigitiser(test_config.vd_config)
        if isinstance(
            test_config,
            (
                ProcessorVdStaticMulticastTestConfig,
                ProcessorVdStaticTrackingMulticastTestConfig,
            ),
        ):
            # needs to be fixed for PTC7
            for route in test_config.spead_multicast_routing:
                route["dst"]["port_bf"] = cnic_port
                route["dst"]["port_corr"] = processor_port
            connector.UpdateMultiplierUnicastEntry(
                spead=test_config.spead_multicast_routing
            )
        if test_config.pulsar_config is None:
            cnic.ConfigurePulsarMode({"enable": False})
        else:
            cnic.ConfigurePulsarMode(
                {"enable": True, "sample_count": test_config.pulsar_config}
            )
        if isinstance(test_config, ProcessorVdStaticTrackingTestConfig):
            print("start source delays")
            cnic.StartSourceDelays(DELAY_EMULATOR_ADDR)
            wait_for_attr(cnic, "enable_vd", value=True, max_duration=60)
        else:
            # CNIC SW needs a manual start signal when not using delay subscription...
            cnic.enable_vd = True
            cnic.vd_datagen__enable_vd = True
    else:
        raise NotImplementedError("Unknown Test Configuration Type")

    if (
        hasattr(test_config, "during_scan_callback")
        and test_config.during_scan_callback is not None
    ):
        test_config.during_scan_callback(cnic, processors[0])

    # Wait for Scan packets to be received.
    # This is a bit messy - PTC 11 uses finished_receive,
    # but in general we may (?) want to capture SPEAD end packets...
    if test_config.before_end_scan_packet_count is not None:
        wait_for_attr(
            cnic,
            "hbm_pktcontroller__rx_packet_count",
            test_config.before_end_scan_packet_count,
        )
    else:
        wait_for_attr(cnic, "finished_receive", max_duration=600, sleep_per_loop=1)
    print(strftime("%H:%M:%S"), "Rx done")

    if isinstance(test_config, ProcessorVdTestConfig):
        cnic.enable_vd = False
        # FIXME ? These routes will not be cleaned up if either of the above
        #  wait_for_attr calls time out (or any other exception since route config.)
        if spead_unicast_route_cfg:
            connector.RemoveSpeadUnicastEntry(spead_unicast_route_cfg)
        if isinstance(test_config, ProcessorVdStaticMulticastTestConfig):
            connector.RemoveSpeadMultiplierEntry(
                spead=test_config.spead_multicast_routing
            )

    # check total packets
    _check_packet_counts(
        connector,
        test_config.before_end_scan_packet_count,
        processor_port,
        cnic_port,
    )
    _check_packet_counts(
        connector, test_config.input_packet_count, cnic_port, processor_port
    )

    empty_subarray(subarray)  # ends scan
    for processor_ in processors:
        processor_.StopRegisterLog()  # not always needed, but shouldn't hurt

    _check_packet_counts(
        connector, test_config.output_packet_count, processor_port, cnic_port
    )

    return open(pcap_file_name, "rb")


def is_fpga_log_enabled():
    """Check if we should we log FPGA register transactions."""
    return os.environ.get("FPGA_LOG_REGISTERS").lower() in ["1", "true"]


def configure_delays_for_test(test_config: ProcessorTestConfig) -> None:
    """Configure our delay emulator for a test, if needed."""
    if isinstance(test_config, ProcessorVdStaticTrackingTestConfig):
        # Configure delay polynomial source, if test uses delays
        delay = tango.DeviceProxy(DELAY_EMULATOR_ADDR)
        # here to loop on the various beams
        for beam_delay_config in test_config.beam_delay_config:
            config_beam_delay(delay, beam_delay_config)
        for source_delay_config in test_config.source_delay_config:
            config_source_delay(delay, source_delay_config)
        delay.SetSecondsAfterEpoch((9 * 60 + 30) * 60)
        if test_config.pst_delay_config:
            if isinstance(test_config.pst_delay_config, list):
                print("pss delays are list")
                for pst_dly_cfg in test_config.pst_delay_config:
                    config_pst_delay(delay, pst_dly_cfg)
            else:
                print("pss delays are NOT list")
                config_pst_delay(delay, test_config.pst_delay_config)


def _add_spead_unicast_routing(
    connector: DeviceProxyJson, cnic_port: str, p4_id: str = "p4_01"
) -> dict:
    """
    Configure SPEAD routing from CNIC to multiple Processors.

    Uses Allocator device to decide routes.

    :returns: Routing configuration for later use in clean-up.
    """
    # remove the default Basic route from first CNIC to first Processor
    connector.RemoveBasicEntry({"basic": [{"src": {"port": cnic_port}}]})
    # getting SPS-to-processor routing from allocator
    allocator = DeviceProxyJson("low-cbf/allocator/0")
    p4_routes = allocator.p4_stn_routes
    spead_cfg = {
        "spead": [
            {
                "src": {
                    "frequency": route[0][2],
                    "beam": route[0][1],
                    "sub_array": route[0][0],
                },
                "dst": {"port": route[1][0]},
            }
            for route in p4_routes[p4_id]
        ]
    }
    try:
        print(f"{strftime('%H:%M:%S')} configure P4 routes")
        connector.UpdateSpeadUnicastEntry(spead_cfg)
    except tango.DevFailed as exception:
        print(
            f"{strftime('%H:%M:%S')} Connector timed out after {CONN_TIMEOUT_MS} ms;"
            f" {exception}"
        )
    return spead_cfg


def configure_scan_for_test(
    subarray: DeviceProxyJson,
    test_config: ProcessorTestConfig,
    load_dummy: bool = False,
):
    """
    Call ConfigureScan on the Subarray, after tweaking the config to suit our test.

    :param subarray: Subarray proxy
    :param test_config: Configuration of current test
    :param processors: Processor proxies (system under test)
    """
    if subarray.obsState == ObsState.EMPTY:
        subarray.AssignResources("{}")
        wait_for_attr(subarray, "obsState", ObsState.IDLE)
    fw_type = test_config.firmware_type
    # Personality enum names are acceptable to the Allocator (in lowercase)
    # this gives a firmware request string like: "corr:1.2.3-main.456789ab:gitlab"
    if load_dummy:
        fw_request = f"{fw_type.name.lower()}:{DUMMY_VERSION[fw_type]}:gitlab"
    else:
        fw_request = f"{fw_type.name.lower()}:{version_under_test(fw_type)}:gitlab"
    print(f'Using firmware request: "{fw_request}"')

    # which part of the Low CBF ConfigureScan message do we need to change?
    fw_config_key = scan_config_keys[fw_type]

    # modify scan configuration to use latest/requested version
    configure_scan = test_config.scan_config
    configure_scan["lowcbf"][fw_config_key]["firmware"] = fw_request

    subarray.Configure(configure_scan)
    wait_for_attr(subarray, "obsState", ObsState.READY, "Subarray not ready")


def _check_packet_counts(
    connector: tango.DeviceProxy | DeviceProxyJson,
    expected_value: Optional[int],
    src_port: str,
    dst_port: str,
) -> None:
    """
    Check packet counts are as expected.

    :param connector: Connector device proxy
    :param expected_value: packet counts must equal this, skip test if None
    :param src_port: Port of device that is sending the packets
    :param dst_port: Port of device that is receiving the packets
    :raises AssertionError: if counts don't match
    """
    counts = get_port_packet_counts(connector, [src_port, dst_port])
    if expected_value is not None:
        # tx/rx here are relative to the switch, not our devices
        assert counts[src_port].rx == expected_value
        assert counts[dst_port].tx == expected_value


def _reset_processor(processor: DeviceProxyJson):
    """Reset the Processor's signal chain."""
    if processor.stats_mode.get("fw_personality") == "CORR":
        processor.DebugRegWrite(
            {"name": "corr_ct1.full_reset", "offset": 0, "value": 1}
        )
        processor.DebugRegWrite(
            {"name": "lfaadecode100g.lfaa_decode_reset", "offset": 0, "value": 1}
        )
        processor.DebugRegWrite(
            {"name": "lfaadecode100g.lfaa_decode_reset", "offset": 0, "value": 0}
        )
        processor.DebugRegWrite(
            {"name": "corr_ct1.full_reset", "offset": 0, "value": 0}
        )


def _reset_ingress_counters(processor: DeviceProxyJson):
    """Reset FPGA ingress counters."""
    registers = (
        "spead_v1_packet_found",
        "spead_v2_packet_found",
        "spead_v3_packet_found",
        "nonspead_packet_count",
        "spead_packet_count",
    )
    for reg in registers:
        try:
            processor.DebugRegWrite(
                {"name": f"lfaadecode100g.{reg}", "offset": 0, "value": 0}
            )
        except tango.DevFailed:
            print(f"Couldn't reset {reg}")


def _tx_pcap(cnic: DeviceProxyJson, test_config: ProcessorPcapTestConfig):
    # CNIC Tx  - split into prepare/begin because large files were timing out when
    # using the blocking 'transmit_pcap' method
    cnic.CallMethod(
        {
            "method": "prepare_transmit",
            "arguments": {
                "in_filename": test_config.input_pcap,
                "rate": test_config.input_rate,
            },
        }
    )
    wait_for_attr(cnic, "ready_to_transmit", failure_message="Tx file not loaded")
    cnic.CallMethod({"method": "begin_transmit", "arguments": {}})
    wait_for_attr(
        cnic,
        "finished_transmit",
        failure_message="Transmit not complete",
    )


def check_processors_firmware(
    config: ProcessorTestConfig, processors: Iterable[DeviceProxyJson]
) -> None:
    """
    Check that the Processors used by a test have loaded the requested firmware.

    :raises: AssertionError if the expected firmware is not loaded.
    """
    type_personality = {"VIS": "CORR"}
    """Translate test spec (allocator terms) to different FPGA personality name."""

    # Collect (personality, version) for all Processors
    active_firmware = [
        (
            processor.stats_mode.get("fw_personality", "").upper().strip(),
            processor.stats_mode.get("fw_version"),
        )
        for processor in processors
    ]

    # default to firmware_type (in uppercase), but check for translations
    request = config.firmware_type.name.upper()
    expected_personality = type_personality.get(request, request)

    expected_firmware = (expected_personality, version_under_test(config.firmware_type))
    # avoid passing config to assert, as it can be extremely verbose on failure
    n_expected = config.nb_processors

    assert active_firmware.count(expected_firmware) >= n_expected, (
        f"Did not see {n_expected} Processors with firmware {expected_firmware}.\n"
        f"Active firmware: {active_firmware}.\n"
        f"Serial numbers: {[p.serialNumber for p in processors]}.\n"
    )


# iterate over different ways to specify FW version:
@pytest.mark.parametrize(
    "fw_request",
    (
        "vis:0.1.0:nexus",
        "vis:ska-low-cbf-fw-corr-u55c-0.1.0.tar.gz:nexus",
        "vis::nexus",
    ),
)
@pytest.mark.hardware_present
def test_processor_download_time(ready_subarray, fw_request):
    """Download Alveo firmware package (from CAR).

    Check the processor becomes ready within the required time window
    (20 seconds).
    Deployment should ensure FW package caching is disabled.
    """
    proc_ready_time = 20
    """The time (seconds) in which processor needs to be ready after Subarray
    configuration"""

    processors = get_proxies_for_class("LowCbfProcessor")
    assert len(processors) > 0, "No processors? (check deployment)"
    processor = processors[0]
    processor.set_timeout_millis(120_000)

    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    scan_config["lowcbf"]["vis"]["firmware"] = fw_request

    ready_subarray.Configure(scan_config)
    # NOTE: in theory wait_for_attribute_value should work here (it works from
    #       the CLI), in practice it doesn't; from a failed psi-low-test log:
    #
    #    try:
    #        if device.read_attribute(attribute).value == value:
    #            break
    #    except DevFailed as err_:
    # >       raise AttributeError from err_
    # E               AttributeError
    # src/ska_low_cbf_integration/tango.py:200: AttributeError
    # ---
    # however the below loop works:
    # add 10 seconds on top so if/when it fails we get an idea how long
    # DOES it take to downlaod
    for i in range(proc_ready_time + 10):
        sleep(1)
        try:
            if getattr(processor, "function_firmware_loaded", None):
                break  # all is well
        except Exception as exc:  # pylint: disable=broad-exception-caught
            print(f"EXCEPTION {exc}")
        print(f"{i:02} are we there yet?")
    assert processor.function_firmware_loaded
    assert i <= proc_ready_time
