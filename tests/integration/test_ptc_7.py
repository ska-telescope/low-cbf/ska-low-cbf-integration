# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 7 - Correlator Timing Accuracy.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #7 Presentation <https://docs.google.com/presentation/d/1zTJ3jEDTSiE7D9Gvx71l8_c2ErBP5ymtmLQzGsPNe-4>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from math import ceil

import matplotlib.pyplot as plt
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import pcap_timestamps
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 450

station_to_channels = {22: 7, 20: 8, 16: 13, 12: 24, 8: 54, 6: 96}
"""number of stations: number of channels"""

station_ids = [
    18,
    34,
    21,
    42,
    54,
    72,
    9,
    69,
    12,
    131,
    150,
    55,
    44,
    98,
    123,
    22,
    25,
    107,
    147,
    75,
    60,
    17,
]

source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}


@pytest.mark.large_capture
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc7_a_corr_timing_accuracy(cnic_and_processor, ready_subarray):
    """
    Implement PTC 7.

    This is basically the same as one scenario from PTC 22,
    but with different analysis on the output.
    (Maybe one day the commonality could be exploited to speed up a bit)
    """
    # pylint: disable=too-many-locals
    print(datetime.now(), "Starting PTC#7")
    n_stations = 6
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 6
    first_channel = ceil(230 - n_channels / 2)
    channels = list(range(first_channel, first_channel + n_channels))
    # Generate CNIC-VD Configuration
    cnic_config = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config = {
        "id": 1235,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": channels,
                        "delay_poly": "low-cbf/delaypoly/0/delay_s01_b01",
                    },
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "22.22.22.22"]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # 101 batches of visibility data per stream, plus SPEAD init packets
        cnic_capture_packets=144 * n_channels * (101 + 1),
        scan_config=scan_config,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        source_delay_config=[source_delay_config],
        beam_delay_config=[beam_delay_config],
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    print(datetime.now(), "Captured PTC#7 output, beginning analysis")
    visibilities = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels,
                "stations": station_ids[:n_stations],
            }
        )
    )
    print(datetime.now(), "VisibilityAnalyser created")
    visibilities.extract_spead_data(correlator_output.name)
    print(datetime.now(), "SPEAD data extracted")
    visibilities.populate_correlation_matrix()  # TODO - required?
    print(datetime.now(), "Correlation matrix populated")
    visibilities.save_heatmap_to_disk(mode="average_all")
    print(datetime.now(), "Heatmap saved")

    error_margin = 0.02  # Requirement is 2% accuracy
    expected_delta = 0.849  # seconds
    expected_ns = expected_delta * 1_000_000_000
    # timestamps according to SPEAD metadata
    timestamps = sorted(visibilities.corr_abs_matrixes_time.keys())
    print(datetime.now(), len(timestamps), "SPEAD timestamps", timestamps)
    for previous_time, time in zip(timestamps, timestamps[1:]):
        # SPEAD metadata uses nanoseconds
        assert time - previous_time == pytest.approx(expected_ns, rel=error_margin)

    # timestamps according to CNIC Rx time
    # Note: we don't need to seek back to zero before reading timestamps,
    # because VisibilityAnalyser only uses the *name* of our file object.
    # skip SPEAD init packets
    rx_timestamps = pcap_timestamps(correlator_output, start_packet=n_channels * 144)
    # grab first packet of each integration period
    integration_packets = n_channels * 144
    deltas = [
        j - i
        for i, j in zip(
            rx_timestamps[::integration_packets],
            rx_timestamps[integration_packets::integration_packets],
        )
    ]
    print(datetime.now(), len(deltas), "Rx timestamp deltas", deltas)
    # deltas will be Decimal, which can't be directly compared to float by approx
    average = float(sum(deltas) / len(deltas))
    assert average == pytest.approx(expected_delta, rel=error_margin)
    averages = [float(sum(deltas[:i]) / len(deltas[:i])) for i in range(1, len(deltas))]
    _ptc7_report(
        test_config,
        timestamps,
        rx_timestamps,
        averages,
    )

    print(datetime.now(), "End of PTC#7")


def _ptc7_report(
    test_config,
    timestamps,
    rx_timestamps,
    average,
):
    """Generate Test Report for PTC7."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "653da1212a47cc70d3de3259"
    )
    if not print_figures_for_ptc7_report(
        timestamps, rx_timestamps, average, local_directory, new_repo_name
    ):
        print("error generating figures ")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/timestamps_integration.pdf")
    repo.git.add(new_repo_name + "/timestamps_analysis.pdf")
    repo.git.add(new_repo_name + "/average_jitter.pdf")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def print_figures_for_ptc7_report(
    timestamps,
    timestamp_analysis,
    average_times,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    plt.clf()
    plt.plot(timestamps, "b-")
    plt.ylabel("timestamp after epoch (ns)")
    plt.xlabel("integration number")
    plt.savefig(
        local_directory + new_repo_name + "/timestamps_integration.pdf", format="pdf"
    )

    plt.clf()
    plt.plot(timestamp_analysis, "b-")
    plt.ylabel("Receiving timestamps")
    plt.xlabel("Packet number")
    plt.savefig(
        local_directory + new_repo_name + "/timestamps_analysis.pdf", format="pdf"
    )

    plt.clf()
    plt.plot(average_times, "-r", label="average time between integrations")
    plt.axhline(
        y=0.84934656, color="b", linestyle="--", label="integration period (target)"
    )
    plt.ylabel("Average time between integration")
    plt.xlabel("Number of Integrations to calculate average")
    plt.legend()
    plt.show()
    plt.savefig(local_directory + new_repo_name + "/average_jitter.pdf", format="pdf")

    return True
