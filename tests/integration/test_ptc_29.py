# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 29 - PST Beam Metadata.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #29 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from collections import defaultdict
from datetime import datetime
from typing import BinaryIO

import matplotlib.pyplot as plt
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import payloads, pcap_stats
from ska_low_cbf_integration.psr_analyser import PulsarPacket

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc29/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC29_beamformer.pcap"
"""PCAP file name"""

TEST_TIMEOUT_SEC = 2000

station_ids = list(range(345, 357)) + list(range(429, 435))

station_weights = {
    11: [1.0] * 18,
    12: [1.0] * 18,
}

DELAY_SUB = "low-cbf/delaypoly/0/delay_s01_b02"
PST_SUB = "low-cbf/delaypoly/0/pst_s01_b02_"

NB_STATIONS = 18
stations = [[station, 1] for station in station_ids[:NB_STATIONS]]
channels = [64]
pst_beam_ids = [11, 12]
PST_SERVER_IP = "192.168.55.55"

# Generate CNIC-VD Configuration
cnic_config_noise = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 0,
        "subarray": 1,
        "station": station,
        "substation": substation,
        "frequency": channel,
        "beam": 2,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 4000},
                {"tone": False, "seed": 1982, "scale": 2000},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 4000},
                {"tone": False, "seed": 1982, "scale": 2000},
            ],
        },
    }
    for station, substation in stations
    for channel in channels
]
# Generate Scan Configuration
scan_config_same_beam = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {"beam_id": 2, "freq_ids": channels, "delay_poly": DELAY_SUB}
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": pst_beam,
                    "stn_beam_id": 2,
                    "delay_poly": PST_SUB + str(station_idx + 1),
                    "jones": "tbd",
                    "destinations": [
                        {
                            "data_host": PST_SERVER_IP,
                            "data_port": 11001,
                            "start_channel": 0,
                            "num_channels": 144,
                        }
                    ],
                    "stn_weights": station_weights[pst_beam],
                }
                for station_idx, pst_beam in enumerate(pst_beam_ids)
            ],
        },
    },
}


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": "-26d49m50.0s",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": "-27d49m50.0s",
    }
    src_radec_beam_3 = {"ra": "2h30m00.00s", "dec": "-28d49m50.0s"}
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": "-29d49m50.0s",
    }
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {"subarray_id": 1, "beam_id": 2, "direction": beam_radec}

    srcdir = {"subarray_id": 1, "beam_id": 2, "direction": pst_radecs}

    pstdirs = {"subarray_id": 1, "beam_id": 2, "direction": pst_radecs}

    return beamdir, srcdir, pstdirs


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc29_pst_beam_metadata(cnic_and_processor, ready_subarray):
    """
    Implement PTC 29.

    In this test we aim at verifying that:
    * Metadata shall make the packet contents self-describing and includes time,
    data type, frequency, bandwidth data dimensions and scale parameters
    * The length of each UDP packet is correct
    """
    # pylint: disable=consider-using-with
    print(datetime.now(), "Starting PTC#29")
    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=1356 * len(channels) * len(pst_beam_ids) * 5,
        scan_config=scan_config_same_beam,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
    )
    results = metadata_per_beam(
        cnic_processor(
            cnic_and_processor,
            ready_subarray,
            test_config,
            PCAP_PATH_SAME_BEAM,
        )
    )

    for key in (
        "numerator",
        "denominator",
        "validity",
        "reserved",
        "separation",
        "valid_channels",
        "number_time_samples",
        "data_precs",
        "version",
        "scan_id",
        "magic",
        "offset_1_2",
        "offset_3_4",
    ):
        print(key, results[key])

    stats = pcap_stats(open(PCAP_PATH_SAME_BEAM, "rb"))
    print(f"pcap statistics: {stats}")
    words_to_replace = _ptc29_report(scan_config_same_beam, results, stats)
    for key, value in words_to_replace.items():
        if key.startswith("REQ"):
            assert value
    print(datetime.now(), "End of PTC#29")


def metadata_per_beam(data: BinaryIO) -> (list, list):
    """Get all metadata and power for all beams."""
    # pylint: disable=too-many-locals
    print(datetime.now(), "Beginning metadata per beam analysis")
    total_timestamps = defaultdict(list)
    numerator = defaultdict(set)
    denominator = defaultdict(set)
    validity = defaultdict(set)
    reserved = defaultdict(set)
    separation = defaultdict(set)
    n_channels = defaultdict(set)
    valid_channels = defaultdict(set)
    number_time_samples = defaultdict(set)
    data_precs = defaultdict(set)
    version = defaultdict(set)
    scan_id = defaultdict(set)
    magic = defaultdict(set)
    offset_1_2 = defaultdict(set)
    offset_3_4 = defaultdict(set)
    sequence_number = defaultdict(list)
    scale = defaultdict(list)
    chan_pkts = defaultdict(lambda: 0)

    for pkt_payload in payloads(data):
        psr_packet = PulsarPacket(pkt_payload)
        beam_number = psr_packet.beam_id
        first_chan = psr_packet.first_channel
        chan_pkts[first_chan] += 1
        # Samples-since-SKA-Epoch x Period Numerator / Period Denominator
        total_timestamps[beam_number].append(
            psr_packet.samples * psr_packet.numerator / psr_packet.denominator
        )
        numerator[beam_number].add(psr_packet.numerator)
        denominator[beam_number].add(psr_packet.denominator)
        validity[beam_number].add(psr_packet.validity)
        reserved[beam_number].add(psr_packet.reserved)
        separation[beam_number].add(psr_packet.separation)
        n_channels[beam_number].add(psr_packet.n_channels)
        valid_channels[beam_number].add(psr_packet.valid_channels)
        number_time_samples[beam_number].add(psr_packet.n_samples)
        data_precs[beam_number].add(psr_packet.data_precs)
        version[beam_number].add(psr_packet.version)
        scan_id[beam_number].add(psr_packet.scan_id)
        magic[beam_number].add(psr_packet.magic)
        offset_1_2[beam_number].add(psr_packet.offset_1_2)
        offset_3_4[beam_number].add(psr_packet.offset_3_4)
        sequence_number[beam_number].append(psr_packet.sequence_number)
        scale[beam_number].append(psr_packet.scale)

    return {
        "total_timestamps": total_timestamps,
        "numerator": numerator,
        "denominator": denominator,
        "validity": validity,
        "reserved": reserved,
        "separation": separation,
        "n_channels": n_channels,
        "valid_channels": valid_channels,
        "number_time_samples": number_time_samples,
        "data_precs": data_precs,
        "version": version,
        "scan_id": scan_id,
        "magic": magic,
        "offset_1_2": offset_1_2,
        "offset_3_4": offset_3_4,
        "sequence_number": sequence_number,
        "scales": scale,
    }


def _ptc29_report(scan_config, results, stats):
    """Generate Test Report for PTC29."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "67174075983b3cd8850d0a44"
    )
    pps = stats.n_packets / stats.duration
    bps = stats.total_bytes * 8 / stats.duration
    words_to_replace = {
        "NUM11": results["numerator"][11],
        "NUM12": results["numerator"][12],
        "REQNUM": (
            results["numerator"][11] == {5184} and results["numerator"][12] == {5184}
        ),
        "DEN11": results["denominator"][11],
        "DEN12": results["denominator"][12],
        "REQDEN": (
            results["denominator"][11] == {25} and results["denominator"][12] == {25}
        ),
        "SEP11": results["separation"][11],
        "SEP12": results["separation"][12],
        "REQSEP": (
            results["separation"][11] == {3616898}
            and results["separation"][12] == {3616898}
        ),
        "CHAN11": results["n_channels"][11],
        "CHAN12": results["n_channels"][12],
        "REQCHAN": (
            results["n_channels"][11] == {24} and results["n_channels"][12] == {24}
        ),
        "VALIDITY11": results["validity"][11],
        "VALIDITY12": results["validity"][12],
        "REQDITY": (results["validity"][11] == {5} and results["validity"][12] == {5}),
        "VAL11": results["valid_channels"][11],
        "VAL12": results["valid_channels"][12],
        "REQVAL": (
            results["valid_channels"][11] == {24}
            and results["valid_channels"][12] == {24}
        ),
        "TIME11": results["number_time_samples"][11],
        "TIME12": results["number_time_samples"][12],
        "REQTIME": (
            results["number_time_samples"][11] == {32}
            and results["number_time_samples"][12] == {32}
        ),
        "BEAM11": 11,
        "BEAM12": 12,
        "REQBEAM": True,
        "MAG11": results["magic"][12],
        "MAG12": results["magic"][12],
        "REQMAG": (
            results["magic"][11] == {3199074029}
            and results["magic"][12] == {3199074029}
        ),
        "PREC11": results["data_precs"][11],
        "PREC12": results["data_precs"][12],
        "REQPREC": (
            results["data_precs"][11] == {16} and results["data_precs"][12] == {16}
        ),
        "SAM11": 111,
        "SAM12": 111,
        "REQSAM": True,
        "EXPVER": "version",
        "VER11": "version",
        "VER12": "version",
        "REQVER": True,
        "SCAN11": results["scan_id"][11],
        "SCAN12": results["scan_id"][12],
        "REQSCAN": (
            results["scan_id"][11] == {123} and results["scan_id"][12] == {123}
        ),
        "OFF11": results["offset_1_2"][11],
        "OFF12": results["offset_1_2"][12],
        "REQOFF": (
            results["offset_1_2"][11] == {0} and results["offset_1_2"][12] == {0}
        ),
        "BPS11": f"{bps:3f}",
        "REQBPS": abs(bps - 137335680) / 137335680 <= 0.02,
        "PPS11": f"{pps:3f}",
        "REQPPS": abs(pps - 2712) / 2712 <= 0.02,
        "PAC11": stats.packet_sizes,
        "REQPAC": stats.packet_sizes == {6330},
    }

    if not print_figures_for_ptc29_report(
        results["total_timestamps"],
        local_directory,
        new_repo_name,
    ):
        print("ERROR when printing figures")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    for word_to_replace, replacement in words_to_replace.items():
        replace_word_in_report(
            file_result_to_replace,
            word_to_replace,
            str(replacement),
        )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/timestamps.png")
    repo.git.add(new_repo_name + "/time_diff.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()

    return words_to_replace


def print_figures_for_ptc29_report(
    total_timestamps,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    plt.figure()
    plt.plot(
        list(range(len(total_timestamps[11]))), total_timestamps[11], label="Beam 11"
    )
    plt.plot(
        list(range(len(total_timestamps[12]))), total_timestamps[12], label="Beam 12"
    )
    plt.title("Timestamps")
    plt.xlabel("Packet number")
    plt.ylabel("Timestamps since EPOCH(ns)")
    plt.yscale("log")
    plt.legend()
    plt.grid()
    plt.show()
    plt.savefig(local_directory + new_repo_name + "/timestamps.png", format="png")
    plt.figure()
    time_difference = {}
    for beam, timestamps in total_timestamps.items():
        time_difference[beam] = [t - s for s, t in zip(timestamps, timestamps[1:])]

    plt.plot(
        list(range(len(time_difference[11][0:250]))),
        time_difference[11][0:250],
        label="Beam 11",
    )
    plt.plot(
        list(range(len(time_difference[12][0:250]))),
        time_difference[12][0:250],
        label="Beam 12",
    )
    plt.title("Time difference between packets ")
    plt.xlabel("Packet number (first 250 packets)")
    plt.ylabel("Time in ns")
    plt.yscale("log")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/time_diff.png", format="png")

    return True
