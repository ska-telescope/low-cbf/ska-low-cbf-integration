# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 39: PSS timestamp accuracy.
(Similar to PTC30 - PST timestamp accuracy)
Summary:
    Show that PSS beamformer packets have accurate timestamps
    - send SPS packets containing pulsed noise into the PSS beamformer
    - form a single beam
    - capture SPS input and PSS output
    - Test pass:
        A. show that first SPS pulse into PSS FPGA results in matching PSS beam pulse output
        B. fold SPS input and PSS output on pulse period, calculate timing difference
          - difference should be less than a few SPS sample periods (limited by test precision)

More information:
    - `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
    - `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
    - `PTC #39 Presentation <https://where.is.it>`_  # TODO
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import math
import os
import shutil
import struct
import time
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import (
    get_timestamps_udp_payload_bytes,
    get_udp_payload_bytes,
)
from ska_low_cbf_integration.pst_timestamps import (
    extract_sps_info,
    find_first_nonzero_sps,
    spead_power,
)

from .latex_reporting import download_overleaf_repo, replace_word_in_report  # noqa
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingMulticastTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 900

SUBARRAY_ID = 1
STN_BEAM_ID = BeamId.PTC39
PSS_BEAM_ID = 1

NUM_STATIONS = 4  # 4 is enough to demonstrate beamforming
NUM_CHANNELS = 1  # 1 is enough to calculate timing
STARTING_EPOCH = 131800  # seconds after SKA epoch
FRAMES_TO_CAPTURE = 300  # 53msec per frame (300 = 16s)
DATA_DIR = "/test-data/PTC/ptc39"
CAPTURE_FILENAME = os.path.join(DATA_DIR, "ptc39.pcap")
RX_WAIT_SECS = 60  # 16s plus 8s start would be enough
BUILD_DIR = "/app/build/ptc39"

PSS_SPS_SAMP = 64  # SPS samples per PSS sample period
FOLD_SAMP = 5 * PSS_SPS_SAMP - 1  # this must match the CNIC pulsar mode period
POINTS_IN_FOLD = FOLD_SAMP // math.gcd(FOLD_SAMP, PSS_SPS_SAMP)

source_cfg = {
    "subarray_id": SUBARRAY_ID,
    "stn_beam_id": STN_BEAM_ID,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
station_beam_cfg = {
    "subarray_id": SUBARRAY_ID,
    "stn_beam_id": STN_BEAM_ID,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc39_pss_timestamp_accuracy(cnic_and_processor, ready_subarray):
    """Implement PTC 39."""
    # pylint: disable=too-many-locals
    os.makedirs(BUILD_DIR, exist_ok=True)
    time_start = time.time()

    my_cfg = {
        "subarray_id": SUBARRAY_ID,
        "stn_beam_id": STN_BEAM_ID,
        "stn_ids": [i + 1 for i in range(0, NUM_STATIONS)],
        "chans": [64 + i for i in range(0, NUM_CHANNELS)],
        "pss_beam_ids": [
            PSS_BEAM_ID,
        ],
    }
    spead_multicast_routings = [
        {
            "src": {
                "frequency": channel,
                "beam": STN_BEAM_ID,
                "sub_array": SUBARRAY_ID,
            },
            # port numbers are inserted in cnic_processor function
            "dst": {"port_bf": "", "port_corr": ""},
        }
        for channel in my_cfg["chans"]
    ]
    # === Set up fixed zero-delay polynomials ====
    # Must use zero-delay because CNIC gates data AFTER delays. If we wanted to
    # use non-zero or sky delays, CNIC would have to gate data BEFORE delaying
    fixed_dlys = [{"stn": stn, "nsec": 0.0} for stn in my_cfg["stn_ids"]]
    beamdir = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": fixed_dlys,
    }
    srcdir = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": [fixed_dlys] * 4,
    }
    # we use a PST polynomial for PSS - Format is the same
    srcdirpst = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": [fixed_dlys] * 4,
    }

    # CNIC transmit configuration
    cnic_config = cnic_configure(
        my_cfg["subarray_id"],
        my_cfg["stn_beam_id"],
        my_cfg["stn_ids"],
        my_cfg["chans"],
    )
    # Note long start delay so we capture first pulse
    pulse_start = 9000480  # delay so we capture first pulse
    pulse_on = (7 * PSS_SPS_SAMP) // 8 - 1  # fraction of a PSS sample on
    pulse_off = FOLD_SAMP - pulse_on  # remainder (~4.5 PSS samples) off
    # 48 PSS pkts/frame/chanl + 24 SPSpkt/frame/ch
    cnic_capture_packets = (48 + NUM_STATIONS * 24) * NUM_CHANNELS * FRAMES_TO_CAPTURE

    scan_config = subarray_pss_config(
        my_cfg["subarray_id"],
        my_cfg["stn_beam_id"],
        my_cfg["stn_ids"],
        my_cfg["chans"],
        my_cfg["pss_beam_ids"],
    )
    test_config = ProcessorVdStaticTrackingMulticastTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=cnic_capture_packets,
        scan_config=scan_config,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        pulsar_config=(pulse_start, pulse_on, pulse_off),
        spead_multicast_routing=spead_multicast_routings,
        source_delay_config=[srcdir],
        beam_delay_config=[beamdir],
        pst_delay_config=srcdirpst,
    )
    pss_sps_output = cnic_processor(
        cnic_and_processor, ready_subarray, test_config, CAPTURE_FILENAME
    )
    pss_sps_output.close()
    print(f"Capture file is {CAPTURE_FILENAME}")
    time_k8s_end = time.time()
    print(f"Took {time_k8s_end - time_start} sec to capture data")

    # Analyse results
    result = Ptc39Results()
    result.t_folded_s, result.timestamps_n_bad = find_timing_offset(CAPTURE_FILENAME)
    print(f"Analyse result: timing error = {result.t_folded_s:.2e} sec")
    result.t_first_s, result.latency_s = check_first_pulses(CAPTURE_FILENAME)
    print(f"First pulse offset = {result.t_first_s} sec, latency = {result.latency_s}")

    shutil.copytree(BUILD_DIR, DATA_DIR, dirs_exist_ok=True)  # put graphs with PCAPs
    update_overleaf_report(test_config, result)  # publish report

    assert result.t_folded_ok, f"Timing error {result.t_folded_s * 1e6}us too large"
    assert result.t_first_ok, f"First pulse too late {result.t_first_s * 1e3}ms"
    assert result.timestamps_ok, f"{result.timestamps_n_bad} Bad Timestamps"
    assert result.latency_ok, f"PSS Latency {result.latency_s}s too large"


@dataclass
class Ptc39Results:
    """Collate PTC#39 Results and assess pass/fail for each parameter."""

    t_folded_s: float = None
    timestamps_n_bad: int = None
    t_first_s: float = None
    latency_s: float = None

    @property
    def t_folded_ok(self) -> bool:
        """PSS Timestamps measured to be within 3 SPS samples of correct value."""
        return abs(self.t_folded_s) < 3 * 1080e-9

    @property
    def t_first_ok(self) -> bool:
        """1st PSS pulse no later than half the pulsar period after 1st SPS pulse."""
        return abs(self.t_first_s) < FOLD_SAMP * 1080e-9 / 2

    @property
    def timestamps_ok(self) -> bool:
        """All timestamps were good."""
        return self.timestamps_n_bad == 0

    @property
    def latency_ok(self) -> bool:
        """PSS Latency is acceptable."""
        return self.latency_s < 1


def check_first_pulses(filename: str, plot_dir: str = BUILD_DIR) -> (float, float):
    """
    Analyse a PCAP file containing simultaneously captured PSS/SPS packets.

    Check that the first PSS-beam pulse occurs at the same timestamp as the
    first SPS station-beam pulse.

    :returns: offset (s), latency (s)
    """
    # pylint: disable=too-many-locals,too-many-statements
    print("Check first PSS pulse out has same timestamp as first SPS pulse in")
    # From PCAP file get PCAP timestamps and UDP payloads for each packet
    tstamps, payloads = get_timestamps_udp_payload_bytes(filename, min_udp_data_size=56)
    spd_pkts_all, subs, pss_pkts_all = get_pss_and_sps_pkts(payloads)

    # latency between packet with first SPS pulse and packet with first PSS pulse
    first_sps = find_first_nonzero_sps(spd_pkts_all)
    first_pss = find_first_nonzero_pss(pss_pkts_all)
    first_sps_ts = tstamps[first_sps]
    first_pss_ts = tstamps[first_pss]
    pkt_latency_sec = float(first_pss_ts - first_sps_ts)
    print(f"pss packet latency={pkt_latency_sec * 1.0e3} msec")

    # get averaged PSS and SPS power samples and time
    sps_samp_no, sps_pwr = spead_power(spd_pkts_all, subs)
    sps_ts = sps_samp_no.astype(float) * 1080e-9
    pss_samp_no, pss_pwr = pss_power(pss_pkts_all)
    pss_ts = pss_samp_no.astype(float) * (1728.0e-6 / 25.0)

    # select portion of SPS data (around first pulse) to plot
    sps_idx = np.argmax(sps_pwr != 0.0)
    assert sps_idx > 3000, "SPS data peaked earlier than expected"
    plt_sps_pwr = sps_pwr[sps_idx - 3000 : sps_idx + 3000]
    plt_sps_pwr = plt_sps_pwr / np.max(plt_sps_pwr)
    plt_sps_ts = sps_ts[sps_idx - 3000 : sps_idx + 3000]
    print(f"first sps power at t={sps_ts[sps_idx]}")

    # select portion of PSS data (around first pulse) to plot
    pss_idx = np.argmax(pss_pwr != 0.0)
    assert pss_idx > 30, "PSS data peaked earlier than expected"
    plt_pss_pwr = pss_pwr[pss_idx - 30 : pss_idx + 40]
    plt_pss_pwr = plt_pss_pwr / np.max(plt_pss_pwr)
    plt_pss_ts = pss_ts[pss_idx - 30 : pss_idx + 40]
    print(f"first pss power at t={pss_ts[pss_idx]}")

    zero_time = min(plt_sps_ts[0], plt_pss_ts[0])
    plt_pss_ts -= zero_time
    plt_sps_ts -= zero_time

    # find highest value in first PSS pulse
    pk_threshold = 0.25 * np.max(plt_pss_pwr)
    over_thresh = False
    pk_idx = -1
    pk_pwr = 0
    for idx, pwr in enumerate(plt_pss_pwr.tolist()):
        if pwr > pk_threshold:
            over_thresh = True
        if not over_thresh:
            continue
        if pwr > pk_pwr:
            pk_pwr = pwr
            pk_idx = idx
        else:
            break
    # Fit quadratic through 3pts around highest value
    fit_pwr = plt_pss_pwr[pk_idx - 1 : pk_idx + 2]
    # avoid numerical precision probs by making t near zero
    fit_t0 = plt_pss_ts[pk_idx - 1]
    fit_time = plt_pss_ts[pk_idx - 1 : pk_idx + 2] - fit_t0
    poly = np.polyfit(fit_time, fit_pwr, 2)
    poly_pk_t = -poly[1] / (2.0 * poly[0]) + fit_t0
    # construct quadratic interpolation for plotting
    fit_time = np.linspace(fit_t0, plt_pss_ts[pk_idx + 1], 10) - fit_t0
    fit_vals = np.polyval(poly, fit_time)
    fit_time += fit_t0

    # find middle of first sps pulse
    start_idx = None
    end_idx = None
    for idx, pwr in enumerate(plt_sps_pwr):
        if start_idx is None:
            if pwr == 0:
                continue
            start_idx = idx
        else:
            if pwr != 0:
                continue
            end_idx = idx - 1
            break
    t_mid = (plt_sps_ts[start_idx] + plt_sps_ts[end_idx]) / 2.0
    # calculate how far first PSS peak is from middle of first SPS pulse
    t_offset = poly_pk_t - t_mid
    print(f"Offset of first PSS pulse from first SPS pulse = {t_offset * 1e6:.1f} usec")

    # Create plots
    _, axis = plt.subplots(figsize=(10, 6))  # fig == _ is unused
    axis.plot(
        plt_sps_ts * 1e3,  # to milliseconds
        plt_sps_pwr,
        label="Average station-beam power",
        color="red",
    )
    axis.scatter(
        plt_pss_ts * 1e3,  # to milliseconds
        plt_pss_pwr,
        label="Average PSS-beam power",
        color="blue",
        marker=".",
    )
    axis.plot(
        fit_time * 1e3,  # to milliseconds
        fit_vals,
        label=f"PSS power fit [peak@ {poly_pk_t * 1e3:.1f} msec]",
        color="green",
    )
    axis.legend()  # show the legend
    title_txt = "First SPS and PSS beam pulses"
    title_txt += f" [offset of PSS from SPS = {t_offset * 1e6:.1f} usec]"
    axis.set_title(title_txt)
    axis.set_xlabel("Time since first plotted sample (milliseconds)")
    axis.set_ylabel("Power")
    plt.grid()

    plt.savefig(os.path.join(plot_dir, "ptc39_first_pulses.png"))

    return t_offset, pkt_latency_sec


def find_timing_offset(filename: str, plot_dir: str = BUILD_DIR) -> (float, int):
    """
    Analyse SPS+PSS packet capture to find timestamp offset of PSS pulses.

    By folding and averaging SPS and PSS pulse data using the known
    pulse period to improve SNR and accuracy of the estimate

    1. Peak of the PSS pulse is estimated by fitting a quadratic through
    the PSS data near the pulse middle.
    2. Centre of the (rectangular) SPS pulse is estimated by finding
    the midpoint of the SPS samples that aren't zero

    :returns: timestamp offset (s), number of bad timestamps
    """
    # pylint: disable=too-many-locals,too-many-statements
    payloads = get_udp_payload_bytes(filename, min_udp_data_size=56)
    sps_pkts_all, subs, pss_pkts_all = get_pss_and_sps_pkts(payloads)
    # Check that capture file meets requirements for analysis
    assert len(subs) == 1, f"Test expects 1 subarray got {len(subs)}"
    for sub in subs.values():
        n_stn_beams = len(sub["beams"])
        assert n_stn_beams == 1, f"Test expects 1 stn-beam, got {n_stn_beams}"

    # Analyse only the packets that occur after pulses have started
    first_pwr = find_first_nonzero_sps(sps_pkts_all)
    sps_pkts = [pkt for pkt in sps_pkts_all if pkt.capture_no > first_pwr + 100]
    pss_pkts = [pkt for pkt in pss_pkts_all if pkt.capture_no > first_pwr + 100]

    # Get list of stations in the station-beam (so we can get pwr for each)
    sa_bm_stn = []
    for sa_id, sub in subs.items():
        for bm_id in sub["beams"]:
            for stn_id in sub["stns"]:
                sa_bm_stn.append(f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}")

    stn_bm_sample_times, folded_stn_pwr = fold_stn_beam(sps_pkts, sa_bm_stn)
    pss_bm_sample_times, folded_pwr, n_bad_tstamp = fold_pss_beam(pss_pkts)

    # ===================== compare peak of beam power to midpoint of pulse

    # find rough middle of PSS pulse by taking maximum
    max_idx = folded_pwr.index(max(folded_pwr))
    max_time = pss_bm_sample_times[max_idx]
    # Adjust PSS & SPS sample times to movepeak of PSS pulse to middle of range
    adjust = (1.5 * FOLD_SAMP * 1080e-9) - max_time
    # 1. adjust station-beam times
    for idx, s_time in enumerate(stn_bm_sample_times):
        stn_bm_sample_times[idx] = (s_time + adjust) % (FOLD_SAMP * 1080e-9)
    # 2. adjust PSS beam time by same amount as station-beam
    for idx, s_time in enumerate(pss_bm_sample_times):
        pss_bm_sample_times[idx] = (s_time + adjust) % (FOLD_SAMP * 1080e-9)

    # find time at middle of adjusted station pulse
    stn_pulse_min_time = FOLD_SAMP * 1080e-9
    stn_pulse_max_time = 0.0
    for s_time, pkt in zip(stn_bm_sample_times, folded_stn_pwr):
        if pkt > 0.0:
            if s_time > stn_pulse_max_time:
                stn_pulse_max_time = s_time
            if s_time < stn_pulse_min_time:
                stn_pulse_min_time = s_time
    stn_pulse_middle_time = (stn_pulse_min_time + stn_pulse_max_time) / 2.0
    print(f"Middle folded station pulse: {stn_pulse_middle_time * 1e6:.3f} usec")

    # Get X-vals near the middle of the pulse for quadratic fit
    np_ut = np.asarray(pss_bm_sample_times)
    centre_idxes = (np_ut > (stn_pulse_middle_time - (0.1 * FOLD_SAMP * 1080e-9))) & (
        np_ut < (stn_pulse_middle_time + (0.1 * FOLD_SAMP * 1080e-9))
    )

    # fit polynomial to the middle
    fit_pwr = np.asarray(folded_pwr)[centre_idxes]  # Y-values for quadratic
    fit_time = np_ut[centre_idxes]  # X-values for quadratic
    poly = np.polyfit(fit_time, fit_pwr, 2)
    fit_peak_time = -poly[1] / (2.0 * poly[0])
    fitted_peak_y = np.polyval(poly, fit_time)

    print(f"PSS polynomial fit max at  : {fit_peak_time * 1e6:.3f} usec")
    pss_timestamp_error_sec = fit_peak_time - stn_pulse_middle_time
    print(f"measured PSS timestamp error {pss_timestamp_error_sec * 1e6:.3e} usec")

    # ================ PLOTTING ================
    sps_ts_usec = np.asarray(stn_bm_sample_times, dtype=float) * 1e6
    pss_ts_usec = np.asarray(pss_bm_sample_times, dtype=float) * 1e6

    # Plot time-domain power for each station of each subarray-beam
    _, axis = plt.subplots(2, 1, figsize=(10, 6))  # fig == _ is unused
    axis[0].scatter(
        sps_ts_usec,
        folded_stn_pwr,
        label=f"Average station power [midpoint@ {stn_pulse_middle_time * 1e6:.1f} μs]",
        color="red",
        marker=".",
    )
    axis[0].grid()
    title_txt = "PSS beam power and SPS station power folded on pulse period"
    axis[0].set_title(title_txt + f" (of {FOLD_SAMP} SPS samples)")
    axis[0].set_ylabel("Power")
    axis[0].legend()  # show the legend

    axis[1].scatter(pss_ts_usec, folded_pwr, label="Average PSS beam power", marker=".")
    axis[1].scatter(
        fit_time * 1e6,
        fitted_peak_y,
        label=f"beam power poly fit [peak@ {fit_peak_time * 1e6:.1f} usec]",
        marker=".",
    )
    axis[1].grid()
    axis[1].set_ylabel("Power")
    axis[1].legend()
    axis[1].set_xlabel("Time (microseconds)")

    axis[1].text(
        0, np.max(folded_pwr) / 2, f"err={pss_timestamp_error_sec * 1e6:.3f}usec"
    )
    plt.savefig(os.path.join(plot_dir, "ptc39_folded_pulses.png"))

    return pss_timestamp_error_sec, n_bad_tstamp


def fold_stn_beam(sps_pkts, sa_bm_stn) -> tuple:  # pylint: disable=too-many-locals
    """
    Fold station-beam packet data on known pulsar period.

    :param sps_pkts: list of SPS packets
    :sa_bm_stn: stations in station beams

    :return: tuple(time, stn_beam_power)
    """
    # From packet number, calculate SPS sample times modulo folding period
    pkt_nos = set()
    for pkt in sps_pkts:
        pkt_nos.add(pkt.pkt_no)
    pkt_nos = sorted(list(pkt_nos))
    # Numpy vector holding sample numbers (x-axis of plot), folded on pulse period
    sample_nos = np.zeros(len(pkt_nos) * 2048, dtype=int)  # 2048 samples/SPS pkt
    for pkt_idx, pkt_no in enumerate(pkt_nos):
        start_sampl = 2048 * pkt_no
        sample_nos[pkt_idx * 2048 : (pkt_idx + 1) * 2048] = np.asarray(
            [i % FOLD_SAMP for i in range(start_sampl, start_sampl + 2048)], dtype=int
        )
    sample_times = sample_nos.astype(float) * 1080e-9
    unique_sample_times = sorted(list(set(sample_times.tolist())))
    print(f"{len(unique_sample_times)} samples of stn power after folding")

    # Numpy vector to hold power for each station of each subarray-beam created all-zero
    stn_powers = [np.zeros(len(pkt_nos) * 2048, dtype=float) for _ in sa_bm_stn]
    # Numpy vector to hold numbr of chans that were summed
    stn_chans = [np.zeros(len(pkt_nos), dtype=int) for _ in sa_bm_stn]
    # Get power for each packet's samples and put in stn_powers list
    for pkt in sps_pkts:
        sa_id = pkt.subarray_id
        bm_id = pkt.stn_bm_id
        stn_id = pkt.stn_id
        sstn_id = pkt.sstn_id
        sa_bm_stn_idx = sa_bm_stn.index(
            f"sa{sa_id:02d}-bm{bm_id}-stn{stn_id}.{sstn_id}"
        )

        pkt_no_idx = pkt_nos.index(pkt.pkt_no)
        pkt_start_idx = pkt_no_idx * 2048
        apol_re = pkt.data[0::4]
        apol_im = pkt.data[1::4]
        bpol_re = pkt.data[2::4]
        bpol_im = pkt.data[3::4]

        pwr = (
            np.multiply(apol_re, apol_re)
            + np.multiply(apol_im, apol_im)
            + np.multiply(bpol_re, bpol_re)
            + np.multiply(bpol_im, bpol_im)
        )

        stn_powers[sa_bm_stn_idx][pkt_start_idx : pkt_start_idx + 2048] += pwr
        stn_chans[sa_bm_stn_idx][pkt_no_idx] += 1
    # normalise station power: divide by number of channels accumulated
    for stn_pwr_data in stn_powers:
        for pkt_no_idx in range(0, len(pkt_nos)):
            if stn_pwr_data[pkt_no_idx] != 0:
                pkt_start_idx = pkt_no_idx * 2048
                if stn_chans[sa_bm_stn_idx][pkt_no_idx] == 0:
                    continue
                stn_pwr_data[pkt_start_idx : pkt_start_idx + 2048] /= stn_chans[
                    sa_bm_stn_idx
                ][pkt_no_idx]

    # accumulate power in SPS packets across all stations
    all_stns_pwr = np.zeros((len(stn_powers[0]),), dtype=float)
    for stn_pwr in stn_powers:
        all_stns_pwr += stn_pwr
    all_stns_pwr /= len(stn_powers)

    # Fold station power
    folded_stn_pwr = [0.0] * len(unique_sample_times)
    stns_added = [0] * len(unique_sample_times)
    for pkt, s_time in zip(all_stns_pwr.tolist(), sample_times):
        idx = unique_sample_times.index(s_time)
        folded_stn_pwr[idx] += pkt
        stns_added[idx] += 1
    # normalise bynumber of samples accumulated
    for idx, fpwr in enumerate(folded_stn_pwr):
        if stns_added[idx] == 0:
            continue
        folded_stn_pwr[idx] = fpwr / stns_added[idx]

    return unique_sample_times, folded_stn_pwr


def fold_pss_beam(pss_pkts):  # pylint: disable=too-many-locals
    """Fold PSS beam data packets."""
    # From PSS sample number, calculate PSS sample times
    pss_sample_nos = set()
    pss_first_chans = set()
    for pkt in pss_pkts:
        pss_sample_nos.add(pkt.sample_no)
        pss_first_chans.add(pkt.first_chan)
    pss_sample_nos = sorted(list(pss_sample_nos))
    pss_first_chans = sorted(list(pss_first_chans))

    # Calculate pss sample times, folded every "FOLD_SAMP" SPS samples
    pss_smp_indicies = [0] * len(pss_sample_nos) * 16
    for cnt, sample_no in enumerate(pss_sample_nos):
        idx = cnt * 16
        pss_smp_indicies[idx : idx + 16] = [
            ((i + sample_no) * PSS_SPS_SAMP) % FOLD_SAMP for i in range(0, 16)
        ]
    pss_sample_times = np.asarray(pss_smp_indicies, dtype=float) * 1080e-9

    # get power for each PSS packet's samples
    # Numpy vector holding power for each group of pss channels, created all-zero
    pss_powers = [
        np.zeros(len(pss_sample_nos) * 16, dtype=float) for _ in pss_first_chans
    ]
    print(f"{len(pss_pkts)} pss packets")
    last_sample_no = None
    num_bad_pss_timestamps = 0
    for pkt in pss_pkts:
        if last_sample_no is None:
            last_sample_no = pkt.sample_no
        if pkt.sample_no not in (last_sample_no, last_sample_no + 16):
            num_bad_pss_timestamps += 1
        last_sample_no = pkt.sample_no

        frq_idx = pss_first_chans.index(pkt.first_chan)
        pkt_idx = pss_sample_nos.index(pkt.sample_no)
        pkt_start_idx = 16 * pkt_idx
        pss_powers[frq_idx][pkt_start_idx : pkt_start_idx + 16] = (
            pkt.power_sum / 16
        )  # why 16? ========================
    print(f"{num_bad_pss_timestamps} bad timestamps in PSS packets")

    # Accumulate PSS beam power across all PSS channels (used noise input)
    all_chans_pwr = np.zeros(pss_powers[0].shape, dtype=float)
    for pss_pwr in pss_powers:
        all_chans_pwr += pss_pwr
    all_chans_pwr /= len(pss_powers)

    # Get list of timestamps for each bin in the fold
    folded_timestamps = sorted(list(set(pss_sample_times.tolist())))
    print(f"{len(folded_timestamps)} timestamp bins in PSS folding")
    # accumulate each sample in one of the bins
    folded_pwr = [0.0] * len(folded_timestamps)  # bins for accumulated power
    num_folded = [0] * len(folded_timestamps)  # bins for number of samples
    for s_time, pkt in zip(pss_sample_times.tolist(), all_chans_pwr.tolist()):
        idx = folded_timestamps.index(s_time)
        folded_pwr[idx] += pkt
        num_folded[idx] += 1
    # normalise each bin (divide by number of samples accumulated)
    for idx, fpwr in enumerate(folded_pwr):
        folded_pwr[idx] = fpwr / num_folded[idx]

    return folded_timestamps, folded_pwr, num_bad_pss_timestamps


def get_pss_and_sps_pkts(payloads: list[bytes]):  # pylint: disable=too-many-locals
    """Extract lists of pss packets, subs, and SPS packets from packet payloads."""
    num_spead = 0
    num_pss = 0
    non_spd_non_pss_pkt = 0
    spead_pkt_list = []
    pss_pkt_list = []
    subs = {}
    for cap_no, payload in enumerate(payloads):
        # unused fields in next line: ipw, haw, zer
        magic, ver, _, _, _, nitm = struct.unpack(">BBBBHH", payload[0:8])
        pss_magic = struct.unpack("<I", payload[60:64])[0]
        if (magic == 0x53) and (ver == 0x4) and (nitm == 6):
            num_spead += 1
            spead = extract_sps_info(payload, cap_no)
            spead_pkt_list.append(spead)

            if spead.subarray_id not in subs:
                subs[spead.subarray_id] = {"stns": set(), "beams": {}}
            stn_repr = f"{spead.stn_id}.{spead.sstn_id}"
            subs[spead.subarray_id]["stns"].add(stn_repr)
            if spead.stn_bm_id not in subs[spead.subarray_id]["beams"]:
                subs[spead.subarray_id]["beams"][spead.stn_bm_id] = set()
            subs[spead.subarray_id]["beams"][spead.stn_bm_id].add(spead.chan_id)
        elif pss_magic == 0xBEADFEED:
            num_pss += 1
            pss = extract_pss_info(payload, cap_no)
            pss_pkt_list.append(pss)
        else:
            non_spd_non_pss_pkt += 1

    print(f"{num_spead} SPEAD packets, {num_pss} pss packets")
    if non_spd_non_pss_pkt != 0:
        print(f"{non_spd_non_pss_pkt} other packets")

    # convert subarray sets to sorted lists
    for sub in subs.values():
        sub["stns"] = sorted(list(sub["stns"]))
        for bm_id in sub["beams"]:
            sub["beams"][bm_id] = sorted(list(sub["beams"][bm_id]))

    return spead_pkt_list, subs, pss_pkt_list


@dataclass
class PssPkt:
    """Structure to save decoded data from PSS packets."""

    seq_no: int  # Packet sequence number (from FPGA, not unique if >1 FPGA)
    sample_no: int  # PSS sample_no (0=SKA epoch, roughly 2000-01-01)
    first_chan: int  # First PSS chan in packet (0 = bottom of SPS ch 64)
    power_sum: np.ndarray  # per-channel average sample power
    capture_no: int  # index of this pkt in original PCAP file


N_POL = 2  # Number of polarisations recorded in packet data
N_VALS_PER_CPLX = 2  # Re+Im data -> 2 values for a complex sample


def extract_pss_info(payload, capture_no):  # pylint: disable=too-many-locals
    """
    Get PSS fields from PSS packet.

    :param payload: PSS packet payload bytes
    :param capture_no: packet number within the original pcap file
    """
    seq_no = struct.unpack("Q", payload[0:8])[0]
    sampl_no = struct.unpack("Q", payload[8:16])[0]
    scale1 = struct.unpack("f", payload[32:36])[0]
    first_chan = struct.unpack("I", payload[48:52])[0]
    num_chan = struct.unpack("H", payload[52:54])[0]
    valid_chan = struct.unpack("H", payload[54:56])[0]
    num_sample = struct.unpack("H", payload[56:58])[0]
    bytes_precision = struct.unpack("B", payload[65:66])[0] // 8
    # beam_id = struct.unpack("H", payload[58:60])[0]
    sample_per_weight = struct.unpack("B", payload[67:68])[0]

    weights_offset = 96  # multiple of 16bytes=128 bits
    n_weight_bytes = num_sample / sample_per_weight * 2 * num_chan
    # weights padded to multiple of 128 bits = 16 bytes
    data_offset = weights_offset + math.ceil(n_weight_bytes / 16) * 16

    fmt = ["b", "h"]  # PSS 8-bit and PST 16-bit struct format names
    n_bytes_per_pol = bytes_precision * N_VALS_PER_CPLX
    n_bytes_per_sample = bytes_precision * N_VALS_PER_CPLX * N_POL

    pwr_sum = np.zeros(num_sample)
    for chan in range(0, valid_chan):
        ch_offset = data_offset + (chan * num_sample * n_bytes_per_sample)
        # sum sample power for both polarisations
        for pol in range(0, N_POL):
            pol_base = ch_offset + pol * num_sample * n_bytes_per_pol
            for sample_idx in range(0, num_sample):
                loc_x = pol_base + sample_idx * n_bytes_per_pol
                x_i, x_q = struct.unpack(
                    fmt[bytes_precision - 1] * 2,  # 2 chars for 2 values
                    payload[loc_x : loc_x + n_bytes_per_pol],
                )
                sample_pwr = x_i * x_i + x_q * x_q
                pwr_sum[sample_idx] += sample_pwr

    # avg pwr per sample per channel
    pwr_sum = pwr_sum / (scale1 * scale1) / num_sample / valid_chan

    return PssPkt(seq_no, sampl_no, first_chan, pwr_sum, capture_no)


def find_first_nonzero_pss(pss_pkts: list[PssPkt]) -> int:
    """
    Find first PSS packet that has non-zero power.

    :param pss_pkts: list of PSS packet objects
    :return: index of packet in original capture list, or -1
    """
    for pkt in pss_pkts:
        if np.count_nonzero(pkt.power_sum) != 0:
            return pkt.capture_no
    return -1


PSS_SAMP_PER_PKT = 16


def pss_power(  # pylint: disable=too-many-locals
    pss_pkts: list[PssPkt],
) -> tuple[np.ndarray, np.ndarray]:
    """
    Get average PSS sample power and timestamp.

    Normalised to a single channel

    :return: (sample_time_since_ska_epoch, sample_power)
    """
    # From PSS sample number, calculate PSS sample times
    pss_sample_nos = set()
    pss_first_chans = set()
    for pkt in pss_pkts:
        pss_sample_nos.add(pkt.sample_no)
        pss_first_chans.add(pkt.first_chan)
    pss_sample_nos = sorted(list(pss_sample_nos))
    pss_first_chans = sorted(list(pss_first_chans))

    # Calculate PSS sample times
    pss_smp_idxs = [0] * len(pss_sample_nos) * PSS_SAMP_PER_PKT
    for cnt, sample_no in enumerate(pss_sample_nos):
        idx = cnt * PSS_SAMP_PER_PKT
        pss_smp_idxs[idx : idx + PSS_SAMP_PER_PKT] = [
            (i + sample_no) for i in range(0, PSS_SAMP_PER_PKT)
        ]
    pss_sample_numbers = np.asarray(pss_smp_idxs, dtype=int)

    # get power for each PSS packet's samples
    # Numpy vector holding power for each group of PSS channels, created all-zero
    pss_powers = [
        np.zeros(len(pss_sample_nos) * PSS_SAMP_PER_PKT, dtype=float)
        for _ in pss_first_chans
    ]
    print(f"{len(pss_pkts)} pss packets")
    last_sample_no = None
    num_bad_pss_timestamps = 0
    for pkt in pss_pkts:
        if last_sample_no is None:
            last_sample_no = pkt.sample_no
        if pkt.sample_no not in (last_sample_no, last_sample_no + PSS_SAMP_PER_PKT):
            num_bad_pss_timestamps += 1
        last_sample_no = pkt.sample_no

        frq_idx = pss_first_chans.index(pkt.first_chan)
        pkt_idx = pss_sample_nos.index(pkt.sample_no)
        pkt_start_idx = PSS_SAMP_PER_PKT * pkt_idx
        pss_powers[frq_idx][pkt_start_idx : pkt_start_idx + PSS_SAMP_PER_PKT] = (
            pkt.power_sum / 16  # Why 16 ? ===================
        )
    # Accumulate PSS beam power across all PSS channels
    all_chans_pwr = np.zeros(pss_powers[0].shape, dtype=float)
    for pss_pwr in pss_powers:
        all_chans_pwr += pss_pwr
    all_chans_pwr /= len(pss_powers)

    return pss_sample_numbers, all_chans_pwr


def cnic_configure(sub_id, beam_id, stn_ids, beam_freq_ids, scale=4000) -> dict:
    """Create CNIC configuration."""
    config = {
        "sps_packet_version": 3,
        "stream_configs": [],  # config is a list of dicts - one per stn-chan
    }

    # Add configs into the stream_configs list
    sc_list = []
    for stn_id in stn_ids:
        for freq_id in beam_freq_ids:
            stream_cfg = {
                "scan": 0,  # this is in packet but unused, dropped by FPGA
                "subarray": sub_id,
                "station": stn_id,
                "substation": 1,
                "frequency": freq_id,
                "beam": beam_id,
                "sources": {},  # initially empty, configured later
            }
            sc_list.append(stream_cfg)
    config["stream_configs"] = sc_list

    # Configure all sources. Noise, Chans have same seed for all stns.
    for stream in config["stream_configs"]:
        beam_id = stream["beam"]
        chan = stream["frequency"]

        stream["sources"] = {
            "x": [
                {"tone": False, "seed": 1000 + chan, "scale": scale},
            ],
            "y": [
                {"tone": False, "seed": 2000 + chan, "scale": scale},
            ],
        }

    return config


def subarray_pss_config(sub_id, stn_bm_id, stn_ids, freq_ids, pss_beams_list):
    """PSS subarray configuration."""
    wts = [1.0]  # all stations used
    pssbeam_list = []
    for pss_bm_id in pss_beams_list:
        beam = {
            "pss_beam_id": pss_bm_id,
            "stn_beam_id": stn_bm_id,
            "delay_poly": f"low-cbf/delaypoly/0/pst_s{sub_id:02d}_b{stn_bm_id:02d}_1",
            "jones": "tbd",
            "destinations": [  # TODO distinguish each destination
                {
                    "data_host": "192.168.0.1",
                    "data_port": 11001,
                    "start_channel": 0,
                    "num_channels": 144,
                },
            ],
            "stn_weights": wts,
        }
        pssbeam_list.append(beam)

    pss_cfg = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": [[s, 1] for s in stn_ids],
                "stn_beams": [
                    {
                        "beam_id": stn_bm_id,
                        "freq_ids": freq_ids,
                        "delay_poly": "low-cbf/delaypoly/0/delay_s"
                        + (f"{sub_id:02d}_b{stn_bm_id:02d}"),
                    },
                ],
            },
            "search_beams": {
                "beams": pssbeam_list,
            },
        },
    }
    return pss_cfg


def update_overleaf_report(test_config, result: Ptc39Results):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "672aa8029ded9cc38ae435a0"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )

    replace_word_in_report(
        file_result_to_replace,
        "T_ERR",
        f"{result.t_folded_s * 1e6:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_TERR_OK",
        f"{result.t_folded_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "BAD_TS",
        f"{result.timestamps_n_bad}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_BADTS_OK",
        f"{result.timestamps_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "T_OFFS",
        f"{result.t_first_s * 1e6:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_TOFFS_OK",
        f"{result.t_first_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "PSS_LATENCY",
        f"{result.latency_s:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_LATENCY_OK",
        f"{result.latency_ok}",
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        os.path.join(BUILD_DIR, "ptc39_folded_pulses.png"),
        local_directory + new_repo_name + "/ptc39_folded_pulses.png",
    )
    shutil.copy(
        os.path.join(BUILD_DIR, "ptc39_first_pulses.png"),
        local_directory + new_repo_name + "/ptc39_first_pulses.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc39_folded_pulses.png")
    repo.git.add(new_repo_name + "/ptc39_first_pulses.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()
