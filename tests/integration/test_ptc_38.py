# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#

"""
Perentie Test Case 38 - PSS beam metadata.

- `Perentie Test Cases <https://is.gd/BCLuaC>`_
- `PTC #38 <https://is.gd/I3gkfS>`_
- `Overleaf report <https://www.overleaf.com/project/672aa7349ded9cc38ae3f892>`_

1. Setup the CNIC to generate data for three sources for 18-stations with AA1
   locations (345-356,  429-434) containing 96 channels (64-160) with noise of
   amplitude 4000 and 2000.

2. Set the LOW CBF delay polynomial simulator to point the
   station beam at
           RA: 2h30m DEC: -26d49m50.0s
   and the PSS beams at:
     B11 - RA: 2h30m Dec: -26d49m50.0s
     B12 - RA: 2h30m Dec: -27d49m50.0s
   which results in PSS beams 0 and 1 degrees offset from the station beam.

3. Setup a subarray to form 3 PSS beams: each uses all stations, and each outputs
   the beams to the same CNIC beam capture Alveo.

4. Start the scan

5. Capture 10 seconds of data in each CNIC-Rx

6. During the capture measure the data and packet rate on each CNIC-Rx 100GbE link

7. Analyse all the packets captured and check that they have the right size

8. Examine the "static" metadata in the packet - period numerator, period
   denominator, channel separation, channels per packet, valid channels per
   packet, number of time samples, beam number, magic number, data preciesion,
   number of samples averaged, beamformer version, scan id, and offsets (all
   zero)

9. Examine the "dynamic" metadata in the packet - packet number, frequency,
   first channel number,  scale and data

NOTE: keep this `test guideline in mind <https://is.gd/vJIF5B>` _
"""  # noqa - RST doesn't like this text for reasons

import json
import os
import shutil
from datetime import datetime

import matplotlib.pyplot as plt
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality, version_under_test
from ska_low_cbf_integration.pcap import pcap_stats

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor
from .test_ptc_29 import metadata_per_beam

DATA_DIR = "/test-data/PTC/ptc38/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_FNAME = f"{DATA_DIR}PTC38_beamformer.pcap"
"""PCAP file name"""

BUILD_DIR = "build/ptc38"
"""Directory to write plots etc."""

station_ids = list(range(345, 357))  # + list(range(429, 435))  # Must be mult. of 4
n_stations = len(station_ids)
assert n_stations % 4 == 0, "PSS needs a multiple of 4 stations"
stations = [(station, 1) for station in station_ids]
channels = tuple(range(64, 64 + 96))

PSS_BEAM_11 = 11
PSS_BEAM_12 = 12
PSS_BEAMS = (PSS_BEAM_11, PSS_BEAM_12)

station_weights = {
    PSS_BEAM_11: [1.0] * n_stations,
    PSS_BEAM_12: [1.0] * n_stations,
}

SUBARRAY_ID = 1
STATION_BEAM_ID = BeamId.SINGLE_BASIC
SCAN_ID = 123
ALLOCATOR_ADDR = "low-cbf/allocator/0"


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
    src_radec_beam_2 = {"ra": "2h30m00.00s", "dec": "-27d49m50.0s"}
    src_radec_beam_3 = {"ra": "2h30m00.00s", "dec": "-28d49m50.0s"}
    src_radec_beam_4 = {"ra": "2h30m00.00s", "dec": "-29d49m50.0s"}
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }
    pstdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }

    return beamdir, srcdir, pstdirs


delay_poly = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}"
pss_delay_poly = (
    f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}_" + "{}"
)

DEST_HOST = {
    "data_host": "192.168.1.1",
    "data_port": 11001,
    "start_channel": 0,
    "num_channels": 144,
}


TEST_TIMEOUT_SEC = 1800


# CNIC-VD Configuration
cnic_vd_config = [
    {
        "scan": 0,
        "subarray": SUBARRAY_ID,
        "station": station,
        "substation": substation,
        "frequency": channel,
        "beam": STATION_BEAM_ID,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 4000},
                {"tone": False, "seed": 1982, "scale": 2000},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 4000},
                {"tone": False, "seed": 1982, "scale": 2000},
            ],
        },
    }
    for station, substation in stations
    for channel in channels
]

scan_config = {
    "id": SCAN_ID,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": STATION_BEAM_ID,
                    "freq_ids": channels,
                    "delay_poly": delay_poly,
                },
            ],
        },
        "search_beams": {
            "beams": [
                {
                    "stn_beam_id": STATION_BEAM_ID,
                    "pss_beam_id": pss_beam,
                    "delay_poly": pss_delay_poly.format(i + 1),
                    "jones": "tbd",
                    "destinations": [DEST_HOST],
                    "stn_weights": station_weights[pss_beam],
                }
                for i, pss_beam in enumerate(PSS_BEAMS)
            ],
        },
    },
}


@pytest.mark.large_capture  # 4.2GB pcap file
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc38_pss_beam_metadata(cnic_and_processor, ready_subarray):
    """
    Implement PTC 38.

    In this test, we aim to verify

    - Metadata shall make the packet contents self-describing and includes time,
      data type, frequency, bandwidth, data dimensions, and scale parameters
    - The length of each UDP packet is correct
    """
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)
    allocator = tango.DeviceProxy(ALLOCATOR_ADDR)
    allocator.internalAlveoLimits = json.dumps(
        {
            "pss": {
                "vch": 900,
            },
        }
    )
    # SPS signal generation and PCAP capture
    # --------------------------------------
    beam_delay_config, source_delay_cfg, pss_delay_cfg = prepare_delay()
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=int(4800 / 5.3) * len(channels) * len(PSS_BEAMS) * 10,
        scan_config=scan_config,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_vd_config,
        during_scan_callback=None,
        source_delay_config=[source_delay_cfg],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pss_delay_cfg,
    )

    pcap = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_FNAME,
    )
    results = metadata_per_beam(pcap)
    # need to rewind?
    pcap.seek(0)
    stats = pcap_stats(pcap)

    print_results(results)
    create_ptc38_plots(results["total_timestamps"], BUILD_DIR)
    report_values = assess_results(results, stats)
    log_results(report_values, results, stats)
    publish_report(scan_config, report_values)

    # Assert that requirements are met
    for key, value in report_values.items():
        if key.startswith("REQ"):
            assert value, f"{key} not met"
    print(datetime.now(), "End of test")


def log_results(report_values, results, stats):
    """Log results to a text file."""
    with open(os.path.join(BUILD_DIR, "results.txt"), "w", encoding="utf-8") as log:
        log.write(f"Test Configuration\n{'='*18}\n\n")
        log.write(f"Station IDs: {station_ids}\n")
        log.write(f"{n_stations} Stations: {stations}\n")
        log.write(f"{len(channels)} Channels: {channels}\n")
        log.write(f"{len(PSS_BEAMS)} PSS Beams: {PSS_BEAMS}\n")
        log.write(f"\nPCAP Stats\n{'=' * 10}\n\n")
        log.write(f"{stats}\n")
        log.write(f"\nResults\n{'=' * 7}\n\n")
        print_results(results, log)
        log.write(f"\nReport Values\n{'=' * 13}\n\n")
        for key, value in report_values.items():
            log.write(f"{key}: {value}\n")


def print_results(results, file=None) -> None:
    """Print (short) results for debugging purposes."""
    for key in (
        "numerator",
        "denominator",
        "validity",
        "reserved",
        "separation",
        "valid_channels",
        "number_time_samples",
        "data_precs",
        "version",
        "scan_id",
        "magic",
        "offset_1_2",
        "offset_3_4",
    ):
        # convert defaultdict to dict for better printing
        print(f"{key:20}: {dict(results[key])}", file=file)


def create_ptc38_plots(all_timestamps, workarea) -> None:
    """Create figures for the report."""
    if not os.path.isdir(workarea):
        raise RuntimeError(f"{workarea} path does not exist")
    plt.figure()
    for beam in PSS_BEAMS:
        x_data = list(range(len(all_timestamps[beam])))
        y_data = all_timestamps[beam]
        plt.plot(x_data, y_data, label=f"Beam {beam}")
    plt.title("Timestamps")
    plt.xlabel("Packet Number")
    plt.ylabel("Time Since SKA Epoch (μs)")
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.savefig(workarea + "/timestamps.png", format="png")
    plt.close()

    plt.figure()
    n_packets = 250
    time_difference = {}
    for beam, timestamps in all_timestamps.items():
        time_difference[beam] = [
            t - s for s, t in zip(timestamps[:n_packets], timestamps[1 : n_packets + 1])
        ]

    for beam in PSS_BEAMS:
        x_data = list(range(len(time_difference[beam])))
        y_data = time_difference[beam]
        plt.plot(x_data, y_data, label=f"Beam {beam}")
    plt.title("Time difference between packets ")
    plt.xlabel(f"Packet number (first {n_packets} packets)")
    plt.ylabel("Time in μs")
    plt.yscale("log")
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.savefig(workarea + "/time_diff.png", format="png")
    plt.close()


def publish_report(scan_config_, words_to_replace):
    """Evaluate Results & Generate Test Report."""
    repo_id = "672aa7349ded9cc38ae3f892"
    git_workarea, test_run_subdir, repo = download_overleaf_repo(repo_id)

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + test_run_subdir
        + """/report}"""
    )
    report_tex = git_workarea + test_run_subdir + "/report.tex"
    replace_word_in_report(report_tex, "TOPUTDATE", test_run_subdir.split("_")[1])
    replace_word_in_report(report_tex, "TOPUTDIRECTORY", test_run_subdir)
    replace_word_in_report(report_tex, "CONFIGURE_SCAN", json.dumps(scan_config_))
    for word_to_replace, replacement in words_to_replace.items():
        replace_word_in_report(report_tex, word_to_replace, str(replacement))

    replace_word_in_report(
        git_workarea + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(test_run_subdir + "/report.tex")
    for plot in ("timestamps.png", "time_diff.png"):
        dst = os.path.join(git_workarea, test_run_subdir, plot)
        shutil.copy(os.path.join(BUILD_DIR, plot), dst)
        repo.git.add(dst)
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(test_run_subdir+"/appendix.tex")

    repo.index.commit("Test with python")
    repo.git.push()


def assess_results(results, stats):
    """Assess pass/fail and collect values of interest for report."""

    def beams_equal(key: str, value: int) -> bool:
        """Check result set contains only the expected value, for all beams."""
        result = True
        for beam in PSS_BEAMS:
            result &= results[key][beam] == {value}
            print(f"Equality test {key}[{beam}]: results[key][beam]== {value}")
        return result

    pps = stats.n_packets / stats.duration
    bps = stats.total_bytes * 8 / stats.duration  # bits per second
    # Observed bps ~4511879594, pps ~152181
    # apparently there are 48 packets in a frame, and 100 frames in 5.3 seconds
    target_pps = 48 * 100 * len(channels) * len(PSS_BEAMS) / 5.3
    # 42: Ethernet/IP/UDP headers, 8: data precision
    target_bps = 48 * 100 * (3664 + 42) * 8 * len(channels) * len(PSS_BEAMS) / 5.3
    # version: major & minor only
    major, minor = version_under_test(Personality.PSS).split(".")[:2]
    expected_version = (int(major) << 8) + int(minor)
    words_to_replace = {
        "REQNUM": beams_equal("numerator", 1728),
        "REQDEN": beams_equal("denominator", 25),
        "REQSEP": beams_equal("separation", 14467593),
        "REQCHAN": beams_equal("n_channels", 54),
        "REQDITY": beams_equal("validity", 23),
        "REQVAL": beams_equal("valid_channels", 54),
        "REQTIME": beams_equal("number_time_samples", 16),
        "REQMAG": beams_equal("magic", 0xBEADFEED),
        "REQPREC": beams_equal("data_precs", 8),
        "EXPVER": expected_version,
        "REQVER": beams_equal("version", expected_version),
        "REQSCAN": beams_equal("scan_id", SCAN_ID),
        "REQOFF": beams_equal("offset_1_2", 0),
        "EXPBPS": f"{target_bps/1e9:.3f}",
        "BPS11": f"{bps/1e9:.3f}",
        "REQBPS": bps == pytest.approx(target_bps, rel=0.02),
        "EXPPPS": f"{target_pps:.0f}",
        "PPS11": f"{pps:.0f}",
        "REQPPS": pps == pytest.approx(target_pps, rel=0.02),
        "PAC11": stats.packet_sizes,
        "REQPAC": stats.packet_sizes == {3664 + 42},
        "REQBEAM": results["total_timestamps"].keys() == set(PSS_BEAMS),
    }
    for beam in PSS_BEAMS:
        words_to_replace[f"NUM{beam}"] = results["numerator"][beam]
        words_to_replace[f"DEN{beam}"] = results["denominator"][beam]
        words_to_replace[f"SEP{beam}"] = results["separation"][beam]
        words_to_replace[f"CHAN{beam}"] = results["n_channels"][beam]
        words_to_replace[f"VALIDITY{beam}"] = results["validity"][beam]
        words_to_replace[f"VAL{beam}"] = results["valid_channels"][beam]
        words_to_replace[f"TIME{beam}"] = results["number_time_samples"][beam]
        words_to_replace[f"BEAM{beam}"] = beam
        words_to_replace[f"MAG{beam}"] = hex(results["magic"][beam].copy().pop())
        words_to_replace[f"PREC{beam}"] = results["data_precs"][beam]
        words_to_replace[f"VER{beam}"] = results["version"][beam]
        words_to_replace[f"SCAN{beam}"] = results["scan_id"][beam]
        words_to_replace[f"OFF{beam}"] = results["offset_1_2"][beam]
    return words_to_replace
