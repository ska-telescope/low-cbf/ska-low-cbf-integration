# SKA LOW CBF Intergration tests

In this directory, we list all the integration tests from the point of view of LOW CBF.

## Additional Non-Python file

In addition to the python files that contains the various tests and various configuration of those tests, we also
provide files to facilitate the CBF and CNIC configurations.

In particular, we have:
* `1ch.json` that provides delay polynomials to the correlator from the model for AA0.5 station configuration and 1
coarse channel from SPS.
* `8ch.json` that provides delay polynomials to the correlator from the model for AA0.5 station configuration and 8
coarse channels from SPS.
* `48ch.json` that provides delay polynomials to the correlator from the model for AA0.5 station configuration and 48
coarse channels from SPS.
