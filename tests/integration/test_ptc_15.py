# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 15 - Visibility Signal to Noise Ratio.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #15 Presentation <URL TBA>`_

For model:
- run matlab model "create config":
  ska-low-cbf-model/src_atomic$ octave
  octave:0> addpath("~/jsonlab")
  octave:1> create_config("run_cor_1sa_6stations", 1, 3)
to generate text files used by Python model
(i.e. tb/HWData_cor.txt )

--> may need to move the files into the 'run_' dir ??

- run CNIC VD -> CNIC Rx (may need to change station IDs in CNIC config)

- replace .raw files with CNIC generated (use pcap_to_raw), e.g.:
src_atomic$ pcap_to_raw ../ptc14/ptc15_float_scale1_25k_stn1-6.pcap
 run_cor_0sa_6stations/LFAAHeaders_fpga1.raw run_cor_1sa_6stations/LFAAData_fpga1.raw

- need to get Correlator .ccfg file (from FW file in CAR)

- correlator python model (seems to use floating point numbers)
  ska-low-cbf-model$ poetry shell
  $ python2 python/ska_low_cbf_model/correlator_model.py
ska-low-cbf-model/src_atomic$
 python2 ../python/ska_low_cbf_model/correlator_model.py -t run_cor_1sa_6stations
   -f correlator-filter.txt -c ../ptc14/correlator.ccfg -w output.pcap

We could use an existing model config (6 station, 1 subarray) with trick data,
code here generates a CNIC configuration padded to 6 stations to facilitate this.
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import csv
import json
import os
import shutil
from copy import deepcopy
from dataclasses import asdict, dataclass, field
from math import sqrt
from typing import Iterable

import matplotlib.pyplot as plt
import pytest

from ska_low_cbf_integration.firmware import Personality, version_under_test
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import (
    ProcessorVdStaticTrackingTestConfig,
    ProcessorVdTestConfig,
    cnic_processor,
)

N_INTEGRATIONS = 5  # Number of integrations to average
DELAY_DISCARD_INTEGRATIONS = 0  # First few integrations may be wrong?

# 15.1 Use CNIC to generate a single SPS channel for two stations and equal
# amplitude correlated noise and uncorrelated noise amplitude S 1/root(2) namely
# RMS of sum is 1, set the delay polynomial to zero, select 350MHz
stations = [423, 459]  # E16, S15, approx 67km E-W. ref. SKA-TEL-SKO-0000422
channels = [100]
uncorrelated_noise_seeds = [5511, 5151]
CORRELATED_SEED = 15
assert CORRELATED_SEED not in uncorrelated_noise_seeds
assert len(uncorrelated_noise_seeds) >= len(stations) * len(channels)
SCALE_RMS_ONE = 65536 / 209.03 / sqrt(2) / sqrt(2)
"""For CNIC-VD with two noise sources, this scale gives an RMS amplitude of 1."""

# 15.2 Process the data in the floating point correlator model
# 15.3 Capture cross and auto correlations for all fine 144 channels. Average across all
# channels and integrations giving Model cross correlator power (Cross_M) and Model
# autocorrelation power (Auto_M)
MODEL_CROSS_AUTO_RATIO = 0.5  # FIXME this is not really from the model, just a guess.
"""Model-generated value for Cross-Correlation/Auto-Correlation."""

EFFICIENCY_THRESHOLD = 0.98
"""REQ-46 is achieved if the result is more than 0.98"""

PTC15_DIR = "build/ptc15"
"""Directory to write plots etc."""

scan_2stn_1sa_1bm_1ch = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": [[station, 1] for station in stations],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": channels,
                    "delay_poly": "low-cbf/delaypoly/0/delay_s01_b01",
                },
            ],
        },
        "vis": {
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.128.2"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 20000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    },
}


def create_vd_config(rms_scale: float) -> dict:
    """Create a Virtual Digitiser configuration for a given RMS scale."""
    uncorrelated_seed = iter(uncorrelated_noise_seeds)
    scale = rms_scale * SCALE_RMS_ONE
    return {
        "stream_configs": [  # one dictionary per SPEAD stream
            {
                "scan": 123,
                # 15.5 In the correlator receive SPS data and process in single subarray
                "subarray": 1,
                "station": station,
                "substation": 1,
                "frequency": channel,
                "beam": 1,
                "sources": {
                    "x": [
                        {"tone": False, "seed": CORRELATED_SEED, "scale": scale},
                        {
                            "tone": False,
                            "seed": next(uncorrelated_seed),
                            "scale": scale,
                        },
                    ],
                    "y": [],
                },
            }
            for station in stations
            for channel in channels
        ],
        "sps_packet_version": 2,  # model only supports v2
    }


@pytest.mark.parametrize("pad", [False, True])
def test_write_ptc15_cnic_config(pad: bool):
    """
    Write PTC#15 CNIC VD Configuration to file for use with model (not really a test).

    The JSON output can be used directly with Tango CNIC, or converted to native data
    types for use directly with ``CnicFpga.configure_vd`` by using
    ``create_vd_stream_configs`` (look in ska_low_cbf_tango_cnic).

    :param pad: Pad output to 6 stations for model?
    """
    filename = "ptc15_cnic_config" + ("_padded" * pad) + ".json"
    file_path = os.path.join(PTC15_DIR, filename)
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    null_stations = [3, 4, 5, 6] if pad else []
    with open(file_path, "w", encoding="utf-8") as log:
        # we use extra stations to trick the model (expects 6 stations)
        model_config = create_vd_config(1)
        null_stream_configs = []
        if pad:
            # append 4 stations with no signal
            null_stream_configs = [
                {
                    "scan": 123,
                    "subarray": 1,
                    "station": station,
                    "substation": 1,
                    "frequency": channel,
                    "beam": 1,
                    "sources": {
                        "x": [],
                        "y": [],
                    },
                }
                for station in null_stations
                for channel in channels
            ]
        print()
        print(f"Writing CNIC VD Configuration to {file_path}")
        print("Stations:", stations + null_stations)
        print()
        model_config["stream_configs"] += null_stream_configs
        log.write(json.dumps(model_config, indent=2))


@dataclass
class Ptc15Result:
    """Results of one PTC#15 iteration."""

    scale: int
    """RMS amplitude of input signal."""
    auto: float
    """Average Auto-Correlation"""
    cross: float
    """Average Cross-Correlation"""
    cross_over_auto: float = field(init=False)
    """Ratio of Cross-Correlation / Auto-Correlation."""
    efficiency: float = field(init=False)
    """Correlator efficiency"""

    # 15.8 Calculate the Alveo correlator efficiency
    # = (Cross_A(P)/Auto_A(P)) / (Cross_M(P)/Auto_M(P)), P is the total input power
    def __post_init__(self):
        """Calculate derived fields."""
        self.cross_over_auto = self.cross / self.auto
        self.efficiency = self.cross_over_auto / MODEL_CROSS_AUTO_RATIO


def create_test_config(mode: str) -> ProcessorVdTestConfig:
    """
    Create a PTC#15 test configuration.

    :param mode: "delay" or "no_delay"
    """
    packets_per_integration = 3 * 144
    # 15.6 Capture output visibilities in a CNIC -
    # 144 visibility channels, 5 integration 2 autos + 1 cross
    zero_delay = [{"stn": station, "nsec": 0.0} for station in stations]
    config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=80,
        # integrations +1 for init packets
        cnic_capture_packets=(N_INTEGRATIONS + 1) * packets_per_integration,
        scan_config=deepcopy(scan_2stn_1sa_1bm_1ch),  # in case modified below
        firmware_type=Personality.CORR,
        vd_config={},  # applied inside test loop
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        beam_delay_config=[
            {
                "subarray_id": 1,
                "beam_id": 1,
                "delay": zero_delay,
            }
        ],
        source_delay_config=[
            {
                "subarray_id": 1,
                "beam_id": 1,
                "delay": [zero_delay] * 4,
            }
        ],
    )
    if mode == "no_delay":
        return config
    if mode != "delay":
        raise NotImplementedError(f"Test mode {mode} not implemented")

    # 15.13 Repeat steps PTC#15.1 to PTC#15.12, but with delay model set for a -30
    # degree declination and field centre transiting, station ~75km EW. Source of
    # coherent noise at centre of field.
    direction = {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"}
    # mostly the same parameters as static, add delay stuff
    config.cnic_capture_packets += DELAY_DISCARD_INTEGRATIONS * packets_per_integration
    config.beam_delay_config = [
        {
            "subarray_id": 1,
            "beam_id": 1,
            "direction": direction,
        }
    ]
    config.source_delay_config = [
        {
            "subarray_id": 1,
            "beam_id": 1,
            "direction": [direction] * 4,
        }
    ]
    return config


# 15.9 Repeat steps PTC#15.1 to PTC#15.8 (except for steps PTC#15.2 and .3 as the ratio
# Cross_M(P)/Auto_M(P) is independent of signal level in floating point correlator)
# with RMS values of 2 to 80 in increments of 1, except 10 to 65 in steps of 10.
# FIXME - the real list is WAY too long for use while developing!
#  - updated list of points to be supplied by JB
# rms_amplitudes = list(range(1, 10)) + list(range(10, 70, 10)) + list(range(70, 81))
rms_amplitudes = [1, 2, 3, 5, 10, 20, 30, 40, 50, 60, 70, 80]


@pytest.mark.parametrize("mode", ("no_delay", "delay"))
@pytest.mark.timeout(60 * N_INTEGRATIONS * len(rms_amplitudes))
@pytest.mark.hardware_present
def test_ptc15_corr_visibility_snr(
    request,
    mode,
    cnic_and_processor,  # noqa
    ready_subarray,  # noqa
):
    """Test PTC#15 (Visibility SNR)."""
    results = []
    corr_fw_version = version_under_test(Personality.CORR)  # used to label graphs
    test_config = create_test_config(mode)

    for scale in rms_amplitudes:
        test_name = f"{request.node.name}_{scale}"
        test_config.vd_config = create_vd_config(rms_scale=scale)
        correlator_output = cnic_processor(
            cnic_and_processor, ready_subarray, test_config
        )
        analyser = VisibilityAnalyser(
            json.dumps(
                {
                    "coarse_channels": channels,
                    "stations": stations,
                }
            )
        )
        analyser.extract_spead_data(correlator_output.name)
        if mode == "delay":
            analyser.remove_visibilities(DELAY_DISCARD_INTEGRATIONS)
        # 15.7 Average across all channels and integrations giving Alveo cross
        # correlator power (Cross_A) and Alveo autocorrelation power (Auto_A)
        os.makedirs(PTC15_DIR, exist_ok=True)
        analyser.save_heatmap_to_disk(
            mode="average_all", case=test_name + "_", directory=PTC15_DIR
        )
        # average_correlation will be a matrix like:
        # [1X*1X, 1Y*1X, N/A  , N/A  ]
        # [1X*1Y, 1Y*1Y, N/A  , N/A  ]
        # [1X*2X, 1Y*2X, 2X*2X, 2Y*2X]
        # [1X*2Y, 1Y*2Y, 2X*2Y, 2Y*2Y]
        results.append(
            Ptc15Result(
                scale=scale,
                cross=float(analyser.average_correlation[2][0]),
                auto=float(analyser.average_correlation[0][0]),
            )
        )

    save_results(
        results,
        graph_title=(
            f"PTC #15 - {mode.replace('_', ' ').title()}.\n"
            f"Correlator FW v{corr_fw_version}. {N_INTEGRATIONS} integrations."
        ),
        file_prefix=mode,
    )

    # 15.11 Below power levels = 25, this is the correlator SNR, above power levels the
    # loss corresponds to reduced Alveo power due to clipping. For low received signal
    # SNR > 1, Clipped value still represent the signal and correlate. This is not added
    # noise but gives a gain reduction which is also deleterious. But SNR must be at
    # least as good as the measured correlator efficiency.
    # 15.12 From previous tests determine signal level which optimise correlator
    # efficiency, at this point the efficiency should be much better than 0.99

    best_result = max(results, key=lambda r: r.efficiency)
    _ptc15_report(test_config.scan_config, results, mode)
    assert best_result.efficiency > EFFICIENCY_THRESHOLD


def save_results(results: Iterable[Ptc15Result], graph_title: str, file_prefix: str):
    """Record PTC#15 Results - Graphs, CSV, etc."""
    all_results_file_path = os.path.join(PTC15_DIR, f"{file_prefix}_all_results.csv")
    os.makedirs(os.path.dirname(all_results_file_path), exist_ok=True)
    with open(all_results_file_path, "w", encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=asdict(results[0]).keys())
        writer.writeheader()
        for result in results:
            writer.writerow(asdict(result))

    figure, axes = plt.subplots()
    axes.plot(
        [r.scale for r in results], [r.cross_over_auto for r in results], marker="o"
    )
    axes.set_title(graph_title)
    axes.set_ylabel("Gain: Cross-Correlation / Auto-Correlation")
    axes.set_xlabel("Input Amplitude (bits RMS)")
    figure.tight_layout()
    plt.savefig(os.path.join(PTC15_DIR, f"{file_prefix}_gain.png"))
    plt.close()
    # 15.10 Plot results of SNR vs input power level
    figure, axes = plt.subplots()
    axes.plot([r.scale for r in results], [r.efficiency for r in results], marker="o")
    plt.axhline(y=EFFICIENCY_THRESHOLD, color="r", linestyle="-")
    axes.set_title(graph_title)
    axes.set_ylabel(f"Efficiency (Cross/Auto vs Model: {MODEL_CROSS_AUTO_RATIO})")
    axes.set_xlabel("Input Amplitude (bits RMS)")
    figure.tight_layout()
    plt.savefig(os.path.join(PTC15_DIR, f"{file_prefix}_efficiency.png"))
    plt.close()

    # Find the input amplitudes where we pass the threshold
    amplitudes_exceed_threshold = [
        result.scale for result in results if result.efficiency > EFFICIENCY_THRESHOLD
    ]
    summary_file_path = os.path.join(PTC15_DIR, f"{file_prefix}_summary.txt")
    os.makedirs(os.path.dirname(summary_file_path), exist_ok=True)
    with open(summary_file_path, "w", encoding="utf-8") as log:
        print(
            f"We exceed the efficiency threshold {EFFICIENCY_THRESHOLD}"
            f" for input RMS amplitudes of {amplitudes_exceed_threshold}",
            file=log,
        )


def _ptc15_report(scan_config, results, mode):
    """Generate Test Report for PTC15."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "671b0a96d4622ca0300afe24"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    amplitudes_exceed_threshold = [
        result.scale for result in results if result.efficiency > EFFICIENCY_THRESHOLD
    ]
    best_result = max(results, key=lambda r: r.efficiency)
    replace_word_in_report(
        file_result_to_replace,
        "AMPLITUDEEXCEED",
        f"{amplitudes_exceed_threshold}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "MODE",
        f"{mode}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "PASSINGREQ",
        f"{best_result.efficiency > EFFICIENCY_THRESHOLD}",
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    for snr in rms_amplitudes:
        shutil.copy(
            f"{PTC15_DIR}/test_ptc15_visibility_snr[{mode}]_{snr}_heat_map.png",
            local_directory
            + new_repo_name
            + f"/test_ptc15_visibility_snr[{mode}]_{snr}_heat_map.png",
        )
        repo.git.add(
            new_repo_name + f"/test_ptc15_visibility_snr[{mode}]_{snr}_heat_map.png"
        )
    shutil.copy(
        f"{PTC15_DIR}/{mode}_efficiency.png",
        local_directory + new_repo_name + f"/{mode}_efficiency.png",
    )
    shutil.copy(
        f"{PTC15_DIR}/{mode}_gain.png",
        local_directory + new_repo_name + f"/{mode}_gain.png",
    )

    repo.git.add(new_repo_name + f"/{mode}_efficiency.png")
    repo.git.add(new_repo_name + f"/{mode}_gain.png")
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")

    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()
