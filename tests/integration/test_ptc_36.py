# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 36 - PSS Beam SNR at different pointing offsets.

PST equivalent: PTC 27

Due to weights limitations, we can't perform the test as written in the spreadsheet.
Instead we do 4 separate scans:

+------+-------+----------+-----------------+----------------------+
| Scan | Beams | Stations | Weights         | PSS Beam Pointing    |
+======+=======+==========+=================+======================+
| 1    | 1     | 100      | All 1.0         | Same as station beam |
+------+-------+----------+-----------------+----------------------+
| 2    | 1     | 100      | 1× 1.0, 99× 0.0 | Same as station beam |
+------+-------+----------+-----------------+----------------------+
| 3    | 1     | 100      | All 1.0         | Offset by 1 deg      |
+------+-------+----------+-----------------+----------------------+
| 4    | 1     | 100      | 1× 1.0, 99× 0.0 | Offset by 1 deg      |
+------+-------+----------+-----------------+----------------------+

In each case, the CNIC-VD source is in the same place as the PSS beam.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC#36 in AA1 FAT Presentation <https://docs.google.com/presentation/d/10b5T6NnAfeJRKAuRaKQ6UQlmLB7VtnlFEGsaBev7Ahc/edit#slide=id.g274a0cfa7da_0_431>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime

import numpy as np
import pytest
from tango import DeviceProxy

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import payloads

from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor
from .test_ptc_27 import (
    ALLOCATOR_ADDR,
    DELAY_SUB,
    PST_SERVER_IP,
    PST_SUB,
    channels,
    cnic_config,
    delay_aligned,
    delay_offset,
    freq_power_per_beam,
    plot_power_ratios,
    ptc27_ptc36_report,
    station_beam_delay,
    stations,
)

BUILD_DIR = "build/ptc36"
"""Build directory for intermediate analysis products & plots."""
DATA_DIR = "/test-data/PTC/ptc36/"
"""Directory name where data file will be stored (PCAP)"""
PCAP_1 = os.path.join(DATA_DIR, "1.pcap")
"""PCAP file - Scenario 1."""
PCAP_2 = os.path.join(DATA_DIR, "2.pcap")
"""PCAP file - Scenario 2."""
PCAP_3 = os.path.join(DATA_DIR, "3.pcap")
"""PCAP file - Scenario 3."""
PCAP_4 = os.path.join(DATA_DIR, "4.pcap")
"""PCAP file - Scenario 4."""
TEST_TIMEOUT_SEC = 1000
BEAM_ID = 11
WEIGHT_ONE_STATION = [1.0] + [0.0] * (len(stations) - 1)
WEIGHT_ALL_STATIONS = [1.0] * len(stations)


def generate_scan_config(weights):
    """
    Generate PTC 36 Scan Configuration.

    We can't share this with PTC 27 because PSS weights don't work the same way.
    Instead, we need to do one beam at a time.
    """
    return {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": DELAY_SUB}
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": BEAM_ID,
                        "stn_beam_id": 1,
                        "delay_poly": PST_SUB + "1",
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": PST_SERVER_IP,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": weights,
                    }
                ],
            },
        },
    }


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc36_pss_beam_snr(cnic_and_processor, ready_subarray):
    """
    Implement PTC 36.

    In this test, we aim to show that:
    * PSS beams can be steered in a different direction to the station beam
    * the FPGA can apply appropriate delays to steer pulsar beams
    """
    print(datetime.now(), "Starting Test")
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)

    # FIXME? Danger! This reduces the virtual channel limit for all future PSS scans
    allocator = DeviceProxy(ALLOCATOR_ADDR)
    allocator.internalAlveoLimits = json.dumps({"pss": {"vch": 900}})

    log_test_settings(allocator, BUILD_DIR)

    print(datetime.now(), "1. All stations contributing, Beam same dir as station")
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # 4800 packets = 5.3 sec of data, only one beam
        cnic_capture_packets=4800 * len(channels) * 1,
        scan_config=generate_scan_config(weights=WEIGHT_ALL_STATIONS),
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        beam_delay_config=[station_beam_delay],
        pst_delay_config=delay_aligned,
        source_delay_config=[delay_aligned],
        do_fw_switcharoo=True,
    )
    f_aligned_all, p_aligned_all = freq_power_per_beam(
        payloads(
            cnic_processor(cnic_and_processor, ready_subarray, test_config, PCAP_1)
        ),
        [BEAM_ID],
    )

    print(datetime.now(), "2. One station contributing, Beam same dir as station")
    test_config.scan_config = generate_scan_config(weights=WEIGHT_ONE_STATION)
    _, p_aligned_one = freq_power_per_beam(
        payloads(
            cnic_processor(cnic_and_processor, ready_subarray, test_config, PCAP_2)
        ),
        [BEAM_ID],
    )

    plot_power_ratios(
        f_aligned_all[BEAM_ID],
        np.array(p_aligned_all[BEAM_ID]) / np.array(p_aligned_one[BEAM_ID]),
        os.path.join(BUILD_DIR, "ratio.png"),
        "PSS",
    )
    ratio_aligned = np.mean(p_aligned_all[BEAM_ID]) / np.mean(p_aligned_one[BEAM_ID])
    req_aligned = ratio_aligned == pytest.approx(len(stations) ** 2, rel=0.02)

    print(datetime.now(), "Repeating with Offset")
    test_config.pst_delay_config = delay_offset
    test_config.source_delay_config = [delay_offset]

    print(datetime.now(), "3. All stations contributing, Beam offset")
    test_config.scan_config = generate_scan_config(weights=WEIGHT_ALL_STATIONS)
    f_offset_all, p_offset_all = freq_power_per_beam(
        payloads(
            cnic_processor(cnic_and_processor, ready_subarray, test_config, PCAP_3)
        ),
        [BEAM_ID],
    )

    print(datetime.now(), "4. One station contributing, Beam offset")
    test_config.scan_config = generate_scan_config(weights=WEIGHT_ONE_STATION)
    _, p_offset_one = freq_power_per_beam(
        payloads(
            cnic_processor(cnic_and_processor, ready_subarray, test_config, PCAP_4)
        ),
        [BEAM_ID],
    )
    plot_power_ratios(
        f_offset_all[BEAM_ID],
        np.array(p_offset_all[BEAM_ID]) / np.array(p_offset_one[BEAM_ID]),
        os.path.join(BUILD_DIR, "ratio_beam.png"),
        "PSS",
    )
    ratio_offset = np.mean(p_offset_all[BEAM_ID]) / np.mean(p_offset_one[BEAM_ID])
    req_offset = ratio_offset == pytest.approx(len(stations) ** 2, rel=0.02)

    ptc27_ptc36_report(
        test_config.scan_config,
        BUILD_DIR,
        repo_hash="672aa1c7b562bd6a1a75d9d6",
        req_aligned_met=req_aligned,
        req_offset_met=req_offset,
        ratio_aligned=ratio_aligned,
        ratio_offset=ratio_offset,
    )
    assert req_aligned, f"Aligned case ratio {ratio_aligned} != {len(stations)**2}"
    assert req_offset, f"Offset case ratio {ratio_offset} != {len(stations)**2}"
    print(datetime.now(), "End of Test")


def log_test_settings(allocator: DeviceProxy, build_dir: str) -> None:
    """
    Record settings used by the test.

    :param allocator: Proxy to Allocator
    :param build_dir: Build directory to write to
    """
    with open(os.path.join(build_dir, "log.txt"), "w", encoding="utf-8") as log:
        log.write("Allocator.internalAlveoLimits\n")
        log.write(str(json.loads(allocator.internalAlveoLimits)))
        log.write("\n")
        log.write(f"# Stations: {len(stations)}\n")
        log.write(f"# Channels: {len(channels)}\n")
