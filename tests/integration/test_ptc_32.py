# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#

"""Perentie Test Case 32 - PST Beams use single station beam.

Based on Jupyter notebook: /PI23/FAT1/PTC32/PTC32.ipynb

- `Low CBF Requirements <https://is.gd/sul9PW>`_
- `Perentie Test Cases <https://is.gd/BCLuaC>`_
- `PTC #32 <https://is.gd/aEH8X1>`_

1. Setup the CNIC to generate data for two sources for 6-stations (321, 351,
   381, 393, 399, 429) containing two station beams: the first has 32 channels
   with increasing amplitude per channel, and the second station beam has 64
   channels with decreasing amplitude per channel.

2. Set the LOW CBF delay polynomial simulator to point the station beam at
   RA: 2h30m DEC:-26d49m50.0s, and the PST beam at RA: 2h30m DEC: -29d49m50.0s,
   which results in PST beams 1 degrees offset from the station beam. Point the
   second station beam at:
   RA: 0h30m DEC:-6d49m50.0s, and the PST beam at RA: 0h30m DEC: -7d49m50.0s

3. Setup a subarray to form 2 PST beams: the first uses frequency channels from
   the first station beam, and the second uses frequency channels from the
   second station beam. Configure the each PST beam data to go to different CNIC
   destinations.

4. Start the scan

5. Capture 5 seconds of data in each CNIC-Rx

6. Input and analyse the captured data from each CNIC - plot a spectragam of
   frequency vs time and check there is a slope across the frequency
   channels. Confirm data belonging to each beam arrived at the correct CNIC.

NOTE: keep this `guideline in mind <https://is.gd/vJIF5B>` _
"""

import json
import shutil

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.pst_utils import freq_power_per_beam

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

# Jupyter notebook creates channel plots, probably not needed
CHANNEL_PLOT_NEEDED = False

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 54321
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
STATION_BEAM_1 = BeamId.SINGLE_BASIC
STATION_BEAM_2 = BeamId.PTC32
PST_BEAM_11 = 11
PST_BEAM_12 = 12
all_pst_beams = (PST_BEAM_11, PST_BEAM_12)

# ---------------------------
# data location (pcap, png)
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc32/"
"""Directory name where data files will be stored (PCAP/graphs)"""


# Step 32.1: a list of required stations, channels
station_ids = (321, 351, 381, 393, 399, 429)
N_STATIONS = len(station_ids)
stations = [(station, 1) for station in station_ids]

N_CHANNELS = 32
channels_1 = list(range(64, 64 + N_CHANNELS))
channels_2 = list(range(64, 64 + N_CHANNELS * 2))
STATION_WEIGHTS = [1.0] * N_STATIONS
DESTINATION_HOSTS = [
    {
        "data_host": "192.168.2.2",
        "data_port": 11001,
        "start_channel": 0,
        "num_channels": 144,
    }
]

# scan configuration
STAT1_DELAY_POLY = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_1:02}"
STAT2_DELAY_POLY = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_2:02}"

PST1_DELAY_POLY = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_1:02}_1"
PST2_DELAY_POLY = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_2:02}_1"
pst_scan_cfg = {
    "id": SCAN_ID,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": STATION_BEAM_1,
                    "freq_ids": channels_1,
                    "delay_poly": STAT1_DELAY_POLY,
                },
                {
                    "beam_id": STATION_BEAM_2,
                    "freq_ids": channels_2,
                    "delay_poly": STAT2_DELAY_POLY,
                },
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": PST_BEAM_11,
                    "stn_beam_id": STATION_BEAM_1,
                    "delay_poly": PST1_DELAY_POLY,
                    "jones": "tbd",
                    "destinations": DESTINATION_HOSTS,
                    "stn_weights": STATION_WEIGHTS,
                },
                {
                    "pst_beam_id": PST_BEAM_12,
                    "stn_beam_id": STATION_BEAM_2,
                    "delay_poly": PST2_DELAY_POLY,
                    "jones": "tbd",
                    "destinations": DESTINATION_HOSTS,
                    "stn_weights": STATION_WEIGHTS,
                },
            ],
        },
    },
}

# STEP 32.2 configure delay poly
beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
src_radec = {"ra": "2h30m00.00s", "dec": "-26d19m50.0s"}
pst_radecs = [src_radec] * 4
beamdir = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": STATION_BEAM_1,
    "direction": beam_radec,
}

srcdir = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": STATION_BEAM_1,
    "direction": pst_radecs,
}
pstdirs = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": STATION_BEAM_1,
    "direction": pst_radecs,
}


# CNIC capture settings
# ---------------------
OUTPUT_PACKET_COUNT = 30_000
OUTPUT_PACKET_SIZE = 200  # to avoid PTP packets
PCAP_PATH = DATA_DIR + f"ptc32_pst_{N_STATIONS}_stations.pcap"

# VD config
# ---------------
vd_beam_1_cfg = [  # list of dicts - one per SPEAD stream
    {
        "scan": SCAN_ID,
        "subarray": SUBARRAY_ID,
        "station": station,
        "substation": substation,
        "frequency": channel,
        "beam": STATION_BEAM_1,
        "sources": {
            "x": [
                {"tone": False, "seed": 1000, "scale": 10 + 10 * channel},
            ],
            "y": [
                {"tone": False, "seed": 1000, "scale": 10 + 10 * channel},
            ],
        },
    }
    for station, substation in stations
    for channel in channels_1
]

vd_beam_2_cfg = [  # list of dicts - one per SPEAD stream
    {
        "scan": SCAN_ID,
        "subarray": SUBARRAY_ID,
        "station": station,
        "substation": substation,
        "frequency": channel,
        "beam": STATION_BEAM_2,
        "sources": {
            "x": [
                {"tone": False, "seed": 2000, "scale": 3000 - 10 * channel},
            ],
            "y": [
                {"tone": False, "seed": 2000, "scale": 3000 - 10 * channel},
            ],
        },
    }
    for station, substation in stations
    for channel in channels_2
]

vd_config = {"sps_packet_version": 3, "stream_configs": vd_beam_1_cfg + vd_beam_2_cfg}

test_config = ProcessorVdStaticTrackingTestConfig(
    cnic_capture_min_size=OUTPUT_PACKET_SIZE,
    cnic_capture_packets=OUTPUT_PACKET_COUNT,
    scan_config=pst_scan_cfg,
    firmware_type=Personality.PST,
    input_packet_count=None,
    start_scan_packet_count=None,
    before_end_scan_packet_count=None,
    output_packet_count=None,
    vd_config=vd_config,
    during_scan_callback=None,
    source_delay_config=[
        srcdir,
    ],
    beam_delay_config=[
        beamdir,
    ],
    pst_delay_config=pstdirs,
)


@pytest.mark.hardware_present
def test_ptc32_pst_beam_single_station_beam(cnic_and_processor, ready_subarray):
    """Implement PTC 32: simulate station data streams, capture, analyse."""
    # SPS signal generation and PCAP capture
    # --------------------------------------
    psr_pcap = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        pcap_file_name=PCAP_PATH,
    )
    psr_pcap.close()

    payloads = get_udp_payload_bytes(PCAP_PATH, min_udp_data_size=64)
    total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads, all_pst_beams)

    # plot the graphs
    # ---------------
    colours = ["blue", "green", "red", "purple"]
    n_colours = len(colours)
    slope_linear_reg = {}
    for i, beam in enumerate(all_pst_beams):
        colour = colours[i % n_colours]
        plt.clf()
        plt.plot(
            total_freqs_beam[beam],
            total_pwr_beam[beam],
            "g",
            label=f"beam {beam}",
            color=colour,
        )
        plt.title("Average PST beam power per complex sample")
        plt.xlabel("PST channel number")
        plt.ylabel("Power (linear scale)")
        plt.legend()
        plt.grid()
        slope = "asc" if beam == PST_BEAM_11 else "dsc"
        params = np.polyfit(total_freqs_beam[beam], total_pwr_beam[beam], 1)
        slope_linear_reg[slope] = params[0]
        plt.savefig(f"{DATA_DIR}beam_{beam}_{slope}.png")

    ptc32_report(
        test_config.scan_config, slope_linear_reg["asc"], slope_linear_reg["dsc"]
    )
    assert slope_linear_reg["asc"] > 0
    assert slope_linear_reg["dsc"] < 0


def ptc32_report(scan_config, slope_asc, slope_dsc):
    """Generate Overleaf test report."""
    repo_id = "6728119d030fe5e76d7058bb"
    git_workarea, test_run_subdir, repo = download_overleaf_repo(repo_id)

    test_results_dir = f"{git_workarea}{test_run_subdir}/"
    report_file = test_results_dir + "report.tex"

    for key, val in (
        ("TOPUTDIRECTORY", test_run_subdir),
        ("CONFIGURE_SCAN", json.dumps(scan_config)),
        ("TOPUTDATE", test_run_subdir.split("_")[1]),
        ("SLOPE_ASC", f"{slope_asc:.3f}"),
        ("REQ_ASC", str(slope_asc > 0)),
        ("SLOPE_DSC", f"{slope_dsc:.3f}"),
        ("REQ_DSC", str(slope_dsc < 0)),
    ):
        replace_word_in_report(report_file, key, val)

    for png_plot in (f"beam_{PST_BEAM_11}_asc.png", f"beam_{PST_BEAM_12}_dsc.png"):
        shutil.copy(f"{DATA_DIR}{png_plot}", test_results_dir)
        repo.git.add(f"{test_run_subdir}/{png_plot}")

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + test_run_subdir
        + """/report}"""
    )
    replace_word_in_report(
        git_workarea + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )

    repo.git.add("report2017.tex")
    repo.git.add(test_run_subdir + "/report.tex")

    repo.index.commit("Test with python")
    repo.git.push()
