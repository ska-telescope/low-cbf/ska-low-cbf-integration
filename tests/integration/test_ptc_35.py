# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 35 - PSS General Functionality Test.
Summary:
    PART 1
    Show that PSS beamformer is generating beamformed PSS data packets
    - send SPS data 6 stations, 1 station beam, 1 coarse channel, one tone source
    - form a two PSS beams, pointed in same direction but with different station weights
    - capture SPS input and PSS output
    - Test pass:
        A. show that the PSSbeam with all stations uniformly weighted has 36x power
           of the PSS beam with only one station unity-weighted and the other five
           zero-weighted
    PART2
    Show that with 4 tone sources at different frequencies, in different directions,
    the received signal in a beam is largest when the beam is pointed at the source
    - send sps data 8 stations, 1 station beam, 1 coarse channel, four tone sources
        in different directions
    - form 4 PSS beams, one pointed at each source, all stations unity weighted
    - Test pass:
        B. show that the tone source in direction of the beam has greater amplitude
          than the other tones


More information:
    - `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
    - `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
    - `PTC #35 Presentation <https://where.is.it>`_  # TODO
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from typing import Iterable

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.psr_analyser import PulsarPacket

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 600
PSS_CHANS_PER_PACKET = 54


station_ids = [
    321,
    351,
    383,
    393,
    399,
    429,
    435,
    471,
    1,
    2,
]

# both tests
beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
# first test
src0_radec = {"ra": "2h30m00.00s", "dec": "-21d49m50.0s"}
# second test
src1_radec = {"ra": "2h30m00.00s", "dec": "-28d49m50.0s"}
src2_radec = {"ra": "2h30m00.00s", "dec": "-30d49m50.0s"}
src3_radec = {"ra": "2h30m00.00s", "dec": "-32d49m50.0s"}
src4_radec = {"ra": "2h30m00.00s", "dec": "-34d49m50.0s"}

beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": beam_radec,
}

pss_radecs_beam_test1 = [src0_radec, src0_radec, src0_radec, src0_radec]
source_delay_config_test1 = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pss_radecs_beam_test1,
}
pst_delay_config_test1 = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pss_radecs_beam_test1,
}
station_weights_test1 = {
    11: [1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    12: [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
    13: [0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
    14: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
}

pss_radecs_beam_test2 = [src1_radec, src2_radec, src3_radec, src4_radec]
source_delay_config_test2 = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pss_radecs_beam_test2,
}
pst_delay_config_test2 = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": pss_radecs_beam_test2,
}
station_weights_test2 = {
    11: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    12: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    13: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    14: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc35_pss_general_functionality(cnic_and_processor, ready_subarray):
    """
    Implement PTC 35.

    In this test we aim to show that:
    * PSS beams can be steered in a different direction to the station beam
    * the FPGA can apply appropriate delays to steer search beams
    """
    # pylint: disable=too-many-locals,too-many-statements
    delay_sub = "low-cbf/delaypoly/0/delay_s01_b01"

    pss_sub = "low-cbf/delaypoly/0/pst_s01_b01_"
    print(datetime.now(), "Starting PTC#35")
    n_stations = 8
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 1
    first_channel = 448
    channels = list(range(first_channel, first_channel + n_channels))
    pss_beam_ids = [11, 12, 13, 14]
    pss_svr_ip = "192.168.55.55"
    n_stations_beam = 8
    stations_beam = [[station, 1] for station in station_ids[:n_stations_beam]]
    # Generate CNIC-VD Configuration
    cnic_config_test1 = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1982, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_test1a = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pss_sub + str(pss_bm_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pss_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": station_weights_test1[pss_beam],
                    }
                    # beam 11 uses only one station
                    for pss_bm_idx, pss_beam in enumerate([11])
                ],
            },
        },
    }
    scan_config_test1b = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pss_sub + str(pss_bm_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pss_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": station_weights_test1[pss_beam],
                    }
                    # beam 14 uses all stations
                    for pss_bm_idx, pss_beam in enumerate([14])
                ],
            },
        },
    }

    tone_freqs = [0, -8192, 8192, 12288]  # tone frequencies in CNIC steps
    cnic_step_per_pss_chan = 512
    pss_middle_chan = 27  # 54 PSS chans per SPS coarse channel
    tone_chans = [
        pss_middle_chan + round(f / cnic_step_per_pss_chan) for f in tone_freqs
    ]

    cnic_config_test2 = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": True, "fine_frequency": tone_freqs[0], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[1], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[2], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[3], "scale": 100},
                ],
                "y": [
                    {"tone": True, "fine_frequency": tone_freqs[0], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[1], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[2], "scale": 100},
                    {"tone": True, "fine_frequency": tone_freqs[3], "scale": 100},
                ],
            },
        }
        for station, substation in stations_beam
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_test2 = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations_beam,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pst_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pss_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pss_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": station_weights_test2[pst_beam],
                    }
                    for station_idx, pst_beam in enumerate(pss_beam_ids)
                ],
            },
        },
    }
    # Run first test First part - 1 station contributing to beam
    test_cfg1a = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=48 * n_channels * 100,  # 48pkts/frame, 100frames=5.3sec
        scan_config=scan_config_test1a,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_test1,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_test1],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_test1,
    )
    opened_pcap_file = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_cfg1a,
        "/test-data/PTC/ptc35/test1a.pcap",
    )
    filename = opened_pcap_file.name
    opened_pcap_file.close()
    print(datetime.now(), f"Captured PTC#35 test1a output: {filename}")
    payloads = get_udp_payload_bytes(filename, 64)
    total_freqs, test1a_total_pwr = freq_power_per_beam(payloads, pss_beam_ids)

    # Run first test Second part - 6 stations contributing to beam
    test_cfg1b = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=48 * n_channels * 100,  # 48pkts/frame, 100frames=5.3sec
        scan_config=scan_config_test1b,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_test1,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_test1],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_test1,
    )
    opened_pcap_file = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_cfg1b,
        "/test-data/PTC/ptc35/test1b.pcap",
    )
    filename = opened_pcap_file.name
    opened_pcap_file.close()
    print(datetime.now(), f"Captured PTC#35 test1b output: {filename}")
    payloads = get_udp_payload_bytes(filename, 64)
    total_freqs, test1b_total_pwr = freq_power_per_beam(payloads, pss_beam_ids)

    # Run second test
    test_cfg2 = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=48 * n_channels * 100,  # 48pkts/frame, 100frames=5.3sec
        scan_config=scan_config_test2,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_test2,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_test2],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_test2,
    )
    opened_pcap_file = cnic_processor(
        cnic_and_processor, ready_subarray, test_cfg2, "/test-data/PTC/ptc35/test2.pcap"
    )
    filename = opened_pcap_file.name
    opened_pcap_file.close()
    print(datetime.now(), f"Captured PTC#35 test2 output: {filename}")
    payloads_beam = get_udp_payload_bytes(filename, 64)
    total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads_beam, pss_beam_ids)

    # TODO Make report
    _ptc35_report(
        test_cfg1a,
        total_freqs,
        test1a_total_pwr,
        test1b_total_pwr,
        total_freqs_beam,
        total_pwr_beam,
        test_cfg2,
        tone_chans,
        n_stations,
    )
    print(f"remove me {len(total_freqs), len(total_freqs_beam)}")

    # Test 1: check 1-station to 8-station beam pwr ratio to prove beamforming
    # Test1b beam 14 has 1.0 weight for all 8 stations. Test1a beam 11 has one station
    relative_power = np.mean(test1b_total_pwr[14]) / np.mean(test1a_total_pwr[11])
    expected_gain = n_stations * n_stations
    assert expected_gain * 0.98 < relative_power < expected_gain * 1.02

    # Test 2: check beamforming results in pointed-to source being strongest source
    # ie that Max tone power for each beam is for the source it is pointed at
    assert total_pwr_beam[11].index(max(total_pwr_beam[11])) == tone_chans[0]
    assert total_pwr_beam[12].index(max(total_pwr_beam[12])) == tone_chans[1]
    assert total_pwr_beam[13].index(max(total_pwr_beam[13])) == tone_chans[2]
    assert total_pwr_beam[14].index(max(total_pwr_beam[14])) == tone_chans[3]
    print(datetime.now(), "End of PTC#35")


def freq_power_per_beam(
    payloads: Iterable[bytes], psr_beam_ids: Iterable[int]
) -> (list, list):
    """Get frequencies and power for the given beams."""
    # pylint: disable=too-many-locals
    total_freqs = {}
    total_pwr = {}
    for beam_number in psr_beam_ids:
        # accumulate per-pst-channel power with first-chan as key
        np_chan_pwr = {}  # numpy power data by sequence num and first chan
        chan_pkts = {}
        # get sample by sample power from packet payloads
        for pkt_payload in payloads:
            psr_packet = PulsarPacket(pkt_payload)
            if psr_packet.beam_id != beam_number:
                continue
            first_chan = psr_packet.first_channel
            if first_chan not in np_chan_pwr:
                np_chan_pwr[first_chan] = np.zeros(PSS_CHANS_PER_PACKET)
                chan_pkts[first_chan] = 0
            np_chan_pwr[first_chan] += psr_packet.channel_power()
            chan_pkts[first_chan] += 1

        # get average power-per-sample numbers by dividing by number added
        for first_chan_key in np_chan_pwr:
            np_chan_pwr[first_chan_key] = (
                np_chan_pwr[first_chan_key] / chan_pkts[first_chan_key]
            )

        freqs = []
        pwr = []
        ordered_chans = sorted(np_chan_pwr.keys())
        for channel in ordered_chans:
            # there are 54 consecutive PSS freqs in each packet/block
            frequencies = [channel + i for i in range(0, PSS_CHANS_PER_PACKET)]
            freqs.extend(frequencies)
            pwr.extend(np_chan_pwr[channel].tolist())
        total_freqs[beam_number] = freqs
        total_pwr[beam_number] = pwr

    return total_freqs, total_pwr


def _ptc35_report(
    test_config,
    total_freqs,
    test1a_total_pwr,
    test1b_total_pwr,
    total_freqs_beam,
    total_pwr_beam,
    test_config_beam,
    tone_chans,
    n_stations,
):
    """Generate Test Report."""
    # pylint: disable=too-many-arguments,too-many-locals
    relative_power = np.mean(test1b_total_pwr[14]) / np.mean(test1a_total_pwr[11])
    requirements_average_pwr = (
        (n_stations**2) * 0.99 < relative_power < (n_stations**2) * 1.01
    )
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "671875ce050f7b937216445e"
    )
    if not print_figures_for_ptc21_report(
        test1a_total_pwr,
        test1b_total_pwr,
        total_freqs,
        total_pwr_beam,
        total_freqs_beam,
        local_directory,
        tone_chans,
        n_stations,
        new_repo_name,
    ):
        print("error generating figures ")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_BEAM_SCAN",
        json.dumps(test_config_beam.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RATIO6O1",
        str(relative_power),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ6O1",
        str(requirements_average_pwr),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ1",
        str(f"{total_pwr_beam[11][tone_chans[0]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ2",
        str(f"{total_pwr_beam[11][tone_chans[1]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ3",
        str(f"{total_pwr_beam[11][tone_chans[2]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1FREQ4",
        str(f"{total_pwr_beam[11][tone_chans[3]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM1REQ",
        str(total_pwr_beam[11].index(max(total_pwr_beam[11])) == tone_chans[0]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ1",
        str(f"{total_pwr_beam[12][tone_chans[0]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ2",
        str(f"{total_pwr_beam[12][tone_chans[1]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ3",
        str(f"{total_pwr_beam[12][tone_chans[2]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2FREQ4",
        str(f"{total_pwr_beam[12][tone_chans[3]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM2REQ",
        str(total_pwr_beam[12].index(max(total_pwr_beam[12])) == tone_chans[1]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ1",
        str(f"{total_pwr_beam[13][tone_chans[0]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ2",
        str(f"{total_pwr_beam[13][tone_chans[1]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ3",
        str(f"{total_pwr_beam[13][tone_chans[2]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3FREQ4",
        str(f"{total_pwr_beam[13][tone_chans[3]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM3REQ",
        str(total_pwr_beam[13].index(max(total_pwr_beam[13])) == tone_chans[2]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ1",
        str(f"{total_pwr_beam[14][tone_chans[0]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ2",
        str(f"{total_pwr_beam[14][tone_chans[1]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ3",
        str(f"{total_pwr_beam[14][tone_chans[2]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4FREQ4",
        str(f"{total_pwr_beam[14][tone_chans[3]]:.2f}"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "BEAM4REQ",
        str(total_pwr_beam[14].index(max(total_pwr_beam[14])) == tone_chans[3]),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.pdf")
    repo.git.add(new_repo_name + "/beam11.pdf")
    repo.git.add(new_repo_name + "/beam12.pdf")
    repo.git.add(new_repo_name + "/beam13.pdf")
    repo.git.add(new_repo_name + "/beam14.pdf")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def print_figures_for_ptc21_report(
    test1a_total_pwr,
    test1b_total_pwr,
    total_freqs,
    total_pwr_beam,
    total_freqs_beam,
    local_directory,
    tone_chans,
    n_stations,
    new_repo_name,
):
    """Print the three figure for the report."""
    # pylint: disable=too-many-arguments
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    ratio = []
    for idx, pwr in enumerate(test1a_total_pwr[11]):
        ratio.append(test1b_total_pwr[14][idx] / pwr)

    plt.plot(total_freqs[14], ratio, label="Ratio 6st/1st")
    plt.hlines(
        n_stations**2,
        total_freqs[14][0],
        total_freqs[14][-1],
        "r",
        "--",
        label="Theoretical",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[11], total_pwr_beam[11], "g", label="beam 11")
    plt.axvline(
        x=total_freqs_beam[11][tone_chans[0]],
        color="c",
        ls="--",
        label="expected beam 11 spike",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()

    # plt.axis([0,216,0,1e6])
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam11.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[12], total_pwr_beam[12], "c", label="beam 12")
    plt.axvline(
        x=total_freqs_beam[12][tone_chans[1]],
        color="g",
        ls="--",
        label="expected beam 12 spike",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power linear scale)")
    plt.legend()

    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam12.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[13], total_pwr_beam[13], "r", label="beam 13")
    plt.axvline(
        x=total_freqs_beam[13][tone_chans[2]],
        color="b",
        ls="--",
        label="expected beam 13 spike",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()

    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam13.pdf", format="pdf")

    plt.figure()
    plt.plot(total_freqs_beam[14], total_pwr_beam[14], "b", label="beam 14")
    plt.axvline(
        x=total_freqs_beam[14][tone_chans[3]],
        color="r",
        ls="--",
        label="expected beam 14 spike",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam14.pdf", format="pdf")
    return True
