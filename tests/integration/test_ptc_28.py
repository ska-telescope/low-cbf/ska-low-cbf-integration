# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 28 - PST Beam power at different pointing offsets.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PST Beamforming Tests Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from math import floor
from typing import Iterable

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.psr_analyser import PulsarPacket

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc28/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC28_beamformer_"
"""PCAP file name"""


TEST_TIMEOUT_SEC = 7200


station_ids = list(range(345, 357)) + list(range(429, 435))


station_weights = {
    11: [1.0] * 18,
    12: [1.0] * 18,
    13: [1.0] * 18,
    14: [1.0] * 18,
}


def prepare_delay(max_offset):
    """Prepare the pointing configuration."""
    angle_offset_rev = []
    for beam in range(64):
        angle_offset_rev.append(
            f"{floor(beam * 60 / 64)}min"
            + f"{(beam * 60 / 64 - floor(beam * 60 / 64)) * 60}s"
        )
    angle_offset = list(reversed(angle_offset_rev))
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d00m00.0s"}
    degrees_half = floor(max_offset / 16)
    minutes = max_offset % 16

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 ]}",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 1]}",
    }
    src_radec_beam_3 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 2]}",
    }
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 3]}",
    }
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {"subarray_id": 1, "beam_id": 2, "direction": beam_radec}

    srcdir = {"subarray_id": 1, "beam_id": 2, "direction": [beam_radec] * 4}

    pstdirs = {"subarray_id": 1, "beam_id": 2, "direction": pst_radecs}

    return beamdir, srcdir, pstdirs


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc28_pst_beam_power(cnic_and_processor, ready_subarray):
    """
    Implement PTC 28.

    In this test we aim to show that with the same experiment repeating 96
    times pointing to 4 different beam direction while having a single source
    in the sky on the same direction as the station beam. Doing so we capture
    384 points of measurement across 3 degrees.
    """
    # pylint: disable=too-many-locals
    delay_sub = "low-cbf/delaypoly/0/delay_s01_b02"

    pst_sub = "low-cbf/delaypoly/0/pst_s01_b02_"
    print(datetime.now(), "Starting test")

    nb_stations = 18
    stations = [[station, 1] for station in station_ids[:nb_stations]]
    channels = [64]
    pst_beam_id = [11, 12, 13, 14]
    pst_svr_ip = "192.168.55.55"

    # Generate CNIC-VD Configuration
    cnic_config_noise = [
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 2,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_same_beam = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 2, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "timing_beams": {
                "beams": [
                    {
                        "pst_beam_id": pst_beam,
                        "stn_beam_id": 2,
                        "delay_poly": pst_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pst_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": station_weights[pst_beam],
                    }
                    for station_idx, pst_beam in enumerate(pst_beam_id)
                ],
            },
        },
    }
    for offset in range(16 * 6):
        (
            beam_delay_config,
            source_delay_config_beam,
            pst_delay_config_same_beam,
        ) = prepare_delay(offset)
        test_config = ProcessorVdStaticTrackingTestConfig(
            cnic_capture_min_size=200,
            cnic_capture_packets=792 * 10 * len(channels),
            scan_config=scan_config_same_beam,
            firmware_type=Personality.PST,
            input_packet_count=None,
            start_scan_packet_count=None,
            before_end_scan_packet_count=None,
            output_packet_count=None,
            vd_config={"sps_packet_version": 3, "stream_configs": cnic_config_noise},
            during_scan_callback=None,
            source_delay_config=[source_delay_config_beam],
            beam_delay_config=[beam_delay_config],
            pst_delay_config=pst_delay_config_same_beam,
        )
        _ = cnic_processor(
            cnic_and_processor,
            ready_subarray,
            test_config,
            PCAP_PATH_SAME_BEAM + f"{offset}.pcap",
        )
        print(datetime.now(), "Captured output, beginning analysis")

    total_freqs = {}
    total_pwr = {}
    pst_beam_id = [11, 12, 13, 14]
    for offset in range(16 * 6):
        payloads = get_udp_payload_bytes(PCAP_PATH_SAME_BEAM + f"{offset}.pcap", 64)
        total_freqs[offset], total_pwr[offset] = freq_power_per_beam(
            payloads, pst_beam_id
        )
    # payloads = get_udp_payload_bytes(correlator_output.name, 64)
    # total_freqs, total_pwr = freq_power_per_beam(payloads, pst_beam_id)

    # payloads_beam = get_udp_payload_bytes(correlator_output_beam.name, 64)
    # total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads_beam, pst_beam_id)
    passing_max, passing_ratio = _ptc28_report(
        scan_config_same_beam,
        total_pwr,
    )
    assert passing_max
    assert passing_ratio
    # Beam 14 has 1.0 weight for each of the 6 stations, 13 has only a single station
    # relative_power = np.mean(total_pwr[14]) / np.mean(total_pwr[13])
    # assert 36 * 0.99 < relative_power < 36 * 1.01
    # assert total_pwr_beam[11].index(max(total_pwr_beam[11])) == 108
    # assert total_pwr_beam[12].index(max(total_pwr_beam[12])) == 108 - 64
    # assert total_pwr_beam[13].index(max(total_pwr_beam[13])) == 108 + 64
    # assert total_pwr_beam[14].index(max(total_pwr_beam[14])) == 108 + 96
    print(datetime.now(), "End of test")


def freq_power_per_beam(
    payloads: Iterable[bytes], pst_beam_ids: Iterable[int]
) -> (list, list):
    """
    Get frequencies and power for the given beams.

    :returns: two lists, each with one entry per beam. first list contains lists
        of frequency channels, second list contains lists of average power per sample
        in each of those frequency channels
    """
    # pylint: disable=too-many-locals
    total_freqs = {}
    total_pwr = {}
    for beam_number in pst_beam_ids:
        # accumulate per-pst-channel power with first-chan as key
        np_chan_pwr = {}  # numpy power data by sequence num and first chan
        chan_pkts = {}
        # get sample by sample power from packet payloads
        for pkt_payload in payloads:
            psr_packet = PulsarPacket(pkt_payload)
            if psr_packet.beam_id != beam_number:
                continue
            first_chan = psr_packet.first_channel
            if first_chan not in np_chan_pwr:
                np_chan_pwr[first_chan] = np.zeros(24)
                chan_pkts[first_chan] = 0
            np_chan_pwr[first_chan] += psr_packet.channel_power()
            chan_pkts[first_chan] += 1

        # get average power-per-sample numbers by dividing by number added
        for first_chan_key in np_chan_pwr:
            np_chan_pwr[first_chan_key] = (
                np_chan_pwr[first_chan_key] / chan_pkts[first_chan_key]
            )

        freqs = []
        pwr = []
        ordered_chans = sorted(np_chan_pwr.keys())
        for channel in ordered_chans:
            # there are 24 consecutive pst freqs in each packet/block
            frequencies = [channel + i for i in range(0, 24)]
            freqs.extend(frequencies)
            pwr.extend(np_chan_pwr[channel].tolist())
        total_freqs[beam_number] = freqs
        total_pwr[beam_number] = pwr

    return total_freqs, total_pwr


def _ptc28_report(
    scan_config_same_beam,
    total_pwr,
):
    """Generate Test Report for PTC21."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6711d3a1510fced1d8f9db1b"
    )
    (
        printing_correctly,
        passing_max,
        passing_ratio,
        offset_mean_log,
    ) = print_figures_for_ptc28_report(
        total_pwr,
        local_directory,
        new_repo_name,
    )
    if not printing_correctly:
        print("error generating figures ")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config_same_beam),
    )

    replace_word_in_report(
        file_result_to_replace,
        "MAXCENTERED",
        str(passing_max),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQRATIO",
        str(passing_ratio),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RATIO15",
        str(offset_mean_log[33]),
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.png")
    repo.git.add(new_repo_name + "/ratio_db.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()

    return passing_max, passing_ratio


def print_figures_for_ptc28_report(
    total_pwr,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    offset_mean = []
    offset_mean_log = []
    offset_axis = []
    for offset in range(0, 16 * 6):
        offset_mean.append(np.mean(total_pwr[offset][11]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][11])))
        offset_axis.append(-2.984375 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][12]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][12])))
        offset_axis.append(-2.96875 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][13]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][13])))
        offset_axis.append(-2.953125 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][14]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][14])))
        offset_axis.append(-2.9375 + offset / 16)
    max_log = np.max(offset_mean_log)
    offset_mean_log = [offset_mean - max_log for offset_mean in offset_mean_log]

    plt.plot(offset_axis, offset_mean, "-x", label="Average power beam")
    # plt.title("Average PST beam power per complex sample")
    plt.xlabel("DEC offset (degree)")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.png", format="png")

    plt.figure()
    plt.plot(offset_axis, offset_mean_log, "-x", label="Average power beam")
    # plt.title("Average PST beam power per complex sample")
    plt.xlabel("DEC offset (degree)")
    plt.ylabel("Power (in dB)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio_db.png", format="png")
    passing_max = offset_mean.index(np.max(offset_mean)) == 191
    passing_ratio = offset_mean_log[offset_mean.index(np.max(offset_mean)) + 5] < -5

    return True, passing_max, passing_ratio, offset_mean_log
