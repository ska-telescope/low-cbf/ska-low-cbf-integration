# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 3 - SDP Visibility Metadata.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #3 Presentation <https://docs.google.com/presentation/d/1MBIdSQL8ZaSU1unV9YwAyklrh3wfmzkDI0IUk-Vni6Y>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import os
import pprint
from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import pytest
import spead2
import spead2.recv

from ska_low_cbf_integration.correlator import integration_periods
from ska_low_cbf_integration.firmware import Personality, version_under_test
from ska_low_cbf_integration.low_psi import serial_spead_hwid
from ska_low_cbf_integration.sps import SPS_COARSE_SPACING_HZ

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 333  # test takes ~235s


configs_ptc3 = {
    1: {
        "scan_id": 123,
        "stations": [[42, 1], [54, 1], [72, 1]],
        "station_beam": [BeamId.PTC3_A, BeamId.PTC3_B],
        "total_channel": 16,
        "channels": [
            list(range(64, 72)),
            list(range(136, 144)),
        ],
        "integration_period": 849,
        "ip_addresses": ["192.168.1.2", "192.168.1.3"],
    },
    4: {
        "scan_id": 456,
        "stations": [[18, 1], [34, 1], [21, 1], [42, 1], [54, 1], [72, 1]],
        "station_beam": [BeamId.PTC3_A],
        "total_channel": 16,
        "channels": [list(range(256, 264)) + list(range(272, 280))],
        "integration_period": 849,
        "ip_addresses": ["192.168.1.4"],
    },
    3: {
        "scan_id": 789,
        "stations": [[18, 1], [34, 1]],
        "station_beam": [BeamId.PTC3_C],
        "total_channel": 24,
        "channels": [list(range(384, 408))],
        "integration_period": 849,
        "ip_addresses": ["192.168.1.5"],
    },
}

cnic_config_ptc3 = {
    "sps_packet_version": 3,
    "stream_configs": [
        {
            "scan": config_values["scan_id"],
            "subarray": config,
            "station": station[0],
            "substation": station[1],
            "frequency": channel,
            "beam": config_values["station_beam"][number_configuration],
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for config, config_values in configs_ptc3.items()
        for number_configuration in range(len(config_values["station_beam"]))
        for channel in config_values["channels"][number_configuration]
        for station in config_values["stations"]
    ],
}

ptc3_scan_configs = {
    "scan_ptc_3_sub_1": {
        "id": configs_ptc3[1]["scan_id"],
        "lowcbf": {
            "stations": {
                "stns": configs_ptc3[1]["stations"],
                "stn_beams": [
                    {
                        "beam_id": configs_ptc3[1]["station_beam"][i],
                        "freq_ids": configs_ptc3[1]["channels"][i],
                        "delay_poly": "some_url",
                    }
                    for i in range(len(configs_ptc3[1]["station_beam"]))
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": configs_ptc3[1]["station_beam"][i],
                        "host": [[0, configs_ptc3[1]["ip_addresses"][i]]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": configs_ptc3[1]["integration_period"],
                    }
                    for i in range(len(configs_ptc3[1]["station_beam"]))
                ],
            },
        },
    },
    "scan_ptc_3_sub_3": {
        "id": configs_ptc3[3]["scan_id"],
        "lowcbf": {
            "stations": {
                "stns": configs_ptc3[3]["stations"],
                "stn_beams": [
                    {
                        "beam_id": configs_ptc3[3]["station_beam"][i],
                        "freq_ids": configs_ptc3[3]["channels"][i],
                        "delay_poly": "some_url",
                    }
                    for i in range(len(configs_ptc3[3]["station_beam"]))
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": configs_ptc3[3]["station_beam"][i],
                        "host": [[0, configs_ptc3[3]["ip_addresses"][i]]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": configs_ptc3[3]["integration_period"],
                    }
                    for i in range(len(configs_ptc3[3]["station_beam"]))
                ],
            },
        },
    },
    "scan_ptc_3_sub_4": {
        "id": configs_ptc3[4]["scan_id"],
        "lowcbf": {
            "stations": {
                "stns": configs_ptc3[4]["stations"],
                "stn_beams": [
                    {
                        "beam_id": configs_ptc3[4]["station_beam"][i],
                        "freq_ids": configs_ptc3[4]["channels"][i],
                        "delay_poly": "some_url",
                    }
                    for i in range(len(configs_ptc3[4]["station_beam"]))
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": configs_ptc3[4]["station_beam"][i],
                        "host": [[0, configs_ptc3[4]["ip_addresses"][i]]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": configs_ptc3[4]["integration_period"],
                    }
                    for i in range(len(configs_ptc3[4]["station_beam"]))
                ],
            },
        },
    },
}
# TODO - these configs should have more differences (they're mostly the same)
ptc3_corr_configs = {
    n: {
        "cnic_capture_packets": 144 * int(configs_ptc3[n]["total_channel"]),
        "scan_config": ptc3_scan_configs[f"scan_ptc_3_sub_{n}"],
    }
    for n in [1, 3, 4]
}
"""PTC3 Correlator Configurations. Subarray ID: Correlator Config"""


@pytest.mark.timeout(TEST_TIMEOUT_SEC)  # default of 300s is not long enough
@pytest.mark.hardware_present
@pytest.mark.parametrize(
    "corr_config, ready_subarray_n, subarray",
    [(config, n, n) for n, config in ptc3_corr_configs.items()],
    indirect=["ready_subarray_n"],
)
def test_ptc3_corr_sdp_metadata(
    cnic_and_processor, ready_subarray_n, corr_config: dict, subarray: int
):
    """Implement Static part of PTC 3."""
    # pylint: disable=too-many-locals
    print(ready_subarray_n)
    test_config = ProcessorVdTestConfig(
        **corr_config,
        cnic_capture_min_size=80,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_ptc3,
    )
    cnics, processors = cnic_and_processor
    _, processor = cnics[0], processors[0]
    print("PTC3, starting correlation")
    correlator_output = cnic_processor(
        (cnics, processors), ready_subarray_n, test_config
    )
    print("PTC3, correlation captured, extracting heaps")
    all_heaps, _ = extract_heaps_from_pcap(correlator_output.name)
    # TODO - instead of using a duplicate `subarray` parameter, should we ask the
    #  subarray device proxy for its name?
    print("PTC3, generating expected heaps")
    expected_heaps = generate_expected_heaps(
        subarray,
        version_under_test(Personality.CORR),
        serial_spead_hwid[processor.serialNumber],
    )
    print("PTC3, comparing heaps")
    different_heap_keys = compare_heap_keys(all_heaps, expected_heaps)
    print("PTC3, creating report")
    local_directory, new_repo_name, repo = download_overleaf_repo()
    nb_items, _ = print_figures_for_report(
        expected_heaps,
        all_heaps,
        different_heap_keys,
        local_directory,
        new_repo_name,
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        pprint.pformat(test_config.scan_config, indent=2),
    )
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTNUMBERDIFFERENTFIELD",
        str(nb_items),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.git.add(new_repo_name + "/heaps_difference_field.pdf")
    repo.git.add(new_repo_name + "/heaps_difference.pdf")
    repo.git.add(new_repo_name + "/heaps_overview.pdf")
    repo.index.commit("Test with python")
    repo.git.push()

    # Assess pass/fail
    assert different_heap_keys == [[], []]
    assert nb_items == 0


def get_bin(input_int: int, n_bits: int) -> str:
    """Convert a number to a binary string."""
    return format(input_int, "b").zfill(n_bits)


def extract_heaps_from_pcap(pcap_filename: str):
    """Extract all heaps and associated metadata from init packets."""
    thread_pool = spead2.ThreadPool()
    stream = spead2.recv.Stream(thread_pool)
    del thread_pool
    stream.add_udp_pcap_file_reader(pcap_filename, filter="")
    item_group = spead2.ItemGroup()
    all_heaps = {}
    num_heaps = 0
    for num_heaps, heap in enumerate(stream):
        # General information about heap
        if heap.is_start_of_stream():
            all_heaps[heap.cnt] = {}
        items = item_group.update(heap)
        for item in items.values():
            all_heaps[heap.cnt][f"{item.name}"] = {}
            all_heaps[heap.cnt][f"{item.name}"][
                "type_and_shape"
            ] = f"{item.dtype}{item.shape}"
            if item.name == "ScaID":
                all_heaps[heap.cnt][f"{item.name}"]["value"] = int(item.value)
            elif item.name == "SrcID":
                all_heaps[heap.cnt][f"{item.name}"]["value"] = str(item.value)
            else:
                all_heaps[heap.cnt][f"{item.name}"]["value"] = item.value
    return all_heaps, num_heaps


template_per_heap = {
    "ScaID": {"type_and_shape": "uint64()", "value": 0},
    "Epoch": {"type_and_shape": "uint32()", "value": 0},
    "Chann": {"type_and_shape": "uint32()", "value": 0},
    "ZoomI": {"type_and_shape": "uint8()", "value": 0},
    "Resol": {"type_and_shape": "uint8()", "value": 0},
    "SrcID": {"type_and_shape": "|S1()", "value": "b'L'"},
    "Subar": {"type_and_shape": "uint16()", "value": 0},
    "Firmw": {"type_and_shape": "uint32()", "value": 0},
    "BeaID": {"type_and_shape": "uint16()", "value": 0},
    "Integ": {"type_and_shape": "float32()", "value": 0.0},
    "Basel": {"type_and_shape": "uint32()", "value": 0},
    "Hardw": {"type_and_shape": "uint32()", "value": 0},
    "Frequ": {"type_and_shape": "float32()", "value": 0},
    "FreHz": {"type_and_shape": "uint32()", "value": 0},
    "VisFl": {"type_and_shape": "uint32()", "value": 0},
}


def generate_expected_heaps(nb_subarray: int, fw_version: str, hwid: int):
    """Generate the expected metadata."""
    expected_heaps = {}
    configuration = deepcopy(configs_ptc3[nb_subarray])
    nb_stations = len(configuration["stations"])
    nb_baselines = int(nb_stations * (nb_stations + 1) / 2)
    for n_beam in range(len(configuration["station_beam"])):
        total_visibilities = 0
        for channel in configuration["channels"][n_beam]:
            for visibility in range(144):
                bytes_val_sub_beam_channel = (
                    get_bin(nb_subarray, 5)
                    + get_bin(int(configuration["station_beam"][n_beam]), 9)
                    + get_bin(total_visibilities, 17)
                    + get_bin(0, 16)
                )
                index = int(bytes_val_sub_beam_channel, 2)
                expected_heaps[index] = deepcopy(template_per_heap)
                expected = expected_heaps[index]
                expected["ScaID"]["value"] = configuration["scan_id"]
                expected["Epoch"]["value"] = 0
                # total_visibilities, or channel*144+visibility
                expected["Chann"]["value"] = total_visibilities
                expected["ZoomI"]["value"] = 0
                expected["Resol"]["value"] = 32
                expected["Subar"]["value"] = nb_subarray
                expected["Firmw"]["value"] = pack_fw_version(fw_version)
                expected["BeaID"]["value"] = int(configuration["station_beam"][n_beam])
                expected["Integ"]["value"] = np.float32(
                    integration_periods[configuration["integration_period"]]
                )
                # expected["Integ"]["value"] = corr_configs.integration_model_to_real[
                #     configuration["integration_period"]
                # ]
                expected["Basel"]["value"] = nb_baselines
                expected["Hardw"]["value"] = hwid
                expected["Frequ"]["value"] = 5425.347
                expected["FreHz"]["value"] = int(
                    np.round(visibility_frequency(channel, 24, visibility))
                )
                expected["VisFl"]["value"] = 0  # init packets use zero
                total_visibilities += 1
    return expected_heaps


CORR_FINE_CH_HZ = (1 / 1080e-9) * (8 / 32768)  # 226.056 134 259 259...


def visibility_frequency(first_freq_id: int, n_fine_ch: int, visibility: int) -> float:
    """
    Calculate a correlation visibility's centre frequency.

    :param first_freq_id: SPS Frequency ID (aka coarse channel)
    :param n_fine_ch: Number of fine channels integrated in our
      correlation.
    :param visibility: index number
    :return: Centre Frequency (Hz)
    """
    return (
        first_freq_id * SPS_COARSE_SPACING_HZ  # centre frequency
        # minus half of the total fine channels BW, gives centre of lowest fine ch.
        - (3456 / 2) * CORR_FINE_CH_HZ
        # minus half a fine channel, gives bottom frequency of our visibility
        - CORR_FINE_CH_HZ / 2
        # plus half a Correlation BW, gives centre freq. of first visibility
        + CORR_FINE_CH_HZ * n_fine_ch / 2
        # increment by one step for each visibility
        + visibility * (CORR_FINE_CH_HZ * n_fine_ch)
    )


def compare_metadata(field: str, heaps_from_alveo: dict, heap_expected: dict):
    """Compare the 2 heaps dictionary for a given fiels."""
    different_items = {}
    for key, value in heaps_from_alveo.items():
        if key in heap_expected:
            if str(value[field]["value"]) != str(heap_expected[key][field]["value"]):
                different_items[key] = f"{value[field]} vs {heap_expected[key][field]}"
    return different_items


def compare_heap_keys(heaps_from_alveo: dict, heap_expected: dict):
    """Compare the list of heaps in the 2 dictionary."""
    return [
        [x for x in heaps_from_alveo.keys() if x not in heap_expected.keys()],
        [x for x in heap_expected.keys() if x not in heaps_from_alveo.keys()],
    ]


def pack_fw_version(fw_version: str) -> int:
    """
    Convert a FPGA firmware version string to an integer, as used in SPEAD metadata.

    :param fw_version: e.g. '0.10.0-dev.c1d93f55e' or '1.2.3'
    """
    prerelease = 0
    version = fw_version
    if "-" in fw_version:
        version, build_metadata = fw_version.split("-")
        prerelease = ord(build_metadata[0])  # ASCII value of first char after -
    major, minor, patch = version.split(".")
    # Most significant part of version number in most significant byte
    # (Processor uses this scheme after applying PERENTIE-2335 mods)
    return (int(major) << 24) | (int(minor) << 16) | (int(patch) << 8) | prerelease


def print_figures_for_report(
    expected_heaps: dict,
    all_heaps: dict,
    comparison,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return 1, {}
    plt.clf()
    plt.bar(["expected", "captured"], [len(expected_heaps), len(all_heaps)])
    plt.ylabel("Number of different heaps")
    plt.savefig(local_directory + new_repo_name + "/heaps_overview.pdf", format="pdf")

    plt.clf()
    plt.bar(["exp to cap", "cap to exp"], [len(comparison[0]), len(comparison[0])])
    plt.ylabel("Number of different heaps")
    plt.savefig(
        local_directory + new_repo_name + "/heaps_difference.pdf",
        format="pdf",
    )

    plt.clf()
    nb_items = 0
    different_meta = {}
    for key in template_per_heap:
        plt.bar([key], [len(compare_metadata(key, all_heaps, expected_heaps))])
        if len(compare_metadata(key, all_heaps, expected_heaps)) > 0:
            nb_items = nb_items + 1
            different_meta[key] = compare_metadata(key, all_heaps, expected_heaps)
        plt.ylabel("Number of different heaps")
    plt.xticks(rotation=45)
    plt.savefig(
        local_directory + new_repo_name + "/heaps_difference_field.pdf",
        format="pdf",
    )
    return nb_items, different_meta
