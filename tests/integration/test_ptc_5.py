# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 5 - Visibility data ordering.


- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import math
import os
from copy import deepcopy
from datetime import datetime
from typing import Iterable

import numpy as np
import pytest

from ska_low_cbf_integration.correlator import packets_per_correlation
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.sps import frequency_from_id
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_1ch
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

PTC5_DIR = "build/ptc5"


def _phases_and_gains(n_stations: int) -> (Iterable[float], Iterable[float]):
    """
    Get Phase Offsets and Gains to apply to CNIC signals for a PTC#5 scenario.

    :param n_stations: Number of stations being tested
    :return: phase offsets (degrees), gains. Two of each per station, for X & Y
      polarisations. Station 1 X, Station 1 Y, Station 2 X, Station 2 Y, ...
    """
    if n_stations == 6:
        # special phase values, almost all combinations are unique
        phases = (0, 1, 4, 9, 17, 26, 36, 48, 63, 81, 100, 121)
        gains = (1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1)
        return phases, gains
    phases = np.linspace(177, 0, num=n_stations * 2)
    if n_stations > 64:
        # sawtooth + ramp pattern
        phases = np.tile(np.linspace(120, 0, num=64), math.ceil(n_stations * 2 / 64))
        phases = phases[: n_stations * 2]
        phases += np.linspace(0, 30, num=n_stations * 2)
    gains = np.linspace(1, 2, num=n_stations * 2)
    return phases, gains


def _expected_values(n_stations: int) -> (np.ndarray, np.ndarray, np.ndarray):
    """
    Calculate PTC#5 Expected Values.

    :returns: Expected Phase (1D), Expected Gain (1D), Mask (2D).
      Mask can be applied to the actual correlation matrices to extract the "lower
      triangle" portion. Returned Phase and Gain arrays are already masked.
    """
    phase_offsets, gains = _phases_and_gains(n_stations)
    shape = (n_stations * 2, n_stations * 2)  # square shape, before masking
    expected_phase = np.empty(shape)
    expected_gain = np.empty(shape)
    mask = np.tri(shape[0], dtype=bool)
    for i in range(shape[0]):
        for j in range(i + 1):
            expected_phase[i][j] = phase_offsets[i] - phase_offsets[j]
            expected_gain[i][j] = gains[i] * gains[j]
    # Mask to keep only the "lower triangle"
    expected_phase = expected_phase[mask]
    expected_gain = expected_gain[mask]
    return expected_phase, expected_gain, mask


def print_masked_triangle(data, decimal_places=2, file=None):
    """
    Print a "masked" correlation matrix in user-friendly triangular grid shape.

    e.g. given ``[1,2,3,4,5,6]``, print:

    ::

        1
        2 3
        4 5 6
    """

    def nth_triangular_number(x: int) -> int:
        """
        Check a number is triangular, and if so which `n` in the series it is.

        e.g. 6 is the 3rd triangular number, so we return 3

        :raises ValueError: if input is not triangular
        """
        n = (np.sqrt(8 * x + 1) - 1) / 2
        if not n == int(n):
            raise ValueError(f"{x} is not a triangular number.")
        return int(n)

    data_list = list(data)
    n_tri = nth_triangular_number(len(data_list))
    digits = len(str(int(max(data_list))))
    format_ = f"{digits + 1}.{decimal_places}f"
    for row in range(n_tri):
        for _ in range(row + 1):
            value = data_list.pop(0)
            print(f"{value:{format_}}", end=",", file=file)
        print(file=file)


def _ns_from_phase(phase: float, channel: int) -> float:
    """
    Convert a desired phase offset to a delay in nanoseconds.

    N.B. fine_frequency is not factored in here as it is always zero.
    Add a fine_frequency parameter if you need to use non-zero values!

    :param phase: Phase offset in degrees
    :param channel: Channel ID number
    :return: Nanoseconds of delay (+ve phase gives +ve delay)
    """
    period_ns = 1e9 / frequency_from_id(channel)
    return (phase / 360) * period_ns


@pytest.mark.parametrize("n_stations", (6, 18, 64, 256))
@pytest.mark.timeout(1500)
@pytest.mark.hardware_present
def test_ptc5_corr_visibility_data_order(
    cnic_and_processor, ready_subarray, n_stations
):
    """Implement PTC 5 - Visibility data ordering."""
    print(datetime.now(), "Starting PTC#5")
    os.makedirs(PTC5_DIR, exist_ok=True)
    stations = [(station, 1) for station in range(1, n_stations + 1)]
    channels = [230]
    number_of_integration = 100 if n_stations <= 64 else 5
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=200,
        # n batches of visibility data per stream, plus SPEAD init packets
        cnic_capture_packets=144
        * (number_of_integration * packets_per_correlation(n_stations) + 1),
        scan_config=_ptc5_scan_config(channels, stations),
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=_ptc5_cnic_config(channels, stations),
        during_scan_callback=None,
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    print(datetime.now(), "Captured PTC#5 output, beginning analysis")
    visibilities = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels,
                "stations": [station for station, _ in stations],
            }
        )
    )
    print(datetime.now(), "VisibilityAnalyser created")
    visibilities.extract_spead_data(correlator_output.name)
    print(datetime.now(), "SPEAD data extracted")
    visibilities.populate_correlation_matrix()
    print(datetime.now(), "Correlation matrices populated")

    mask, expected_phase, expected_gain, relative_gain = _ptc5_report(
        scan_config=test_config.scan_config, visibility_analyser=visibilities
    )
    # We want < 1% variation, but when we have >64 stations we use fewer integrations,
    # which means more noise in the result. Therefore, a larger tolerance.
    variation = 0.01 if n_stations <= 64 else 0.10
    np.testing.assert_allclose(
        visibilities.average_correlation_phase[mask], expected_phase, rtol=variation
    )
    np.testing.assert_allclose(relative_gain[mask], expected_gain, rtol=variation)

    print(datetime.now(), "End of PTC#5")


def _ptc5_report(scan_config, visibility_analyser=None):
    """Generate Test Report for PTC5."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "65ceb01b35f96f7fc41ca686"
    )
    # Figure generation
    n_stations = len(scan_config["lowcbf"]["stations"]["stns"])
    visibility_analyser.save_heatmap_to_disk(
        mode="average_all", case="ptc5_", directory=local_directory + new_repo_name
    )
    visibility_analyser.save_phase_heatmap_to_disk(
        mode="average_all",
        case=local_directory + new_repo_name + "/ptc5_",
        label_format=".0f" if n_stations <= 6 else None,
    )
    print(datetime.now(), "Heatmap saved")

    expected_phase, expected_gain, mask = _expected_values(
        visibility_analyser.nb_station
    )
    with open(
        os.path.join(PTC5_DIR, f"expected_phase_{n_stations}.csv"),
        "w",
        encoding="utf-8",
    ) as file:
        print_masked_triangle(expected_phase, decimal_places=1, file=file)
    with open(
        os.path.join(PTC5_DIR, f"actual_phase_{n_stations}.csv"), "w", encoding="utf-8"
    ) as file:
        print_masked_triangle(
            visibility_analyser.average_correlation_phase[mask],
            decimal_places=1,
            file=file,
        )

    # gains measured relative to first station XX value
    relative_gain = (
        visibility_analyser.average_correlation
        / visibility_analyser.average_correlation[0][0]
    )
    visibility_analyser.render_heatmap(
        relative_gain,
        "ptc5_relative_gain.png",
        "Relative Gain",
        directory=local_directory + new_repo_name,
        label_format=".2f" if n_stations <= 6 else None,
    )
    with open(
        os.path.join(PTC5_DIR, f"expected_gain_{n_stations}.csv"), "w", encoding="utf-8"
    ) as file:
        print_masked_triangle(expected_gain, file=file)
    with open(
        os.path.join(PTC5_DIR, f"actual_gain_{n_stations}.csv"), "w", encoding="utf-8"
    ) as file:
        print_masked_triangle(relative_gain[mask], file=file)
    case = "ptc5_"
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    file_to_send = [
        "/report.tex",
        f"/{case}phase_heat_map.png",
        f"/{case}heat_map.png",
        f"/{case}relative_gain.png",
    ]
    repo.git.add("report2017.tex")
    for file in file_to_send:
        repo.git.add(new_repo_name + file)
    repo.index.commit("Test with python")
    repo.git.push()
    return mask, expected_phase, expected_gain, relative_gain


def _ptc5_scan_config(channels, stations):
    """Generate Scan Configuration for PTC#5."""
    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels
    scan_config["lowcbf"]["stations"]["stns"] = stations
    return scan_config


def _ptc5_cnic_config(channels, stations):
    """Generate CNIC-VD Configuration for PTC#5."""
    cnic_config = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": True, "fine_frequency": 0, "scale": 10},
                    {"tone": False, "seed": 1981, "scale": 3141},  # ~10 units of noise
                ],
                "y": [
                    {"tone": True, "fine_frequency": 0, "scale": 10},
                    {"tone": False, "seed": 1981, "scale": 3141},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Apply phase offsets & gains
    phase_offsets, gains = _phases_and_gains(len(stations))
    for n, stream in enumerate(cnic_config):
        sources = stream["sources"]

        x_delay = _ns_from_phase(phase_offsets[2 * n], 230)
        y_delay = _ns_from_phase(phase_offsets[2 * n + 1], 230)

        sources["x"][0]["delay_polynomial"] = [x_delay] + [0] * 5
        sources["y"][0]["delay_polynomial"] = [y_delay] + [0] * 5
        sources["x"][1]["delay_polynomial"] = [x_delay] + [0] * 5
        sources["y"][1]["delay_polynomial"] = [y_delay] + [0] * 5

        sources["x"][0]["scale"] *= gains[2 * n]
        sources["y"][0]["scale"] *= gains[2 * n + 1]
        sources["x"][1]["scale"] *= gains[2 * n]
        sources["y"][1]["scale"] *= gains[2 * n + 1]
    return cnic_config
