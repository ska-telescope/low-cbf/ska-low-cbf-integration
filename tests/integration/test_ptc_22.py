# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 22 - Trading Bandwidth for Stations/Substations.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #22 Presentation <https://docs.google.com/presentation/d/1IGzjvcGCaf8S9G4WUY08Q55S2bbzMq-B0iMJckAnoRM>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import shutil
from math import ceil

import pytest

from ska_low_cbf_integration.correlator import expectation
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import pcap_stats
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report  # noqa
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 900

station_to_channels = {
    256: 1,
    128: 1,
    50: 1,
    35: 2,
    22: 7,
    20: 8,
    16: 13,
    12: 24,
    8: 54,
    6: 96,
}
"""number of stations: number of channels"""

station_to_integration = {
    256: 5,
    128: 5,
    50: 36,
    35: 36,
    22: 36,
    20: 36,
    16: 36,
    12: 36,
    8: 36,
    6: 36,
}
"""number of stations: number of integration to capture"""


source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.parametrize("n_stations", [6, 12, 16, 50, 128, 256])
@pytest.mark.hardware_present
def test_ptc22_corr_trading_stations_for_bw(
    cnic_and_processor, ready_subarray, n_stations
):  # noqa
    """Implement PTC 22."""
    # pylint: disable=too-many-locals
    stations = [[station, 1] for station in range(1, n_stations + 1)]
    n_channels = station_to_channels[n_stations]
    first_channel = ceil(230 - n_channels / 2)
    channels = list(range(first_channel, first_channel + n_channels))
    n_integrations = station_to_integration[n_stations]
    exp_packet_size, exp_nb_packet, exp_rate, exp_duration = expectation(
        n_stations, n_channels, n_integrations
    )
    # Generate CNIC-VD Configuration
    cnic_config = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config = {
        "id": 1235,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": channels,
                        "delay_poly": "low-cbf/delaypoly/0/delay_s01_b01",
                    },
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "22.22.22.22"]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # 36 batches of visibility data per stream, plus SPEAD init packets
        cnic_capture_packets=144 * n_channels + int(exp_nb_packet),
        scan_config=scan_config,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        source_delay_config=[source_delay_config],
        beam_delay_config=[beam_delay_config],
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    # skip SPEAD init packets
    stats = pcap_stats(correlator_output, start_packet=n_channels * 144)
    error_margin = 0.01 if n_stations < 50 else 0.1
    _ptc22_report(
        correlator_output,
        test_config,
        stats,
        error_margin,
        channels,
        n_stations,
        exp_duration,
        exp_nb_packet,
        exp_packet_size,
        exp_rate,
    )
    for packet_size in exp_packet_size:
        assert packet_size in stats.packet_sizes
    assert len(stats.packet_sizes) == len(
        exp_packet_size
    ), f"Only {exp_packet_size} packet size(s) expected"
    assert stats.n_packets == exp_nb_packet
    assert float(stats.total_bytes * 8 / stats.duration) == pytest.approx(
        exp_rate, rel=error_margin
    ), "Data rate not as expected"
    assert float(stats.duration) == pytest.approx(exp_duration, rel=error_margin)


def _ptc22_report(
    correlator_output,
    test_config,
    stats,
    error_margin,
    channels,
    n_stations,
    exp_duration,
    exp_nb_packet,
    exp_packet_size,
    exp_rate,
):
    """Generate Test Report for PTC22."""
    # pylint: disable=too-many-arguments,too-many-locals
    station_ids = list(range(1, n_stations + 1))
    analyser = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels,
                "stations": station_ids,
            }
        )
    )
    analyser.extract_spead_data(correlator_output.name)
    figure_generated = True
    try:
        analyser.angle_analysis((station_ids[0], station_ids[1]), "XX", 1, channels)
    except ValueError:
        figure_generated = False

    local_directory, new_repo_name, repo = download_overleaf_repo(
        "653da13e2a47cc70d3de3618"
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    # results in the table
    replace_word_in_report(file_result_to_replace, "VISRATEEXP", f"{exp_rate:.2f}")
    replace_word_in_report(
        file_result_to_replace,
        "VISRATECAP",
        f"{float(stats.total_bytes * 8 / stats.duration):.2f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "VISRATEREQ",
        str(
            float(stats.total_bytes * 8 / stats.duration)
            == pytest.approx(exp_rate, rel=error_margin)
        ),
    )
    replace_word_in_report(file_result_to_replace, "VISSIZEEXP", str(exp_packet_size))
    replace_word_in_report(
        file_result_to_replace, "VISSIZECAP", str(stats.packet_sizes)
    )
    size_requirements = True
    for packet_size in exp_packet_size:
        size_requirements = size_requirements and packet_size in stats.packet_sizes
    replace_word_in_report(file_result_to_replace, "VISSIZEREQ", str(size_requirements))
    replace_word_in_report(file_result_to_replace, "VISNBEXP", str(exp_nb_packet))
    replace_word_in_report(file_result_to_replace, "VISNBCAP", str(stats.n_packets))
    replace_word_in_report(
        file_result_to_replace, "VISNBREQ", str(stats.n_packets == exp_nb_packet)
    )
    replace_word_in_report(file_result_to_replace, "VISTIMEEXP", str(exp_duration))
    replace_word_in_report(
        file_result_to_replace, "VISTIMECAP", f"{float(stats.duration):.2f}"
    )
    replace_word_in_report(
        file_result_to_replace,
        "VISTIMEREQ",
        str(float(stats.duration) == pytest.approx(exp_duration, rel=error_margin)),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    if figure_generated:
        shutil.copy(
            f"./{station_ids[0]}x{station_ids[1]}_" + "XX_angle.png",
            local_directory + new_repo_name + "/phase_analysis.png",
        )
        repo.git.add(new_repo_name + "/phase_analysis.png")
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")

    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()
