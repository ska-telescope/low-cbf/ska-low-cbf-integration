# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 43 - PSS Beam continuous time sequence.
Summary:
    Show that PSS beamformer packets have accurate timestamps
    - send SPS packets containing pulsed noise into the PST beamformer
    - form a single beam
    - capture SPS input and PST output
    - Test pass:
        A. show that first SPS pulse into PSS results in matching PSS beam pulse output
        B. fold SPS input and PSS output on pulse period, calculate timing difference
          - difference should be less than a few SPS sample periods (limited by test precision)

More information:
    - `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
    - `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
    - `PTC #43 Presentation <https://where.is.it>`_  # TODO
"""  # noqa
# pylint: enable=line-too-long

import argparse
import json
import math
import os
import shutil
import struct
import typing
from dataclasses import dataclass
from datetime import datetime

import dpkt
import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

# from ska_low_cbf_integration.pcap import get_udp_payload_bytes


# from ska_low_cbf_integration.pcap import get_udp_payload_bytes
# from ska_low_cbf_integration.psr_analyser import PulsarPacket


TEST_TIMEOUT_SEC = 600
PSS_CHANS_PER_PACKET = 54

station_ids = [
    321,
    351,
    383,
    393,
    399,
    429,
    435,
    471,
]


stn_beam_radec = {"ra": "2h30m00.00s", "dec": "-23d49m50.0s"}
pss_beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
src0_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": stn_beam_radec,
}
source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [src0_radec] * 4,  # DelayPolySim always expects 4 sources
}
pss_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [pss_beam_radec],
}
DATA_DIR = "/test-data/PTC/ptc43/"


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc43_pss_continuous(cnic_and_processor, ready_subarray):
    """
    Implement PTC 43.

    In this test we aim to show that:
    * PSS timedomain data is continuous across packet boundaries
    * no timestamps are missing
    * Tones in PSS output data are spectrally clean
    """
    # pylint: disable=too-many-locals,too-many-statements
    os.makedirs(DATA_DIR, exist_ok=True)
    delay_sub = "low-cbf/delaypoly/0/delay_s01_b01"

    pss_sub = "low-cbf/delaypoly/0/pst_s01_b01_"
    print(datetime.now(), "Starting PTC#43")
    n_stations = len(station_ids)
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 1
    first_channel = 64
    channels = list(range(first_channel, first_channel + n_channels))
    pss_beam_ids = [501]
    pss_svr_ip = "192.168.43.43"

    tone_freqs = [8]
    cnic_step_per_pss_chan = 512
    pss_middle_chan = 27  # 54 PSS chans per SPS coarse channel
    tone_chans = [
        pss_middle_chan + round(f / cnic_step_per_pss_chan) for f in tone_freqs
    ]

    cnic_config_test1 = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    # scale 100 -> nearly all the range of SPS packets (ok for PSS)
                    {"tone": True, "fine_frequency": tone_freqs[0], "scale": 100},
                ],
                "y": [
                    {"tone": True, "fine_frequency": tone_freqs[0], "scale": 100},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]

    scan_config_test1 = {
        "id": 4343,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {"beam_id": 1, "freq_ids": channels, "delay_poly": delay_sub}
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": 1,
                        "delay_poly": pss_sub + str(pss_dly_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pss_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": [1.0] * n_stations,
                    }
                    for pss_dly_idx, pss_beam in enumerate(pss_beam_ids)
                ],
            },
        },
    }
    # Run first test First part - 1 station contributing to beam
    test_cfg1 = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # frames are 53msec, need 1300 frames to get 69 seconds of data
        # PSS produces 48 pkts/frame/coarse
        cnic_capture_packets=48 * n_channels * 1300,
        scan_config=scan_config_test1,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_test1,
        during_scan_callback=None,
        source_delay_config=[source_delay_config],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pss_delay_config,  # delaypoly PST works for PSS too
    )
    opened_pcap_file = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_cfg1,
        DATA_DIR + "/test1.pcap",
    )
    filename = opened_pcap_file.name
    opened_pcap_file.close()
    print(datetime.now(), f"Captured PTC#43 test output: {filename}")

    # analysis - extract timedomain data for channel with tone
    tone_chan = tone_chans[0]
    td_data = extract_psr_chan_samples(filename, pss_beam_ids[0], tone_chan)

    # plot the first few samples giving indication it is smooth
    _, ax = plt.subplots(4, 1)
    for pol in range(0, 2):
        re_ax = pol * 2
        im_ax = pol * 2 + 1
        ax[re_ax].plot(np.real(td_data[:500, pol]))
        ax[re_ax].grid()
        ax[re_ax].set_ylabel(f"re(pol_{pol})")
        ax[im_ax].plot(np.imag(td_data[:500, pol]))
        ax[im_ax].grid()
        ax[im_ax].set_ylabel(f"im(pol_{pol})")
    ax[0].set_title("PTC43 time-domain waveforms")
    ax[0].set_ylabel("Apol real")
    ax[1].set_ylabel("Apol imag")
    ax[2].set_ylabel("Bpol real")
    ax[3].set_ylabel("Bpol imag")
    ax[3].set_xlabel("Sample number")
    plt.savefig(DATA_DIR + "time_domain_ptc43.png")

    # plot the FFT of the time-domain data
    rows, _ = td_data.shape
    # Tone samples/cycle = 512/tone_freq = 512/8 = 64
    fft_len = 64 * (rows // 64)  # get whole number of tone cycles
    apol_fft = np.fft.fft(td_data[0:fft_len, 0])
    bpol_fft = np.fft.fft(td_data[0:fft_len, 1])

    apol_db = 20.0 * np.log10(np.abs(apol_fft))
    apol_db = apol_db - np.max(apol_db)
    bpol_db = 20.0 * np.log10(np.abs(bpol_fft))
    bpol_db = bpol_db - np.max(bpol_db)

    _, ax2 = plt.subplots(2, 1, figsize=(10, 8))
    ax2[0].plot(apol_db)
    ax2[0].grid()
    ax2[0].set_title("FFT of PTC43 tone")
    ax2[0].set_ylabel("A-pol dB")
    ax2[1].plot(bpol_db)
    ax2[1].grid()
    ax2[1].set_ylabel("B-pol dB")
    ax2[1].set_xlabel("FFT bin number")
    plt.savefig(DATA_DIR + "fft_ptc43.png")

    # Get deviation of tone from best-fit sinusoid
    t = np.arange(rows)
    tone = np.exp(1j * 2 * np.pi * (tone_freqs[0] / 512) * t)
    tone_scale = np.vdot(tone, tone)
    apol_scale = np.vdot(tone, td_data[:, 0]) / tone_scale
    bpol_scale = np.vdot(tone, td_data[:, 0]) / tone_scale
    apol_diff = td_data[:, 0] - apol_scale * tone
    bpol_diff = td_data[:, 1] - bpol_scale * tone

    # plot measured tone vs expected tone in time-domain
    _, ax4 = plt.subplots(2, 1)
    ax4[0].plot(t[0:500], td_data[0:500, 0].real)  # measured
    ax4[0].plot(t[0:500], (apol_scale * tone[0:500]).real)  # expected
    ax4[0].grid()
    ax4[0].set_title("PTC43 tone vs expected tone")
    ax4[0].set_ylabel("Apol real")
    ax4[1].plot(t[0:500], td_data[0:500, 1].real)  # measured
    ax4[1].plot(t[0:500], (bpol_scale * tone[0:500]).real)  # expected
    ax4[1].grid()
    ax4[1].set_xlabel("sample number")
    ax4[1].set_ylabel("Bpol real")
    plt.savefig(DATA_DIR + "expected_comparison_ptc43.png")

    # plot tone error histogram
    _, ax3 = plt.subplots(2, 1, figsize=(10, 8))
    ax3[0].hist((np.abs(apol_diff / tone_scale)) ** 2, bins=100)
    ax3[0].grid()
    ax3[0].set_title("PTC43 Tone error power histogram")
    ax3[0].set_ylabel("A-polarisation counts")
    ax3[1].hist((np.abs(bpol_diff / tone_scale)) ** 2, bins=100)
    ax3[1].grid()
    ax3[1].set_ylabel("B-polaristion counts")
    ax3[1].set_xlabel("Error power relative to expected tone")
    plt.savefig(DATA_DIR + "error_hist_ptc43.png")

    # Check that tone spectrum is clean enough -> output is continuous
    apol_pk0, apol_pk1 = largest_values(apol_fft)
    bpol_pk0, bpol_pk1 = largest_values(bpol_fft)
    apol_error_max_db = 20.0 * math.log10(math.fabs(apol_pk1 / apol_pk0))
    bpol_error_max_db = 20.0 * math.log10(math.fabs(bpol_pk1 / bpol_pk0))

    # TODO overleaf report and its text customisation goes here
    _update_overleaf_report(
        test_cfg1.scan_config,
        apol_error_max_db,
        bpol_error_max_db,
        apol_pk0,
        apol_pk1,
        bpol_pk0,
        bpol_pk1,
    )
    # Assert that max errors are "less than (8-bit) quantisation"
    assert apol_error_max_db < -48.0  # sic
    assert bpol_error_max_db < -48.0  # sic


def _update_overleaf_report(  # pylint: disable=too-many-arguments
    scan_config,
    apol_error_max_db,
    bpol_error_max_db,
    apol_pk0,
    apol_pk1,
    bpol_pk0,
    bpol_pk1,
):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "672aaf24179506f2ab31bcbc"
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )

    replace_word_in_report(
        file_result_to_replace,
        "REQ155a",
        str(apol_error_max_db < -48.0),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXFFTa",
        f"{20.0 * math.log10(apol_pk0):.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "SECONDFFTa",
        f"{20.0 * math.log10(apol_pk1):.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXERRORa",
        str(apol_error_max_db),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ155b",
        str(bpol_error_max_db < -48.0),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXFFTb",
        f"{20.0 * math.log10(bpol_pk0):.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "SECONDFFTb",
        f"{20.0 * math.log10(bpol_pk1):.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXERRORb",
        str(bpol_error_max_db),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    files_to_move = [
        "error_hist_ptc43.png",
        "expected_comparison_ptc43.png",
        "fft_ptc43.png",
        "time_domain_ptc43.png",
    ]
    for file_to_move in files_to_move:
        dst = os.path.join(local_directory, new_repo_name, file_to_move)
        shutil.copy(os.path.join(DATA_DIR, file_to_move), dst)
        repo.git.add(dst)

    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def get_udp_payload_bytes(pcap_filename) -> typing.List[bytes]:
    """
    Read UDP payload bytes from a .pcap(ng) file.

    return list with one entry per packet, entry containing udp payload
    """
    timestamps = []
    try:
        with open(pcap_filename, "rb") as filename:
            pcap_read = dpkt.pcap.UniversalReader(filename)
            udp_payloads = []
            for tstamp, buf in pcap_read:  # pylint: disable=not-an-iterable
                # skip packet if not long enough to contain IP+UDP+PSS hdrs
                if len(buf) < (34 + 8 + 96):
                    print(f"WARNING: Packet is too small for PSS {len(buf)}Bytes")
                    continue
                eth = dpkt.ethernet.Ethernet(buf)
                ip = eth.data
                # skip non-UDP packets
                if ip.p != dpkt.ip.IP_PROTO_UDP:
                    print(f"WARNING: Found packet that is not UDP {ip.p} type")
                    continue
                # add the UDP payload data into the list of payloads
                udp = ip.data
                udp_payloads.append(udp.data)
                timestamps.append(tstamp)
    except FileNotFoundError as fnf_err:
        print(fnf_err)
        assert 0, f"file {pcap_filename} does not exist"

    return timestamps, udp_payloads


@dataclass
class PssPkt:  # pylint: disable=too-many-instance-attributes
    """Structure to save decoded data from SDP spead packets."""

    seq_no: int
    sample_no: int
    scale1: int
    first_chan: int
    num_chan: int
    valid_chan: int
    num_sample: int
    data_precision: int
    beam_id: int
    weight_bytes: list
    pol_a: list[np.ndarray]  # time domain samples for a channel
    pol_b: list[np.ndarray]


def decode_pss_packet(payload):  # pylint: disable=too-many-locals
    """Decode the data+metadata content in a PSS packet."""
    seq_no = struct.unpack("Q", payload[0:8])[0]
    sample_no = struct.unpack("Q", payload[8:16])[0]
    scale1 = struct.unpack("f", payload[32:36])[0]
    first_chan = struct.unpack("I", payload[48:52])[0]
    num_chan = struct.unpack("H", payload[52:54])[0]
    valid_chan = struct.unpack("H", payload[54:56])[0]
    num_sample = struct.unpack("H", payload[56:58])[0]
    data_precision = struct.unpack("B", payload[65:66])[0]
    beam_id = struct.unpack("H", payload[58:60])[0]
    sample_per_weight = struct.unpack("B", payload[67:68])[0]
    weights_offset = 96  # start of weights (multiple of 16bytes=128 bits)
    n_weight_bytes = int(num_sample / sample_per_weight * 2 * num_chan)  # 2bytes
    # weights padded to multiple of 128 bits = 16 bytes
    data_offset = weights_offset + math.ceil(n_weight_bytes / 16) * 16

    # Get channel data in a floating-point numpy array
    data_type = np.int8
    if data_precision == 16:
        data_type = np.int16

    raw_data_np = (
        np.frombuffer(payload[data_offset:], dtype=data_type).astype(float) / scale1
    )

    assert (
        len(raw_data_np) == num_sample * 4 * num_chan
    ), f"Expecting {num_sample} * {num_chan} * 4 samples, got {len(raw_data_np)}"

    # === Decode the data into channels with 2 polarisations having complex samples ===
    # data format is (chanx, pola) (chanx, polb), (chanx+1, pola), (chanx+1 polb), ...
    # with each of the brackets above containing "num_sample" complex entries
    apol = []
    bpol = []
    for chan in range(0, num_chan):
        start = chan * 4 * num_sample
        pol_a = raw_data_np[start + 0 : start + 2 * num_sample : 2] + (
            1j * raw_data_np[start + 1 : start + 2 * num_sample : 2]
        )
        apol.append(pol_a)

        start += 2 * num_sample
        pol_b = raw_data_np[start + 0 : start + 2 * num_sample : 2] + (
            1j * raw_data_np[start + 1 : start + 2 * num_sample : 2]
        )
        bpol.append(pol_b)

    return PssPkt(
        seq_no,
        sample_no,
        scale1,
        first_chan,
        num_chan,
        valid_chan,
        num_sample,
        data_precision,
        beam_id,
        payload[weights_offset : weights_offset + n_weight_bytes],
        apol,
        bpol,
    )


def extract_psr_chan_samples(  # pylint: disable=too-many-locals
    filename: str, beam_id: int, chan_no: int
) -> np.ndarray[float]:
    """Extract time-domain samples for a PSS channel."""
    _, payloads = get_udp_payload_bytes(filename)

    chan_data = {}
    for pkt in payloads:
        pss = decode_pss_packet(pkt)
        chan_idx = chan_no - pss.first_chan
        if chan_idx < 0 or chan_idx >= pss.num_chan or pss.beam_id != beam_id:
            # packet doesn't contain beam/channel we're interested in
            continue
        chan_data[pss.sample_no] = (pss.pol_a[chan_idx], pss.pol_b[chan_idx])

    pol_a = 0
    pol_b = 1
    sample_nos = sorted(list(chan_data))
    samples_per_pkt = len(chan_data[sample_nos[0]][pol_a])  # assumes all pkts same
    n_sample = sample_nos[-1] - sample_nos[0] + samples_per_pkt
    first_sample_no = sample_nos[0]

    # get time domain data
    td_data = np.zeros((n_sample, 2), dtype=complex)
    for sample_no in sample_nos:
        idx = sample_no - first_sample_no
        td_data[idx : idx + samples_per_pkt, pol_a] = chan_data[sample_no][pol_a]
        td_data[idx : idx + samples_per_pkt, pol_b] = chan_data[sample_no][pol_b]

    return td_data


def largest_values(fft_data: np.ndarray[complex]) -> tuple[complex, complex]:
    """
    Find highest peak and next highest not near (+-2) highest peak.

    :param fft_data: raw FFT of time-domain data
    :return: complex value of data at peak, and next highest peak
    """
    mags = np.abs(fft_data)

    pk_idx = np.argmax(mags)
    peak = mags[pk_idx]
    if pk_idx <= 2:  # close to beginning of data
        remainder = mags[pk_idx + 3 :]
    elif pk_idx >= len(mags) - 3:  # close to end of data
        remainder = mags[: pk_idx - 2]
    else:
        remainder = np.append(mags[: pk_idx - 2], mags[pk_idx + 3 :])
    next_peak = np.max(remainder)
    return peak, next_peak


def parse_args():
    """Argument parsing for when analysis is run stand-alone from cmd line."""
    parser = argparse.ArgumentParser(prog="pss packet capture analyser")
    parser.add_argument("-f", "--file", required=True, help="File to analyse")
    parser.add_argument("-b", "--beam", required=True, type=int, help="beam to plot")
    parser.add_argument("-c", "--chan", required=True, type=int, help="chan to plot")

    return parser.parse_args()


def main():
    """Manually analyse a ptc43 capture file."""
    # pylint: disable=too-many-statements,too-many-locals
    arg_parser = parse_args()
    pss_file = arg_parser.file
    plt_chan = arg_parser.chan
    plt_beam = arg_parser.beam

    td_data = extract_psr_chan_samples(pss_file, plt_beam, plt_chan)

    _, ax = plt.subplots(4, 1)
    for pol in range(0, 2):
        re_ax = pol * 2
        im_ax = pol * 2 + 1
        ax[re_ax].plot(np.real(td_data[:500, pol]))
        ax[re_ax].grid()
        ax[re_ax].set_ylabel(f"re(pol_{pol})")
        ax[im_ax].plot(np.imag(td_data[:500, pol]))
        ax[im_ax].grid()
        ax[im_ax].set_ylabel(f"im(pol_{pol})")
    ax[0].set_title("PTC43 time-domain waveforms")
    ax[0].set_ylabel("Apol real")
    ax[1].set_ylabel("Apol imag")
    ax[2].set_ylabel("Bpol real")
    ax[3].set_ylabel("Bpol imag")
    ax[3].set_xlabel("Sample number")

    plt.savefig("plt_ptc43.png")

    rows, _ = td_data.shape
    fft_len = 64 * (rows // 64)
    apol_fft = np.fft.fft(td_data[0:fft_len, 0])
    bpol_fft = np.fft.fft(td_data[0:fft_len, 1])

    apol_db = 20.0 * np.log10(np.abs(apol_fft))
    apol_db = apol_db - np.max(apol_db)
    bpol_db = 20.0 * np.log10(np.abs(bpol_fft))
    bpol_db = bpol_db - np.max(bpol_db)

    # _, ax2 = plt.subplots(2,1, figsize=(10,8))
    _, ax2 = plt.subplots(2, 1)
    ax2[0].plot(apol_db)
    ax2[0].grid()
    ax2[0].set_title("FFT of PTC43 tone")
    ax2[0].set_ylabel("A-pol dB")
    ax2[1].plot(bpol_db)
    ax2[1].grid()
    ax2[1].set_ylabel("B-pol dB")
    ax2[1].set_xlabel("FFT bin number")
    plt.savefig("fft_ptc43.png")

    # Get deviation of tone from best-fit sinusoid
    tone_freq = 8
    t = np.arange(rows)
    tone = np.exp(1j * 2 * np.pi * (tone_freq / 512) * t)
    tone_scale = np.vdot(tone, tone)
    apol_scale = np.vdot(tone, td_data[:, 0]) / tone_scale
    bpol_scale = np.vdot(tone, td_data[:, 1]) / tone_scale
    apol_diff = td_data[:, 0] - apol_scale * tone
    bpol_diff = td_data[:, 1] - bpol_scale * tone

    # _, ax4 = plt.subplots(2,1,figsize=(10,8))
    _, ax4 = plt.subplots(2, 1)
    ax4[0].plot(t[0:500], td_data[0:500, 0].real)
    ax4[0].plot(t[0:500], (apol_scale * tone[0:500]).real)
    ax4[0].grid()
    ax4[0].set_title("PTC43 tone vs expected tone")
    ax4[0].set_ylabel("Apol real")
    ax4[1].plot(t[0:500], td_data[0:500, 1].real)
    ax4[1].plot(t[0:500], (bpol_scale * tone[0:500]).real)
    ax4[1].grid()
    ax4[1].set_xlabel("sample number")
    ax4[1].set_ylabel("Bpol real")
    plt.savefig("expected_comparison_ptc43.png")

    # plot tone error histogram
    # _, ax3 = plt.subplots(2, 1, figsize=(10, 8))
    _, ax3 = plt.subplots(2, 1)
    ax3[0].hist((np.abs(apol_diff / tone_scale)) ** 2, bins=100)
    ax3[0].grid()
    ax3[0].set_title("PTC43 Tone error power histogram")
    ax3[0].set_ylabel("A-polarisation counts")
    ax3[1].hist((np.abs(bpol_diff / tone_scale)) ** 2, bins=100)
    ax3[1].grid()
    ax3[1].set_ylabel("B-polaristion counts")
    ax3[1].set_xlabel("Error power relative to expected tone")
    plt.savefig("error_hist_ptc43.png")

    apol_pk0, apol_pk1 = largest_values(apol_fft)
    bpol_pk0, bpol_pk1 = largest_values(bpol_fft)

    apol_error_max_db = 20.0 * math.log10(math.fabs(apol_pk0 / apol_pk1))
    bpol_error_max_db = 20.0 * math.log10(math.fabs(bpol_pk0 / bpol_pk1))

    print(f"apol {apol_error_max_db:.1f}dB, bpol {bpol_error_max_db:.1f}dB")

    plt.show()


if __name__ == "__main__":
    main()
