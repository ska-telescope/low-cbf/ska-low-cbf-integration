# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=too-many-locals,redefined-outer-name
"""Perentie Test Case 26 - Multistation beam delay tracking.

Based on Jupyter notebook: PI23/FAT1/PTC26

- `Low CBF Requirements <https://is.gd/sul9PW>`_
- `Perentie Test Cases <https://is.gd/BCLuaC>`_
- `PTC #26 <https://is.gd/YeT5nv>`_
- `Overleaf <https://www.overleaf.com/project/671c0680ef862a8d6ae009ab>` _

1. Setup the CNIC to generate data using 18-stations with AA1 locations (345-356,
   429-434) for 3 sources using 16/32/48 SPS channels with noise amplitude
   25*channel#.
2. Use astropy to calculate RA needed for source/stationbeam directions to get
   it overhead (zenith) at the current time, as well as offset 10 and 30 degrees
3. Setup the correlator for a single subarray, with delay tracking, for 3
   station beams
4. Transmit the SPS data using the CNIC and process in the correlator
5. Capture the resulting visibility data with SDP CNIC for the 30 seconds scan
6. Plot the power of autocorrelations/crosscorrelations for each station beam
   vs visibility frequency
7. For each step in power, count the number of visibility channels with the same
   power
8. Plot the phases of all crosscorrelations for each station beam/frequency/baseline

NOTE: for full test (18 stations, 16/32/48 channels per beam) you need 2 Alveos
      the usual way to acquire them is environment variable: N_PROCESSORS=2

NOTE2: keep this `guideline in mind <https://is.gd/vJIF5B>` _
"""

import json
import shutil
from collections import defaultdict

import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

# ---------------------------
# DEBUG settings
# ---------------------------
SKIP_ANALYSIS = False  # default: False
USE_16_STATIONS = True  # default: False, us 16 stations instead of 18
CURTAIL_CHANNELS = False  # default: False, 3rd beam uses 40 channels instead of 48
SINGLE_BEAM = False  # default: False, testing only, use single beam 96 channels
# End test if 'assert' fails, however sometimes we want to continue
# e.g. in order to examine graphs
END_TEST_ON_ASSERT_FAILURE = True  # default: True
GENERATE_OVERLEAF_REPORT = True  # default: True

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 54321
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture

# ---------------------------
# data location (pcap, png)
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc26/"
"""Directory name where data files will be stored (PCAP/graphs)"""


def get_cnic_vd_config(
    stations: list[int], channels: list[int], beam_id: int
) -> list[dict]:
    """ "Return CNIC-VD configuration for given combination of stations, beams
    and channels."""  # noqa
    scale = 25
    return [
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": beam_id,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1980 + beam_id, "scale": scale * channel},
                ],
                "y": [
                    {"tone": False, "seed": 1980 + beam_id, "scale": scale * channel},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]


# Step 1: a list of required stations

station_ids = list(range(345, 345 + 12)) + list(
    range(429, 429 + 4 if USE_16_STATIONS else 6)
)
n_stations = len(station_ids)
stations = [[station, 1] for station in station_ids]

# Step 1: 3 sources using 16, 32 and 48 SPS channels
# channels_beam_5  = list(range(92, 92 + 16))
channels_beam_5 = list(range(92, 92 + 16))
if SINGLE_BEAM:
    # debug only, simulates PTC-6 (1 beam, 96 channels)
    channels_beam_5 = list(range(92, 92 + 96))
channels_beam_9 = list(range(64, 64 + 32))
if CURTAIL_CHANNELS:
    channels_beam_14 = list(range(71, 71 + 40))  # debugging only
else:
    channels_beam_14 = list(range(71, 71 + 48))  # real test


BEAM_5 = 5
BEAM_9 = 9
BEAM_14 = 14

# group beams/channels for iteration
all_beam_channels = (
    (BEAM_5, channels_beam_5),
    (BEAM_9, channels_beam_9),
    (BEAM_14, channels_beam_14),
)
if SINGLE_BEAM:
    # DEBUGGING: override full test
    all_beam_channels = ((BEAM_5, channels_beam_5),)
all_beams = tuple(beam for (beam, _) in all_beam_channels)

INTEGRATION_MS = 849

DEST_NET = "192.168.20."  # receiving host prefix IP address

vd_src_cfg = []  # all sources for CNIC-VD
scan_stn_beams = []
scan_dest_host = []  # destination hosts for visibilites, one per beam
DELAY_POLY_URL = "low-cbf/delaypoly/0/delay_s01_b{:02}"
for beam, channels in all_beam_channels:
    vd_src_cfg.extend(get_cnic_vd_config(stations, channels, beam))
    scan_stn_beams.append(
        {
            "beam_id": beam,
            "freq_ids": channels,
            "delay_poly": DELAY_POLY_URL.format(beam),
        }
    )
    scan_dest_host.append(
        {
            "stn_beam_id": beam,
            "host": [[0, f"{DEST_NET}{beam}"]],
            "mac": [[0, "0c-42-a1-9c-a2-1b"]],
            "port": [[0, 9000, 1]],
            "integration_ms": INTEGRATION_MS,
        }
    )

vd_config = {"sps_packet_version": 3, "stream_configs": vd_src_cfg}

scan_ptc_26 = {
    "id": SCAN_ID,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": scan_stn_beams,
        },
        "vis": {
            "stn_beams": scan_dest_host,
        },
    },
}

# Delay Polynomial configuration
# STEP 26.2: 0, 10, 30 deg offset from zenith
direction_beam1 = {"ra": "00h00m00.0s", "dec": "00d00m00.0s"}
direction_beam2 = {"ra": "00h00m00.0s", "dec": "-10d00m00.0s"}
direction_beam3 = {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"}

# from the Notebook:
beam_dir1 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_5,
    "direction": direction_beam1,
}
src_dir1 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_5,
    "direction": [direction_beam1] * 4,
}
beam_dir2 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_14,
    "direction": direction_beam2,
}
src_dir2 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_14,
    "direction": [direction_beam2] * 4,
}
beam_dir3 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_9,
    "direction": direction_beam3,
}
src_dir3 = {
    "subarray_id": SUBARRAY_ID,
    "beam_id": BEAM_9,
    "direction": [direction_beam3] * 4,
}


# CNIC capture settings
# ---------------------
PCAP_PATH = DATA_DIR + f"ptc26_correlator_{n_stations}_stations.pcap"
VIS_CHAN_COUNT = 144
MIN_PACKET_SIZE = 80
N_VIS = 35
total_channels = sum(len(channels) for _, channels in all_beam_channels)
RX_PACKETS = VIS_CHAN_COUNT * N_VIS * total_channels

beam_count = len(all_beam_channels)
if SINGLE_BEAM:
    source_delay_config = [src_dir1]
    beam_delay_config = [beam_dir1]
else:
    source_delay_config = [src_dir1, src_dir2, src_dir3]
    beam_delay_config = [beam_dir1, beam_dir2, beam_dir3]

test_config = ProcessorVdStaticTrackingTestConfig(
    cnic_capture_min_size=MIN_PACKET_SIZE,
    cnic_capture_packets=RX_PACKETS,
    scan_config=scan_ptc_26,
    firmware_type=Personality.CORR,
    input_packet_count=None,
    start_scan_packet_count=None,
    before_end_scan_packet_count=None,
    output_packet_count=None,
    vd_config=vd_config,
    during_scan_callback=None,
    # use 'beam_count' for "SHORT_TEST"
    source_delay_config=source_delay_config[:beam_count],
    beam_delay_config=beam_delay_config[:beam_count],
    nb_processors=2,
    do_fw_switcharoo=True,
)


# Analysis specific functions
# ---------------------------
def visibilities_same_power(visibilities, channels, stations, integration_cnt):
    """Calculate channels with the same power."""
    # pylint: disable=too-many-locals
    all_vis_channel_integration = defaultdict(list)

    n_stations = len(stations)
    for chan in channels:
        for vis in range(VIS_CHAN_COUNT):
            absolute_value = 0
            vis_position = chan * VIS_CHAN_COUNT + vis
            vis_index = visibilities.vis_number.index(vis_position)
            vis_chan = visibilities.visibilities[vis_index]
            for i in range(int(n_stations * (n_stations + 1) / 2)):
                for integ in range(integration_cnt):
                    time_index = int(sorted(visibilities.time_sequence)[integ])
                    for polarisation in range(4):
                        absolute_value += np.abs(
                            vis_chan[time_index]["VIS"][i][polarisation]
                        )
            all_vis_channel_integration[chan].append(absolute_value)
    return all_vis_channel_integration


def max_min_and_max_max(all_visibilities) -> tuple:
    """Calculate normalised distance of min and max from the mean visibilites.

    Return a tuple of largest distances from the mean.
    """
    max_min = max_max = 0.0
    for vis in all_visibilities.values():
        max_min = max(max_min, (np.mean(vis) - np.min(vis)) / np.mean(vis))
        max_max = max(max_max, (np.max(vis) - np.mean(vis)) / np.mean(vis))
    return max_min, max_max


def get_baselines(n_stations: int) -> list[int]:
    """Return a list of baseline IDs for given number of stations."""
    start, length = 0, 1
    baselines = []
    full_range = list(range(n_stations**2))
    while length <= n_stations:
        # -1 below: drop autocorrelation
        baselines.extend(full_range[start : start + length - 1])
        start += length
        length += 1
    return baselines


N_INTEGRATIONS = 5
vis_analyser_cfg = {"stations": station_ids, "coarse_channels": []}

# beam analysis takes ~13.5 minutes for 16 channels, 18 stations; we have (16, 32, 48)
# channels per source,  in total 6 multiples of 16
TEST_TIMEOUT_SEC = 14 * 60 * 6
all_min_max = []  # collect min/max for Overleaf report

BASELINES = get_baselines(n_stations)
MAX_PHASE_BIAS_DEG = 0.8  # average phaseacross all channels should be less than this
MAX_PHASE_STD_DEV = 1


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc26_corr_multi_beam_delay_tracking(
    cnic_and_processor, ready_subarray
):  # noqa
    """Implement PTC 26: simulate station data streams, capture, analyse."""
    # pylint: disable=too-many-locals

    # SPS signal generation and visibilities capture
    # ----------------------------------------------
    corr_pcap = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        pcap_file_name=PCAP_PATH,
    )
    corr_pcap.close()  # file reopened by analysis section

    # the analysis stage
    # ------------------
    for beam, channels in all_beam_channels:
        vis_analyser_cfg["coarse_channels"] = channels
        visibilities = VisibilityAnalyser(json.dumps(vis_analyser_cfg))
        visibilities.extract_spead_data(PCAP_PATH, filter_=f"host {DEST_NET}{beam}")

        # Calling this creates a graph
        visibilities.amplitude_freq_analysis(
            "XX", N_VIS - N_INTEGRATIONS, channels, False
        )
        shutil.move(
            "./amplitude_vs_freq_polarisation_XX.png",
            f"{DATA_DIR}amplitude_vs_freq_polarisation_{beam_count}_beams_b{beam}.png",
        )

        visibilities.populate_correlation_matrix()
        png_prefix = f"beam_{beam}_"
        visibilities.save_phase_stddev_to_disk(DATA_DIR, case=png_prefix)
        visibilities.save_phase_heatmap_to_disk(
            mode="average_all", directory=DATA_DIR, case=png_prefix
        )

        if SKIP_ANALYSIS:
            del visibilities
            continue
        # phase statistics
        for baseline in BASELINES:
            average, std_dev = phase_stats(visibilities, baseline)
            with open(f"{DATA_DIR}phase_stats.txt", "a+", encoding="ascii") as fout:
                fout.write(
                    f"beam: {beam}, baseline {baseline}\n"
                    f"  average {average}\n"
                    f"  std dev {std_dev}\n"
                )
            if END_TEST_ON_ASSERT_FAILURE:
                assert (
                    abs(average) < MAX_PHASE_BIAS_DEG
                ), "average phase for baseline {baseline}: {average}"
                assert (
                    std_dev < MAX_PHASE_STD_DEV
                ), "phase std dev for baseline {baseline}: {std_dev}"
        all_vis = visibilities_same_power(
            visibilities, channels, station_ids, N_VIS - N_INTEGRATIONS
        )
        max_min, max_max = max_min_and_max_max(all_vis)
        # save the min/max values for reference
        filename = f"{DATA_DIR}beam_{beam}_chan_{len(channels)}.txt"
        with open(filename, "w", encoding="ascii") as outfile:
            outfile.write(
                f"channels {len(channels)}\n"
                f"max_min {max_min}\n"
                f"max_max {max_max}\n"
            )

        for i in (max_min, max_max):
            all_min_max.append(i)
            if END_TEST_ON_ASSERT_FAILURE:
                assert i < 0.01

    if GENERATE_OVERLEAF_REPORT:
        ptc26_report(scan_ptc_26, all_min_max)


def ptc26_report(scan_config, all_min_max):
    """Generate Overleaf test report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "671c0680ef862a8d6ae009ab"
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    target_directory = f"{local_directory}{new_repo_name}/"
    file_result_to_replace = target_directory + "report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )

    for key, val in (
        ("TOPUTDIRECTORY", new_repo_name),
        ("CONFIGURE_SCAN", json.dumps(scan_config)),
    ):
        replace_word_in_report(file_result_to_replace, key, val)

    for i, beam in enumerate(all_beams):
        replace_word_in_report(
            file_result_to_replace,
            f"MIN_MAX_{beam}",
            f"{all_min_max[2 * i]}",
        )
        replace_word_in_report(
            file_result_to_replace,
            f"MAX_MAX_{beam}",
            f"{all_min_max[2 * i + 1]}",
        )
        # channel power image
        # -------------------
        src = f"{DATA_DIR}amplitude_vs_freq_polarisation_{beam_count}_beams_b{beam}.png"
        fname = f"amplitude_vs_freq_polarisation_{beam_count}_beams_b{i+1}.png"
        dest = f"{target_directory}{fname}"
        shutil.copy(src, dest)
        repo.git.add(f"{new_repo_name}/{fname}")
        # phase heat map image
        # --------------------
        # form a file name the same way the save_phase_heatmap_to_disk()
        # function call would:
        fname = f"beam_{beam}_phase_heat_map.png"
        src = f"{DATA_DIR}{fname}"
        dest = f"{target_directory}{fname}"
        shutil.copy(src, dest)
        repo.git.add(f"{new_repo_name}/{fname}")
        # phase std.dev. heat map image
        # -----------------------------
        # form a file name the same way the save_phase_heatmap_to_disk()
        # function call would:
        fname = f"beam_{beam}_phase_stddev_heat_map.png"
        src = f"{DATA_DIR}{fname}"
        dest = f"{target_directory}{fname}"
        shutil.copy(src, dest)
        repo.git.add(f"{new_repo_name}/{fname}")

    report_path = local_directory + "report2017.tex"
    replace_word_in_report(report_path, "%NEW_RESULTS", extension_of_new_results)
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def phase_stats(visibilities, baseline: int) -> tuple[int, int]:
    """Calculate phase average and standard deviation across all visibilities
    for given baseline.

    Return tuple of (average, std.dev) that are maximum values across all
    channels.
    """  # noqa
    vis_chan_phase = [[0] for _ in range(144)]
    vis_chan_average = [[0] for _ in range(144)]
    vis_chan_stddev = [[0] for _ in range(144)]
    print("i_time", len(visibilities.time_sequence))
    for times in visibilities.time_sequence:
        for vis_chan in range(144):
            phase = visibilities.visibilities[vis_chan][times]["VIS"][baseline][0]
            vis_chan_phase[vis_chan].append(np.angle(phase, deg=True))

    for vis_chan in range(144):
        vis_chan_average[vis_chan] = abs(np.mean(vis_chan_phase[vis_chan]))
        vis_chan_stddev[vis_chan] = np.std(vis_chan_phase[vis_chan])

    return max(vis_chan_average), max(vis_chan_stddev)
