# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""
Perentie Test Case 9 - Visibility channel resolution.

Checks that channels are linearly spaced 5.4 kHz apart.

Based on Jupyter notebook: PI23/FAT1/PTC9

- `Low CBF Requirements <https://is.gd/sul9PW>`_
- `Perentie Test Cases <https://is.gd/BCLuaC>`_
- `PTC #9 <https://is.gd/Xcamdx>`_

1. Set up a SPS CNIC to generate a single complex sinusoid for the X polarisation for
   2 stations, single SPS channel, sinusoid amplitude of 2, frequency = n*5.4kHz where
   n=-72 to 72 in steps of 0.5 (so 289 data sets) where this frequency is relative to
   centre of the visibility channel closest to the centre of SPS channel (=m*781250Hz)
   (these  frequencies are at every second channel boundary), add noise of amplitude 1,
   zero delay, 5 seconds data, identical data for both stations
2. For each frequency setup the CNIC to transmit SPS packets to the correlator Alveo.
3. Configure the correlator subarray to have 0.849s integrations, with zero delays
4. Set up SDP CNIC to capture data for all 144 fine channels of the SPS channel, auto
   and cross correlation of the two stations
5. Run the scan for each tone frequency
6. Average the autocorrelation data for each data set in time (or just pick one
   integration)
7. Analyse the visibility channel autocorrelation powers follow the expected pattern.
   For each scan (or CNIC tone), the power in the sinusoid is split between two
   adjacent 5.4kHz channels, except those where it is seen only in the end channels.
   This shows the channel boundaries are uniformly spaced.
"""
import json
import os
import shutil
from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_1ch
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 123  # needs to match scan id in scan_6stn_1sa_1bm_1ch
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
BEAM_ID = 1  # needs to match beam id in scan_6stn_1sa_1bm_1ch

# ---------------------------
# PCAP/debug file names
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc9/"
"""Directory name where data files will be stored (PCAP and calculations)"""

# PCAP file name template:
PCAP_PATH = DATA_DIR + "PTC9_correlator_{:03}_stations.pcap"

# power value in each frequency channel
VALUES_FNAME = DATA_DIR + "values_{:03}.txt"

SCALE_FILE_NAME = "fbscale_linear.txt"
SCALE_FILE_PATH = f"./src/ska_low_cbf_integration/{SCALE_FILE_NAME}"

FULL_RANGE = 287  # 32 takes ~1h, the runner may cut the test short
# 64 fails in CI pipeline; works ok from CLI

# ---------------------------
# Analysis related constants
# ---------------------------
# when signal falls into a single frequency channel the power
# should be within those limits:
#  SINGLE_SLOT_LO_LIMIT <= x <= SINGLE_SLOT_HI_LIMIT
SINGLE_SLOT_LO_LIMIT = 0.92
SINGLE_SLOT_HI_LIMIT = 0.96

# when signal is spread over two frequency channels the power in each
# channel should be within those limits:
#   TWO_SLOTS_LO_LIMIT <= x <= TWO_SLOTS_HI_LIMIT
TWO_SLOTS_LO_LIMIT = 0.46
TWO_SLOTS_HI_LIMIT = 0.49

NO_SIGNAL_MAX_POWER = 0.05
"""Maximum power in frequency channel with no signal"""


def _cnic_vd_cfg(tone_position, stations: list, channels: list) -> list:
    """Return CNIC-VD configuration for given stations, channels and frequency."""
    fine_freq = int(192 * tone_position) - 4
    return [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": chan,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    {"tone": False, "seed": station, "scale": 40},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 4,
                    },
                ],
                "y": [
                    {"tone": False, "seed": station, "scale": 40},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 4,
                    },
                ],
            },
        }
        for station, substation in stations
        for chan in channels
    ]


# CONTINUE the test from last failure rather than from the beginning
LAST_FAILURE = 0


@pytest.mark.timeout(FULL_RANGE * 340)
@pytest.mark.hardware_present
@pytest.mark.large_capture
@pytest.mark.long_test
def test_ptc9_corr_visibility_chan_resolution(
    cnic_and_processor, ready_subarray
):  # pylint: disable=too-many-locals,too-many-statements
    """Implement PTC 9: simulate station data stream, capture, analyse."""
    # basic settings
    n_stations = 6
    station_ids = list(range(1, 1 + n_stations))
    stations = [[station, 1] for station in station_ids]
    channels = [448]
    # tweak the stock standard scan configuration
    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    stn = scan_config["lowcbf"]["stations"]
    stn["stn_beams"][0]["freq_ids"] = channels
    stn["stns"] = stations

    # output packet related constants
    channel_count = len(channels)
    n_vis = n_stations * 2 * 2 + 1  # x/y polarisations, real/imag, 1 init
    rx_packets = 144 * n_vis * channel_count
    rx_size = 120

    n_integrations = 5  # the number of integrations

    # pull the 'scale' file into execution directory so we don't need
    # to change VisibilityAnalyser class
    # the path is relative to container's /app/ directory where the test runs
    if os.path.exists(SCALE_FILE_PATH):
        shutil.copyfile(SCALE_FILE_PATH, SCALE_FILE_NAME)

    for i in range(LAST_FAILURE, FULL_RANGE):
        tone_position = i / 2 - 71.5

        test_config = ProcessorVdTestConfig(
            cnic_capture_min_size=rx_size,
            # n batches of visibility data per stream, plus SPEAD init packets
            cnic_capture_packets=rx_packets,
            scan_config=scan_config,
            firmware_type=Personality.CORR,
            input_packet_count=None,
            start_scan_packet_count=None,
            before_end_scan_packet_count=None,
            output_packet_count=None,  # proc packets after end of scan
            vd_config=_cnic_vd_cfg(tone_position, stations, channels),
            during_scan_callback=None,
        )
        file_h = cnic_processor(
            cnic_and_processor,
            ready_subarray,
            test_config,
            PCAP_PATH.format(i),
        )
        file_h.close()  # Anayser will handle the file's contents

    # Analysis
    analyser_cfg = {"coarse_channels": channels, "stations": station_ids}
    values_around_center = {}
    even_values = []
    odd_values = []
    other_values = []
    for position in range(FULL_RANGE):
        visibilities = VisibilityAnalyser(json.dumps(analyser_cfg))
        visibilities.extract_spead_data(PCAP_PATH.format(position))

        visibilities.remove_visibilities(n_integrations)
        visibilities.populate_correlation_matrix()
        ampl = visibilities.amplitude_freq_analysis(
            "XX", n_integrations, channels, False
        )
        key = position / 2 - 71.5
        ampl_sum = np.sum(ampl[0])
        ampl_values = [amplitude / ampl_sum for amplitude in ampl[0]]
        values_around_center[key] = ampl_values

        # DEBUG: write to a file so we can inspect data
        with open(VALUES_FNAME.format(position), "w", encoding="ascii") as file_h:
            file_h.write(f"key: {key} {repr(ampl_values)}\n")

        # frequency increment step is 0.5, adjust index
        array_index = position // 2
        if position % 2:
            # for odd values the power is spred over two frequency channels
            # however the last freq. won't be able to reach beyond the array's end
            if position == FULL_RANGE - 1:
                current = ampl_values[array_index]
            else:
                previous, current = ampl_values[array_index : array_index + 2]
                odd_values.append(previous)
                assert TWO_SLOTS_LO_LIMIT <= previous <= TWO_SLOTS_HI_LIMIT
            odd_values.append(current)
            assert TWO_SLOTS_LO_LIMIT <= current <= TWO_SLOTS_HI_LIMIT
            other_values += ampl_values[0:array_index]
            if array_index + 2 < len(ampl_values):
                other_values += ampl_values[array_index + 2 : (len(ampl_values) + 1)]
            # remove these entries so we can check power is low in the
            # remaining frequency channels
            del ampl_values[array_index : array_index + 2]

        else:
            # for even values the power is concentrated in a single frequency channel
            current = ampl_values[array_index]
            assert SINGLE_SLOT_LO_LIMIT <= current <= SINGLE_SLOT_HI_LIMIT
            even_values.append(current)
            other_values += ampl_values[0:array_index]
            if array_index + 1 < len(ampl_values):
                other_values += ampl_values[array_index + 1 : (len(ampl_values) + 1)]
            # remove this entry so we can check other channels
            del ampl_values[array_index]
        # the power in remaining frequency channels should be very low
        assert all(i < NO_SIGNAL_MAX_POWER for i in ampl_values)
        del ampl, visibilities
    # time to generate overleaf report
    ptc9_report(scan_config, even_values, odd_values, other_values)


def ptc9_report(scan_config, even_values, odd_values, other_values):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66f9e85927de40231a9bfb7a"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    plt.clf()
    plt.boxplot(even_values)
    plt.ylabel("Amplitude (% of full SPS channel)")
    plt.legend(prop={"size": 10})
    plt.title("Visibility with full tone")
    plt.savefig(
        local_directory + new_repo_name + "/ptc13_box_plot_tone.png",
        format="png",
    )
    plt.clf()
    plt.boxplot(odd_values)
    plt.ylabel("Amplitude (% of full SPS channel)")
    plt.legend(prop={"size": 10})
    plt.title("Visibilities with half tone")
    plt.savefig(
        local_directory + new_repo_name + "/ptc13_box_plot_dual.png",
        format="png",
    )
    plt.clf()
    plt.boxplot(other_values)
    plt.ylabel("Amplitude (% of full SPS channel)")
    plt.legend(prop={"size": 10})
    plt.title("Visibility with no tone")
    plt.savefig(
        local_directory + new_repo_name + "/ptc13_box_plot_other.png",
        format="png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc13_box_plot_tone.png")
    repo.git.add(new_repo_name + "/ptc13_box_plot_dual.png")
    repo.git.add(new_repo_name + "/ptc13_box_plot_other.png")
    repo.index.commit("Test with python")
    repo.git.push()
