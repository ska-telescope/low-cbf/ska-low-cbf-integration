# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 14 - Visibility biases

Based on Jupyter notebook: FAT0.5/PTC14/PTC14.ipynb

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #14 <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E/edit?gid=725540233#gid=725540233>` _

1.  Using the CNIC virtual digitiser to generate independent pseudorandom noise sequences (i.e., uncorrelated noise)
for 6-stations, dual polarisation, 12 SPS channels, signal level 10 rms
2. Check the CNIC-VD configuration by capturing data and showing RMS level and independent seeds for all channels
and stations.
3. Configure the Alveo correlator with a single subarray with all stations and all bandwidth, 0.849s integrations,
standard 5.4kHz channels
4. Set the delay polynomial to zero for all time
5. Setup the capture of visibilities in an SDP CNIC for 4096 correlator integration periods (4096*0.849=58 minutes),
where each SPD packet is 820Bytes, so 4096*12*144*820B = 5.8GB
6. Process the data with the correlator and capture the result in CNIC-Rx
7. Calculate the RMS_total value of the cross-correlations using all integrations, all frequencies, all
polarisations, and all baselines (i.e., effectively over 4096 integrations * 12*144 frequencies * 4pol*15baselines
=424M values). Repeat the RMS calculation for the auto-correlations.
8. Calculate the complex sum, called SUM_total, of all cross correlation values in the dataset, i.e., all
integrations, all frequencies, all baselines, all polarisations. Repeat the measurement for auto-correlations.
9. Form a 2^12=4096 long vector, called SUM_1, which is the average complex sum of all data (visibility channels
and cross correlations) belonging to a single integration.
10. Form a new data set (SUM_n) by adding pairs of complex values in SUM_n-1 and dividing by 2 (averaging), this
will have half as many values. For each SUM_n calculate the RMS of the data.
11. Compliance to SKAO-CSP_Low_CBF_REQ-199 is achieved by demonstrating that the slope of the plotted line on a log
scale for SUM_n is -0.5.
12. Introduce a second source of correlated noise (same seed) for all stations and channels at a level 316 times
weaker than the uncorrelated noise. Re-run tests #14.6-10
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import shutil
from copy import deepcopy
from time import sleep

import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.print_rms import (
    print_rms_final_graph,
    print_rms_sum,
    print_rms_sum_imag,
    print_rms_sum_real,
)
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_1ch
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

MAX_STD_DEVIATION = 0.000_06  # see comment above

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 123  # needs to match scan id in scan_6stn_1sa_1bm_1ch
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
BEAM_ID = 1  # needs to match beam id in scan_6stn_1sa_1bm_1ch

# ---------------------------
# PCAP/debug file names
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc14/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH = DATA_DIR + "PTC14_correlator.pcap"
"""PCAP file name"""

EXPECTED_NUMBER_PACKETS = 4098 * 12 * 144


def _cnic_vd_cfg(stations: list, channels: list) -> dict:
    """Return CNIC-VD configuration for given stations, channels and frequency.

    :param tone_position: tone position
    :param stations: a list of (station, substation) tuples
    :param channels: a list of channels
    :return: a list of dictionaries for CNIC-VD configuration
    """
    seeds = np.zeros([len(stations), len(channels)])
    seed = 0
    for station in range(len(stations)):
        for channel in range(len(channels)):
            seeds[station][channel] = seed
            seed = seed + 1
    print(seeds)
    print(channels)
    print(stations)
    cfg = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": chan,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    {
                        "tone": False,
                        "seed": int(seeds[index_station][index_chan]),
                        "scale": 4000,
                    },
                ],
                "y": [
                    {
                        "tone": False,
                        "seed": int(
                            len(stations) * len(channels)
                            + seeds[index_station][index_chan]
                        ),
                        "scale": 4000,
                    },
                ],
            },
        }
        for index_station, (station, substation) in enumerate(stations)
        for index_chan, chan in enumerate(channels)
    ]
    return {"sps_packet_version": 3, "stream_configs": cfg}


CHAN = 330
VIS_CHAN_COUNT = 144
CAPTURE_TIME_SEC = 7200


@pytest.mark.long_test
@pytest.mark.large_capture
@pytest.mark.timeout(CAPTURE_TIME_SEC + 300)
@pytest.mark.hardware_present
def test_ptc14_corr_visibility_biases(
    cnic_and_processor, ready_subarray
):  # pylint: disable=too-many-locals
    """Implement PTC 14: simulate station data, capture packages, run analysis."""
    # basic settings
    n_stations = 6
    n_channels = 12
    station_ids = list(range(1, 1 + n_stations))
    stations = [[station, 1] for station in station_ids]
    channels = list(range(CHAN, CHAN + n_channels))
    # tweak the stock standard scan configuration (see corr_configs.py)
    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels

    # output packet related constants
    channel_count = len(channels)
    rx_size = 64
    n_vis = 8 * 512
    rx_packets = VIS_CHAN_COUNT * (n_vis + 1) * channel_count

    # Generate / capture packets
    # --------------------------
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=rx_size,
        cnic_capture_packets=rx_packets,
        firmware_type=Personality.CORR,
        scan_config=scan_config,
        vd_config=_cnic_vd_cfg(stations, channels),
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,  # proc packets after end of scan
        during_scan_callback=_ptc_14_waiting_longer,
    )
    f_handle = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH,
    )
    f_handle.close()  # Anayser will consume the file's contents

    # Analysis
    # --------
    analyser_cfg = {"coarse_channels": channels, "stations": station_ids}
    visibilities = VisibilityAnalyser(json.dumps(analyser_cfg))
    visibilities.extract_spead_data(PCAP_PATH)

    _, sum1 = visibilities.rms_slice_total_preparation(6480)
    _, _, slope = print_rms_final_graph(
        slice_size=6480, index_rms=16, sum_1=sum1, case="/app/build/"
    )
    print_rms_sum(slice_size=6480, index_rms=16, sum_1=sum1, case="/app/build/")
    print_rms_sum_real(slice_size=6480, index_rms=16, sum_1=sum1, case="/app/build/")
    print_rms_sum_imag(slice_size=6480, index_rms=16, sum_1=sum1, case="/app/build/")
    assertion = ptc14_report(test_config, slope)
    assert assertion


def _ptc_14_waiting_longer(cnic, processor):
    """Increment delay in steps."""
    packets_received = cnic.hbm_pktcontroller__rx_packet_count
    expected_packets = EXPECTED_NUMBER_PACKETS
    n_same = 0
    while not cnic.finished_receive or packets_received < expected_packets:
        print(f"Packets Received: {packets_received}")
        print(f"Stat delay {json.loads(processor.stats_delay)}")
        sleep(30)
        n_same += 1
        if packets_received == cnic.hbm_pktcontroller__rx_packet_count:
            n_same += 1
        else:
            n_same = 0
        if n_same == 4:
            break
        packets_received = cnic.hbm_pktcontroller__rx_packet_count


def ptc14_report(test_config, slope):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66fdf69294a9c90906bd9675"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "FITTINGSLOPE",
        json.dumps(slope),
    )
    assertion = -0.55 < slope < -0.45
    replace_word_in_report(
        file_result_to_replace,
        "PASSINGSLOPE",
        str(assertion),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "/app/build/ptc_14_sum.png",
        local_directory + new_repo_name + "/ptc_14_sum.png",
    )
    shutil.copy(
        "/app/build/ptc_sum_dev_imag.png",
        local_directory + new_repo_name + "/ptc_sum_dev_imag.png",
    )
    shutil.copy(
        "/app/build/ptc_sum_dev_real.png",
        local_directory + new_repo_name + "/ptc_sum_dev_real.png",
    )
    shutil.copy(
        "/app/build/ptc_sum_imag.png",
        local_directory + new_repo_name + "/ptc_sum_imag.png",
    )
    shutil.copy(
        "/app/build/ptc_sum_real.png",
        local_directory + new_repo_name + "/ptc_sum_real.png",
    )
    shutil.copy(
        "/app/build/rms_ptc_14.png",
        local_directory + new_repo_name + "/rms_ptc_14.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc_14_sum.png")
    repo.git.add(new_repo_name + "/ptc_sum_dev_imag.png")
    repo.git.add(new_repo_name + "/ptc_sum_dev_real.png")
    repo.git.add(new_repo_name + "/ptc_sum_imag.png")
    repo.git.add(new_repo_name + "/ptc_sum_real.png")
    repo.git.add(new_repo_name + "/rms_ptc_14.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return assertion
