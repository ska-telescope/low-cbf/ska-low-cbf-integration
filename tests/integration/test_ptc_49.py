# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2025 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# We use log level DEBUG so we don't care about speed gains from lazy interpolation
# pylint: disable=logging-fstring-interpolation
# pylint: disable=line-too-long
"""
Perentie Test Case 49 - Multiple Beamformer Subarrays.

More information:
- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_

Step numbers in comments/messages are taken from the Perentie Test Cases spreadsheet.
"""  # noqa
# pylint: enable=line-too-long
import json
import logging
import math
import os
from collections import defaultdict
from functools import partial
from pathlib import Path
from typing import BinaryIO, Iterable, TypeAlias

import matplotlib.colors as mpl_colors
import matplotlib.pyplot as plt
import numpy as np
import pytest
import tango
from tango import DeviceProxy

from ska_low_cbf_integration.allocator import allocator_proxy
from ska_low_cbf_integration.cnic import VD_NOISE_SCALE
from ska_low_cbf_integration.connector import assert_packet_counts_zero
from ska_low_cbf_integration.delay_poly import wait_for_delay_poly
from ska_low_cbf_integration.firmware import Personality, scan_config_keys
from ska_low_cbf_integration.low_psi import device_ports, get_connector_proxy
from ska_low_cbf_integration.pcap import data_start_stop_per_dst, payloads
from ska_low_cbf_integration.processor import debug_reg_read
from ska_low_cbf_integration.pulsar_protocol import PsrPacket, PssValidityFlags
from ska_low_cbf_integration.subarray import empty_subarray, get_ready_subarray_proxy
from ska_low_cbf_integration.tango import wait_for_attr
from tests.integration.station_beam_ids import BeamId
from tests.integration.test_processor import (
    CONN_TIMEOUT_MS,
    DELAY_EMULATOR_ADDR,
    ProcessorVdStaticTrackingTestConfig,
    check_processors_firmware,
    configure_delays_for_test,
    configure_scan_for_test,
    is_fpga_log_enabled,
    log_registers,
)
from tests.integration.test_ptc_47 import control_subarrays, plot_subarray_data_times

PCAP_DIR = Path("/test-data/PTC/ptc49/")
BUILD_DIR = Path("build/ptc49")
"""Directory to write plots etc."""

subarrays_firmware = {Personality.PST: (1, 2, 3), Personality.PSS: (1, 2)}
"""Which (how many) subarrays to use for each firmware personality."""


def subarray_config(n: int, personality: Personality) -> dict:
    """
    Create Subarray Configuration for PTC#49.

    49.1 Configure 3 subarrays within one PST beamformer Alveo.
    49.7 Repeat for the PSS beamformer

    Subarray 1 has  4 stations and 3 channels,
    Subarray 2 has  8 stations and 5 channels,
    Subarray 3 has 16 stations and 2 channels.

    Use different station beam numbers and differing channel numbers in each subarray.

    Each subarray has a different PST beam number.

    :param personality: Firmware under test
    :param n: Subarray number
    :return: dict for use with ConfigureScan
    """
    station_beam = _station_beam_id_subarray(n)
    station_delay_trl = f"low-cbf/delaypoly/0/delay_s{n:02}_b{station_beam:02}"
    beam_delay_trl = f"low-cbf/delaypoly/0/pst_s{n:02}_b{station_beam:02}_1"

    return {
        "id": 100 + n,
        "lowcbf": {
            "stations": {
                "stns": _stations_subarray(n),
                "stn_beams": [
                    {
                        "beam_id": station_beam,
                        "freq_ids": _channels_subarray(n),
                        "delay_poly": station_delay_trl,
                    },
                ],
            },
            scan_config_keys[personality]: {
                "beams": [
                    {
                        f"{personality.name.lower()}_beam_id": n,
                        "stn_beam_id": station_beam,
                        "delay_poly": beam_delay_trl,
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": _ip_addr_subarray(n),
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": [1.0] * len(_stations_subarray(n)),
                    }
                ],
            },
        },
    }


def _channels_subarray(n: int) -> list[int]:
    """
    Get the list of channels for a Subarray.

    See :py:func:`subarray_config` for specification.
    """
    n_channels = None
    if n == 1:
        n_channels = 3
    elif n == 2:
        n_channels = 5
    elif n == 3:
        n_channels = 2

    if n_channels is None:
        raise NotImplementedError(f"Subarray {n} not implemented.")

    first_channel = 100 + 10 * n

    return list(range(first_channel, first_channel + n_channels))


def _stations_subarray(n: int) -> list[(int, int)]:
    """
    Get the list of stations and substations for a Subarray.

    See :py:func:`subarray_config` for specification.

    :returns: [(station, substation), ...]
    """
    n_stations = None
    if n == 1:
        n_stations = 4
    elif n == 2:
        n_stations = 8
    elif n == 3:
        n_stations = 16

    if n_stations is None:
        raise NotImplementedError(f"Subarray {n} not implemented.")

    return [(station, 1) for station in range(1, n_stations + 1)]


def _station_beam_id_subarray(n: int) -> int:
    """Get the Station Beam ID used for a given Subarray."""
    # we could use getattr here,
    # but being explicit makes it easier to check for usages of enum values in future
    if n == 1:
        return BeamId.PTC49_1
    if n == 2:
        return BeamId.PTC49_2
    if n == 3:
        return BeamId.PTC49_3

    raise NotImplementedError(f"Subarray {n} not implemented.")


def _ip_addr_subarray(n: int) -> str:
    """Get destination IP address used for a given Subarray."""
    return f"10.10.10.{n}"


def _cnic_scale(channel, subarray) -> int:
    """Calculate the scale factor for use with CNIC-VD."""
    return 1000 + channel * subarray**2


def get_cnic_config(subarrays: Iterable[int]) -> dict:
    """
    Create CNIC Configuration for PTC#49.

    49.2 Set up the CNIC to produce data for all 3 subarrays.
    CNIC data amplitude is proportional to subarray * station beam * channel.
    """
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 100 + subarray,
                "subarray": subarray,
                "station": station,
                "substation": substation,
                "frequency": channel,
                "beam": _station_beam_id_subarray(subarray),
                "sources": {
                    "x": [
                        {
                            "tone": False,
                            "seed": 4949,
                            "scale": _cnic_scale(channel, subarray),
                        },
                    ],
                    "y": [
                        {
                            "tone": False,
                            "seed": 4949,
                            "scale": _cnic_scale(channel, subarray),
                        },
                    ],
                },
            }
            for subarray in subarrays
            for channel in _channels_subarray(subarray)
            for station, substation in _stations_subarray(subarray)
        ],
    }


def delay_configs(subarray_id: int) -> ([dict], [dict], dict):
    """
    Generate delay configurations.

    :param subarray_id: Subarray ID
    :return: delay emulator configurations: (station beam, source, PST beam).
    """
    station_beam_id = _station_beam_id_subarray(subarray_id)
    station_beam_dir = {"ra": "6h19m00.00s", "dec": "14d23m00.0s"}
    # a small offset here lets us see that both station and beam delays are applied
    pst_beam_dir = {"ra": "6h19m00.00s", "dec": "14d23m10.0s"}

    # no copy here as they just get passed to a Tango device command as-is
    station_beam_delay = {
        "subarray_id": subarray_id,
        "beam_id": station_beam_id,
        "direction": station_beam_dir,
    }
    source_and_pst_delay = {
        "subarray_id": subarray_id,
        "beam_id": station_beam_id,
        "direction": [pst_beam_dir] * 4,
    }

    return [station_beam_delay], [source_and_pst_delay], source_and_pst_delay


# 49.3 Scan each subarray with a different start time and duration
# (e.g., start 0/10/20 seconds once configured, duration 10/20/30 seconds)
scenario = {
    1: {"start": 0, "duration": 10},
    2: {"start": 10, "duration": 20},
    3: {"start": 20, "duration": 30},
}
assert all(
    t % 10 == 0 for subarray_spec in scenario.values() for t in subarray_spec.values()
), "PTC#49 start times & durations must be multiples of 10"


# 49.7 Repeat for the PSS beamformer
# This test function name violates our naming convention slightly,
# by using "bf" instead of a firmware image name.
# Instead, we use IDs of "pss" and "pst" in these parameters.
# This means we can still select the right test with the usual filters,
# fulfilling the intent of the naming convention.
@pytest.mark.parametrize(
    "test_firmware",
    [pytest.param(Personality.PST, id="pst"), pytest.param(Personality.PSS, id="pss")],
)
@pytest.mark.hardware_present
def test_ptc49_bf_multiple_subarrays(cnic_and_processor, test_firmware: Personality):
    """Run PTC#49 - Multiple Beamformer Subarrays."""
    # pylint: disable=too-many-locals,too-many-statements
    os.makedirs(PCAP_DIR, exist_ok=True)
    pcap_filename = str(PCAP_DIR / f"{test_firmware.name.lower()}.pcap")
    build_dir = BUILD_DIR / test_firmware.name.lower()
    os.makedirs(build_dir, exist_ok=True)
    logger = logging.getLogger("ptc49")
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(build_dir / "debug.log")
    file_handler.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)

    subarray_ids = subarrays_firmware[test_firmware]
    cnic_config = get_cnic_config(subarray_ids)
    with open(build_dir / "cnic_config.json", "w", encoding="utf-8") as cnic_file:
        cnic_file.write(json.dumps(cnic_config, indent=2))

    # TODO - use subarray fixtures so subarrays clean up more reliably?
    subarrays = {n: get_ready_subarray_proxy(n) for n in subarray_ids}

    # we can't use our standard test fixture here because we need to stagger scans :(
    cnics, processors = cnic_and_processor
    cnic = cnics[0]
    processor = processors[0]

    connector = get_connector_proxy()
    connector.set_timeout_millis(CONN_TIMEOUT_MS)
    delay = DeviceProxy(DELAY_EMULATOR_ADDR)

    # We use a TestConfig object for ease of sharing common functions.
    # Note that we are not using the normal cnic_processor function to drive the test.
    test_config = ProcessorVdStaticTrackingTestConfig(
        # We shouldn't be too precise in the Rx packet count in case of small timing
        # variations. Will stop Rx manually.
        cnic_capture_packets=1_000_000,
        cnic_capture_min_size=64,
        firmware_type=test_firmware,
        vd_config=cnic_config,
        # below parameters will be supplied in a per-subarray loop
        scan_config={},
        source_delay_config=[{}],
        beam_delay_config=[{}],
        pst_delay_config={},
    )
    subarray_configs = {}

    # calculate expected end time for control and plotting
    for entry in scenario.values():
        entry["end"] = entry["start"] + entry["duration"]
    first = True
    for n, subarray in subarrays.items():
        test_config.scan_config = subarray_config(n, test_firmware)
        subarray_configs[n] = subarray_config(n, test_firmware)

        (
            test_config.beam_delay_config,
            test_config.source_delay_config,
            test_config.pst_delay_config,
        ) = delay_configs(n)

        with open(
            build_dir / f"scan_config_subarray_{n}.json", "w", encoding="utf-8"
        ) as scan_file:
            scan_file.write(json.dumps(subarray_configs[n], indent=2))

        configure_delays_for_test(test_config)
        configure_scan_for_test(subarray, test_config)
        check_processors_firmware(test_config, processors)
        wait_for_delay_poly(delay, test_config.scan_config)

        if first and is_fpga_log_enabled():
            # We will miss the first subarray configuration, but that's hard to avoid...
            # Can't enable register logging unless FPGA is already programmed!
            log_registers(processor)
            first = False

    n_processors_used = len(allocator_proxy().internal_alveo)
    assert n_processors_used == 1, "PTC49 Subarrays expected to fit in one FPGA"
    assert_packet_counts_zero(connector, device_ports(cnics + processors))

    # CNIC Rx
    cnic.CallMethod(
        method="receive_pcap",
        arguments={
            "out_filename": pcap_filename,
            "packet_size": test_config.cnic_capture_min_size,
            "n_packets": test_config.cnic_capture_packets,
        },
    )

    register_debug_log = partial(
        log_all_lfaa_counters, logger, processor, test_firmware
    )
    # 49.2 The CNIC has to produce all the SPS data from the beginning,
    # and not be incremental.
    logger.info("Starting CNIC-VD")
    register_debug_log(timestamp="before")
    logger.debug("CNIC HBM Rx %s", cnic.hbm_pktcontroller__rx_packet_count)
    logger.debug("CNIC Eth Tx %s", cnic.system__eth100g_tx_total_packets)
    cnic.ConfigureVirtualDigitiser(test_config.vd_config)
    cnic.StartSourceDelays(DELAY_EMULATOR_ADDR)  # will turn on VD when delays arrive
    wait_for_attr(cnic, "enable_vd", value=True, max_duration=60)

    # 49.3 Scan each subarray with a different start time and duration
    t_first_scan = control_subarrays(
        subarrays=subarrays,
        cnic=cnic,
        scan_times=scenario,
        active_subarrays=subarray_ids,
        debug_callback=register_debug_log,
    )
    logger.info("First scan started at: %s", t_first_scan)
    logger.debug("CNIC HBM Rx %s", cnic.hbm_pktcontroller__rx_packet_count)
    logger.debug("CNIC Eth Tx %s", cnic.system__eth100g_tx_total_packets)
    register_debug_log(timestamp="immediately after")

    # Stop CNIC Tx & Rx
    logger.info("Stopping CNIC-VD")
    cnic.enable_vd = False
    logger.info("Stopping CNIC Rx")
    cnic.CallMethod(method="stop_receive")
    cnic.StopSourceDelays()  # TODO - move to fixture that provides CNIC?
    logger.debug("CNIC HBM Rx %s", cnic.hbm_pktcontroller__rx_packet_count)
    logger.debug("CNIC Eth Tx %s", cnic.system__eth100g_tx_total_packets)
    register_debug_log(timestamp="the end")

    # Clean up for politeness
    for subarray in subarrays.values():
        empty_subarray(subarray)

    wait_for_attr(cnic, "finished_receive", max_duration=600, sleep_per_loop=5)
    subarray_t = data_start_stop_per_dst(pcap_filename)
    logger.debug("subarray_t before doing IP address lookup")
    logger.debug(subarray_t)
    subarray_t = {n: subarray_t.get(_ip_addr_subarray(n)) for n in subarray_ids}
    logger.debug("subarray_t after doing IP address lookup")
    logger.debug(subarray_t)

    plot_subarray_data_times(t_first_scan, subarray_t, scenario, str(build_dir))

    processor.StopRegisterLog()  # not always needed, but shouldn't hurt

    with open(pcap_filename, "rb") as pcap_file:
        beam_time_power_valid = read_beam_time_power_valid(pcap_file)
    plot_power_valid_beam(beam_time_power_valid, test_firmware, build_dir)

    results = ptc49_check(
        t_first_scan, subarray_t, beam_time_power_valid, test_firmware, scenario
    )

    # TODO - reporting to happen in between check & assert function calls.

    ptc49_assert(results)


Check: TypeAlias = dict[int, dict[int, bool | None]]


def ptc49_check(
    t_0,
    subarray_t_start_stop,
    beam_time_power_valid,
    personality: Personality,
    schedule,
) -> Check:
    """
    Check if requirements are met.

    :param t_0: Start time (when first scan command was sent)
    :param subarray_t_start_stop: {subarray_id: (start, stop), ... }
    :param beam_time_power_valid: from read_beam_time_power_valid()
    :param personality: Firmware being tested
    :param schedule: {subarray_id: {"start": 10, "duration": 20}, ... }
    :returns: { requirement: {subarray id: passed?, ... }, ... }
      where requirement is 4, 5, 6, from PTC steps - see comments
      'passed?' is True for pass, False for fail, None if not checked
    """
    # pylint: disable=too-many-locals
    logger = logging.getLogger("ptc49")
    result: Check = defaultdict(lambda: defaultdict(lambda: None))
    for n, times in subarray_t_start_stop.items():
        # If we didn't see any packets for this subarray (times is None),
        # expand to two values (start/stop) so we get failures recorded
        start, stop = times or (None, None)
        # 49.4 Confirm the timestamps of the beams matches the start times of the
        # subarrays
        result[4][n] = start == pytest.approx(schedule[n]["start"] + t_0, abs=5)
        logger.info(f"Subarray {n} req. 49.4 {'pass' if result[4][n] else 'fail'}")
        # 49.5 Confirm using PTP the time when data flowed
        # (we use CNIC clock without PTP, but that's close enough)
        try:
            duration = stop - start
        except TypeError:
            duration = None
        result[5][n] = duration == pytest.approx(schedule[n]["duration"], abs=5)
        logger.info(f"Subarray {n} req. 49.5 {'pass' if result[5][n] else 'fail'}")

    # 49.6 Confirm that the data produced is valid for each frequency channel
    # Beam amplitudes as expected
    # (product of beamformer gain, number of stations, channel amplitude)
    for beam, (_, power, valid) in beam_time_power_valid.items():
        # N.B. beam ID = subarray ID, very convenient
        subarray = beam
        expected_power = ptc49_expected_power(subarray, personality)
        logger.info(f"Beam {beam} expected power: {expected_power}")
        logger.debug(f"Beam {beam} mean power (all packets): {np.mean(power):.0f}")
        logger.debug(f"Beam {beam} max power (all packets): {np.max(power):.0f}")
        logger.debug(f"Beam {beam} min power (all packets): {np.min(power):.0f}")
        len_pre_filter = len(power)
        power = filter_valid_power(power, valid)
        logger.info(f"Beam {beam} mean power (w/ valid delays): {np.mean(power):.0f}")
        logger.info(f"Beam {beam} max power (w/ valid delays): {np.max(power):.0f}")
        logger.info(f"Beam {beam} min power (w/ valid delays): {np.min(power):.0f}")
        logger.info(f"Dropped {len_pre_filter - len(power)} invalid power values")
        # Large rtol here because our observed values are ~1.5x expectation
        # (probably some error in calculating the expectation)
        # TODO - hopefully we can tighten this later,
        #  but for now it's good enough as beams are separated by ~10x
        result[6][subarray] = (
            np.allclose(power, expected_power, rtol=0.6) and len(power) > 0
        )
        logger.info(
            f"Subarray {subarray} req. 49.6 "
            f"{'pass' if result[6][subarray] else 'fail'}"
        )

    return result


def filter_valid_power(power_values: Iterable[float], validity_flags: Iterable[int]):
    """
    Extract only the Power values where the delay polynomials were valid.

    :param power_values: Power values
    :param validity_flags: Validity flag values
    :returns: power values
    """

    def delay_ok(validity: int) -> bool:
        """For a given validity flags value, check if the delay polynomials are OK."""
        valid = PssValidityFlags.from_int(validity)  # PSS flags are a superset of PST
        return valid.station_delay_poly and valid.beam_delay_poly

    return [
        power for power, valid in zip(power_values, validity_flags) if delay_ok(valid)
    ]


def ptc49_assert(results: Check):
    """
    Drive pytest Pass/Fail by asserting requirements are met.

    :param results: From ptc49_check
    :raises AssertionError: If requirements are not met
    """
    fail_message = {
        4: "Wrong start time (per CNIC clock)",
        5: "Wrong duration (per CNIC clock)",
        6: "Invalid data (amplitude not as expected)",
    }
    for n_requirement, subarray_pass in results.items():
        for n, passed in subarray_pass.items():
            assert passed, f"Subarray {n}: " + fail_message[n_requirement]


def ptc49_expected_power(subarray: int, personality: Personality) -> float:
    """
    Calculate expected power for a PTC 49 subarray.

    :param subarray: ID number
    :param personality: firmware being tested
    """
    # Reference slides for calculations:
    # PST: https://docs.google.com/presentation/d/1JLm2KXVJ1lDhSL3aV5o6hPDe_jCaZRSBPAZZbfHSmm0/edit#slide=id.g313fc980766_0_24  # noqa pylint: disable=line-too-long
    # PSS: https://docs.google.com/presentation/d/1PbG6LOFc1s8S6OHuxH9SRYebQDCqnMfhLGBqpNBOxw8/edit#slide=id.g3318d613d97_0_0  # noqa pylint: disable=line-too-long
    gain = {
        Personality.PST: 16,  # firmware will scale down by 256 later
        Personality.PSS: 2 * 32 / 512,
    }

    channels = _channels_subarray(subarray)
    stations = _stations_subarray(subarray)
    #  Note: power measurement currently averages all channels together.
    #  We could return two low/high values here to create a band for checking,
    #  but our calculation is not very accurate, so we'll just use one value for now.
    cnic_scales = [_cnic_scale(i, subarray) for i in channels]
    sps_amplitude = math.sqrt(2) * max(cnic_scales) / VD_NOISE_SCALE

    amplitude = sps_amplitude * gain[personality] * len(stations)
    logger = logging.getLogger("ptc49")
    logger.debug(f"\nExpected power debug - subarray {subarray}")
    logger.debug(f"CNIC Scales: {cnic_scales}")
    logger.debug(f"amplitude\t{amplitude:.0f}\t=> Power: {amplitude ** 2:.0f}")

    return amplitude**2


def read_beam_time_power_valid(pcap: BinaryIO):
    """
    Extract Timestamps, Average Power, and Validity flags per beam from a PSR PCAP.

    :param pcap:
    :returns: { Beam: ([time], [power], [validity]), ... }
      Each list contains one element per packet.
    """
    beam_time_power_valid = defaultdict(lambda: ([], [], []))
    """Beam: ([time], [power], [validity])"""

    first = True
    for psr in map(PsrPacket.from_bytes, payloads(pcap)):
        if first:
            ptc48_packet_debug(psr)
            first = False
        # convert times µs -> s
        beam_time_power_valid[psr.beam_id][0].append(psr.ska_timestamp_us / 1e6)
        beam_time_power_valid[psr.beam_id][1].append(
            sum(psr.average_channel_power())
            / psr.n_channels
            # below divide for 2 polarisations. FIXME - move to the class method?
            / 2
        )
        # instead of power, we could use RMS amplitude...
        # beam_time_power_valid[psr.beam_id][1].append(
        #     np.sqrt(np.mean(np.abs(np.square(psr.complex_samples))))
        # )
        beam_time_power_valid[psr.beam_id][2].append(psr.validity)

    return beam_time_power_valid


def plot_power_valid_beam(
    beam_time_power_valid,
    personality: Personality,
    dirname: Path | str = ".",
    cut: slice = slice(None),
) -> None:
    """
    Plot power & validity per beam.

    :param beam_time_power_valid: from read_beam_time_power_valid()
    :param personality: firmware being tested
    :param dirname: where to write the plot
    :param cut: a slice of samples to plot (default: all)
    """
    if isinstance(dirname, str):
        dirname = Path(dirname)

    colors = dict(enumerate(mpl_colors.TABLEAU_COLORS))
    subarrays = subarrays_firmware[personality]
    # Common plot for power (values probably quite distinct)
    # Separate plots for validity (values probably all the same)
    fig, (ax_power, *ax_valid) = plt.subplots(
        len(subarrays) + 1,
        1,
        sharex=True,
        height_ratios=[len(subarrays)] + [0.75] * len(subarrays),
        layout="constrained",
    )
    # I tried using subfigures to squash the validity plots together,
    # but couldn't manage a satisfactory result...

    first = True
    for beam, (t, power, validity) in beam_time_power_valid.items():
        ax_power.plot(t[cut], power[cut], color=colors[beam], label=f"Beam {beam}")
        ax_power.axhline(
            y=ptc49_expected_power(beam, personality),
            color=colors[beam],
            ls="dotted",
            label="Expected" if first else "",
        )
        first = False
        ax_valid[beam - 1].plot(t[cut], validity[cut], color=colors[beam])
        ax_valid[beam - 1].set_yticks(sorted(set(validity)))
        ax_valid[beam - 1].set(ylabel=f"B {beam}")

    fig.suptitle("Power & Validity per Beam")
    ax_power.set_yscale("log")
    ax_power.set(ylabel="Power (log scale)")
    ax_power.legend()
    ax_valid[0].set(title="Validity")
    ax_valid[-1].set(xlabel="Time since SKA Epoch (s)")
    fig.savefig(dirname / "beams.png")


def ptc48_packet_debug(psr: PsrPacket) -> None:
    """Print debug info for a packet."""
    logger = logging.getLogger("ptc49")
    logger.debug("-- PSR Packet Debug --")
    logger.debug("n_samples %s", psr.n_samples)
    logger.debug("n_channels %s", psr.n_channels)
    logger.debug("scale1 %s", psr.scale1)
    logger.debug("scale2 %s", psr.scale2)
    logger.debug("scale3 %s", psr.scale3)
    logger.debug("scale4 %s", psr.scale4)
    logger.debug("data precision %s", psr.data_precs)
    logger.debug("First few bytes")
    bytes_str = "".join(f"0x{i:02X} " for i in psr.sample_bytes[:16])
    logger.debug(bytes_str)
    logger.debug("")
    logger.debug("Decoding bytes...")
    for i in range(8):
        x = psr.sample_bytes[i * 2 : i * 2 + 2]
        y = int.from_bytes(x, byteorder="little", signed=True)
        x = int.from_bytes(x, byteorder="little", signed=False)
        logger.debug(f"0x{x:04X}\t{y:6d} / scale = {y / psr.scale1:.2f}")
    logger.debug("")
    logger.debug("First few samples (ch 0, pol 0, t 0:8) scaled:")
    logger.debug(psr.complex_samples[0, 0, 0:8])
    logger.debug("")
    logger.debug("Abs. Val of those same samples:")
    logger.debug(np.abs(psr.complex_samples[0, 0, 0:8]))
    logger.debug("")
    logger.debug("Measurements using all samples:")
    rms_sample = np.sqrt(np.mean(np.abs(np.square(psr.complex_samples))))
    logger.debug(f"RMS amplitude {rms_sample:.1f}")
    sum_sq = np.sum(np.abs(np.square(psr.complex_samples)))
    logger.debug(f"Sum of Squares {sum_sq:.0f}")
    sum_sq_per = sum_sq / psr.n_channels / psr.n_samples / 2
    logger.debug(f"Sum of Squares / n_channels / n_samples / 2 = {sum_sq_per:.0f}")
    logger.debug(f"Sqrt of that ^^^ : {np.sqrt(sum_sq_per):.1f}")
    logger.debug("- End PSR Packet Debug -")


def log_all_lfaa_counters(logger: logging.Logger, proc, personality, timestamp):
    """
    Log all LFAA counters.

    :param logger: logging.Logger
    :param proc: Processor device proxy
    :param personality: firmware being tested
    :param timestamp: Subarray control loop time value
    """
    logger.debug(
        "Processor.stats_io [note: values update only every 30s] %s", proc.stats_io
    )
    if personality == Personality.PST:
        suffixes = ("", "_2", "_3")
    elif personality == Personality.PSS:
        suffixes = ("", "_2")
    else:
        raise NotImplementedError(f"Dunno how to log LFAA regs for {personality}")

    registers = {}
    registers.update(
        {
            "system": (
                "eth100g_rx_total_packets",
                "eth100g_rx_bad_fcs",
                "eth100g_rx_bad_code",
            )
        }
    )
    registers.update(
        {
            "packetiser" + s: ("control_vector", "stats_packets_rx_sig_proc_valid")
            for s in suffixes
        }
    )
    registers.update(
        {
            "lfaadecode100g"
            + s: (
                "spead_packet_count",
                "spead_v1_packet_found",
                "spead_v2_packet_found",
                "spead_v3_packet_found",
                "lfaa_decode_reset",
                "total_stations_table0",
                "total_coarse_table0",
                "total_channels_table0",
                "total_stations_table1",
                "total_coarse_table1",
                "total_channels_table1",
                "no_virtual_channel_count",
                "hbm_reset",
                "hbm_reset_status",
                "lfaa_tx_fsm_debug",
                "lfaa_rx_fsm_debug",
                "lfaa_lookup_fsm_debug",
                "uram_buffer_level",
            )
            for s in suffixes
        }
    )

    if personality == Personality.PST:
        registers.update({"pst_ct1" + s: ("frame_count_low",) for s in suffixes})
        registers.update({"ct2" + s: ("readoutclocks",) for s in suffixes})

    for peripheral in registers:  # pylint: disable=consider-using-dict-items
        for register in registers[peripheral]:
            try:
                logger.debug(
                    f"t={timestamp} {peripheral}.{register} "
                    f"{debug_reg_read(proc, peripheral, register.lower())}"
                )
            except tango.DevFailed:
                logger.error(f"Couldn't read {peripheral}.{register} for debug log")

    for suffix in suffixes:
        peripheral = "lfaadecode100g" + suffix
        register = "data"  # VCStats
        for offset in (8192, 8192 + 2048):
            logger.debug(
                f"t={timestamp} {peripheral}.{register}[{offset}] "
                + str(
                    debug_reg_read(
                        proc, peripheral, register.lower(), offset=offset, length=8
                    )
                )
            )
