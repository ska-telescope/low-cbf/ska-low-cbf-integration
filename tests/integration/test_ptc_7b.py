# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 7 - Correlator Timing Accuracy.
Steps PTC#7.7 onwards - timing accuracy.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #7 Presentation <https://docs.google.com/presentation/d/1zTJ3jEDTSiE7D9Gvx71l8_c2ErBP5ymtmLQzGsPNe-4>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from math import ceil
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.correlator import samples_in_integration_period
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor
from .test_ptc_7 import station_ids

TEST_TIMEOUT_SEC = 6000


@pytest.mark.large_capture
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
@pytest.mark.parametrize("integration_ms", [849])
@pytest.mark.long_test
def test_ptc7_b_corr_timing_accuracy(
    cnic_and_processor, ready_subarray, integration_ms
):
    """Implement PTC 7, part B (step 7.7 onward)."""
    # pylint: disable=too-many-locals, too-many-statements
    print(datetime.now(), "Starting PTC#7B")
    n_stations = 6
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 1
    first_channel = ceil(230 - n_channels / 2)
    channels = list(range(first_channel, first_channel + n_channels))
    # Generate CNIC-VD Configuration
    cnic_config = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config = {
        "id": 1235,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": channels,
                        "delay_poly": "some_urls",
                    },
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "22.22.22.22"]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": integration_ms,
                    },
                ],
            },
        },
    }

    samples_in_integration = samples_in_integration_period(integration_ms)
    # steps of 250k SPS samples at the edge of the search
    # reduced to 100 SPS samples in the center
    start_samples_offset = (
        list(range(-751000, -1000, 250000))
        + list(range(-1000, 1100, 100))
        + list(range(251000, 1001000, 250000))
    )
    # we substract 2048 to compensate what the filterbank is doing
    start_samples = [
        round(x + 3.0 * samples_in_integration - 2048) for x in start_samples_offset
    ]
    pulse_on = samples_in_integration
    pulse_off = round(samples_in_integration * 5.05)
    capture_integrations = 7
    results_from_multiple_run = {}

    # looping on the start time of the burst hence going through all
    # scan states from the SKA scan state machine.
    for pulse_start in start_samples:
        print(f"SAMPLES: Start {pulse_start}, On {pulse_on}, Off {pulse_off}")
        print(
            f"INTEGRATIONS: Start {pulse_start / samples_in_integration}"
            f", On {pulse_on / samples_in_integration}"
            f", Off {pulse_off / samples_in_integration}"
        )
        test_config = ProcessorVdTestConfig(
            cnic_capture_min_size=200,
            cnic_capture_packets=144 * n_channels * (capture_integrations + 1),
            scan_config=scan_config,
            firmware_type=Personality.CORR,
            input_packet_count=None,
            start_scan_packet_count=None,
            before_end_scan_packet_count=None,
            output_packet_count=None,
            vd_config=cnic_config,
            during_scan_callback=_ptc_7b_start_traffic,
            pulsar_config=(pulse_start, pulse_on, pulse_off),
        )
        correlator_output = cnic_processor(
            cnic_and_processor, ready_subarray, test_config
        )

        print(
            datetime.now(),
            "Captured PTC#7B output, beginning analysis",
        )
        visibilities = VisibilityAnalyser(
            json.dumps(
                {
                    "coarse_channels": channels,
                    "stations": station_ids[:n_stations],
                }
            )
        )
        print(datetime.now(), "VisibilityAnalyser created")
        visibilities.extract_spead_data(correlator_output.name)
        print(datetime.now(), "SPEAD data extracted")

        list_of_correlation = []
        list_of_correlation_name = []
        averaged_output = []
        for i in range(visibilities.nb_station):
            for j in range(0, i + 1):
                list_of_correlation_name.append(f"{i + 1}x{j + 1}")
                list_of_correlation.append([])

        n_baselines = (visibilities.nb_station * (visibilities.nb_station + 1)) // 2
        # FIXME - the below conversion/sort is working around VisibilityAnalyser bug
        #  -> the wrong type is used for time_sequence, it should be a list, not a set
        time_sequence = sorted(list(visibilities.time_sequence))[
            0 : (capture_integrations - 1)
        ]
        for time in map(int, time_sequence):
            absolute_value_time = 0
            for i in range(n_baselines):
                absolute_value = 0
                for coarse_channel in channels:
                    for vis_channel in range(144):
                        n_vis = visibilities.vis_number.index(
                            coarse_channel * 144 + vis_channel
                        )
                        abs_vis = np.abs(
                            visibilities.visibilities[n_vis][time]["VIS"][i][0]  # 0=XX
                        )
                        absolute_value += abs_vis
                        absolute_value_time += abs_vis
                list_of_correlation[i].append(absolute_value / (len(channels) * 144))
            averaged_output.append(
                absolute_value_time / (len(channels) * 144 * n_baselines)
            )

        results_from_multiple_run[pulse_start] = (averaged_output, time_sequence)
        print("Sums:")
        print(averaged_output)
    print(results_from_multiple_run)
    correlation_values = []
    samples_index = []
    correlation_values_int_2 = []
    correlation_values_int_4 = []
    timestamps = []
    general_result = ""
    for samples_start, corr_time in results_from_multiple_run.items():
        correlation_values.append(corr_time[0][2])
        if (
            3.0 * samples_in_integration - 3048
            <= samples_start
            <= 3.0 * samples_in_integration - 1048
        ):
            correlation_values_int_2.append(corr_time[0][1])
            correlation_values_int_4.append(corr_time[0][3])
        samples_index.append(samples_start)
        timestamps = corr_time[1]
        # Making sure the timestamps are what we expect for all scans
        assert corr_time[1] == [
            1696480200,
            2545826760,
            3395173320,
            4244519880,
            5093866440,
            5943213000,
        ]
        general_result = (
            general_result
            + f"{samples_start}, {corr_time[0][0]}, {corr_time[0][1]}"
            + f", {corr_time[0][2]}, {corr_time[0][3]}, {corr_time[0][4]} \n"
        )
        print(
            f"{samples_start}, {corr_time[0][0]}, {corr_time[0][1]}"
            + f", {corr_time[0][2]}, {corr_time[0][3]}, {corr_time[0][4]}"
        )
    # First assertion, we check that the max is where it should be
    assert 4 <= correlation_values.index(max(correlation_values)) <= 22
    poly_up = np.polyfit(samples_index[3:24], correlation_values_int_4, deg=2)
    poly_down = np.polyfit(samples_index[3:24], correlation_values_int_2, deg=2)
    poly_middle = np.polyfit(samples_index[3:24], correlation_values[3:24], deg=2)
    intersection = np.roots(poly_up - poly_down)
    # the two curves should cross for a start time of burst equal to
    # 4 * samples in integration + 12.5 % of sample in integration to center
    # the burst in an integration period (assuming a burst of 75% sample in
    # integration
    expected_center = 3.0 * samples_in_integration
    # The expected difference is because it takes 4096 SPS samples to produce
    # 1 sample at the filterbank output
    # Half of the SPS samples will correspond to a time before the filterbank
    # output sample (and half after), thence half is 2048 samples
    expected_difference = 2048
    # Finally we need the time (sample) difference must be within 1/256th
    # of the correlator integration time (sample in integration)
    allowed_margin = samples_in_integration / 256
    assert abs(expected_center - intersection[0]) == pytest.approx(
        expected_difference, abs=allowed_margin
    ) or abs(expected_center - intersection[1]) == pytest.approx(
        expected_difference, abs=allowed_margin
    )
    analytics = [poly_up, poly_down, intersection, expected_center, poly_middle]
    _ptc7b_report(
        scan_config,
        timestamps,
        samples_index,
        correlation_values,
        correlation_values_int_2,
        correlation_values_int_4,
        general_result,
        analytics,
    )
    print(datetime.now(), "End of PTC#7B")


def _ptc_7b_start_traffic(cnic, processor):
    """Reset Correlator & Start CNIC traffic."""
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
    sleep(0.2)
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
    print(f"Pulsar configuration: start {cnic.vd__pulsar_start_sample_count}")
    print(f"Pulsar configuration: on {cnic.vd__pulsar_on_sample_count}")
    print(f"Pulsar configuration: stop {cnic.vd__pulsar_off_sample_count}")
    print(f"Pulsar configuration: enabling {cnic.vd__enable_pulsar_timing}")
    sleep(0.2)
    cnic.enable_vd = True


def _ptc7b_report(
    scan_config,
    timestamps,
    samples_index,
    correlation_values,
    correlation_values_int_2,
    correlation_values_int_4,
    general_result,
    analytics,
):
    """Generate Test Report for PTC7."""
    # pylint: disable=too-many-arguments, too-many-locals
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "65c06edfec2e5eee0c950693"
    )
    if not print_figures_for_ptc7b_report(
        samples_index,
        correlation_values,
        correlation_values_int_2,
        correlation_values_int_4,
        analytics,
        local_directory,
        new_repo_name,
    ):
        print("error generating figures ")

    second_poly = f"{analytics[0][0]}x^2+{analytics[0][1]}x+{analytics[0][2]}"
    fourth_poly = f"{analytics[1][0]}x^2+{analytics[1][1]}x+{analytics[1][2]}"
    resolution = f"{analytics[2][0]}, {analytics[2][1]}"
    expected = f"{analytics[3]-2048}"
    real_solution = [
        x for x in analytics[2] if ((analytics[3] - 3048) < x < (analytics[3] - 1048))
    ]
    x_max_second = -1 * analytics[4][1] / (2 * analytics[4][0])
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RESULTS_PTC7B",
        general_result,
    )
    replace_word_in_report(
        file_result_to_replace,
        "TIMESTAMPS_UPDATE",
        str(timestamps).replace("[", "").replace("]", "").replace(",", " &"),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SECONDPOLY",
        second_poly,
    )
    replace_word_in_report(
        file_result_to_replace,
        "FOURTHPOLY",
        fourth_poly,
    )
    replace_word_in_report(
        file_result_to_replace,
        "SOLUTIONPOLY",
        resolution,
    )
    replace_word_in_report(
        file_result_to_replace,
        "EXPECTEDCENTRE",
        expected,
    )
    replace_word_in_report(
        file_result_to_replace,
        "REALXRESULT",
        str(real_solution[0]),
    )
    replace_word_in_report(
        file_result_to_replace,
        "ABSDIFFERENCE",
        str(np.abs(real_solution[0] - (analytics[3] - 2048))),
    )
    replace_word_in_report(
        file_result_to_replace,
        "XMAXRESULT",
        str(x_max_second),
    )
    replace_word_in_report(
        file_result_to_replace,
        "ERRORSECOND",
        str(x_max_second - (analytics[3] - 2048)),
    )
    replace_word_in_report(
        file_result_to_replace,
        "AVGERROR",
        str((real_solution[0] + x_max_second - 2 * (analytics[3] - 2048)) / 2),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/third_integration.pdf")
    repo.git.add(new_repo_name + "/third_integration_zoom.pdf")
    repo.git.add(new_repo_name + "/other_integration.pdf")
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def print_figures_for_ptc7b_report(
    samples_index,
    correlation_values,
    correlation_values_int_2,
    correlation_values_int_4,
    analytics,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    # pylint: disable=too-many-arguments
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False
    x_points = np.linspace(samples_index[3], samples_index[23], 1000)
    plt.clf()
    plt.plot(samples_index, correlation_values, "b-.", label="Third Integration")
    plt.legend(loc=2, prop={"size": 6})
    plt.ylabel("Average Correlation")
    plt.xlabel("Burst Starting Samples (in SPS samples)")
    plt.savefig(
        local_directory + new_repo_name + "/third_integration.pdf", format="pdf"
    )

    plt.clf()
    plt.plot(
        samples_index[3:24], correlation_values[3:24], "b-.", label="Third Integration"
    )
    plt.plot(
        x_points,
        analytics[4][0] * x_points**2 + analytics[4][1] * x_points + analytics[4][2],
        "r--",
        label="Polynomial fitting (3rd)",
    )
    plt.legend(prop={"size": 10})
    plt.ylabel("Average Correlation")
    plt.xlabel("Burst Starting Samples (in SPS samples)")
    plt.savefig(
        local_directory + new_repo_name + "/third_integration_zoom.pdf", format="pdf"
    )

    plt.clf()
    plt.plot(
        samples_index[3:24], correlation_values_int_2, "r-.", label="Second Integration"
    )
    plt.plot(
        x_points,
        analytics[1][0] * x_points**2 + analytics[1][1] * x_points + analytics[1][2],
        "b--",
        label="Polynomial fitting (2nd)",
    )
    plt.plot(
        samples_index[3:24], correlation_values_int_4, "g-.", label="Fourth Integration"
    )
    plt.plot(
        x_points,
        analytics[0][0] * x_points**2 + analytics[0][1] * x_points + analytics[0][2],
        "y--",
        label="Polynomial fitting (4th)",
    )
    plt.legend(prop={"size": 10})
    plt.ylabel("Average Correlation")
    plt.xlabel("Burst Starting Samples (in SPS samples)")
    plt.savefig(
        local_directory + new_repo_name + "/other_integration.pdf", format="pdf"
    )

    return True
