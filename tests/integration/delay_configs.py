"""Correlator configurations for integration testing."""

source_constant_delay = {
    "subarray_id": 1,
    "beam_id": 1,
    "delay": [
        [
            {"stn": 1, "nsec": 0.0},
            {"stn": 2, "nsec": 0.0},
            {"stn": 3, "nsec": 0.0},
            {"stn": 4, "nsec": 0.0},
            {"stn": 5, "nsec": 0.0},
            {"stn": 6, "nsec": 0.0},
        ],
        [
            {"stn": 1, "nsec": 0.0},
            {"stn": 2, "nsec": 0.0},
            {"stn": 3, "nsec": 0.0},
            {"stn": 4, "nsec": 0.0},
            {"stn": 5, "nsec": 0.0},
            {"stn": 6, "nsec": 0.0},
        ],
        [
            {"stn": 1, "nsec": 0.0},
            {"stn": 2, "nsec": 0.0},
            {"stn": 3, "nsec": 0.0},
            {"stn": 4, "nsec": 0.0},
            {"stn": 5, "nsec": 0.0},
            {"stn": 6, "nsec": 0.0},
        ],
        [
            {"stn": 1, "nsec": 0.0},
            {"stn": 2, "nsec": 0.0},
            {"stn": 3, "nsec": 0.0},
            {"stn": 4, "nsec": 0.0},
            {"stn": 5, "nsec": 0.0},
            {"stn": 6, "nsec": 0.0},
        ],
    ],
}
constant_beam_delay = {
    "subarray_id": 1,
    "beam_id": 1,
    "delay": [
        {"stn": 1, "nsec": 0.0},
        {"stn": 2, "nsec": 0.0},
        {"stn": 3, "nsec": 0.0},
        {"stn": 4, "nsec": 0.0},
        {"stn": 5, "nsec": 0.0},
        {"stn": 6, "nsec": 0.0},
    ],
}
