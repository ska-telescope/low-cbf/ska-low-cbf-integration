# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 23 - Correlator Endurance Test

Based on Jupyter notebook: PI23/FAT1/PTC23/ptc23.ipynb

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #23 <https://docs.google.com/presentation/d/1OTnAaR_fR1FdHuZYjt-Y4Gfw561r_ZnH4BAgdpr9tDg/edit#slide=id.g274ec4c9400_0_222>` _

1.  Setup the CNIC to generate data using 6-stations over 20km (405, 393, 315, 261, 255, 243) for a
    single source using 1 SPS channel with correlated noise amplitude 4000.
2.  Use astropy to calculate RA needed for source/station beam directions to get it on the horizon
    at the current time. Configure the LOW CBF delay polynomial simulator using this (done offline)
3.  Configure the correlator for a single subarray, with delay tracking
4.  Transmit the SPS data using the CNIC and process in the correlator
5.  Capture the resulting visibility data with SDP CNIC for 12 hours
6.  Analyse the log file to determine when polynomials were updated and check for any alarms
7.  Download the visibility data and start the analysis by plotting the phases of all cross
    correlations for each baseline across all 144 visibility channels
8.  Check the visibility flag that indicates that the delay polynomial was valid for all integrations
    and plot this
9.  Plot the power of each visibility product for all channels and for all integrations.
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import shutil
from collections import defaultdict
from copy import deepcopy
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.correlator import integration_periods
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.tango import DeviceProxyJson
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_ch448_delay_tracking
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import (
    DELAY_EMULATOR_ADDR,
    ProcessorVdStaticTrackingTestConfig,
    cnic_processor,
)

MAX_STD_DEVIATION = 0.000_06  # see comment above
COLOURS = "rgbcmykrgb"

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 123  # needs to match scan id in scan_6stn_1sa_1bm_1ch
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
BEAM_ID = 1  # needs to match beam id in scan_6stn_1sa_1bm_1ch

# ---------------------------
# PCAP/debug file names
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc23/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH = DATA_DIR + "PTC23_correlator.pcap"
"""PCAP file name"""

source_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [
        {"ra": "6h59m00.0s", "dec": "-29d00m00.0s"},
        {"ra": "6h59m00.0s", "dec": "-29d00m00.0s"},
        {"ra": "6h59m00.0s", "dec": "-29d00m00.0s"},
        {"ra": "6h59m00.0s", "dec": "-29d00m00.0s"},
    ],
}
beam_delay_config = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": {"ra": "6h59m00.0s", "dec": "-29d00m00.0s"},
}


def _cnic_vd_cfg(stations: list, channels: list) -> dict:
    """Return CNIC-VD configuration for given stations, channels and frequency.

    :param tone_position: tone position
    :param stations: a list of (station, substation) tuples
    :param channels: a list of channels
    :return: a list of dictionaries for CNIC-VD configuration
    """
    cfg = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": chan,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    {
                        "tone": False,
                        "seed": 1981,
                        "scale": 4000,
                    },
                ],
                "y": [
                    {
                        "tone": False,
                        "seed": 1981,
                        "scale": 4000,
                    },
                ],
            },
        }
        for station, substation in stations
        for chan in channels
    ]
    return {"sps_packet_version": 3, "stream_configs": cfg}


CHAN = 448
VIS_CHAN_COUNT = 144
CAPTURE_TIME_SEC = 60 * 60 * 12

BASELINES = [1, 3, 4, 6, 7, 8, 10, 11, 12, 13, 15, 16, 17, 18, 19]


@pytest.mark.timeout(CAPTURE_TIME_SEC + 15000)
@pytest.mark.hardware_present
@pytest.mark.large_capture
@pytest.mark.long_test
def test_ptc23_corr_endurance(
    cnic_and_processor, ready_subarray
):  # pylint: disable=too-many-locals
    """Implement PTC 23: simulate station data, capture packages, run analysis."""
    # basic settings
    n_channels = 1
    station_ids = [243, 255, 261, 315, 393, 405]
    stations = [[station, 1] for station in station_ids]
    channels = list(range(CHAN, CHAN + n_channels))
    # tweak the stock standard scan configuration (see corr_configs.py)
    scan_config = deepcopy(scan_6stn_1sa_1bm_ch448_delay_tracking)
    scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels

    # output packet related constants
    integration_time_sec = integration_periods[849]
    channel_count = len(channels)
    rx_size = 64
    n_vis = int(CAPTURE_TIME_SEC / integration_time_sec)
    rx_packets = VIS_CHAN_COUNT * (n_vis + 1) * channel_count
    delay = DeviceProxyJson(DELAY_EMULATOR_ADDR)
    delay.update_seconds = 180

    # Generate / capture packets
    # --------------------------
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=rx_size,
        cnic_capture_packets=rx_packets,
        scan_config=scan_config,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=_cnic_vd_cfg(stations, channels),
        during_scan_callback=_ptc_23_waiting_longer,
        source_delay_config=[source_delay_config],
        beam_delay_config=[beam_delay_config],
    )
    f_handle = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH,
    )
    f_handle.close()  # Anayser will consume the file's contents

    # Analysis
    # --------
    analyser_cfg = {"coarse_channels": channels, "stations": station_ids}
    visibilities = VisibilityAnalyser(json.dumps(analyser_cfg))
    visibilities.extract_spead_data(PCAP_PATH)
    # 23.6 we are supposed to analyse the log file from the processor but do not know
    # how to get those

    # 23.7 plotting the phase per channel for all baseline and for all timestamps
    (
        freq_time_mixed_angle,
        freq_time_mixed_amplitude,
    ) = freq_time_mixed_angle_and_amplitude(visibilities)
    # 23.8 plotting visibility flags
    data_flag = _plotting_data_flag(visibilities)

    passing_angle, passing_amplitude, passing_flag = ptc23_report(
        test_config,
        visibilities,
        freq_time_mixed_angle,
        freq_time_mixed_amplitude,
        data_flag,
    )
    assert passing_angle
    assert passing_amplitude
    assert passing_flag


def freq_time_mixed_angle_and_amplitude(visibilities):
    """Plot phase across all visibilities."""
    freq_time_mixed_angle = defaultdict(lambda: defaultdict(lambda: []))
    freq_time_mixed_amplitude = defaultdict(lambda: defaultdict(lambda: []))
    for t_index, times in enumerate(visibilities.time_sequence):
        for index_base, baseline in enumerate(BASELINES):
            for vis_channel in range(144):
                freq_time_mixed_angle[index_base][t_index].append(
                    np.angle(
                        visibilities.visibilities[vis_channel][times]["VIS"][baseline][
                            0
                        ],
                        deg=True,
                    )
                )
                freq_time_mixed_amplitude[index_base][vis_channel].append(
                    np.abs(
                        visibilities.visibilities[vis_channel][times]["VIS"][baseline][
                            0
                        ],
                    )
                )
    for index_base, baseline in enumerate(BASELINES):
        plt.clf()
        for t_index, times in enumerate(visibilities.time_sequence):
            #
            # if np.max(freq_time_mixed_angle[t_index]) > 2:
            plt.plot(
                range(144),
                freq_time_mixed_angle[index_base][t_index],
                COLOURS[t_index % 10] + "-",
                label=f"timestamp {times}",
            )
        plt.xlabel("visibility channels")
        plt.ylabel("phase in degrees")
        # plt.legend()
        plt.grid()
        plt.savefig(
            f"/app/build/phase_analysis_baseline_{baseline:02d}.png", format="png"
        )

    for index_base, baseline in enumerate(BASELINES):
        plt.clf()
        for vis_channel in range(144):
            #
            # if np.max(freq_time_mixed_angle[t_index]) > 2:
            plt.plot(
                visibilities.time_sequence,
                freq_time_mixed_amplitude[index_base][vis_channel],
                COLOURS[vis_channel % 10] + "-",
                label=f"timestamp {times}",
            )
        plt.xlabel("Time since epoch in ns")
        plt.ylabel("Amplitude")
        # plt.legend()
        plt.grid()
        plt.savefig(f"/app/build/amplitude_baseline_{baseline:02d}.png", format="png")

    return freq_time_mixed_angle, freq_time_mixed_amplitude


def _plotting_data_flag(visibilities):
    """Plot vis flag across all visibilities."""
    data_flag = []
    for t_index, times in enumerate(visibilities.time_sequence):
        data_flag.append([])
        for vis_channel in range(144):
            match = int(visibilities.visibilities_flag[vis_channel][times])
            frequency = match.to_bytes(4, byteorder="little")
            data_flag[t_index].append(int.from_bytes(frequency, byteorder="big"))
    plt.clf()
    for t_index, times in enumerate(visibilities.time_sequence):
        plt.plot(
            range(144),
            data_flag[t_index],
            COLOURS[t_index % 10] + "-",
            label=f"timestamp {times}",
        )
    plt.xlabel("visibility channels")
    plt.ylabel("Visibility flag")
    # plt.legend()
    plt.grid()
    plt.savefig("/app/build/visibility_flag.png", format="png")
    return data_flag


def _ptc_23_waiting_longer(cnic, processor):
    """Wait for the cnic to capture the data."""
    packets_received = cnic.hbm_pktcontroller__rx_packet_count
    n_same = 0
    while not cnic.finished_receive:
        print(f"Packets Received: {packets_received}")
        print(f"Stat delay {processor.stats_delay}")
        sleep(30)
        n_same += 1
        if packets_received == cnic.hbm_pktcontroller__rx_packet_count:
            n_same += 1
        else:
            n_same = 0
        if n_same == 4:
            break
        packets_received = cnic.hbm_pktcontroller__rx_packet_count


def ptc23_report(
    test_config,
    visibilities,
    freq_time_mixed_angle,
    freq_time_mixed_amplitude,
    data_flag,
):  # pylint: disable=too-many-locals
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6705bade760e6bb9b213c169"
    )
    # overall_std_dev = np.std(average) / np.mean(average)
    passing_angle = True
    passing_amplitude = True
    passing_flag = True
    for index_base, baseline in enumerate(BASELINES):
        for t_index, _ in enumerate(visibilities.time_sequence):
            if passing_angle:
                angle_analysis_max = np.max(freq_time_mixed_angle[index_base][t_index])
                angle_analysis_min = np.min(freq_time_mixed_angle[index_base][t_index])
                passing_angle = angle_analysis_max < 1 and angle_analysis_min > -1
        shutil.copy(
            f"/app/build/phase_analysis_baseline_{baseline:02d}.png",
            local_directory
            + new_repo_name
            + f"/phase_analysis_baseline_{baseline:02d}.png",
        )
        repo.git.add(new_repo_name + f"/phase_analysis_baseline_{baseline:02d}.png")

    for index_base, baseline in enumerate(BASELINES):
        for vis_channel in range(144):
            if passing_amplitude:
                amplitude_analysis = np.std(
                    freq_time_mixed_amplitude[index_base][vis_channel]
                ) / np.mean(freq_time_mixed_amplitude[index_base][vis_channel])
                passing_amplitude = amplitude_analysis < 0.1
        shutil.copy(
            f"/app/build/amplitude_baseline_{baseline:02d}.png",
            local_directory + new_repo_name + f"/amplitude_baseline_{baseline:02d}.png",
        )
        repo.git.add(new_repo_name + f"/amplitude_baseline_{baseline:02d}.png")

    for t_index, _ in enumerate(visibilities.time_sequence):
        if passing_flag:
            flag_analysis = np.std(data_flag[t_index])
            passing_flag = flag_analysis == 0.0
        shutil.copy(
            "/app/build/visibility_flag.pdf",
            local_directory + new_repo_name + "/visibility_flag.png",
        )
        repo.git.add(new_repo_name + "/visibility_flag.png")

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SUCCESS_TEST_PHASE",
        str(passing_angle),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SUCCESS_TEST_AMPLITUDE",
        str(passing_amplitude),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SUCCESS_TEST_FLAG",
        str(passing_flag),
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.index.commit("Test with python")
    repo.git.push()
    return passing_angle, passing_amplitude, passing_flag
