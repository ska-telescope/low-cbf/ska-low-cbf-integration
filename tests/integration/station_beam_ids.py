# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""
Coordinate Station Beam IDs used by tests.

Reduce the risk of routing rule collisions between concurrent tests by establishing
conventions to be followed when implementing integration tests (aka PTCs):

* Single Processor, Basic Routing (i.e. most tests): may use station beam 1
* Multiple Processors, or Multicast Routing: must use a unique station beam ID
* Tests that specifically require multiple Station Beam IDs:
  - may use 1 for the first beam (if only single processor & basic routing in use)
  - use unique station beam IDs for subsequent beams
"""

from enum import IntEnum, unique


@unique
class BeamId(IntEnum):
    """Station Beam IDs (aka SPS SPEAD BeamID) for PTCs."""

    SINGLE_BASIC = 1
    """Station Beam ID for tests using Single Processor & Basic Routing."""

    # Unique Beam IDs for tests that need them
    PTC3_A = 3
    PTC3_B = 8
    PTC3_C = 13
    PTC6_A = 6  # PTC 6 originally used 8, but that's not unique
    PTC6_B = 15  # was 9, but 26 seems more demanding...
    PTC6_C = 10
    PTC6_D = 11
    PTC7 = 7
    PTC26_A = 5  # PTC 26's original values
    PTC26_B = 9
    PTC26_C = 14
    PTC30 = 30
    PTC31 = 31
    PTC32 = 32
    PTC39 = 39
    PTC41 = 41
    PTC49_1 = 49
    PTC49_2 = 50
    PTC49_3 = 51
