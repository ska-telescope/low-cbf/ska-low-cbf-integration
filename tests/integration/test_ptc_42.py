# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 42 - PSS Beam packets flagged without valid poly.
(PTC 33 is the PST equivalent)

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os.path
import shutil
from typing import BinaryIO, Iterable, List, Optional

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import payloads
from ska_low_cbf_integration.pulsar_protocol import (
    PSS_CHANS_PER_PACKET,
    PsrMetadata,
    PssValidityFlags,
)

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor
from .test_ptc_33 import (
    PST_SVR_IP,
    STATION_BEAM_ID,
    SUBARRAY_ID,
    TEST_TIMEOUT_SEC,
    change_delay_poly_time,
    channels,
    delay_sub,
    prepare_delay,
    pst_beam_id,
    pst_sub,
    station_weights,
    stations,
)

DATA_DIR = "/test-data/PTC/ptc42/"
"""Directory name where data file will be stored (PCAP)"""
PCAP_FILE_NAME = os.path.join(DATA_DIR, "ptc42.pcap")

BUILD_DIR = "build/ptc42"
"""Directory to write plots etc."""

stations = stations[:4]  # must be a multiple of 4 stations for PSS
# 42.1 Setup the CNIC to generate data for a single source for 5-stations (1-5)
# containing 8 channels (441-448) with noise of amplitude 4000.
cnic_config = {
    "sps_packet_version": 3,
    "stream_configs": [
        {
            "scan": 0,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": STATION_BEAM_ID,
            "sources": {
                "x": [
                    {"tone": False, "seed": 2000 + channel, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 2000 + channel, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ],
}
# 42.3 Setup a subarray to form one PSS beam using all stations
scan_config = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": STATION_BEAM_ID,
                    "freq_ids": channels,
                    "delay_poly": delay_sub,
                }
            ],
        },
        "search_beams": {
            "beams": [
                {
                    "pss_beam_id": pst_beam,
                    "stn_beam_id": STATION_BEAM_ID,
                    "delay_poly": pst_sub + str(beam_index + 1),
                    "jones": "tbd",
                    "destinations": [
                        {
                            "data_host": PST_SVR_IP,
                            "data_port": 11001,
                            "start_channel": 0,
                            "num_channels": 54,
                        }
                    ],
                    "stn_weights": station_weights[pst_beam],
                }
                for beam_index, pst_beam in enumerate(pst_beam_id)
            ],
        },
    },
}

# I'm not sure if this is guaranteed to be the same as PSS_CHANS_PER_PACKET or not
N_PSS_CHANNELS = 54
"""Number of PSS channels we expect to receive."""


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc42_pss_validity_flags(cnic_and_processor, ready_subarray):
    """Test that data is flagged correctly when delay polynomials are valid or not."""
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)
    # 42.2 Set the LOW CBF delay polynomial simulator to point the station beam at
    # RA: 2h30m DEC:-26d49m50.0s, and the PSS beam at RA: 2h30m DEC: -25d49m50.0s, which
    # results in PSS beams 1 degrees offset from the station beam.
    # Ensure the delay polynomial cadence is 10seconds.
    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    # 42.4 Start the scan
    # 42.5 Capture 120 seconds of data using the CNIC-Rx, and 20 seconds into the scan
    # pause/stop the delay polynomial simulator
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # need to run for > 2 mins to see the polynomial expire
        cnic_capture_packets=int(48 * 100 * 500 * len(channels) / 5.3),  # ~ 500 s
        scan_config=scan_config,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=change_delay_poly_time,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
    )
    beam_indices, validity = beam_validity(
        cnic_processor(
            cnic_and_processor,
            ready_subarray,
            test_config,
            PCAP_FILE_NAME,
        )
    )
    # 42.6 Input and analyse the captured data -
    # [Not implemented: plot the power in the beam for each channel over 120 seconds.]
    # Extract the metadata and plot the poly validity flags.
    plot_beam_validity(beam_indices, validity, os.path.join(BUILD_DIR, "validity.png"))
    valid_flags_work, last_value = check_validity(
        validity, os.path.join(BUILD_DIR, "flags.txt")
    )
    _ptc42_report(test_config.scan_config, last_value)
    assert valid_flags_work, "Validity Flags did not work as expected"
    # Not implemented:
    # 42.7 Check the logs to see that an alert was raised after approximately 20 seconds
    # into the scan. If watching the scan live observe that an alert was raised on the
    # alarms dashboard.


def check_validity(validity, logfile: Optional[str] = None) -> (bool, int):
    """
    Check that Validity flags worked as expected.

    :param validity:
    :param logfile: Text file to record some details in
    """
    first = PssValidityFlags.from_int(validity[0][0][0])
    last = PssValidityFlags.from_int(validity[0][0][-1])
    first_ok = first.station_delay_poly and first.beam_delay_poly
    last_ok = not last.station_delay_poly and not last.beam_delay_poly
    valid_flags_work = first_ok and last_ok
    if logfile is not None:
        with open(logfile, "a", encoding="utf-8") as log:
            log.write("First Packet:\n")
            log.write(str(first) + "\n")
            log.write(f"First packet OK? {first_ok}\n")
            log.write("******\n")
            log.write("Last Packet:\n")
            log.write(str(last) + "\n")
            log.write(f"Last packet OK? {last_ok}\n")
            log.write("******\n")
            log.write(f"Valid flags worked? {valid_flags_work}\n")
    return valid_flags_work, last


# consider moving to pulsar_protocol?
def beam_validity(data: BinaryIO) -> (List[int], np.ndarray):
    """
    Extract validity data per beam.

    :returns: Beam IDs (in order matching the validity flags array),
      Validity Flags array, indexed by [beam, channel group, time]
    """
    max_t_periods = 2_000_000  # maximum number of packets per beam / set of channels
    validity = np.zeros(
        (len(pst_beam_id), N_PSS_CHANNELS // PSS_CHANS_PER_PACKET, max_t_periods),
        dtype=np.uint8,
    )
    beam_ids = []
    n = 0
    for n, payload in enumerate(payloads(data)):
        if n >= max_t_periods:
            # raise exception?
            break
        psr = PsrMetadata.from_bytes(payload)
        t = psr.sequence_number  # should start from zero
        if psr.beam_id not in beam_ids:
            beam_ids.append(psr.beam_id)
        beam = beam_ids.index(psr.beam_id)
        validity[beam, psr.first_channel // PSS_CHANS_PER_PACKET, t] = psr.validity

    # truncate to number of time periods captured
    validity = validity[:, :, :n]
    return beam_ids, validity


# consider moving to pulsar_protocol?
def plot_beam_validity(
    beam_ids: Iterable[int], validity: np.ndarray, base_filename: str
) -> None:
    """
    Plot Validity flags per beam.

    :param beam_ids: Beam IDs in order of ``validity`` array
    :param validity: Array of validity per [beam, channel group, time]
    :param base_filename: Where to save to, "_beam<n>" will be inserted.
      e.g. If set to "foo.png" and data for beam 123 is supplied, "foo_beam123.png"
      will be written.
    """
    for n, beam in enumerate(beam_ids):
        this_beam_validity = validity[n]
        for i in range(this_beam_validity.shape[0]):
            # i = first channel index: 0,1,...
            #   = (first channel number) / (channels per packet)
            plt.plot(
                this_beam_validity[i, :],
                label=(
                    f"Channels {i*PSS_CHANS_PER_PACKET}-{(i+1)*PSS_CHANS_PER_PACKET-1}"
                ),
            )
        plt.title(f"Beam {beam} Validity")
        plt.xlabel("Time (Sequence Number)")
        plt.ylabel("Validity Flags Value")
        plt.legend()
        plt.savefig(f"_beam{beam}".join(os.path.splitext(base_filename)))
        plt.close()


def _ptc42_report(scan_config_, last_value):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "672aaec1af25bf20c86c0b3f"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config_),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )

    shutil.copy(
        os.path.join(BUILD_DIR, "validity_beam11.png"),
        local_directory + new_repo_name + "/validity.png",
    )
    replace_word_in_report(
        file_result_to_replace,
        "ENDFLAGVALUESTATION",
        str(last_value.station_delay_poly),
    )
    replace_word_in_report(
        file_result_to_replace,
        "ENDFLAGVALUEBEAM",
        str(last_value.beam_delay_poly),
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/validity.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return last_value
