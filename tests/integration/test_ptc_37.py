# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#

"""
Perentie Test Case 37 - PSS Beam power at different pointing offsets.

- `Low CBF Requirements <https://is.gd/sul9PW>`_
- `Perentie Test Cases <https://is.gd/BCLuaC>`_
- `PTC-37 <https://is.gd/wjbnqR>`_
- `Overleaf report <https://www.overleaf.com/project/672aa3322371ab87c3cc3ad4>`_

1. Setup the CNIC to generate data for a single source for 18-stations
   with AA1 locaions (345-356, 429-434) containing one channel (64)
   with noise of amplitude 4000.
2. Set the LOW CBF delay polynomial simulator to point the station beam at
           RA: 2h30m DEC: -26d49m50.0s,
   and the four PSS beams at
     B11 - RA: 2h30m Dec: -36d   49m50.0s
     B12 - RA: 2h30m Dec: -35.5d 49m50.0s,
     B13 - RA: 2h30m Dec: -35d   49m50.0s,
     B14 - RA: 2h30m Dec: -34.5d 49m50.0s,
   which results in PSS beams -10 to -8 degrees offset from the station beam.
3. Setup a subarray to form 4 PSS beams using all 18 AA1 stations, and
   1 SPS channel
4. Start the scan
5. Capture 1 second of data using the CNIC-Rx
6. Input and analyse the captured data - plot the power spectrum of
   each beam as well as the sum of the power contained in each PSS
   beam.
7. Stop the CNIC/Beamformer and increment the look directions of the
   beams all by 2 degrees
8. Repeat steps PTC#37.3 to PTC#37.7 a further nine times
9. Plot the beam power as a function of offset angle direction
10. If time permits (10 times longer) create a 3d beam pattern by
    incrementing the RA direction as well in 2 minute increments
    (there are 15° in one hour of right ascension, 0.5 degree step = 2
    minutes)

Implementation based on PTC-28 (PST)
"""  # noqa

import json
import os
from datetime import datetime
from math import floor

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.pss_utils import freq_power_per_beam

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

# debug:
GENERATE_PCAP = True  # default: True
CREATE_REPORT = True  # default: True

DATA_DIR = "/test-data/PTC/ptc37/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_FNAME = DATA_DIR + "PTC37_beamformer_{}.pcap"
"""PCAP file name (with a placeholder for beam offset)."""

TEST_TIMEOUT_SEC = 7200

SUBARRAY_ID = 1
STN_BEAM_ID = 2

# NOTE: current (28-Nov-2024) PSS FW rquires n_stations % 4 == 0
# 12 + 4 stations:
station_ids = list(range(345, 345 + 12)) + list(range(429, 429 + 4))
n_stations = len(station_ids)

station_weights = {
    11: [1.0] * n_stations,
    12: [1.0] * n_stations,
    13: [1.0] * n_stations,
    14: [1.0] * n_stations,
}


def prepare_delay(max_offset):
    """Prepare the pointing configuration."""
    angle_offset_rev = []
    for beam in range(64):
        angle_offset_rev.append(
            f"{floor(beam * 60 / 64)}min"
            + f"{(beam * 60 / 64 - floor(beam * 60 / 64)) * 60}s"
        )
    angle_offset = list(reversed(angle_offset_rev))
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d00m00.0s"}
    degrees_half = floor(max_offset / 16)
    minutes = max_offset % 16

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 ]}",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 1]}",
    }
    src_radec_beam_3 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 2]}",
    }
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": f"-{28 - degrees_half}d{angle_offset[minutes * 4 + 3]}",
    }
    pss_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STN_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STN_BEAM_ID,
        "direction": [beam_radec] * 4,
    }
    pssdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STN_BEAM_ID,
        "direction": pss_radecs,
    }
    return beamdir, srcdir, pssdirs


FULL_RANGE = 16 * 6


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc37_pss_beam_power(cnic_and_processor, ready_subarray):
    """
    Implement PTC 37.

    In this test we aim to show that with the same experiment repeating 96
    times pointing to 4 different beam direction while having a single source
    in the sky on the same direction as the station beam. Doing so we capture
    384 points of measurement across 3 degrees.
    """
    # pylint: disable=too-many-locals
    os.makedirs(DATA_DIR, exist_ok=True)
    delay_sub = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STN_BEAM_ID:02}"
    pss_sub = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STN_BEAM_ID:02}_"

    print(datetime.now(), "Starting test")

    stations = [[station, 1] for station in station_ids]
    channels = [64]
    pss_beam_id = [11, 12, 13, 14]
    pss_svr_ip = "192.168.55.55"

    # Generate CNIC-VD Configuration
    cnic_config_noise = [
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 2,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config_same_beam = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": STN_BEAM_ID,
                        "freq_ids": channels,
                        "delay_poly": delay_sub,
                    }
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": STN_BEAM_ID,
                        "delay_poly": pss_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": pss_svr_ip,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": station_weights[pss_beam],
                    }
                    for station_idx, pss_beam in enumerate(pss_beam_id)
                ],
            },
        },
    }
    if GENERATE_PCAP:
        for offset in range(FULL_RANGE):
            (
                beam_delay_config,
                source_delay_config_beam,
                pss_delay_config_same_beam,
            ) = prepare_delay(offset)
            test_config = ProcessorVdStaticTrackingTestConfig(
                cnic_capture_min_size=200,
                cnic_capture_packets=792 * 10 * len(channels),
                scan_config=scan_config_same_beam,
                firmware_type=Personality.PSS,
                input_packet_count=None,
                start_scan_packet_count=None,
                before_end_scan_packet_count=None,
                output_packet_count=None,
                vd_config={
                    "sps_packet_version": 3,
                    "stream_configs": cnic_config_noise,
                },
                during_scan_callback=None,
                source_delay_config=[source_delay_config_beam],
                beam_delay_config=[beam_delay_config],
                pst_delay_config=pss_delay_config_same_beam,
            )
            _ = cnic_processor(
                cnic_and_processor,
                ready_subarray,
                test_config,
                PCAP_FNAME.format(offset),
            )
        print(datetime.now(), "Captured output, beginning analysis")

    total_freqs = {}
    total_pwr = {}
    for offset in range(FULL_RANGE):
        payloads = get_udp_payload_bytes(PCAP_FNAME.format(offset), 64)
        total_freqs[offset], total_pwr[offset] = freq_power_per_beam(
            payloads, pss_beam_id
        )

    passing_max, passing_ratio = _ptc37_report(
        scan_config_same_beam,
        total_pwr,
    )
    assert passing_max
    assert passing_ratio
    print(datetime.now(), "End of test")


def _ptc37_report(
    scan_config_same_beam,
    total_pwr,
):
    """Generate Test Report."""
    repo_id = "672aa3322371ab87c3cc3ad4"
    local_directory, new_repo_name, repo = download_overleaf_repo(repo_id)
    (
        printing_correctly,
        passing_max,
        passing_ratio,
        offset_mean_log,
    ) = create_ptc37_report_plots(
        total_pwr,
        local_directory,
        new_repo_name,
    )
    if not printing_correctly:
        print("error generating figures ")
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config_same_beam),
    )

    replace_word_in_report(
        file_result_to_replace,
        "MAXCENTERED",
        str(passing_max),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQRATIO",
        str(passing_ratio),
    )
    replace_word_in_report(
        file_result_to_replace,
        "RATIO15",
        str(offset_mean_log[33]),
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.png")
    repo.git.add(new_repo_name + "/ratio_db.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")

    if CREATE_REPORT:
        repo.index.commit("Test with python")
        repo.git.push()

    return passing_max, passing_ratio


def create_ptc37_report_plots(
    total_pwr,
    local_directory: str,
    new_repo_name: str,
):
    """Print the figures for the report."""
    os.makedirs(local_directory + new_repo_name, exist_ok=True)
    offset_mean = []
    offset_mean_log = []
    offset_axis = []
    for offset in range(FULL_RANGE):  # range(0, 16 * 6):
        offset_mean.append(np.mean(total_pwr[offset][11]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][11])))
        offset_axis.append(-2.984375 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][12]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][12])))
        offset_axis.append(-2.96875 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][13]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][13])))
        offset_axis.append(-2.953125 + offset / 16)

        offset_mean.append(np.mean(total_pwr[offset][14]))
        offset_mean_log.append(10 * np.log10(np.mean(total_pwr[offset][14])))
        offset_axis.append(-2.9375 + offset / 16)
    max_log = np.max(offset_mean_log)
    offset_mean_log = [offset_mean - max_log for offset_mean in offset_mean_log]

    plt.plot(offset_axis, offset_mean, "-x", label="Average power beam")
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("DEC offset (degree)")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.png", format="png")

    plt.figure()
    plt.plot(offset_axis, offset_mean_log, "-x", label="Average power beam")
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("DEC offset (degree)")
    plt.ylabel("Power (in dB)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio_db.png", format="png")
    passing_max = offset_mean.index(np.max(offset_mean)) == 191
    passing_ratio = offset_mean_log[offset_mean.index(np.max(offset_mean)) + 5] < -5

    return (True, passing_max, passing_ratio, offset_mean_log)
