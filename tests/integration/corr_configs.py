"""Correlator configurations for integration testing."""
from copy import deepcopy

from .station_beam_ids import BeamId

scan_6stn_1sa_1bm_1ch = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": BeamId.SINGLE_BASIC,
                    "freq_ids": [100],
                    "delay_poly": "some_url",
                },
            ],
        },
        "vis": {
            "firmware": "vis",
            "stn_beams": [
                {
                    "stn_beam_id": BeamId.SINGLE_BASIC,
                    "host": [[0, "192.168.128.2"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 20000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    },
}


# Used by PTC11
scan_6stn_1sa_1bm_ch448_delay = deepcopy(scan_6stn_1sa_1bm_1ch)
scan_6stn_1sa_1bm_ch448_delay["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = [448]
scan_6stn_1sa_1bm_ch448_delay["lowcbf"]["stations"]["stn_beams"][0][
    "delay_poly"
] = f"low-cbf/delaypoly/0/delay_s01_b{BeamId.SINGLE_BASIC:02d}"

# Used by both PTC11 & 23
scan_6stn_1sa_1bm_ch448_delay_tracking = deepcopy(scan_6stn_1sa_1bm_ch448_delay)
scan_6stn_1sa_1bm_ch448_delay_tracking["lowcbf"]["stations"]["stns"] = [
    [405, 1],
    [393, 1],
    [315, 1],
    [261, 1],
    [255, 1],
    [243, 1],
]
