# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Overleaf utilities to write reports of tests."""
import os
import shutil
from datetime import datetime

from git import Repo


def download_overleaf_repo(repo_number: str = "65246feac3c31d8e8e2900c5"):
    """Clone or access the overleaf repo."""
    date_for_name = datetime.now().strftime("%Y-%m-%d:%H:%M:%S")
    local_directory = "/app/overleaf_" + repo_number + "/"
    new_repo_name = "test_" + date_for_name
    if os.path.isdir(local_directory):
        print("Already cloned, will update...")
        repo = Repo(local_directory)
        repo.remotes.origin.pull()
    else:
        url = (
            "https://git:olp_2mpaQChADGLVqp0gQxD5PBQsDBEmDh1ddCy0"
            + f"@git.overleaf.com/{repo_number}"
        )
        print("Not there, will clone...")
        repo = Repo.clone_from(
            url=url,
            to_path=local_directory,
        )
    if not os.path.isdir(local_directory + new_repo_name):
        os.mkdir(local_directory + new_repo_name)
    shutil.copy(local_directory + "report.tex", local_directory + new_repo_name)
    shutil.copy(local_directory + "appendix.tex", local_directory + new_repo_name)
    # report_file_name = new_repo_name + "/report.tex"
    return local_directory, new_repo_name, repo


def replace_word_in_report(file_name, string_to_find, replacement):
    """Replace a word in file with its replacement."""
    with open(file_name, encoding="UTF8") as file_to_read:
        new_text = file_to_read.read().replace(string_to_find, replacement)

    with open(file_name, "w", encoding="UTF8") as file_to_write:
        file_to_write.write(new_text)
