# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Pytest configuration.

See https://docs.pytest.org for more details.
"""

from typing import List

import pytest

from ska_low_cbf_integration.cnic import load_firmware_and_reset, wait_until_fpga_ready
from ska_low_cbf_integration.connector import (
    add_basic_route,
    clear_routes_to_ports,
    load_ports,
    print_routing_tables,
)
from ska_low_cbf_integration.low_psi import (
    device_ports,
    get_connector_proxy,
    serial_port,
)
from ska_low_cbf_integration.processor import get_json_processor_proxies
from ska_low_cbf_integration.subarray import empty_subarray, get_ready_subarray_proxy
from ska_low_cbf_integration.tango import DeviceProxyJson, get_proxies_for_class


@pytest.fixture(scope="function")
def ready_subarray():
    """
    Get a Low CBF Subarray device that is ready to Configure.

    Empty it after use.
    """
    subarray = get_ready_subarray_proxy(1)
    yield subarray
    # restore to initial state
    empty_subarray(subarray)


@pytest.fixture(scope="function")
def ready_subarray_n(request):
    """
    Get the Low CBF Subarray <n>, ready to Configure.

    Use the 'indirect' option when requesting this fixture:

    .. code-block:: python

        @pytest.mark.parametrize(
            "ready_subarray_n", [1,2,3], indirect=["ready_subarray_n"]
        )

    """
    subarray = get_ready_subarray_proxy(request.param)
    yield subarray
    # restore to initial state
    empty_subarray(subarray)


@pytest.fixture
def cnic_and_processor() -> (List[DeviceProxyJson], List[DeviceProxyJson]):
    """
    Get all CNIC & Processor devices.

    Output of all Processors will be routed to first CNIC input.
    Output of first CNIC will be routed to input of the first Processor to register
    with the Allocator (i.e. the Processor that will be used first by a Subarray).
    """
    connector = get_connector_proxy()
    cnics = [
        DeviceProxyJson(proxy, json_commands=["CallMethod", "SelectPersonality"])
        for proxy in get_proxies_for_class("CnicDevice")
    ]
    processors = get_json_processor_proxies()  # note: in Allocator order
    assert len(cnics) >= 1, "No CNICs!"
    assert len(processors) >= 1, "No Processors!"

    # Load CNIC firmware (required to activate serial number attribute)
    for cnic in cnics:
        load_firmware_and_reset(cnic)

    for fpga in cnics + processors:
        if fpga.serialNumber not in serial_port:
            raise RuntimeError(f"Unexpected Alveo hardware! Serial={fpga.serialNumber}")

    # P4 port numbers
    cnic_port = device_ports(cnics)[0]
    all_ports = device_ports(cnics + processors)

    # load port configurations into switch if required
    load_ports(connector, all_ports)
    # remove wicked tricksy false routes from our precious ports
    print(f"Removing all routes from ports {all_ports}")
    clear_routes_to_ports(connector, all_ports)

    # Route from each Processor to the (first) CNIC
    first = True
    for processor_port in device_ports(processors):
        proc_port = processor_port
        cnic_port_str = cnic_port
        if not isinstance(processor_port, str):
            proc_port = processor_port[0]
        if not isinstance(cnic_port, str):
            cnic_port_str = cnic_port[0]
        print(f"Add Basic route from Processor {proc_port} to CNIC {cnic_port_str}")
        add_basic_route(connector, proc_port, cnic_port_str)
        # Route from first CNIC to first Processor
        if first:
            first = False
            print(f"Add Basic route from CNIC {cnic_port_str} to Processor {proc_port}")
            add_basic_route(connector, cnic_port_str, proc_port)

    print("Connector routing tables:")
    print_routing_tables(connector)
    connector.ResetPortStatistics()

    print("Activating Duplex mode")
    for cnic in cnics:
        wait_until_fpga_ready(cnic)
        cnic.hbm_pktcontroller__duplex = True
    yield cnics, processors

    # clean up our routes
    print(f"Removing all routes from ports {all_ports}")
    clear_routes_to_ports(connector, all_ports)

    print("Resetting CNIC(s)")
    for cnic in cnics:
        cnic.CallMethod(method="reset")
