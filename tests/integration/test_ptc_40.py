# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 40 - PSS trade stations for bandwidth

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #40 Presentation <https://docs.google.com/presentation/d/10b5T6NnAfeJRKAuRaKQ6UQlmLB7VtnlFEGsaBev7Ahc>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import pcap_stats
from ska_low_cbf_integration.pulsar_protocol import channel_power_beam
from ska_low_cbf_integration.sps import SPS_COARSE_SPACING_HZ

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc40/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC40_beamformer"
"""PCAP file name"""


TEST_TIMEOUT_SEC = 2000
SAMPLE_RATE = (SPS_COARSE_SPACING_HZ / 216) * (4 / 3)
MAX_NUMBER_SPS_CHANNELS = 384
N_POL = 2
N_VALS_PER_CPLX = 2
N_BYES_PER_VAL = 2
DELAY_EMULATOR_ADDR = "low-cbf/delaypoly/0"

station_ids = (
    list(range(1, 5))
    + list(range(303, 327))
    + list(range(339, 357))
    + list(range(363, 399))
    + list(range(429, 441))
    + list(range(471, 477))
)

PST_BEAM_ID = 11

CNIC_FINE_FREQ = 2
SUBARRAY_ID = 1
STATION_BEAM_ID = BeamId.SINGLE_BASIC
TONE_POSITION = 96 + 12
STATION_CHANNEL = {4: 256, 8: 128, 16: 64, 32: 32, 64: 16}
PST_SVR_IP = "192.168.55.55"
ALLOCATOR_ADDR = "low-cbf/allocator/0"


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": "-26d49m50.0s",
    }

    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_1,
        src_radec_beam_1,
        src_radec_beam_1,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }
    pstdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }

    return beamdir, srcdir, pstdirs


@pytest.mark.parametrize("n_stations", (4, 8, 16, 32, 64))
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc40_pss_trading_stations_for_bw(
    cnic_and_processor, ready_subarray, n_stations  # noqa
):  # noqa
    """Implement PTC 40."""
    # pylint: disable=too-many-locals,consider-using-with
    os.makedirs(DATA_DIR, exist_ok=True)
    print(datetime.now(), "Starting test")
    # Generate Scan Configuration
    stations = [[station, 1] for station in station_ids[:n_stations]]
    channels = [64 + channel for channel in range(STATION_CHANNEL[n_stations])]
    station_weights = {
        11: [1.0] * n_stations,
        12: [1.0] + [0.0] * (n_stations - 1),
    }

    delay_sub = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}"
    pss_sub = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}_"
    scan_config_test1a = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": channels,
                        "delay_poly": delay_sub,
                    }
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": STATION_BEAM_ID,
                        "delay_poly": pss_sub + str(pss_bm_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": PST_SVR_IP,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": station_weights[pss_beam],
                    }
                    # beam 11 uses only one station
                    for pss_bm_idx, pss_beam in enumerate([11])
                ],
            },
        },
    }
    scan_config_test1b = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": channels,
                        "delay_poly": delay_sub,
                    }
                ],
            },
            "search_beams": {
                "beams": [
                    {
                        "pss_beam_id": pss_beam,
                        "stn_beam_id": STATION_BEAM_ID,
                        "delay_poly": pss_sub + str(pss_bm_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": PST_SVR_IP,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 54,
                            }
                        ],
                        "stn_weights": station_weights[pss_beam],
                    }
                    # beam 14 uses all stations
                    for pss_bm_idx, pss_beam in enumerate([12])
                ],
            },
        },
    }

    # Generate CNIC-VD Configuration
    cnic_config_noise = {
        "sps_packet_version": 3,
        "stream_configs": [  # config is a list of dicts - one per SPEAD stream
            {
                "scan": 0,
                "subarray": SUBARRAY_ID,
                "station": station,
                "substation": substation,
                "frequency": channel,
                "beam": STATION_BEAM_ID,
                "sources": {
                    "x": [
                        {
                            "tone": False,
                            "seed": 1000 + channel,
                            "scale": 2000 + 5 * index_channel,
                        },
                    ],
                    "y": [
                        {
                            "tone": False,
                            "seed": 2000 + channel,
                            "scale": 2000 + 5 * index_channel,
                        },
                    ],
                },
            }
            for station, substation in stations
            for index_channel, channel in enumerate(channels)
        ],
    }

    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    allocator = tango.DeviceProxy(ALLOCATOR_ADDR)
    allocator.internalAlveoLimits = json.dumps(
        {
            "pss": {
                "vch": 512,
            },
        }
    )
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=100_000,
        scan_config=scan_config_test1a,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
        # do_fw_switcharoo=True,
    )
    file = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH_SAME_BEAM + f"test_a_{n_stations}_stations.pcap",
    )

    channels_power = channel_power_beam(file, 11, len(channels) * 54)
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=100_000,
        scan_config=scan_config_test1b,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
        # do_fw_switcharoo=True,
    )
    file = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH_SAME_BEAM + f"test_b_{n_stations}_stations.pcap",
    )
    channels_power2 = channel_power_beam(file, 12, len(channels) * 54)
    beam_11 = np.array(channels_power)
    beam_12 = np.array(channels_power2)
    square_stations = n_stations**2
    ratio = beam_11 / beam_12
    stats_beam11 = pcap_stats(
        open(PCAP_PATH_SAME_BEAM + f"test_a_{n_stations}_stations.pcap", "rb")
    )
    stats_beam12 = pcap_stats(
        open(PCAP_PATH_SAME_BEAM + f"test_b_{n_stations}_stations.pcap", "rb")
    )
    _ptc40_report(
        test_config.scan_config,
        beam_11,
        beam_12,
        n_stations,
        stats_beam11,
        stats_beam12,
    )
    assert square_stations * 0.98 < np.mean(ratio) < square_stations * 1.02


def _ptc40_report(
    scan_config, beam_11, beam_12, n_stations, stats_beam11, stats_beam12
):
    """Generate Test Report."""
    # pylint: disable=too-many-locals,too-many-arguments
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "672aaacb9fa02305d39cacea"
    )
    pps = float(stats_beam11.n_packets / stats_beam11.duration)
    bps = float(stats_beam11.total_bytes * 8 / stats_beam11.duration)
    pps_beam12 = float(stats_beam11.n_packets / stats_beam11.duration)
    bps_beam12 = float(stats_beam11.total_bytes * 8 / stats_beam11.duration)
    ratio = print_figures_for_ptc31_report(
        beam_11,
        beam_12,
        n_stations,
        local_directory,
        new_repo_name,
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    target_pps = 48 * 100 * STATION_CHANNEL[n_stations] / 5.3
    target_bps = 48 * 100 * 3706 * 8 * STATION_CHANNEL[n_stations] / 5.3
    target_average = n_stations**2
    words_to_replace = {
        "NSTATIONS": n_stations,
        "NSQUARE": target_average,
        "RATIO6O1": np.mean(ratio),
        "REQ6O1": 0.98 * target_average < np.mean(ratio) < 1.02 * target_average,
        "BPS11": f"{bps/1e9:.3f}",
        "TARGETBPS": f"{target_bps/1e9:.3f}",
        "REQBPS": abs(bps - target_bps) / target_bps <= 0.02,
        "TARGETPPS": f"{pps:.0f}",
        "PPS11": f"{pps:.0f}",
        "REQPPS": abs(pps - target_pps) / target_pps <= 0.02,
        "PAC11": stats_beam11.packet_sizes,
        "REQPAC": stats_beam11.packet_sizes == {3706},
        "BPS11b": f"{bps_beam12/1e9:.3f}",
        "REQBPSb": abs(bps_beam12 - target_bps) / target_bps <= 0.02,
        "PPS11b": f"{pps_beam12:.0f}",
        "REQPPSb": abs(pps_beam12 - target_pps) / target_pps <= 0.02,
        "PAC11b": stats_beam12.packet_sizes,
        "REQPACb": stats_beam12.packet_sizes == {3706},
    }
    for word_to_replace, replacement in words_to_replace.items():
        replace_word_in_report(
            file_result_to_replace,
            word_to_replace,
            str(replacement),
        )

    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.png")
    repo.git.add(new_repo_name + "/beam11.png")
    repo.git.add(new_repo_name + "/beam12.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return ratio


def print_figures_for_ptc31_report(
    beam_11,
    beam_12,
    n_station,
    local_directory: str,
    new_repo_name: str,
):
    """Print the figures for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False

    ratio = []
    for idx, pwr in enumerate(beam_11):
        if pwr != 0 and beam_12[idx] != 0:
            ratio.append(pwr / beam_12[idx])
        else:
            ratio.append(0)

    plt.plot(range(len(ratio)), ratio, label=f"Ratio {n_station}st/1st")
    plt.hlines(
        n_station**2,
        0,
        len(ratio),
        "r",
        "--",
        label="Theoretical",
    )
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.png", format="png")

    plt.figure()
    plt.plot(range(len(beam_11)), beam_11, "g", label="beam 11")
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam11.png", format="png")

    plt.figure()
    plt.plot(range(len(beam_12)), beam_12, "c", label="beam 12")
    plt.title("Average PSS beam power per complex sample")
    plt.xlabel("PSS channel number")
    plt.ylabel("Power linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam12.png", format="png")

    return ratio
