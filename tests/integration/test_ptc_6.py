# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 6 - Correlator processed bandwidth.

Test:
    Measure the total processed bandwidth of the correlator by sweeping a tone
    through all visibility channels. Using configuration:
    - 96 SPS channels (AA1) chans 200-295
    - 18 stations
    - Single CNIC source (tone).
    - All zero delay polynomials

More information:
- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #6 Presentation <https://docs.google.com/presentation/d/1OTnAaR_fR1FdHuZYjt-Y4Gfw561r_ZnH4BAgdpr9tDg>`_
- `PTC #6 Overleaf <https://www.overleaf.com/read/rgcmswsvhzys#5dfcb5>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import shutil
from itertools import combinations
from typing import Dict, List

import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

stations_closure = [0, 1, 2, 3, 4, 5]

combinations_of_three = list(combinations(stations_closure, 3))
combinations_of_four = list(combinations(stations_closure, 4))

TEST_TIMEOUT_SEC = 7200
TEST_TIMEOUT_TRACKING_SEC = 900

# AA1.0
stations_aa10 = list(range(345, 357)) + list(range(429, 435))
source_constant_delay_aa10_1beam = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_A,
    "delay": [
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
    ],
}
constant_beam_delay__aa10_1beam = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_A,
    "delay": [{"stn": station, "nsec": 0.0} for station in stations_aa10],
}
source_constant_delay_aa10_3beam_b1 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_B,
    "delay": [
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
    ],
}
constant_beam_delay__aa10_3beams_b1 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_B,
    "delay": [{"stn": station, "nsec": 0.0} for station in stations_aa10],
}
source_constant_delay_aa10_3beam_b2 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_C,
    "delay": [
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
    ],
}
constant_beam_delay__aa10_3beams_b2 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_C,
    "delay": [{"stn": station, "nsec": 0.0} for station in stations_aa10],
}
source_constant_delay_aa10_3beam_b3 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_D,
    "delay": [
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
        [{"stn": station, "nsec": 0.0} for station in stations_aa10],
    ],
}
constant_beam_delay__aa10_3beams_b3 = {
    "subarray_id": 1,
    "beam_id": BeamId.PTC6_D,
    "delay": [{"stn": station, "nsec": 0.0} for station in stations_aa10],
}


@pytest.mark.long_test
@pytest.mark.large_capture
@pytest.mark.timeout(TEST_TIMEOUT_SEC)  # default of 300s is not long enough
@pytest.mark.hardware_present
def test_ptc6_corr_processed_bw(cnic_and_processor, ready_subarray):
    """Implement 1 beam 18 stations - 96 channel part of PTC 6."""
    # pylint: disable = too-many-locals
    vd_config_multi_beam = {
        "stream_configs": cnic_config_ptc06_three_beams_b1
        + cnic_config_ptc06_three_beams_b2
        + cnic_config_ptc06_three_beams_b3,
        "sps_packet_version": 3,
    }
    test_config_beams = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=80,
        cnic_capture_packets=96 * 144 * 35,
        scan_config=scan_18stn_1sa_3bm_96ch,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=vd_config_multi_beam,
        during_scan_callback=None,
        source_delay_config=[
            source_constant_delay_aa10_3beam_b1,
            source_constant_delay_aa10_3beam_b2,
            source_constant_delay_aa10_3beam_b3,
        ],
        beam_delay_config=[
            constant_beam_delay__aa10_3beams_b1,
            constant_beam_delay__aa10_3beams_b2,
            constant_beam_delay__aa10_3beams_b3,
        ],
        nb_cnics=1,
        nb_processors=2,
    )
    correlator_output_3_beams = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config_beams,
        pcap_file_name="/test-data/ci-rx-ptc6.pcap",
    )
    analyser_3_beams_b1 = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_aa10_three_beams_b1,
                "stations": stations_aa10,
            }
        )
    )
    analyser_3_beams_b1.extract_spead_data(
        correlator_output_3_beams.name, filter_="host 192.168.9.9"
    )
    # Calling this creates a graph, but we don't care about the return value
    _ = analyser_3_beams_b1.amplitude_freq_analysis(
        "XX", 30, channels_aa10_three_beams_b1, False
    )

    all_vis_9 = sum_visibility_abs(
        analyser_3_beams_b1,
        channels_aa10_three_beams_b1,
        stations_aa10,
        30,
    )
    max_min_9, max_max_9 = max_min_and_max_max(all_vis_9)

    shutil.copy(
        "./amplitude_vs_freq_polarisation_XX.png",
        "./amplitude_vs_freq_polarisation_3_beams_b1.png",
    )
    analyser_3_beams_b2 = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_aa10_three_beams_b2,
                "stations": stations_aa10,
            }
        )
    )
    analyser_3_beams_b2.extract_spead_data(
        correlator_output_3_beams.name, filter_="host 192.168.10.10"
    )
    _ = analyser_3_beams_b2.amplitude_freq_analysis(
        "XX", 30, channels_aa10_three_beams_b2, False
    )

    all_vis_10 = sum_visibility_abs(
        analyser_3_beams_b2,
        channels_aa10_three_beams_b2,
        stations_aa10,
        30,
    )
    max_min_10, max_max_10 = max_min_and_max_max(all_vis_10)

    shutil.copy(
        "./amplitude_vs_freq_polarisation_XX.png",
        "./amplitude_vs_freq_polarisation_3_beams_b2.png",
    )
    analyser_3_beams_b3 = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_aa10_three_beams_b3,
                "stations": stations_aa10,
            }
        )
    )
    analyser_3_beams_b3.extract_spead_data(
        correlator_output_3_beams.name, filter_="host 192.168.11.11"
    )
    _ = analyser_3_beams_b3.amplitude_freq_analysis(
        "XX", 30, channels_aa10_three_beams_b3, False
    )

    all_vis_11 = sum_visibility_abs(
        analyser_3_beams_b3,
        channels_aa10_three_beams_b3,
        stations_aa10,
        30,
    )
    max_min_11, max_max_11 = max_min_and_max_max(all_vis_11)

    shutil.copy(
        "./amplitude_vs_freq_polarisation_XX.png",
        "./amplitude_vs_freq_polarisation_3_beams_b3.png",
    )
    vd_config = {
        "stream_configs": cnic_config_ptc06_single_beam,
        "sps_packet_version": 3,
    }

    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=80,
        cnic_capture_packets=96 * 144 * 35,
        scan_config=scan_18stn_1sa_1bm_96ch,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=vd_config,
        during_scan_callback=None,
        source_delay_config=[source_constant_delay_aa10_1beam],
        beam_delay_config=[constant_beam_delay__aa10_1beam],
        nb_cnics=1,
        nb_processors=2,
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    analyser = VisibilityAnalyser(
        json.dumps(
            {
                "coarse_channels": channels_aa10_single_beam,
                "stations": stations_aa10,
            }
        )
    )
    analyser.extract_spead_data(correlator_output.name)
    _ = analyser.amplitude_freq_analysis("XX", 30, channels_aa10_single_beam, False)

    all_vis_8 = sum_visibility_abs(
        analyser, channels_aa10_single_beam, stations_aa10, 30
    )
    max_min_8, max_max_8 = max_min_and_max_max(all_vis_8)

    shutil.copy(
        "./amplitude_vs_freq_polarisation_XX.png",
        "./amplitude_vs_freq_polarisation_1_beam.png",
    )
    # ptc_11_generate_report(parameters)
    all_min_max = [
        max_min_8,
        max_max_8,
        max_min_9,
        max_max_9,
        max_min_10,
        max_max_10,
        max_min_11,
        max_max_11,
    ]
    ptc6_report(test_config, test_config_beams, all_min_max)
    assert max_min_8 < 0.01
    assert max_max_8 < 0.01
    assert max_min_9 < 0.01
    assert max_max_9 < 0.01
    assert max_min_10 < 0.01
    assert max_max_10 < 0.01
    assert max_min_11 < 0.01
    assert max_max_11 < 0.01


def sum_visibility_abs(
    visibilities_structure: VisibilityAnalyser,
    channels,
    stations,
    number_of_integration: int,
) -> Dict[str, List[float]]:
    """
    Sum the absolute values of visibilities for a number of integrations.

    :returns: {SPS Channel: [144 absolute value sums]}
    """
    all_vis_channel_integration = {}
    for chan in channels:
        all_vis_channel_integration[chan] = []
        for vis in range(144):  # Visibility Channels per SPS channel
            absolute_value = 0
            vis_index = visibilities_structure.vis_number.index(chan * 144 + vis)
            vis_chan = visibilities_structure.visibilities[vis_index]
            for i in range(int((len(stations) * (len(stations) + 1)) / 2)):
                for time in range(number_of_integration):
                    time_index = int(
                        sorted(list(visibilities_structure.time_sequence))[time]
                    )
                    for polarisation in range(4):
                        absolute_value += np.abs(
                            vis_chan[time_index]["VIS"][i][polarisation]
                        )

            all_vis_channel_integration[chan].append(absolute_value)
    return all_vis_channel_integration


def max_min_and_max_max(all_vis):
    """Calculate min/max per step."""
    max_min = 0.0
    max_max = 0.0
    for _, vis in all_vis.items():
        max_min = max(max_min, (np.mean(vis) - np.min(vis)) / np.mean(vis))
        max_max = max(max_max, (np.max(vis) - np.mean(vis)) / np.mean(vis))
    return max_min, max_max


def ptc6_report(test_config, test_config_3_beams, all_min_max):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66e8f326ee33e62de4b49b8b"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN_BEAMS",
        json.dumps(test_config_3_beams.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    for index, beam_numbers in enumerate([8, 9, 10, 11]):
        replace_word_in_report(
            file_result_to_replace,
            f"MIN_MAX_{beam_numbers}",
            f"{all_min_max[2 * index]}",
        )
        replace_word_in_report(
            file_result_to_replace,
            f"MAX_MAX_{beam_numbers}",
            f"{all_min_max[2 * index+1]}",
        )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "amplitude_vs_freq_polarisation_1_beam.png",
        local_directory + new_repo_name + "/amplitude_vs_freq_polarisation_1_beam.png",
    )
    shutil.copy(
        "amplitude_vs_freq_polarisation_3_beams_b1.png",
        local_directory
        + new_repo_name
        + "/amplitude_vs_freq_polarisation_3_beams_b1.png",
    )
    shutil.copy(
        "amplitude_vs_freq_polarisation_3_beams_b2.png",
        local_directory
        + new_repo_name
        + "/amplitude_vs_freq_polarisation_3_beams_b2.png",
    )
    shutil.copy(
        "amplitude_vs_freq_polarisation_3_beams_b3.png",
        local_directory
        + new_repo_name
        + "/amplitude_vs_freq_polarisation_3_beams_b3.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/amplitude_vs_freq_polarisation_1_beam.png")
    repo.git.add(new_repo_name + "/amplitude_vs_freq_polarisation_3_beams_b1.png")
    repo.git.add(new_repo_name + "/amplitude_vs_freq_polarisation_3_beams_b2.png")
    repo.git.add(new_repo_name + "/amplitude_vs_freq_polarisation_3_beams_b3.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


# AA1.0 configuration
channels_aa10_single_beam = list(range(64, 160))
channels_aa10_three_beams_b1 = list(range(92, 92 + 16))
channels_aa10_three_beams_b2 = list(range(64, 64 + 32))
channels_aa10_three_beams_b3 = list(range(71, 71 + 48))
stations_corr_aa10 = [[station, 1] for station in stations_aa10]
scan_18stn_1sa_1bm_96ch = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations_corr_aa10,
            "stn_beams": [
                {
                    "beam_id": BeamId.PTC6_A,
                    "freq_ids": channels_aa10_single_beam,
                    "delay_poly": f"low-cbf/delaypoly/0/delay_s01_b{BeamId.PTC6_A:02d}",
                },
            ],
        },
        "vis": {
            "stn_beams": [
                {
                    "stn_beam_id": BeamId.PTC6_A,
                    "host": [[0, "192.168.8.8"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 20000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    },
}
scan_18stn_1sa_3bm_96ch = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations_corr_aa10,
            "stn_beams": [
                {
                    "beam_id": BeamId.PTC6_B,
                    "freq_ids": channels_aa10_three_beams_b1,
                    "delay_poly": f"low-cbf/delaypoly/0/delay_s01_b{BeamId.PTC6_B:02d}",
                },
                {
                    "beam_id": BeamId.PTC6_C,
                    "freq_ids": channels_aa10_three_beams_b2,
                    "delay_poly": f"low-cbf/delaypoly/0/delay_s01_b{BeamId.PTC6_C:02d}",
                },
                {
                    "beam_id": BeamId.PTC6_D,
                    "freq_ids": channels_aa10_three_beams_b3,
                    "delay_poly": f"low-cbf/delaypoly/0/delay_s01_b{BeamId.PTC6_D:02d}",
                },
            ],
        },
        "vis": {
            "stn_beams": [
                {
                    "stn_beam_id": BeamId.PTC6_B,
                    "host": [[0, "192.168.9.9"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
                {
                    "stn_beam_id": BeamId.PTC6_C,
                    "host": [[0, "192.168.10.10"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
                {
                    "stn_beam_id": BeamId.PTC6_D,
                    "host": [[0, "192.168.11.11"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    },
}

cnic_config_ptc06_single_beam = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.PTC6_A,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 25 * channel},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 25 * channel},
            ],
        },
    }
    for station in stations_aa10
    for channel in channels_aa10_single_beam
]
cnic_config_ptc06_three_beams_b1 = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.PTC6_B,
        "sources": {
            "x": [
                {"tone": False, "seed": 1981, "scale": 25 * channel},
            ],
            "y": [
                {"tone": False, "seed": 1981, "scale": 25 * channel},
            ],
        },
    }
    for station in stations_aa10
    for channel in channels_aa10_three_beams_b1
]
cnic_config_ptc06_three_beams_b2 = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.PTC6_C,
        "sources": {
            "x": [
                {"tone": False, "seed": 1982, "scale": 25 * channel},
            ],
            "y": [
                {"tone": False, "seed": 1982, "scale": 25 * channel},
            ],
        },
    }
    for station in stations_aa10
    for channel in channels_aa10_three_beams_b2
]
cnic_config_ptc06_three_beams_b3 = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 123,
        "subarray": 1,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": BeamId.PTC6_D,
        "sources": {
            "x": [
                {"tone": False, "seed": 1983, "scale": 25 * channel},
            ],
            "y": [
                {"tone": False, "seed": 1983, "scale": 25 * channel},
            ],
        },
    }
    for station in stations_aa10
    for channel in channels_aa10_three_beams_b3
]
