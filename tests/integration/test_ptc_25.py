# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 25 - Offset delay tracking between polarisations.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
import shutil
from math import isclose

import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.tango import DeviceProxyJson
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import DELAY_EMULATOR_ADDR, ProcessorVdTestConfig, cnic_processor

PTC25_DIR = "build/ptc25"
"""Directory to write plots etc."""

POLARISATIONS = ("XX", "XY", "YX", "YY")
COLOURS = "gbcmykgb"

SUBARRAY_ID = 1
station_ids = [345, 350, 352, 431]
channels_beam_9 = list(range(64, 64 + 8))
stations = [[station, 1] for station in station_ids]

stream_config_beam_9 = [  # config is a list of dicts - one per SPEAD stream
    {
        "scan": 0,
        "subarray": SUBARRAY_ID,
        "station": station,
        "substation": 1,
        "frequency": channel,
        "beam": 9,
        "sources": {
            "x": [
                # {"tone": False, "seed": 1981, "scale": 1000*(1+station/nb_station)},
                {
                    "tone": False,
                    "seed": 1981,
                    "scale": 4000,
                    "delay_polynomial": [0.0, 0, 0, 0, 0, 0],
                },
            ],
            "y": [
                # {"tone": False, "seed": 1981, "scale": 1000*(1+station/nb_station)},
                {
                    "tone": False,
                    "seed": 1981,
                    "scale": 4000,
                    "delay_polynomial": [1.0, 0, 0, 0, 0, 0],
                },
            ],
        },
    }
    for station in station_ids
    for channel in channels_beam_9
]

cnic_config = {
    "sps_packet_version": 3,
    # "ska_time": seconds_after_epoch,  # for v3 only
    "stream_configs": stream_config_beam_9,
    # +stream_config_beam_14+stream_config_beam_5,
}

scan_ptc_25 = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": 9,
                    "freq_ids": channels_beam_9,
                    "delay_poly": f"{DELAY_EMULATOR_ADDR}/delay_s01_b09",
                },
            ],
        },
        "vis": {
            "stn_beams": [
                {
                    "stn_beam_id": 9,
                    "host": [[0, "192.168.9.9"]],
                    "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    },
}


@pytest.mark.hardware_present
@pytest.mark.parametrize("delay_compensation", (True, False))
def test_ptc25_corr_offset_delay(
    request,
    cnic_and_processor,  # noqa
    ready_subarray,  # noqa
    delay_compensation,
):
    """Test PTC#25 (Offset delay tracking between polarisations)."""
    n_vis = 25
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=120,
        cnic_capture_packets=144 * n_vis * (len(channels_beam_9)),
        scan_config=scan_ptc_25,
        vd_config=cnic_config,
        firmware_type=Personality.CORR,
    )
    delay = DeviceProxyJson(DELAY_EMULATOR_ADDR)
    if delay_compensation:
        delay.ypol_offset_ns = 1.0
    else:
        delay.ypol_offset_ns = 0.0
    delay.setsecondsafterepoch(0)
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)
    visibilities = VisibilityAnalyser(
        json.dumps({"coarse_channels": channels_beam_9, "stations": station_ids})
    )
    visibilities.extract_spead_data(correlator_output.name)
    visibilities.remove_visibilities(5)
    visibilities.populate_correlation_matrix()
    os.makedirs(PTC25_DIR, exist_ok=True)
    gradient, _ = visibilities.angle_analysis(
        (station_ids[0], station_ids[2]),
        "YX",
        1,
        channels_beam_9,
        filename=os.path.join(PTC25_DIR, f"delay_comp_{delay_compensation}.png"),
    )
    visibilities.save_heatmap_to_disk(
        mode="average_all", case=request.node.name, directory=PTC25_DIR
    )

    if delay_compensation:
        test_threshold = 0.01
        test_pass = isclose(gradient, 0, abs_tol=test_threshold)
        test_threshold_str = f"$< {test_threshold}$"
    else:
        test_threshold = 0.1
        test_pass = gradient > test_threshold
        test_threshold_str = f"$>{test_threshold}$"

    ptc25_report(
        test_config, gradient, delay_compensation, test_threshold_str, test_pass
    )
    assert test_pass, f"Phase slope {gradient} exceeds threshold {test_threshold}"


def ptc25_report(
    test_config: ProcessorVdTestConfig,
    gradient: float,
    delay_compensation: bool,
    slope_exp: str,
    passing_test: bool,
):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66ff78f3d3e937ac751f8649"
    )
    with_or_without = "with" if delay_compensation else "without"
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "WITHORWITHOUT",
        with_or_without,
    )
    replace_word_in_report(
        file_result_to_replace,
        "SLOPE_EXP",
        slope_exp,
    )
    replace_word_in_report(
        file_result_to_replace,
        "RESULT_REQ",
        str(passing_test),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SLOPE_OBS",
        f"{gradient:.03E}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "DELAYCOMP",
        f"{delay_compensation}",
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        "/app/" + PTC25_DIR + f"/delay_comp_{delay_compensation}.png",
        local_directory + new_repo_name + f"/delay_comp_{delay_compensation}.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + f"/delay_comp_{delay_compensation}.png")
    repo.index.commit("Test with python")
    repo.git.push()
