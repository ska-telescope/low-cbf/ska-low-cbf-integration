# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 7 - Correlator Timing Accuracy.
Step PTC#7.4 latency accuracy.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #7 Presentation <https://docs.google.com/presentation/d/1zTJ3jEDTSiE7D9Gvx71l8_c2ErBP5ymtmLQzGsPNe-4>`_
- `Overleaf report <https://www.overleaf.com/read/ykrdptqqtrsg#24f596>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime
from math import ceil
from time import sleep

import matplotlib.pyplot as plt
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import latency_stats

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticMulticastTestConfig, cnic_processor
from .test_ptc_7 import station_ids

TEST_TIMEOUT_SEC = 1000
BEAM_ID = BeamId.PTC7


@pytest.mark.large_capture
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
@pytest.mark.parametrize("integration_ms", [849])
def test_ptc7_c_corr_visibility_latency(
    cnic_and_processor, ready_subarray, integration_ms
):
    """Implement PTC 7, part C (step 7.4)."""
    # pylint: disable=too-many-locals
    print(datetime.now(), "Starting PTC#7C")
    n_stations = 6
    stations = [[station, 1] for station in station_ids[:n_stations]]
    n_channels = 96
    first_channel = ceil(230 - n_channels / 2)
    channels = list(range(first_channel, first_channel + n_channels))
    # Generate CNIC-VD Configuration
    cnic_config = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    # {"tone": True, "fine_frequency": 0, "scale": 4},
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    # {"tone": True, "fine_frequency": 0, "scale": 4},
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]
    # Generate Scan Configuration
    scan_config = {
        "id": 1235,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": BEAM_ID,
                        "freq_ids": channels,
                        "delay_poly": "some_urls",
                    },
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": BEAM_ID,
                        "host": [[0, "22.22.22.22"]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": integration_ms,
                    },
                ],
            },
        },
    }
    spead_multicast_routings = [
        {
            "src": {"frequency": channel, "beam": BEAM_ID, "sub_array": 1},
            "dst": {"port_bf": "12/0", "port_corr": "13/0"},
        }
        for channel in [
            channels[0],
            channels[ceil(len(channels) / 2)],
            channels[len(channels) - 1],
        ]
    ]
    capture_integrations = 4
    # looping on the start time of the burst hence going through all
    # scan states from the SKA scan state machine.
    cnic_capture_packets = 144 * n_channels * (capture_integrations + 1) + len(
        spead_multicast_routings
    ) * n_stations * 384 * (capture_integrations + 1)

    test_config = ProcessorVdStaticMulticastTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=cnic_capture_packets,
        scan_config=scan_config,
        firmware_type=Personality.CORR,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=_ptc_7c_start_traffic,
        pulsar_config=None,
        spead_multicast_routing=spead_multicast_routings,
    )
    correlator_output = cnic_processor(cnic_and_processor, ready_subarray, test_config)

    print(
        datetime.now(),
        "Captured output, beginning analysis",
    )
    sps_timestamps, visibility_data = latency_stats(
        correlator_output.name, 144 * 96 - 1
    )
    print(datetime.now(), "SPEAD data extracted")
    latencies = _ptc7c_report(
        sps_timestamps, visibility_data, scan_config, integration_ms
    )
    for freq, results in latencies.items():
        print(f"Checking max latency inferior 3xintegration for freq {freq}")
        assert (
            round(sorted(results.values(), reverse=True)[0], 6)
            < 3 * integration_ms / 1000
        )
    print(datetime.now(), "End of PTC#7C")


def _ptc_7c_start_traffic(cnic, processor):
    """Start CNIC traffic."""
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
    sleep(0.2)
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
    print(f"Pulsar configuration: start {cnic.vd__pulsar_start_sample_count}")
    print(f"Pulsar configuration: on {cnic.vd__pulsar_on_sample_count}")
    print(f"Pulsar configuration: stop {cnic.vd__pulsar_off_sample_count}")
    print(f"Pulsar configuration: enabling {cnic.vd__enable_pulsar_timing}")
    sleep(0.2)
    cnic.enable_vd = True


def _ptc7c_report(sps_timestamps, visibility_data, scan_config, integration_ms):
    """Generate Test Report for PTC7."""
    # pylint: disable=too-many-locals
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "65c82c8ad9e535bd67cb0123"
    )
    success_print, difference = print_figures_for_ptc7c_report(
        sps_timestamps,
        visibility_data,
        local_directory,
        new_repo_name,
    )
    if not success_print:
        print("error generating figures ")

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    freq_number = 0
    max_max = 0
    for freq, results in difference.items():
        freq_number += 1
        replace_word_in_report(
            file_result_to_replace,
            f"FREQ_{freq_number}",
            str(freq),
        )
        replace_word_in_report(
            file_result_to_replace,
            f"MINFREQ{freq_number}",
            str(round(sorted(results.values())[0], 6)),
        )
        replace_word_in_report(
            file_result_to_replace,
            f"MAXFREQ{freq_number}",
            str(round(sorted(results.values(), reverse=True)[0], 6)),
        )
        max_max = max([round(sorted(results.values(), reverse=True)[0], 6), max_max])

    replace_word_in_report(
        file_result_to_replace,
        "MAXMAX",
        str(max_max),
    )
    replace_word_in_report(
        file_result_to_replace,
        "UPPERLIMIT",
        str(3 * integration_ms / 1000),
    )
    file_to_send = [
        "/report.tex",
        "/timestamps_1.png",
        "/difference_1.png",
        "/timestamps_2.png",
        "/difference_2.png",
        "/timestamps_3.png",
        "/difference_3.png",
        "/boxplot_1.pdf",
        "/boxplot_2.pdf",
        "/boxplot_3.pdf",
    ]
    repo.git.add("report2017.tex")
    for file in file_to_send:
        repo.git.add(new_repo_name + file)
    repo.index.commit("Test with python")
    repo.git.push()
    return difference


def print_figures_for_ptc7c_report(
    sps_timestamps,
    visibility_data,
    local_directory: str,
    new_repo_name: str,
):
    """Print the three figure for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False, {}
    difference = {}
    for freq, results in sps_timestamps.items():
        difference[freq] = {}
        for sample, time in results.items():
            if sample in visibility_data[freq]:
                difference[freq][sample] = float(visibility_data[freq][sample] - time)

    freq_number = 0
    for freq, results in sps_timestamps.items():
        freq_number += 1
        if freq in visibility_data:
            plt.clf()
            plt.plot(
                visibility_data[freq].keys(),
                visibility_data[freq].values(),
                "r.-",
                label="timestamp visibilities",
            )
            plt.plot(
                results.keys(), results.values(), "b.-", label="timestamp SPS sample"
            )
            plt.ylabel("Time (s)")
            plt.xlabel("SPS sample number since offset")
            plt.legend(prop={"size": 10})
            plt.savefig(
                local_directory + new_repo_name + f"/timestamps_{freq_number}.png",
                format="png",
                dpi=200,
            )
    freq_number = 0
    for freq, results in difference.items():
        freq_number += 1
        plt.clf()
        plt.plot(results.keys(), results.values(), "r-", label="Correlator latency")
        plt.ylabel("Time (s)")
        plt.xlabel("SPS sample number since offset")
        plt.legend(prop={"size": 10})
        plt.savefig(
            local_directory + new_repo_name + f"/difference_{freq_number}.png",
            format="png",
            dpi=200,
        )
    freq_number = 0
    for freq, results in difference.items():
        freq_number += 1
        plt.clf()
        plt.boxplot(results.values())
        plt.ylabel("Time (s)")
        plt.legend(prop={"size": 10})
        plt.title(f"SPS coarse channel {freq}")
        plt.savefig(
            local_directory + new_repo_name + f"/boxplot_{freq_number}.pdf",
            format="pdf",
        )

    return True, difference
