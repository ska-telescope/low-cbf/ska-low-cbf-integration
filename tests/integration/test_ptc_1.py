# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# Long URLs upset pylint, despite 'noqa' after the string?
# pylint: disable=line-too-long
"""
Perentie Test Case 1 - Exercising the Observing state machine.


More information:
- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_

Step numbers in comments/messages are taken from the Perentie Test Cases spreadsheet.
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import copy
import os
import time
from collections import OrderedDict
from functools import partial
from itertools import cycle

import matplotlib.pyplot as plt
import pytest
import tango
from ska_tango_base.control_model import AdminMode, ObsState

from ska_low_cbf_integration.allocator import allocator_proxy, serials_per_subarray
from ska_low_cbf_integration.firmware import Personality, version_under_test
from ska_low_cbf_integration.tango import DeviceProxyJson, wait_for_attr

from . import corr_configs
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdTestConfig, cnic_processor

BUILD_DIR = "build/ptc1"


def generate_scan_config(n_processors: int = 1) -> dict:
    """Generate a Scan Configuration."""
    if n_processors > 12:
        raise NotImplementedError("This algorithm only works up to 12 Processors.")

    config = copy.deepcopy(corr_configs.scan_6stn_1sa_1bm_1ch)
    fw_request = f"corr:{version_under_test(Personality.CORR)}:gitlab"
    config["lowcbf"]["vis"]["firmware"] = fw_request

    if n_processors > 1:
        stations = [[i + 1, 1] for i in range(32)]
        channels = [64 + i for i in range(32 * n_processors)]
        config["lowcbf"]["stations"]["stns"] = stations
        config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels
    return config


def generate_cnic_config(scan_config: dict) -> dict:
    """Generate CNIC configuration to suit a Scan configuration."""
    stations = scan_config["lowcbf"]["stations"]["stns"].copy()
    channels = scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"].copy()

    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": scan_config["id"],
                "subarray": 1,  # dangerous? dunno
                "station": station,
                "substation": substation,
                "frequency": channel,
                "beam": BeamId.SINGLE_BASIC,
                "sources": {
                    "x": [
                        {"tone": False, "seed": 1, "scale": 4000},
                    ],
                    "y": [
                        {"tone": False, "seed": 1, "scale": 4000},
                    ],
                },
            }
            for station, substation in stations
            for channel in channels
        ],
    }


def timestr(t: float | None = None) -> str:
    """Write time as a string, reading current time if no time value provided."""
    if t is None:
        t = time.time()
    return f"{t:.2f}"


class ObsStateMonitoringSubarray:
    """Subarray proxy that logs commands & ObsState updates."""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, subarray: DeviceProxyJson, log_file_name: str):
        """Create ObsStateMonitoringSubarray."""
        # pylint: disable=consider-using-with
        self.subarray = subarray
        self.logfile = open(log_file_name, "w", encoding="utf-8")
        self._commands = [command.lower() for command in subarray.get_command_list()]
        self._attributes = [attr.lower() for attr in subarray.get_attribute_list()]
        self._command_log = {}
        """time: command sent"""
        self._obsstate_log = {}
        """time: ObsState value received in event"""
        self.subscription_id = subarray.subscribe_event(
            "obsState", tango.EventType.CHANGE_EVENT, self.log_event
        )
        self._init_complete = True  # should be the last line of __init__

    def log_event(self, event_data: tango.EventData):
        """Log an (ObsState) Event."""
        t = time.time()
        value = ObsState(event_data.attr_value.value)
        event_time = event_data.reception_date
        print(
            timestr(t),
            "Received Event",
            event_time,
            value,
            file=self.logfile,
            flush=True,
        )
        self._obsstate_log[t] = value

    def _logging_command(self, command_name):
        """Log command call and possible exception."""

        def _command(arg=None):
            t = time.time()
            print(timestr(t), "Sending Command", command_name, file=self.logfile)
            self._command_log[t] = command_name
            try:
                # call the command
                if arg is not None:  # TODO - branching may not be req'd
                    getattr(self.subarray, command_name)(arg)
                else:
                    getattr(self.subarray, command_name)()
            except Exception as exception:
                print("EXCEPTION", exception, file=self.logfile)
                raise exception

        return _command

    def __getattr__(self, name):
        """Log & Delegate subarray attrs to Tango."""
        if name in self.__dir__():
            raise AttributeError(
                "AttributeError occurred reading "
                f"{self.__class__.__name__}.{name}. Good luck!"
            )

        if name.lower() in self._commands:
            return self._logging_command(name)

        if name.lower() in self._attributes:
            attr = getattr(self.subarray, name)
            print(timestr(), "Reading Attribute", name, attr, file=self.logfile)
            return attr

        raise AttributeError(f"{self.__class__.__name__} has no attribute {name}")

    def __setattr__(self, name, value):
        """Log & Delegate subarray attr writes to Tango."""
        if "_init_complete" in self.__dict__ and name in self._attributes:
            print(timestr(), "Writing Attribute", name, value, file=self.logfile)
            setattr(self.subarray, name, value)
        else:
            super().__setattr__(name, value)

    def __del__(self):
        """Clean-up when deleted."""
        self.subarray.unsubscribe_event(self.subscription_id)
        self.logfile.close()

    def plot_commands_and_obsstate_events(
        self, filename: str, aux_plot: dict | None = None
    ) -> None:
        """
        Plot logged history of Commands and obsState events.

        :param filename: filename to save plot to
        :param aux_plot: auxiliary plot data, needs keys:
         x (x-axis time values)
         y (y-axis auxiliary values)
         title (y-axis title)
        """
        fig, ax = plt.subplots()

        ax2 = None
        if aux_plot is not None:
            ax2 = ax.twinx()
            ax2.plot(aux_plot["x"], aux_plot["y"], color="tab:orange", marker="x")
            ax2.set_ylabel(aux_plot["title"])

        text_y_and_alignment = cycle(
            [(min(ObsState), "bottom"), (max(ObsState), "top")]
        )
        for t, command in self._command_log.items():
            ax.axvline(t, color="lightgrey", linestyle="dotted")
            y, alignment = next(text_y_and_alignment)
            ax.text(
                t,
                y,
                command,
                rotation="vertical",
                horizontalalignment="right",
                verticalalignment=alignment,
                color="tab:gray",
                bbox={
                    "boxstyle": "square",
                    "facecolor": "white",
                    "alpha": 0.5,
                    "edgecolor": "white",
                },
            )

        ax.step(
            self._obsstate_log.keys(),
            self._obsstate_log.values(),
            marker="o",
            color="tab:blue",
            where="post",  # step extends after data point
        )
        ax.set_yticks(
            ticks=list(map(lambda state: state.value, ObsState)),
            labels=list(map(lambda state: state.name, ObsState)),
        )
        ax.set_ylabel("ObsState")
        ax.set_xlabel("UNIX Timestamp")

        if ax2 is not None:
            # draw auxiliary graph behind primary (subarray states)
            ax.set_zorder(1)
            ax.set_frame_on(False)

        fig.tight_layout()
        fig.savefig(filename)


# TODO - use a fixture for the ObsStateMonitoringSubarray so we can plot on test fails?


@pytest.mark.hardware_present
def test_ptc1_a_subarray_obsstate(ready_subarray):
    """Test Subarray ObsState transitions."""
    # pylint: disable=too-many-statements
    os.makedirs(BUILD_DIR, exist_ok=True)
    # we don't really need a READY subarray to start, but that fixture already exists,
    # so we might as well use it.
    subarray = ObsStateMonitoringSubarray(
        ready_subarray, os.path.join(BUILD_DIR, "ptc1a_subarray_obsstate.txt")
    )
    good_scan_config = generate_scan_config(1)
    wait_obs_state = partial(wait_for_attr, subarray, "obsState")

    # 1. Turn subarray Tango device off and on
    subarray.Init()
    print(f"Immediately after init, subarray obs state is {subarray.ObsState}")
    wait_obs_state(ObsState.EMPTY, "1. Init didn't move to EMPTY state")
    subarray.Off()
    print(f"Immediately after off, subarray obs state is {subarray.ObsState}")
    wait_obs_state(ObsState.EMPTY, "1. Off didn't move to EMPTY state")
    subarray.On()
    print(f"Immediately after on, subarray obs state is {subarray.ObsState}")
    wait_obs_state(ObsState.EMPTY, "1. On didn't move to EMPTY state")

    # 2. Starting in obsState=EMPTY, then Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "2. AssignResources didn't move to IDLE state",
    )
    # 3. Good subarray configure()
    subarray.Configure(good_scan_config)
    wait_obs_state(ObsState.READY, "3. Configure didn't move to READY state")
    # 4. Scan
    subarray.Scan({"id": 54321})
    wait_obs_state(ObsState.SCANNING, "4. Scan didn't move to SCANNING state")
    # 5. Try to AssignResources during SCANNING
    with pytest.raises(tango.DevFailed):
        subarray.AssignResources({})
    # I can't see how to attach a failure message to `pytest.raises`
    # e.g. Step 5 - didn't see exception when doing a naughty thing ...

    # 6. End Scan
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "6. EndScan didn't move to READY state")

    # 7. Good subarray configure()
    subarray.Configure(good_scan_config)
    # There is a race condition here - we were in READY before, and it's possible to
    # read obsState before it has moved into CONFIGURING.
    # It's also possible that we miss the CONFIGURING state if we look for it!
    if subarray.obsState == ObsState.READY:
        time.sleep(2)  # Allow time for the Subarray to move into CONFIGURING
    wait_obs_state(ObsState.READY, "7. Configure didn't move to READY state")
    # 8. Scan
    subarray.Scan({"id": 123})
    wait_obs_state(ObsState.SCANNING, "8. Scan didn't move to SCANNING state")
    # 9. End Scan
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "9. EndScan didn't move to READY state")

    # 10. Do bad subarray configure()
    subarray.Configure({"foo": "bar"})
    wait_obs_state(
        ObsState.FAULT,
        "10. Bad Configure didn't move to FAULT state",
    )
    # 11. Do obsreset()
    subarray.ObsReset()
    wait_obs_state(ObsState.IDLE, "11. ObsReset didn't move to IDLE state")
    # 12. Good subarray configure()
    subarray.Configure(good_scan_config)
    wait_obs_state(ObsState.READY, "12. Configure didn't move to READY state")
    # 13. Scan
    subarray.Scan({"id": 54321})
    wait_obs_state(
        ObsState.SCANNING,
        "13. Scan didn't move to SCANNING state",
    )
    # 14. End Scan
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "14. EndScan didn't move to READY state")

    # 15. Do bad subarray configure()
    subarray.Configure({"foo": "bar"})
    wait_obs_state(
        ObsState.FAULT,
        "15. Bad Configure didn't move to FAULT state",
    )
    # 16. Restart()
    subarray.Restart()
    wait_obs_state(ObsState.EMPTY, "16. Restart didn't move to EMPTY state")

    # 17. Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "17. AssignResources didn't move to IDLE state",
    )
    # 18. Good subarray configure()
    subarray.Configure(good_scan_config)
    wait_obs_state(ObsState.READY, "18. Configure didn't move to READY state")
    # 19. Scan
    subarray.Scan({"id": 54321})
    wait_obs_state(
        ObsState.SCANNING,
        "19. Scan didn't move to SCANNING state",
    )
    # 20. End Scan
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "20. EndScan didn't move to READY state")
    # 21. End
    subarray.End()
    wait_obs_state(ObsState.IDLE, "21. End didn't move to IDLE state")
    # 22. Release Resources
    subarray.ReleaseAllResources()
    wait_obs_state(
        ObsState.EMPTY,
        "22. ReleaseAllResources didn't move to EMPTY state",
    )

    # 23. Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "23. AssignResources didn't move to IDLE state",
    )
    # 24. Abort
    subarray.Abort()
    wait_obs_state(ObsState.ABORTED, "24. Abort didn't move to ABORTED state")
    # 25. Restart()
    subarray.Restart()
    wait_obs_state(ObsState.EMPTY, "25. Restart didn't move to EMPTY state")
    # 26. Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "26. AssignResources didn't move to IDLE state",
    )
    # 27. Good subarray configure()
    subarray.Configure(good_scan_config)
    wait_obs_state(ObsState.READY, "27. Configure didn't move to READY state")
    # 28. Abort
    subarray.Abort()
    wait_obs_state(ObsState.ABORTED, "28. Abort didn't move to ABORTED state")
    # 29. Restart()
    subarray.Restart()
    wait_obs_state(ObsState.EMPTY, "29. Restart didn't move to EMPTY state")

    subarray.plot_commands_and_obsstate_events(os.path.join(BUILD_DIR, "ptc1a.png"))


class AbortRestartChecker:
    """Check that Subarray Abort/Restart stops Processor."""

    def __init__(self, subarray: ObsStateMonitoringSubarray, log_file_name: str):
        """Create AbortRestartChecker object."""
        # pylint: disable=consider-using-with
        self.subarray = subarray
        self.packet_count_log = OrderedDict()
        """time: Tx packet count"""
        self.logfile = open(log_file_name, "w", encoding="utf-8")

    def __del__(self):
        """Clean-up when deleted."""
        self.logfile.close()

    def log_tx_count(self, processor):
        """Log Processor Transmitted Packets Counter."""
        t = time.time()
        tx_count = processor.stats_io["vis_pkts_out"]
        self.packet_count_log[t] = tx_count
        print(timestr(t), tx_count, file=self.logfile)

    def abort_restart_check(self, _, processor):
        """Perform abort/restart test steps for PTC#1 part B."""
        t_sleep = 31  # Processor.stats_io only updates every ~30s (?!)
        wait_obs_state = partial(wait_for_attr, self.subarray, "obsState")
        print(timestr(), "Abort/restart check started", file=self.logfile)
        self.log_tx_count(processor)
        # sleep a bit to let some scan happen
        time.sleep(t_sleep)
        self.log_tx_count(processor)
        time.sleep(t_sleep)
        self.log_tx_count(processor)
        # 33. Abort
        print(timestr(), "Sending Abort", file=self.logfile)
        self.subarray.Abort()
        wait_obs_state(
            ObsState.ABORTED,
            "33. Abort didn't move to ABORTED state",
        )
        self.log_tx_count(processor)
        # 34. Restart()
        # Should be in EMPTY state and dataflow has stopped for that subarray
        print(timestr(), "Sending Restart", file=self.logfile)
        self.subarray.Restart()
        wait_obs_state(
            ObsState.EMPTY,
            "34. Restart didn't move to EMPTY state",
        )
        self.log_tx_count(processor)
        time.sleep(t_sleep)
        self.log_tx_count(processor)
        print(timestr(), "My work is done", file=self.logfile, flush=True)


@pytest.mark.hardware_present
def test_ptc1_b_subarray_restart_stops_processor(cnic_and_processor, ready_subarray):
    """Test Abort/Restart Subarray Commands."""
    os.makedirs(BUILD_DIR, exist_ok=True)
    subarray = ObsStateMonitoringSubarray(
        ready_subarray, os.path.join(BUILD_DIR, "ptc1b_restart_subarray.txt")
    )
    checker = AbortRestartChecker(
        subarray, os.path.join(BUILD_DIR, "ptc1b_restart_checker.txt")
    )
    scan_config = corr_configs.scan_6stn_1sa_1bm_1ch
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=0,
        scan_config=scan_config,
        firmware_type=Personality.CORR,
        vd_config=generate_cnic_config(scan_config),
        during_scan_callback=checker.abort_restart_check,
    )
    cnic_processor(cnic_and_processor, subarray, test_config)
    subarray.plot_commands_and_obsstate_events(
        os.path.join(BUILD_DIR, "ptc1b_subarray_only.png")
    )
    packet_data = {
        "x": checker.packet_count_log.keys(),
        "y": checker.packet_count_log.values(),
        "title": "Processor Packet Count",
    }
    subarray.plot_commands_and_obsstate_events(
        os.path.join(BUILD_DIR, "ptc1b.png"), aux_plot=packet_data
    )

    # confirm dataflow stopped
    tx_counts = list(checker.packet_count_log.values())
    assert tx_counts[1] > tx_counts[0], "Data never flowed!"
    assert tx_counts[-1] == tx_counts[-2], "34. Restart did not stop data flow"


@pytest.mark.hardware_present
def test_ptc1_c_subarray_engineering_adminmode(cnic_and_processor, ready_subarray):
    """Test Subarray/Processor AdminMode ENGINEERING."""
    # pylint: disable=too-many-statements,invalid-name
    # - Yes we have a lot of steps. Doesn't particularly matter - we are a test script!
    # - adminMode is the canonical attribute name, but it violates snake_case rules
    os.makedirs(BUILD_DIR, exist_ok=True)
    subarray_id = int(ready_subarray.name().split("/")[-1])
    subarray = ObsStateMonitoringSubarray(
        ready_subarray, "build/ptc1/ptc1c_engineering_adminmode.txt"
    )
    processors = cnic_and_processor[1]  # Ignore CNICs
    assert len(processors) >= 3, "Need 3+ Processors for this test"

    engineering_processor = processors[0]
    online_processors = processors[1:]  # Will hold 2+ - guaranteed above
    # We need to ask the Allocator about which Processors are in use per Subarray
    # There is a Processor.subarrayIds attribute, but it doesn't work here
    # because it only updates on delay updates
    allocator = allocator_proxy()
    wait_obs_state = partial(wait_for_attr, subarray, "obsState")

    # begin in EMPTY state
    subarray.Init()
    wait_obs_state(ObsState.EMPTY, "Init didn't move to EMPTY state")
    # 35 Set one Tango Processor device AdminMode to ENGINEERING
    engineering_processor.adminMode = AdminMode.ENGINEERING
    wait_for_attr(
        engineering_processor,
        "adminMode",
        AdminMode.ENGINEERING,
        "35. Processor.AdminMode ENGINEERING didn't work",
    )
    # 36 Set one Tango Subarray device AdminMode to ENGINEERING
    subarray.adminMode = AdminMode.ENGINEERING  # pylint: disable=W0201

    wait_for_attr(
        subarray,
        "adminMode",
        AdminMode.ENGINEERING,
        "36. Subarray.AdminMode ENGINEERING didn't work",
    )
    # 37 Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "37. AssignResources didn't move to IDLE state",
    )
    # 38 Subarray configure()
    subarray.Configure(generate_scan_config(1))
    wait_obs_state(ObsState.READY, "38. Configure didn't move to READY state")
    # Check what Alveo are used by the subarray - should be ENGINEERING Alveo only
    assert serials_per_subarray(allocator)[subarray_id] == {
        engineering_processor.serialNumber
    }, "38. Engineering subarray didn't use expected Processor"

    # 39 Scan()
    subarray.Scan({"id": 54321})
    wait_obs_state(
        ObsState.SCANNING,
        "39. Scan didn't move to SCANNING state",
    )
    # 40 End scan()
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "40. EndScan didn't move to READY state")
    # 41 End()
    subarray.End()
    wait_obs_state(ObsState.IDLE, "41. End didn't move to IDLE state")
    # 42 Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "42. AssignResources didn't move to IDLE state",
    )
    # 43 Configure with configuration larger than number of ENGINEERING Alveo available
    subarray.Configure(generate_scan_config(2))
    wait_obs_state(
        ObsState.FAULT,
        "43. Configure didn't move to FAULT state",
    )

    # 44 Do obsreset()
    subarray.ObsReset()
    wait_obs_state(ObsState.IDLE, "44. ObsReset didn't move to IDLE state")
    # 45 Set Tango Subarray device AdminMode to ONLINE
    subarray.adminMode = AdminMode.ONLINE  # pylint: disable=W0201
    wait_for_attr(
        subarray,
        "adminMode",
        AdminMode.ONLINE,
        "45. Subarray.AdminMode ONLINE didn't work",
    )
    # 46 Configure with configuration larger than number of ENGINEERING Alveo available
    subarray.Configure(generate_scan_config(2))
    wait_obs_state(
        ObsState.READY,
        "46. Configure didn't move to READY state",
    )
    # Check what Alveo are used by the subarray - should be ONLINE Alveo only
    assert serials_per_subarray(allocator)[subarray_id] == {
        processor.serialNumber for processor in online_processors
    }, "46. Online subarray didn't use expected Processors"

    # 47 Scan()
    subarray.Scan({"id": 54321})
    wait_obs_state(
        ObsState.SCANNING,
        "47. Scan didn't move to SCANNING state",
    )
    # 48 End scan()
    subarray.EndScan()
    wait_obs_state(ObsState.READY, "48. EndScan didn't move to READY state")
    # 49 End()
    subarray.End()
    wait_obs_state(ObsState.IDLE, "49. End didn't move to IDLE state")
    # 50 Assign resources()
    subarray.AssignResources({})
    wait_obs_state(
        ObsState.IDLE,
        "50. AssignResources didn't move to IDLE state",
    )
    # 51 Configure with configuration that uses all Alveo (including ENGINEERING Alveo)
    subarray.Configure(generate_scan_config(len(processors)))
    wait_obs_state(
        ObsState.FAULT,
        "51. Configure didn't move to FAULT state",
    )
    # 52 Do obsreset()
    subarray.ObsReset()
    wait_obs_state(ObsState.IDLE, "52. ObsReset didn't move to IDLE state")
    # 53 ReleaseResources()
    subarray.ReleaseAllResources()
    wait_obs_state(
        ObsState.EMPTY,
        "53. ReleaseAllResources didn't move to EMPTY state",
    )
    # 54 Set all Tango Processor devices AdminMode to ONLINE
    for processor in processors:
        processor.adminMode = AdminMode.ONLINE
        wait_for_attr(
            processor,
            "adminMode",
            AdminMode.ONLINE,
            "54. Processor.AdminMode ONLINE didn't work",
        )

    subarray.plot_commands_and_obsstate_events(os.path.join(BUILD_DIR, "ptc1c.png"))
