# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 12 - Visibility stability

Based on Jupyter notebook: PI23/FAT1/PTC12/ptc12.ipynb

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #12 <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E/edit?gid=1341598201#gid=1341598201>` _

1. Configure CNIC with six stations of a complex sinusoid, rms magnitude 2 samples
   with Gaussian noise rms level 1 in a single SPS channel.
2. Configure Correlator Alveo with zero delay for all channels
3. Capture visibilities for 600s and average in 20 second blocks.
4. Show the standard deviation across all average data blocks is less than 0.006%
   which demonstrates compliance to SKAO-CSP_Low_CBF_REQ-43
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
from collections import defaultdict
from copy import deepcopy

import numpy as np
import pytest

from ska_low_cbf_integration.correlator import integration_periods
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser

from .corr_configs import scan_6stn_1sa_1bm_1ch
from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdTestConfig, cnic_processor

MAX_STD_DEVIATION = 0.000_06  # see comment above

# ---------------------------
# Subarray related constants
# ---------------------------
SCAN_ID = 123  # needs to match scan id in scan_6stn_1sa_1bm_1ch
SUBARRAY_ID = 1  # needs to be the same as in ready_subarray test fixture
BEAM_ID = 1  # needs to match beam id in scan_6stn_1sa_1bm_1ch

# ---------------------------
# PCAP/debug file names
# ---------------------------
# NOTE: in a container there's no '/media/' top level directory
DATA_DIR = "/test-data/PTC/ptc12/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH = DATA_DIR + "PTC12_correlator.pcap"
"""PCAP file name"""


def _cnic_vd_cfg(tone_position: float, stations: list, channels: list) -> dict:
    """Return CNIC-VD configuration for given stations, channels and frequency.

    :param tone_position: tone position
    :param stations: a list of (station, substation) tuples
    :param channels: a list of channels
    :return: a list of dictionaries for CNIC-VD configuration
    """
    fine_freq = int(tone_position)
    cfg = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": SCAN_ID,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": chan,
            "beam": BEAM_ID,
            "sources": {
                "x": [
                    {"tone": False, "seed": station, "scale": 80},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 4,
                    },
                ],
                "y": [
                    {"tone": False, "seed": station, "scale": 80},
                    {
                        "tone": True,
                        "fine_frequency": fine_freq,
                        "scale": 4,
                    },
                ],
            },
        }
        for station, substation in stations
        for chan in channels
    ]
    return {"sps_packet_version": 3, "stream_configs": cfg}


CHAN = 448
VIS_CHAN_COUNT = 144
CAPTURE_TIME_SEC = 600


@pytest.mark.timeout(CAPTURE_TIME_SEC + 300)
@pytest.mark.hardware_present
def test_ptc12_corr_visibility_stability(
    cnic_and_processor, ready_subarray
):  # pylint: disable=too-many-locals
    """Implement PTC 12: simulate station data, capture packages, run analysis."""
    # basic settings
    n_stations = 6
    station_ids = list(range(1, 1 + n_stations))
    stations = [[station, 1] for station in station_ids]
    channels = [CHAN]
    # tweak the stock standard scan configuration (see corr_configs.py)
    scan_config = deepcopy(scan_6stn_1sa_1bm_1ch)
    scan_config["lowcbf"]["stations"]["stn_beams"][0]["freq_ids"] = channels

    integration_time_sec = integration_periods[849]

    # output packet related constants
    channel_count = len(channels)
    rx_size = 64
    n_vis = int(CAPTURE_TIME_SEC / integration_time_sec)
    rx_packets = VIS_CHAN_COUNT * n_vis * channel_count

    # Generate / capture packets
    # --------------------------
    test_config = ProcessorVdTestConfig(
        cnic_capture_min_size=rx_size,
        cnic_capture_packets=rx_packets,
        firmware_type=Personality.CORR,
        scan_config=scan_config,
        vd_config=_cnic_vd_cfg(0, stations, channels),
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,  # proc packets after end of scan
        during_scan_callback=None,
    )
    f_handle = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH,
    )
    f_handle.close()  # Anayser will consume the file's contents

    # Analysis
    # --------
    analyser_cfg = {"coarse_channels": channels, "stations": station_ids}
    visibilities = VisibilityAnalyser(json.dumps(analyser_cfg))
    visibilities.extract_spead_data(PCAP_PATH)
    n_integrations = 5
    visibilities.remove_visibilities(n_integrations)

    visibilities.populate_correlation_matrix()

    # put all visibilities across all polarisations inside a single vector
    # so that we can cut in slices of 20s
    flat_version = defaultdict(list)
    for timing in visibilities.time_sequence:
        for i in range(n_stations):
            for j in range(i + 1):
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i][2 * j]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i + 1][2 * j]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i][2 * j + 1]
                )
                flat_version[timing].append(
                    visibilities.corr_abs_matrixes_time[timing][CHAN][2 * i + 1][
                        2 * j + 1
                    ]
                )

    flat_version_20 = defaultdict(list)
    integrations_to_average = round(20 / integration_time_sec)
    for i, timing in enumerate(visibilities.time_sequence):
        flat_version_20[i // integrations_to_average].extend(flat_version[timing])

    average = []
    std_dev = []
    for key, values in flat_version_20.items():
        average.append(np.mean(values))
        std_dev.append(np.std(values) / np.mean(values))
        print(
            f"Standard Dev for period {key} of 20 seconds: "
            f"{np.std(values)/np.mean(values)}"
        )
    overall_std_dev = np.std(average) / np.mean(average)
    print(
        "standard deviation across all average data blocks of 20 seconds: "
        f"{overall_std_dev}"
    )
    ptc12_report(test_config, std_dev, overall_std_dev)
    assert overall_std_dev <= MAX_STD_DEVIATION


def ptc12_report(test_config, std_devs, overall_std_dev):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66fdfae53408945acf69174f"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )
    replace_word_in_report(
        file_result_to_replace,
        "STD_DEV_GLOBAL",
        f"{overall_std_dev:.2E}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "SUCEESS_TEST",
        f"{overall_std_dev<= MAX_STD_DEVIATION}",
    )
    for index_std, std_dev in enumerate(std_devs):
        replace_word_in_report(
            file_result_to_replace,
            f"STD_DEV{index_std:02d}",
            f"{std_dev:.02E}",
        )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.index.commit("Test with python")
    repo.git.push()
