# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 27 - PST Beam SNR at different pointing offsets.

PSS equivalent: PTC 36

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #27 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
import shutil
from collections import defaultdict
from datetime import datetime
from typing import Dict, Iterable

import matplotlib.pyplot as plt
import numpy as np
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import payloads
from ska_low_cbf_integration.psr_analyser import PulsarPacket

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

BUILD_DIR = "build/ptc27"
"""Build directory for intermediate analysis products & plots."""

DATA_DIR = "/test-data/PTC/ptc27/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC27_beamformer.pcap"
"""PCAP file name"""

PCAP_PATH_DIFF_BEAM = DATA_DIR + "PTC27_different_beamformer.pcap"
"""PCAP file name for different beam direction"""

TEST_TIMEOUT_SEC = 1000
ALLOCATOR_ADDR = "low-cbf/allocator/0"
ID_ALL_STN = 11  # ID number of beam with all stations contributing
ID_ONE_STN = 12  # ID number of beam with only one station contributing

station_ids = (
    list(range(1, 5))
    + list(range(303, 327))
    + list(range(339, 357))
    + list(range(363, 399))
    + list(range(429, 441))
    + list(range(471, 477))
)

station_beam_dir = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
# Half Power Beam Width (HPBW) is ~0.1 degrees, so let's use a 5' offset.
offset_dir = {"ra": "2h30m00.00s", "dec": "-25d49m45.0s"}

station_beam_delay = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": station_beam_dir,
}
delay_aligned = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [station_beam_dir] * 4,
}
delay_offset = {
    "subarray_id": 1,
    "beam_id": 1,
    "direction": [offset_dir] * 4,
}
station_weights = {
    ID_ALL_STN: [1.0] * len(station_ids),
    ID_ONE_STN: [1.0] + [0.0] * (len(station_ids) - 1),
}

DELAY_SUB = "low-cbf/delaypoly/0/delay_s01_b01"
PST_SUB = "low-cbf/delaypoly/0/pst_s01_b01_"
PST_SERVER_IP = "192.168.55.55"
pst_beam_ids = [ID_ALL_STN, ID_ONE_STN]

stations_beam = [[station, 1] for station in station_ids]
stations = [[station, 1] for station in station_ids]
channels = list(range(64, 448, 24))

# Generate CNIC-VD Configuration
cnic_config = {
    "sps_packet_version": 3,
    "stream_configs": [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": 1,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
                "y": [
                    {"tone": False, "seed": 1981, "scale": 4000},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ],
}
"""CNIC VD Configuration, shared between PTC 27 & PTC 36."""

# Generate Scan Configuration
scan_config = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {"beam_id": 1, "freq_ids": channels, "delay_poly": DELAY_SUB}
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": pst_beam,
                    "stn_beam_id": 1,
                    "delay_poly": PST_SUB + str(station_idx + 1),
                    "jones": "tbd",
                    "destinations": [
                        {
                            "data_host": PST_SERVER_IP,
                            "data_port": 11001,
                            "start_channel": 0,
                            "num_channels": 144,
                        }
                    ],
                    "stn_weights": station_weights[pst_beam],
                }
                for station_idx, pst_beam in enumerate(pst_beam_ids)
            ],
        },
    },
}
"""
PTC 27 Scan Configuration.
Can't be shared with PTC 36 because PSS doesn't allow different weights per beam.
"""


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc27_pst_beam_snr(cnic_and_processor, ready_subarray):
    """
    Implement PTC 27.

    In this test, we aim to show that:
    * PST beams can be steered in a different direction to the station beam
    * the FPGA can apply appropriate delays to steer pulsar beams
    """
    print(datetime.now(), "Starting PTC#27")
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)

    allocator = tango.DeviceProxy(ALLOCATOR_ADDR)
    allocator.internalAlveoLimits = json.dumps(
        {
            "pst": {
                "vch": 900,
            },
        }
    )

    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        # 5 sec of data
        cnic_capture_packets=1356 * 5 * len(channels) * len(pst_beam_ids),
        scan_config=scan_config,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        beam_delay_config=[station_beam_delay],
        pst_delay_config=delay_aligned,
        source_delay_config=[delay_aligned],
        do_fw_switcharoo=True,
    )
    print(datetime.now(), "Running 'aligned' scenario")
    freqs_aligned, pwr_aligned = freq_power_per_beam(
        payloads(
            cnic_processor(
                cnic_and_processor, ready_subarray, test_config, PCAP_PATH_SAME_BEAM
            )
        ),
        pst_beam_ids,
    )
    plot_power_ratios(
        freqs_aligned[ID_ALL_STN],
        np.array(pwr_aligned[ID_ALL_STN]) / np.array(pwr_aligned[ID_ONE_STN]),
        os.path.join(BUILD_DIR, "ratio.png"),
        "PST",
    )
    ratio_same_dir = np.mean(pwr_aligned[ID_ALL_STN]) / np.mean(pwr_aligned[ID_ONE_STN])
    req_same_dir = ratio_same_dir == pytest.approx(len(stations) ** 2, rel=0.02)

    # Repeat with offset
    print(datetime.now(), "Running 'offset' scenario")
    test_config.pst_delay_config = delay_offset
    test_config.source_delay_config = [delay_offset]
    freqs_offset, pwr_offset = freq_power_per_beam(
        payloads(
            cnic_processor(
                cnic_and_processor, ready_subarray, test_config, PCAP_PATH_DIFF_BEAM
            )
        ),
        pst_beam_ids,
    )
    plot_power_ratios(
        freqs_offset[ID_ALL_STN],
        np.array(pwr_offset[ID_ALL_STN]) / np.array(pwr_offset[ID_ONE_STN]),
        os.path.join(BUILD_DIR, "ratio_beam.png"),
        "PST",
    )
    ratio_diff_dir = np.mean(pwr_offset[ID_ALL_STN]) / np.mean(pwr_offset[ID_ONE_STN])
    req_diff_dir = ratio_diff_dir == pytest.approx(len(stations) ** 2, rel=0.02)

    ptc27_ptc36_report(
        test_config.scan_config,
        BUILD_DIR,
        repo_hash="6708be09ad0abe35ba2b5148",
        req_offset_met=req_diff_dir,
        req_aligned_met=req_same_dir,
        ratio_offset=ratio_diff_dir,
        ratio_aligned=ratio_same_dir,
    )
    assert req_same_dir, f"Aligned case ratio {ratio_same_dir} != {len(stations)**2}"
    assert req_diff_dir, f"Offset case ratio {ratio_diff_dir} != {len(stations)**2}"
    print(datetime.now(), "End of PTC#27")


def freq_power_per_beam(
    udp_payloads: Iterable[bytes], psr_beam_ids: Iterable[int]
) -> (Dict[int, list], Dict[int, list]):
    """
    Get frequencies and power for the given beams (PSS or PST).

    :returns: {beam: [channels]}, {beam: [average power per sample for each channel]}
    """
    # pylint: disable=too-many-locals, cell-var-from-loop
    total_freqs = {}
    total_pwr = {}
    ch_per_packet = None  # need to read from a packet
    # accumulate per-pst-channel power with first-chan as key
    chan_pkts = {beam: defaultdict(lambda: 0) for beam in psr_beam_ids}
    # we don't know how many channels yet, so we create this in the loop
    np_chan_pwr = None

    # get sample by sample power from packet payloads
    for pkt_payload in udp_payloads:
        psr_packet = PulsarPacket(pkt_payload)
        # allow for either PST/PSS
        if ch_per_packet is None:
            ch_per_packet = psr_packet.n_channels
            np_chan_pwr = {
                beam: defaultdict(lambda: np.zeros(ch_per_packet))
                for beam in psr_beam_ids
            }
        if psr_packet.beam_id not in psr_beam_ids:
            continue
        first_chan = psr_packet.first_channel
        beam_id = psr_packet.beam_id
        np_chan_pwr[beam_id][first_chan] += psr_packet.channel_power()
        chan_pkts[beam_id][first_chan] += 1

    for beam in psr_beam_ids:
        # get average power-per-sample numbers by dividing by number added
        for first_chan in np_chan_pwr[beam]:
            np_chan_pwr[beam][first_chan] = (
                np_chan_pwr[beam][first_chan] / chan_pkts[beam][first_chan]
            )

        freqs = []
        pwr = []
        ordered_chans = sorted(np_chan_pwr[beam].keys())
        for channel in ordered_chans:
            frequencies = [channel + i for i in range(0, ch_per_packet)]
            freqs.extend(frequencies)
            pwr.extend(np_chan_pwr[beam][channel].tolist())
        total_freqs[beam] = freqs
        total_pwr[beam] = pwr

    return total_freqs, total_pwr


def ptc27_ptc36_report(
    scan_config_,
    build_dir,
    repo_hash="6708be09ad0abe35ba2b5148",
    req_aligned_met: bool = False,
    req_offset_met: bool = False,
    ratio_aligned: int = -1,
    ratio_offset: int = -1,
):
    """
    Generate Test Report for PTC27/PTC36.

    :param repo_hash: Overleaf repository hash
    :param mode: PSS/PST selector (for graphs)
    """
    # pylint: disable=too-many-arguments
    local_directory, new_repo_name, repo = download_overleaf_repo(repo_hash)

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config_),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ272",
        str(req_offset_met),
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ271",
        str(req_aligned_met),
    )
    replace_word_in_report(
        file_result_to_replace,
        "AVG272",
        f"{ratio_offset:,.2f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "AVG271",
        f"{ratio_aligned:,.2f}",
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    for plot in ("ratio.png", "ratio_beam.png"):
        dst = os.path.join(local_directory, new_repo_name, plot)
        shutil.copy(os.path.join(build_dir, plot), dst)
        repo.git.add(dst)

    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def plot_power_ratios(channel_ids, power_ratios, filename: str, mode_name: str):
    """
    Plot Power Ratio per channel (with average and expected values) to a file.

    :param channel_ids: for X-axis positions
    :param power_ratios: 100 station power / 1 station power (Y-axis)
    :param filename: where to save the plot
    :param mode_name: PSS/PST
    """
    plt.grid()
    plt.hlines(
        np.mean(power_ratios),
        channel_ids[0],
        channel_ids[-1],
        "c",
        label="Average",
    )
    plt.plot(channel_ids, power_ratios, label="Observed", linestyle="None", marker=".")
    plt.hlines(
        10000,
        channel_ids[0],
        channel_ids[-1],
        "r",
        "--",
        label="Theoretical",
    )
    plt.title(
        f"Ratio of {mode_name} beam power per complex sample\n"
        "100 stations : 1 station"
    )
    plt.xlabel(f"{mode_name} channel number")
    plt.ylabel("Ratio 100 stations : 1 station power (linear scale)")
    plt.legend()
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()
