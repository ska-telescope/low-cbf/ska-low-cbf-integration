# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 31 - PST trade stations for bandwidth

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #31 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pytest
import tango

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes, pcap_stats
from ska_low_cbf_integration.pst_utils import freq_power_per_beam
from ska_low_cbf_integration.sps import SPS_COARSE_SPACING_HZ

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc31/"
"""Directory name where data file will be stored (PCAP)"""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC31_beamformer"
"""PCAP file name"""


TEST_TIMEOUT_SEC = 2000
SAMPLE_RATE = (SPS_COARSE_SPACING_HZ / 216) * (4 / 3)
MAX_NUMBER_SPS_CHANNELS = 384
N_POL = 2
N_VALS_PER_CPLX = 2
N_BYES_PER_VAL = 2
DELAY_EMULATOR_ADDR = "low-cbf/delaypoly/0"

station_ids = (
    list(range(1, 5))
    + list(range(303, 327))
    + list(range(339, 357))
    + list(range(363, 399))
    + list(range(429, 441))
    + list(range(471, 477))
)

PST_BEAM_ID = 11

CNIC_FINE_FREQ = 2
SUBARRAY_ID = 1
STATION_BEAM_ID = BeamId.PTC31
TONE_POSITION = 96 + 12
STATION_CHANNEL = {4: 384, 8: 192, 16: 96, 32: 48, 64: 24}
PST_SVR_IP = "192.168.55.55"
ALLOCATOR_ADDR = "low-cbf/allocator/0"


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": "-26d34m50.0s",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": "-26d34m50.0s",
    }
    src_radec_beam_3 = {"ra": "2h30m00.00s", "dec": "-26d34m50.0s"}
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": "-26d34m50.0s",
    }
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }
    pstdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }

    return beamdir, srcdir, pstdirs


@pytest.mark.parametrize("n_stations", (4, 8, 16, 32, 64))
@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc31_pst_trading_stations_for_bw(
    cnic_and_processor, ready_subarray, n_stations  # noqa
):  # noqa
    """Implement PTC 31."""
    # pylint: disable=too-many-locals
    print(datetime.now(), "Starting PTC#34")
    # Generate Scan Configuration
    # N_STATIONS = 5
    stations = [[station, 1] for station in station_ids[:n_stations]]
    channels = [64 + channel for channel in range(STATION_CHANNEL[n_stations])]
    pst_beam_id = [11, 12]
    station_weights = {
        11: [1.0] * n_stations,
        12: [1.0] + [0.0] * (n_stations - 1),
    }

    delay_sub = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}"
    pst_sub = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}_"
    scan_config_same_beam = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": stations,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": channels,
                        "delay_poly": delay_sub,
                    }
                ],
            },
            "timing_beams": {
                "beams": [
                    {
                        "pst_beam_id": pst_beam,
                        "stn_beam_id": STATION_BEAM_ID,
                        "delay_poly": pst_sub + str(station_idx + 1),
                        "jones": "tbd",
                        "destinations": [
                            {
                                "data_host": PST_SVR_IP,
                                "data_port": 11001,
                                "start_channel": 0,
                                "num_channels": 144,
                            }
                        ],
                        "stn_weights": station_weights[pst_beam],
                    }
                    for station_idx, pst_beam in enumerate(pst_beam_id)
                ],
            },
        },
    }

    # Generate CNIC-VD Configuration
    cnic_config_noise = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": STATION_BEAM_ID,
            "sources": {
                "x": [
                    {
                        "tone": False,
                        "seed": 1000 + channel,
                        "scale": 2000 + 5 * index_channel,
                    },
                ],
                "y": [
                    {
                        "tone": False,
                        "seed": 2000 + channel,
                        "scale": 2000 + 5 * index_channel,
                    },
                ],
            },
        }
        for station, substation in stations
        for index_channel, channel in enumerate(channels)
    ]

    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    allocator = tango.DeviceProxy(ALLOCATOR_ADDR)
    allocator.internalAlveoLimits = json.dumps(
        {
            "pst": {
                "vch": (512 if n_stations in [4, 8] else 900),
            },
        }
    )
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=500_000,  # 1356 * len(channels) * len(pst_beam_id) * 5,
        scan_config=scan_config_same_beam,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
        do_fw_switcharoo=True,
    )
    correlator_output_beam = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        PCAP_PATH_SAME_BEAM + f"_{n_stations}_stations.pcap",
    )
    payloads_beam = get_udp_payload_bytes(correlator_output_beam.name, 64)
    total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads_beam, pst_beam_id)
    stats = pcap_stats(correlator_output_beam)
    ratio, req_pps, req_bps, req_pac = _ptc31_report(
        test_config.scan_config,
        total_freqs_beam,
        total_pwr_beam,
        n_stations,
        stats,
    )
    print(f"pcap statistics: {stats}")
    square_stations = n_stations**2
    assert square_stations * 0.98 < np.mean(ratio) < square_stations * 1.02
    assert req_pps
    assert req_bps
    assert req_pac
    print(datetime.now(), "End of PTC#31")


def _ptc31_report(scan_config, total_freqs_beam, total_pwr_beam, n_station, stats):
    """Generate Test Report."""
    # pylint: disable=too-many-locals
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "67299a4307900c15c224a942"
    )
    pps = stats.n_packets / stats.duration
    bps = stats.total_bytes * 8 / stats.duration
    ratio = print_figures_for_ptc31_report(
        total_pwr_beam,
        total_freqs_beam,
        n_station,
        local_directory,
        new_repo_name,
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    target_pps = 2712 * STATION_CHANNEL[n_station]
    target_bps = 137335680 * STATION_CHANNEL[n_station]
    target_average = n_station**2
    req_pps = abs(pps - target_pps) / target_pps <= 0.02
    req_bps = abs(bps - target_bps) / target_bps <= 0.02
    req_pac = stats.packet_sizes == {6330}
    words_to_replace = {
        "NSTATIONS": n_station,
        "NSQUARE": target_average,
        "RATIO6O1": np.mean(ratio),
        "REQ6O1": 0.98 * target_average < np.mean(ratio) < 1.02 * target_average,
        "BPS11": f"{bps:.3f}",
        "TARGETBPS": target_bps,
        "REQBPS": req_bps,
        "TARGETPPS": target_pps,
        "PPS11": f"{pps:.3f}",
        "REQPPS": req_pps,
        "PAC11": stats.packet_sizes,
        "REQPAC": req_pac,
    }
    for word_to_replace, replacement in words_to_replace.items():
        replace_word_in_report(
            file_result_to_replace,
            word_to_replace,
            str(replacement),
        )

    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ratio.png")
    repo.git.add(new_repo_name + "/beam11.png")
    repo.git.add(new_repo_name + "/beam12.png")
    repo.index.commit("Test with python")
    repo.git.push()

    return ratio, req_pps, req_bps, req_pac


def print_figures_for_ptc31_report(
    total_pwr_beam,
    total_freqs_beam,
    n_station,
    local_directory: str,
    new_repo_name: str,
):
    """Print the figures for the report."""
    if not os.path.isdir(local_directory + new_repo_name):
        print(f"{local_directory + new_repo_name} path does not exist")
        return False

    ratio = []
    for idx, pwr in enumerate(total_pwr_beam[11]):
        ratio.append(pwr / total_pwr_beam[12][idx])

    plt.plot(total_freqs_beam[11], ratio, label="Ratio 6st/1st")
    plt.hlines(
        n_station**2,
        total_freqs_beam[11][0],
        total_freqs_beam[11][-1],
        "r",
        "--",
        label="Theoretical",
    )
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/ratio.png", format="png")

    plt.figure()
    plt.plot(total_freqs_beam[11], total_pwr_beam[11], "g", label="beam 11")
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power (linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam11.png", format="png")

    plt.figure()
    plt.plot(total_freqs_beam[12], total_pwr_beam[12], "c", label="beam 12")
    plt.title("Average PST beam power per complex sample")
    plt.xlabel("PST channel number")
    plt.ylabel("Power linear scale)")
    plt.legend()
    plt.grid()
    plt.savefig(local_directory + new_repo_name + "/beam12.png", format="png")

    return ratio
