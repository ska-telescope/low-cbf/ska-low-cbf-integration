# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 41: PSS Beams use single station beam.

(Similar to PTC32 - PST equivalent)
Summary:
    Show that PSS beams come from one station beam (not a mixture of station beams)
    - create CNIC configuration producing data for two station-beams
      - source in first beam is noise with power rising with channel number
      - source in second beam is noise with power falling with channel number
    - create a subarray configuration that forms a PSS beam from each of the station
    beams
    - capture PSS beam output in CNIC
    - Test pass:
        A. power spectrum of first beam has expected power increase with channel
        B. power spectrum of second beam has expected power decrease with channel

More information:
    - `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
    - `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
    - `PTC #41 Presentation <https://where.is.it>`_  # TODO
"""  # noqa # To allow the URLs to stay in one line
# pylint: enable=line-too-long

import json
import os
import shutil
import time
from typing import Iterable

import matplotlib.pyplot as plt
import numpy as np
import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pcap import get_udp_payload_bytes
from ska_low_cbf_integration.psr_analyser import PulsarPacket

from .latex_reporting import download_overleaf_repo, replace_word_in_report  # noqa
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

TEST_TIMEOUT_SEC = 900
DATA_DIR = "/test-data/PTC/ptc41/"
PCAP_FILENAME = "ptc41.pcap"
PCAP_PATH = DATA_DIR + PCAP_FILENAME

SUBARRAY_ID = 1
STATIONS = [321, 351, 381, 393, 399, 429, 1, 2]  # must be N*4 stns for PSS
PSS_BEAM_IDS = [1, 502]
STN_BEAM_CFG = {  # Beams for a single subarray
    BeamId.SINGLE_BASIC: {
        "chans": list(range(64, 64 + 64)),  # should be 64 chan
        "beam_dirn": {"ra": "2h30m00.0s", "dec": "-26d49m50.0s"},
        "pss": [
            {
                "beam_id": PSS_BEAM_IDS[0],
                "dirn": {"ra": "2h30m00.0s", "dec": "-29d49m50.0s"},
            }
        ],
        "srcs": [
            {"ra": "2h30m00.0s", "dec": "-29d49m50.0s", "type": "nr", "level": 4000},
        ],
    },
    BeamId.PTC41: {
        "chans": list(range(415, 415 + 32)),  # should be 32 chan
        "beam_dirn": {"ra": "00h30m00.0s", "dec": "-6d49m50.0s"},
        "pss": [
            {
                "beam_id": PSS_BEAM_IDS[1],
                "dirn": {"ra": "00h30m00.0s", "dec": "-7d49m50.0s"},
            }
        ],
        "srcs": [
            {"ra": "00h30m00.0s", "dec": "-7d49m50.0s", "type": "nf", "level": 4000},
        ],
    },
}
SUBARRAY_CFG = {"stns": STATIONS, "beams": STN_BEAM_CFG}

STARTING_EPOCH = 131800  # seconds after SKA epoch
FRAMES_TO_CAPTURE = 10  # 53msec per frame (300 = 16s)
RX_WAIT_SECS = 60  # CNIC capture timeout - 16s plus 8s start would be enough

MIN_PACKET_SIZE = 200  # to avoid PTP packets


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc41_pss_beam_single_station_beam(  # pylint: disable=too-many-locals
    cnic_and_processor, ready_subarray
):
    """Implement PTC 41."""
    time_start = time.time()

    os.makedirs(DATA_DIR, exist_ok=True)

    stn_bm_dly, src_dly, pss_dly = delay_config(SUBARRAY_ID, SUBARRAY_CFG)
    print(f"stn_bm_dly = {stn_bm_dly}")
    print(f"src_dly = {src_dly}")
    print(f"pss_dly = {pss_dly}")
    scan_cfg = pss_scan_config(SUBARRAY_ID, SUBARRAY_CFG, pss_dly)
    print(f"subarray_cfg: {scan_cfg}")
    cnic_cfg = cnic_config(SUBARRAY_ID, SUBARRAY_CFG)
    # print(f"cnic_cfg = {cnic_cfg}")

    n_beam_ch = num_pss_beam_chans(SUBARRAY_CFG)
    print(f"{n_beam_ch} beam channels in total")
    # 48 PSS pkts/frame/chanl
    cnic_capture_packets = n_beam_ch * 48 * FRAMES_TO_CAPTURE

    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=MIN_PACKET_SIZE,
        cnic_capture_packets=cnic_capture_packets,
        scan_config=scan_cfg,
        firmware_type=Personality.PSS,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_cfg,
        during_scan_callback=None,
        source_delay_config=src_dly,
        beam_delay_config=stn_bm_dly,
        pst_delay_config=pss_dly,
    )
    pss_pcap = cnic_processor(
        cnic_and_processor,
        ready_subarray,
        test_config,
        pcap_file_name=PCAP_PATH,
    )
    pss_pcap.close()

    time_capture = time.time()
    print(f"capture data: {time_capture-time_start:.3f}secs")

    # Analyse captured data
    payloads = get_udp_payload_bytes(PCAP_PATH, min_udp_data_size=64)
    total_freqs_beam, total_pwr_beam = freq_power_per_beam(payloads, PSS_BEAM_IDS)

    # plot the graphs
    # ---------------
    colours = ["blue", "green", "red", "purple"]
    n_colours = len(colours)
    slope_linear_reg = {}
    for i, beam in enumerate(PSS_BEAM_IDS):
        colour = colours[i % n_colours]
        plt.clf()
        plt.plot(
            total_freqs_beam[beam],
            total_pwr_beam[beam],
            "g",
            label=f"PSS beam_{beam}",
            color=colour,
        )
        plt.title("Beam power spectrum")
        plt.xlabel("PSS channel number")
        plt.ylabel("Power (linear scale)")
        plt.legend()
        plt.grid()
        # plt.savefig(f"{DATA_DIR}beam_{beam}.jpg", format="jpeg")

        # Fit first order polynomial to get slope of frequency spectrum
        coeffs = np.polyfit(total_freqs_beam[beam], total_pwr_beam[beam], 1)
        slope_direction = "asc" if beam == PSS_BEAM_IDS[0] else "dsc"
        slope_linear_reg[slope_direction] = coeffs[0]

        plt.savefig(f"{DATA_DIR}beam_{beam}_{slope_direction}.jpg", format="jpeg")

    time_end = time.time()
    print(f"analysis: {time_end-time_capture:.3f}secs")
    print(f"elapsed: {time_end-time_start:.3f}secs")

    overleaf_report_update(
        test_config.scan_config, slope_linear_reg["asc"], slope_linear_reg["dsc"]
    )
    assert slope_linear_reg["asc"] > 0, "Expected plot to have positive slope"
    assert slope_linear_reg["dsc"] < 0, "Expected plot to have negative slope"
    # assert 0, "End of PTC41 (forced fail)"


def overleaf_report_update(scan_config, slope_asc, slope_dsc):
    """Generate Overleaf test report."""
    repo_id = "672aae327822fdc7e8280190"
    git_workarea, test_run_subdir, repo = download_overleaf_repo(repo_id)

    test_results_dir = f"{git_workarea}{test_run_subdir}/"
    report_file = test_results_dir + "report.tex"

    for key, val in (
        ("TOPUTDIRECTORY", test_run_subdir),
        ("CONFIGURE_SCAN", json.dumps(scan_config)),
        ("TOPUTDATE", test_run_subdir.split("_")[1]),
        ("SLOPE_ASC", f"{slope_asc:.3f}"),
        ("REQ_ASC", str(slope_asc > 0)),
        ("SLOPE_DSC", f"{slope_dsc:.3f}"),
        ("REQ_DSC", str(slope_dsc < 0)),
    ):
        replace_word_in_report(report_file, key, val)

    # FIXME: The hard-coded beam indexes are bad (copied from PTC32)
    for figure in (
        f"beam_{PSS_BEAM_IDS[0]}_asc.jpg",
        f"beam_{PSS_BEAM_IDS[1]}_dsc.jpg",
    ):
        shutil.copy(f"{DATA_DIR}{figure}", test_results_dir)
        repo.git.add(f"{test_run_subdir}/{figure}")

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + test_run_subdir
        + """/report}"""
    )
    replace_word_in_report(
        git_workarea + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )

    repo.git.add("report2017.tex")
    repo.git.add(test_run_subdir + "/report.tex")

    repo.index.commit("Test with python")
    repo.git.push()


def num_pss_beam_chans(subarray_cfg) -> int:
    """Calculate total number of PST beam channels."""
    n_beam_ch = 0
    for stn_beam_spec in subarray_cfg["beams"].values():
        n_ch = len(stn_beam_spec["chans"])
        n_pss_bm = len(stn_beam_spec["pss"])
        n_beam_ch += n_ch * n_pss_bm
    return n_beam_ch


def delay_config(sub_id, subarray_cfg) -> tuple:
    """Create CNIC configuration dictionary.

    :param sub_id: integer subarray ID
    :param subarray_cfg: specification of subarray configuration
    :return: subarray configuration dictionary
    """
    # Processors subscribe to station beam delays generated by DelayPolySim
    stn_beam_dlys = []
    for beam_id, beam_spec in subarray_cfg["beams"].items():
        # Arguments for delayPolysim "BeamDelay" command
        dly = {
            "subarray_id": sub_id,
            "beam_id": beam_id,
            "direction": beam_spec["beam_dirn"],
        }
        stn_beam_dlys.append(dly)

    # CNICS subscribe to source delays generated by DelayPolySim
    src_dlys = []
    for beam_id, beam_spec in subarray_cfg["beams"].items():
        # Arguments for delayPolySim "SourceRaDec"command
        dirs = []
        for dirn in beam_spec["srcs"]:
            dirs.append({"ra": dirn["ra"], "dec": dirn["dec"]})
        # CNIC expects to subscribe 4 sources.
        while len(dirs) < 4:
            dirs.append(
                {"ra": beam_spec["srcs"][-1]["ra"], "dec": beam_spec["srcs"][-1]["dec"]}
            )
        dly = {"subarray_id": sub_id, "beam_id": beam_id, "direction": dirs}
        src_dlys.append(dly)

    # Processors subscribe to PST/PSS delays
    # because CI test framework only allows one PST subarray/beam we flatten
    # all PST delays to subarray 0, beam 0, and adjust the strings in
    # the PST config later
    all_pst_dlys = []
    for beam_id, beam_spec in subarray_cfg["beams"].items():
        pss_dirs_for_stn_bm = {
            "subarray_id": sub_id,
            "beam_id": beam_id,
            "direction": [],
        }
        for pss_bm in beam_spec["pss"]:
            pss_ra = pss_bm["dirn"]["ra"]
            pss_dec = pss_bm["dirn"]["dec"]
            pss_dirs_for_stn_bm["direction"].append({"ra": pss_ra, "dec": pss_dec})
        all_pst_dlys.append(pss_dirs_for_stn_bm)

    return stn_beam_dlys, src_dlys, all_pst_dlys


def cnic_config(sub_id, subarray_cfg) -> dict:
    """Create CNIC configuration dictionary.

    :param sub_id: integer subarray ID
    :param subarray_cfg: specification of subarray configuration
    :return: subarray configuration dictionary
    """
    stn_bm_cfg = subarray_cfg["beams"]
    stn_ids = subarray_cfg["stns"]

    sc_list = []
    for stn_bm_id, spec in stn_bm_cfg.items():
        for freq_id in spec["chans"]:
            for stn_id in stn_ids:
                stream_cfg = {
                    "scan": 0,  # this is in packet but unused, dropped by FPGA
                    "subarray": sub_id,
                    "beam": stn_bm_id,
                    "frequency": freq_id,
                    "station": stn_id,
                    "substation": 1,
                    "sources": {
                        "x": [],
                        "y": [],
                    },
                }
                for src in spec["srcs"]:
                    # there are 383 channels, numbered 64-448
                    if src["type"] == "nr":  # noise that rises with channel
                        scale = int(src["level"] * (0.2 + 0.8 * (freq_id - 64) / 383))
                    elif src["type"] == "nf":  # noise that falls with channel
                        scale = int(src["level"] * (0.2 + 0.8 * (448 - freq_id) / 383))
                    srcx = {"tone": False, "seed": 1000 + freq_id, "scale": scale}
                    stream_cfg["sources"]["x"].append(srcx)
                    srcy = {"tone": False, "seed": 2000 + freq_id, "scale": scale}
                    stream_cfg["sources"]["y"].append(srcy)

                sc_list.append(stream_cfg)

    config = {
        "sps_packet_version": 3,
        "stream_configs": sc_list,
    }

    return config


def pss_scan_config(  # pylint: disable=too-many-locals
    sub_id, subarray_cfg, pss_dly
) -> dict:
    """Create PSS subarray configuration dictionary.

    :param sub_id: integer subarray ID
    :param subarray_cfg: specification of subarray config
    :param pss_dly: "PST" delay config given to CI
    :return: subarray configuration dictionary
    """
    stn_bm_cfg = subarray_cfg["beams"]
    stn_ids = subarray_cfg["stns"]
    # Create list of station-beams
    stnbm_list = []
    for stn_bm_id, spec in stn_bm_cfg.items():
        cfg_item = {
            "beam_id": stn_bm_id,
            "freq_ids": spec["chans"],
            "delay_poly": "low-cbf/delaypoly/0/delay_s"
            + (f"{sub_id:02d}_b{stn_bm_id:02d}"),
        }
        stnbm_list.append(cfg_item)

    # create list of PSS beams
    cnic_src_num = 1
    wts = [1.0]  # all stations used
    pssbeam_list = []
    for stn_bm_id, spec in stn_bm_cfg.items():
        # find list of PSS delays for this station beam
        pss_bm_dlys = []
        for pst_dly_spec in pss_dly:
            if (
                pst_dly_spec["subarray_id"] == sub_id
                and pst_dly_spec["beam_id"] == stn_bm_id
            ):
                pss_bm_dlys = pst_dly_spec["direction"]
        assert len(pss_bm_dlys) > 0

        for pss_bm_spec in spec["pss"]:
            # Find which of the delay atributes is for this PSS beam
            pss_bm_ra = pss_bm_spec["dirn"]["ra"]
            pss_bm_dec = pss_bm_spec["dirn"]["dec"]
            pss_poly_idx = pss_bm_dlys.index({"ra": pss_bm_ra, "dec": pss_bm_dec}) + 1

            pss_bm_id = pss_bm_spec["beam_id"]
            pss_dly_poly = f"low-cbf/delaypoly/0/pst_s{sub_id:02d}"
            pss_dly_poly += f"_b{stn_bm_id:02d}_{pss_poly_idx}"
            pss_bm_item = {
                "pss_beam_id": pss_bm_id,
                "stn_beam_id": stn_bm_id,
                "delay_poly": pss_dly_poly,
                "jones": "tbd",
                "destinations": [
                    {
                        "data_host": "192.168.0.1",
                        "data_port": 11001,
                        "start_channel": 0,
                        "num_channels": len(spec["chans"]) * 54,  # 54 PSS chans/coarse
                    },
                ],
                "stn_weights": wts,
            }
            pssbeam_list.append(pss_bm_item)
            cnic_src_num += 1
            if cnic_src_num > 4:
                print("ERROR CNIC has only 4 sources")
                cnic_src_num = 4  # re-use source 4 as direction

    # Add components into a complete subarray configuration request
    pss_cfg = {
        "id": 123,
        "lowcbf": {
            "stations": {"stns": [[s, 1] for s in stn_ids], "stn_beams": stnbm_list},
            "search_beams": {
                "beams": pssbeam_list,
            },
        },
    }
    return pss_cfg


# TODO remove this and replace it with pss_utils.freq_power_per_beam
def freq_power_per_beam(
    payloads: Iterable[bytes], pst_beam_ids: Iterable[int]
) -> tuple[list, list]:
    """
    Get frequencies and power for the given PSS beams.

    This is free of constants and hopefully also works for PST. If so, it could
    replace ska_low_cbf_integration.pst_utils.freq_power_per_beam
    (which is PST-specific)
    """
    # pylint: disable=too-many-locals
    total_freqs = {}
    total_pwr = {}
    for beam_number in pst_beam_ids:
        # accumulate per-pst-channel power with first-chan as key
        np_chan_pwr = {}  # numpy power data by sequence num and first chan
        chan_pkts = {}
        # get sample by sample power from packet payloads
        for pkt_payload in payloads:
            psr_packet = PulsarPacket(pkt_payload)
            if psr_packet.beam_id != beam_number:
                continue
            first_chan = psr_packet.first_channel
            if first_chan not in np_chan_pwr:
                np_chan_pwr[first_chan] = np.zeros(psr_packet.n_channels)
                chan_pkts[first_chan] = 0
            np_chan_pwr[first_chan] += psr_packet.channel_power()
            chan_pkts[first_chan] += 1

        # get average power-per-sample numbers by dividing by number added
        for first_chan_key in np_chan_pwr:
            np_chan_pwr[first_chan_key] = (
                np_chan_pwr[first_chan_key] / chan_pkts[first_chan_key]
            )

        freqs = []
        pwr = []
        ordered_chans = sorted(np_chan_pwr.keys())

        n_chan = len(np_chan_pwr[ordered_chans[0]])  # num chan/packet
        for channel in ordered_chans:
            # there are 54 consecutive pss freqs in each packet/block 24 form PST
            frequencies = [channel + i for i in range(0, n_chan)]
            freqs.extend(frequencies)
            pwr.extend(np_chan_pwr[channel].tolist())
        total_freqs[beam_number] = freqs
        total_pwr[beam_number] = pwr

    return total_freqs, total_pwr
