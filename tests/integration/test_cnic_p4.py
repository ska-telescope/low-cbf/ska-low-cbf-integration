# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
"""
Verify CNIC operation by using one FPGA in duplex mode.

Packets are routed via P4, assuming a Connector device is running in
ska-low-cbf-conn namespace.
"""

import os

import pytest

from ska_low_cbf_integration.cnic import load_firmware_and_reset, wait_until_fpga_ready
from ska_low_cbf_integration.connector import load_ports
from ska_low_cbf_integration.low_psi import get_connector_proxy, serial_port
from ska_low_cbf_integration.tango import (
    DeviceProxyJson,
    get_proxies_for_class,
    wait_for_attr,
)


@pytest.mark.timeout(600)  # in case default of 300s is not long enough
@pytest.mark.hardware_present
def test_duplex_pcap():
    """Test CNIC in duplex mode, with a PCAP input file."""
    pcap_test_file = "/test-data/spead-5a.pcap"  # test file
    n_packets = 10  # expected number of packets in test file
    rx_pcap = "/test-data/rx.pcap"  # received data file

    connector = get_connector_proxy()
    cnics = [
        DeviceProxyJson(proxy, json_commands=["CallMethod", "SelectPersonality"])
        for proxy in get_proxies_for_class("CnicDevice")
    ]
    assert len(cnics) >= 1, "No CNICs!"
    cnic = cnics[0]

    # Load CNIC firmware (required to activate serial number attribute)
    load_firmware_and_reset(cnic)

    if cnic.serialNumber not in serial_port:
        raise RuntimeError(f"Unexpected alveo hardware! Serial={cnic.serialNumber}")

    # define P4 port numbers
    rx_port = serial_port[cnic.serialNumber]
    tx_port = rx_port

    # load port configurations into switch if required
    load_ports(connector, [rx_port, tx_port])

    # Setup P4 routing between the 2 processors we are using
    route_config = {"basic": [{"src": {"port": tx_port}, "dst": {"port": rx_port}}]}
    connector.UpdateBasicEntry(route_config)  # add or modify rule
    print(f"Switch routing table: {connector.basicRoutingTable}")

    # wait for FPGA to come online
    wait_until_fpga_ready(cnic)
    print("")

    print("Activating Duplex mode")
    cnic.hbm_pktcontroller__duplex = True
    print(f"Configuring {cnic.name()} for Rx\n")
    cnic.CallMethod(
        {
            "method": "receive_pcap",
            "arguments": {
                "out_filename": rx_pcap,
                "packet_size": 8306,
                "n_packets": n_packets,
            },
        }
    )

    # Prepare for Tx
    cnic.CallMethod(
        {"method": "prepare_transmit", "arguments": {"in_filename": pcap_test_file}}
    )
    wait_for_attr(
        cnic,
        "ready_to_transmit",
        max_duration=60,
        failure_message="Not ready to Transmit",
    )
    print("Starting data transmit")
    cnic.CallMethod({"method": "begin_transmit", "arguments": {}})

    wait_for_attr(
        cnic,
        "finished_receive",
        max_duration=60,
        failure_message="Receive not finished",
    )

    # clean up our route
    route_to_clean = {"basic": [{"src": {"port": tx_port}}]}
    connector.RemoveBasicEntry(route_to_clean)

    print("Transmit complete, checking Received data...")
    command_str = (
        f"compare_pcap {pcap_test_file} {rx_pcap} --ignore-fields ip.ttl ip.checksum"
    )
    status = os.system(command_str)
    if status == 0:
        os.remove(rx_pcap)
    assert status == 0


# TODO - make into a function with parameters for file, rate
# TODO - looping variant
# TODO - PTP triggering (requires P4 to be configured for PTP)
