# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 34 - PST Beam continuous time sequence.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
- `PTC #34 Presentation <https://docs.google.com/presentation/d/1tBCV7XZGnXlMNHtRkTyS_grgTj8hBo9tOsDzK2Qz7lU>`_
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
import shutil
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pytest
from numpy.fft import fft, fftshift

from ska_low_cbf_integration.cnic import VD_FINE_FREQ_HZ
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pulsar_protocol import ts_samples_beam_channel
from ska_low_cbf_integration.sps import SPS_COARSE_SPACING_HZ

from .latex_reporting import download_overleaf_repo, replace_word_in_report
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingTestConfig, cnic_processor

DATA_DIR = "/test-data/PTC/ptc34/"
"""Directory name where data file will be stored (PCAP)"""
BUILD_DIR = "build/ptc34"
"""Directory to write plots etc."""

PCAP_PATH_SAME_BEAM = DATA_DIR + "PTC34_beamformer.pcap"
"""PCAP file name"""


TEST_TIMEOUT_SEC = 2000
SAMPLE_RATE = (SPS_COARSE_SPACING_HZ / 216) * (4 / 3)
MAX_NUMBER_SPS_CHANNELS = 384
N_POL = 2
N_VALS_PER_CPLX = 2
N_BYES_PER_VAL = 2

station_ids = list(range(345, 357)) + list(range(429, 435))
PST_BEAM_ID = 11
station_weights = {
    PST_BEAM_ID: [1.0] * 5,
    12: [1.0] * 5,
}
CNIC_FINE_FREQ = 2
SUBARRAY_ID = 1
STATION_BEAM_ID = BeamId.SINGLE_BASIC
TONE_POSITION = 96 + 12


def prepare_delay():
    """Prepare the pointing configuration."""
    beam_radec = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}

    src_radec_beam_1 = {
        "ra": "2h30m00.00s",
        "dec": "-26d49m50.0s",
    }
    src_radec_beam_2 = {
        "ra": "2h30m00.00s",
        "dec": "-26d49m50.0s",
    }
    src_radec_beam_3 = {"ra": "2h30m00.00s", "dec": "-26d49m50.0s"}
    src_radec_beam_4 = {
        "ra": "2h30m00.00s",
        "dec": "-26d49m50.0s",
    }
    pst_radecs = [
        src_radec_beam_1,
        src_radec_beam_2,
        src_radec_beam_3,
        src_radec_beam_4,
    ]  # 4xoffset to src

    beamdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": beam_radec,
    }
    srcdir = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }
    pstdirs = {
        "subarray_id": SUBARRAY_ID,
        "beam_id": STATION_BEAM_ID,
        "direction": pst_radecs,
    }

    return beamdir, srcdir, pstdirs


# Generate Scan Configuration
N_STATIONS = 5
stations = [[station, 1] for station in station_ids[:N_STATIONS]]
channels = [64]
pst_beam_id = [11]
PST_SVR_IP = "192.168.55.55"
delay_sub = f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}"
pst_sub = f"low-cbf/delaypoly/0/pst_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}_"
scan_config_same_beam = {
    "id": 123,
    "lowcbf": {
        "stations": {
            "stns": stations,
            "stn_beams": [
                {
                    "beam_id": STATION_BEAM_ID,
                    "freq_ids": channels,
                    "delay_poly": delay_sub,
                }
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": pst_beam,
                    "stn_beam_id": STATION_BEAM_ID,
                    "delay_poly": pst_sub + str(station_idx + 1),
                    "jones": "tbd",
                    "destinations": [
                        {
                            "data_host": PST_SVR_IP,
                            "data_port": 11001,
                            "start_channel": 0,
                            "num_channels": 144,
                        }
                    ],
                    "stn_weights": station_weights[pst_beam],
                }
                for station_idx, pst_beam in enumerate(pst_beam_id)
            ],
        },
    },
}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc34_pst_continuous(cnic_and_processor, ready_subarray):
    """
    Implement PTC 34.

    In this test we aim:
    * To show that the beam data for the particular PST channel containing a given
    tone
    * To show that the sinusoid is continuous across packets
    """
    print(datetime.now(), "Starting PTC#34")
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)

    # Generate CNIC-VD Configuration
    cnic_config_noise = [  # config is a list of dicts - one per SPEAD stream
        {
            "scan": 0,
            "subarray": SUBARRAY_ID,
            "station": station,
            "substation": substation,
            "frequency": channel,
            "beam": STATION_BEAM_ID,
            "sources": {
                "x": [
                    {"tone": True, "fine_frequency": CNIC_FINE_FREQ, "scale": 32},
                ],
                "y": [
                    {"tone": True, "fine_frequency": CNIC_FINE_FREQ, "scale": 32},
                ],
            },
        }
        for station, substation in stations
        for channel in channels
    ]

    (
        beam_delay_config,
        source_delay_config_beam,
        pst_delay_config_same_beam,
    ) = prepare_delay()
    test_config = ProcessorVdStaticTrackingTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=1356 * len(channels) * len(pst_beam_id) * 180,
        scan_config=scan_config_same_beam,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config_noise,
        during_scan_callback=None,
        source_delay_config=[source_delay_config_beam],
        beam_delay_config=[beam_delay_config],
        pst_delay_config=pst_delay_config_same_beam,
    )
    data = ts_samples_beam_channel(
        cnic_processor(
            cnic_and_processor,
            ready_subarray,
            test_config,
            PCAP_PATH_SAME_BEAM,
        ),
        beam_id=PST_BEAM_ID,
        channel_id=TONE_POSITION,
    )
    print(datetime.now(), "Captured PTC#34 output, beginning analysis")

    plot_time_series(data)
    max_error_power = compare_expected(data.x, os.path.join(BUILD_DIR, "compare.png"))
    fft_pass, fft_peak, fft_next_peak = check_fft(data)
    _ptc34_report(
        scan_config_same_beam,
        max_error_power,
        fft_pass,
        fft_peak,
        fft_next_peak,
    )
    assert fft_pass, "Tone headroom (FFT) failed requirement"
    assert max_error_power < 1000  # we see ~300, so this should detect gaps in data
    print(datetime.now(), "End of PTC#34")


def fft_complex(
    samples,
    channel,
    pst_channel,
):
    """Print FFT of the PST channel of interest."""
    f_tone = CNIC_FINE_FREQ * VD_FINE_FREQ_HZ
    x = SAMPLE_RATE / f_tone
    # minimise fraction in n_samples before rounding
    for offset in range(int(x)):
        n_samples = (int(len(samples) / x) - offset) * x
        if n_samples % 1 < 0.001 or n_samples % 1 > 0.999:
            break
    else:
        # didn't break out of loop
        raise RuntimeError("FFT sample offset goal seek failed")
    n_samples = round(n_samples)
    samples = samples[:n_samples]
    horiz_fft = 20 * np.log10(np.abs(fftshift(fft(samples))))
    freq_t = (
        np.linspace(-SAMPLE_RATE / 2, SAMPLE_RATE / 2, len(samples))
        + channel * SPS_COARSE_SPACING_HZ
        + (pst_channel - 108) * SPS_COARSE_SPACING_HZ / 216
        # maybe this last 1/2 step should only be applied if len(samples) is even?
        - SAMPLE_RATE / len(samples) / 2
    )
    fig, ax1 = plt.subplots(1, 1, sharex=True, sharey=True)

    ax1.axhline(y=np.max(horiz_fft) - 48, ls="--", color="red", label="Max - 48dB")
    ax1.stem(
        freq_t,
        horiz_fft,
        "blue",
        markerfmt=" ",
        basefmt="blue",
        label="Beam 11",
    )
    ax1.set_xlabel("Frequency (Hz)")
    ax1.set_ylabel("Station Beam power (dB)")
    ax1.axis(ymin=0, ymax=200)
    ax1.grid()
    ax1.set_title(f"Power Spectrum - PST Channel {pst_channel}")
    fig.legend()
    plt.savefig(os.path.join(BUILD_DIR, "FFT_PST_channel_64.png"))

    plt.clf()
    index_of_max_value = np.argmax(horiz_fft)
    expected_peak = channel * SPS_COARSE_SPACING_HZ + f_tone
    plt.axvline(
        expected_peak,
        label=f"Expected peak: {expected_peak:,.3f} Hz",
        color="orange",
        linestyle="--",
    )
    plt.axvline(
        freq_t[index_of_max_value],
        label=f"Actual peak: {freq_t[index_of_max_value]:,.3f} Hz",
        color="c",
        linestyle=":",
    )
    plt.plot(
        freq_t[index_of_max_value - 100 : index_of_max_value + 100],
        horiz_fft[index_of_max_value - 100 : index_of_max_value + 100],
    )
    plt.annotate(
        f"{horiz_fft[index_of_max_value]:,.1f} dB",
        xy=(freq_t[index_of_max_value], horiz_fft[index_of_max_value]),
        xytext=(8, -8),
        textcoords="offset points",
    )
    plt.title("Power Spectrum - Region Near Peak")

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Station Beam power (dB)")
    plt.axis(ymin=0, ymax=200)
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(BUILD_DIR, "zoomed_FFT_PST_channel_64.png"))
    plt.close()

    return horiz_fft


def _ptc34_report(scan_config, max_abs_error, fft_pass, fft_peak, fft_next_peak):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "6718dbfa950c407afe7b0014"
    )
    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)
    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(scan_config),
    )

    replace_word_in_report(
        file_result_to_replace,
        "REQ155",
        str(fft_pass),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXFFT",
        str(fft_peak),
    )
    replace_word_in_report(
        file_result_to_replace,
        "SECONDFFT",
        str(fft_next_peak),
    )
    replace_word_in_report(
        file_result_to_replace,
        "MAXERROR",
        str(max_abs_error),
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    files_to_move = [
        "FFT_PST_channel_64.png",
        "compare_all_error.png",
        "compare_error_hist.png",
        "compare_max.png",
        "compare_min.png",
        "time_series.png",
        "zoomed_FFT_PST_channel_64.png",
    ]
    for file_to_move in files_to_move:
        dst = os.path.join(local_directory, new_repo_name, file_to_move)
        shutil.copy(os.path.join(BUILD_DIR, file_to_move), dst)
        repo.git.add(dst)

    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.index.commit("Test with python")
    repo.git.push()


def check_fft(ts_samples: np.recarray) -> (bool, float, float):
    """
    Perform FFT and analyse results.

    :param ts_samples: timestamps & samples
    :return: pass/fail, FFT peak, next highest FFT value
    """
    horiz_fft = fft_complex(
        ts_samples.x,
        64,
        TONE_POSITION,
    )
    first_value_not_near_max = 0
    index_of_max_value = 0
    for index_sorted, value in enumerate(sorted(horiz_fft, reverse=True)):
        if index_sorted == 0:
            index_of_max_value = np.where(horiz_fft == value)[0][0]
        index = np.where(horiz_fft == value)[0][0]
        if not index_of_max_value - 2 < index < index_of_max_value + 2:
            first_value_not_near_max = value
            break
    peak = np.max(horiz_fft)
    passing_requirement = (peak - first_value_not_near_max) > 48
    return passing_requirement, peak, first_value_not_near_max


def plot_time_series(
    ts_samples: np.recarray,
):
    """Plot a time series of samples for the report."""
    plt.plot(ts_samples.t[:100], np.real(ts_samples.x[:100]))
    plt.plot(ts_samples.t[:100], np.imag(ts_samples.x[:100]))
    plt.axvline(
        x=ts_samples.t[32],
        color="r",
        ls="--",
        label="packet boundary",
    )
    plt.axvline(
        x=ts_samples.t[64],
        color="r",
        ls="--",
    )
    plt.axvline(
        x=ts_samples.t[96],
        color="r",
        ls="--",
    )
    plt.title("PST samples")
    plt.xlabel("Time (us)")
    plt.ylabel("Voltage ")
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(BUILD_DIR, "time_series.png"), format="png")
    plt.close()


def compare_expected(data, filename="compare_expected.png"):
    """
    Compare time series to expectation.

    Plots stuff as a side effect.

    :param data: time series of complex samples
    :param filename: base filename to save figures (_min & _max will be inserted)
    :returns: maximum absolute error
    """
    data = np.array(data)
    freq = CNIC_FINE_FREQ * VD_FINE_FREQ_HZ
    t = np.arange(len(data)) / SAMPLE_RATE
    expected = np.exp(1j * 2 * np.pi * freq * t)
    scale = np.vdot(expected, data) / np.vdot(expected, expected)
    expected *= scale

    error = data - expected
    max_index = np.argmax(error)
    min_index = np.argmin(error)
    largest_error = np.max(np.abs(error))

    plot_comparison_region(
        t=t,
        data=data,
        expected=expected,
        error=error,
        amplitude=np.abs(scale),
        rate=SAMPLE_RATE,
        filename="_max".join(os.path.splitext(filename)),
        title="Largest +ve Error Point (Complex Samples)",
        start=max_index - 100,
        stop=max_index + 100,
        vline=max_index,
    )
    plot_comparison_region(
        t=t,
        data=data,
        expected=expected,
        error=error,
        amplitude=np.abs(scale),
        rate=SAMPLE_RATE,
        filename="_min".join(os.path.splitext(filename)),
        title="Largest -ve Error Point (Complex Samples)",
        start=min_index - 100,
        stop=min_index + 100,
        vline=min_index,
    )

    error_power = np.abs(error) ** 2
    plt.plot(t, error_power)
    plt.title("Error Power (Complex Samples)")
    plt.xlabel("Time (s)")
    plt.savefig("_all_error".join(os.path.splitext(filename)), format="png")
    plt.close()

    plt.hist(error_power, bins=100)
    plt.title("Error Power Histogram (Complex Samples)")
    plt.ylabel("Count")
    plt.xlabel("Error Power")
    plt.savefig("_error_hist".join(os.path.splitext(filename)), format="png")
    plt.close()

    return largest_error


def plot_comparison_region(
    t,
    error,
    rate,
    filename,
    title,
    amplitude=None,
    data=None,
    expected=None,
    start=None,
    stop=None,
    vline=None,
):  # pylint: disable=too-many-arguments
    """Plot samples, expected values, and error."""
    if vline:
        vline = t[vline]
    t = t[start:stop]
    plt.axhline(y=0, color="k", linestyle="--")
    if vline:
        plt.axvline(vline, color="r", linestyle="--")
    if expected is not None:
        expected = expected[start:stop]
        plt.plot(t, np.real(expected), label="expected (Re)")
        plt.plot(t, np.imag(expected), label="expected (Im)")
    if data is not None:
        data = data[start:stop]
        plt.plot(t, np.real(data), label="actual (Re)")
        plt.plot(t, np.imag(data), label="actual (Im)")
    error = error[start:stop]
    plt.plot(t, np.real(error), label="error (Re)")
    plt.plot(t, np.imag(error), label="error (Im)")
    if amplitude is not None:
        title += f"\nUsing Detected Amplitude {amplitude:,.1f}"
    plt.title(title)
    plt.legend(loc="lower right")
    plt.xlabel(f"Time (s) [Fs = {rate / 1000:0.3f} kHz]")
    plt.savefig(filename, format="png")
    plt.close()
