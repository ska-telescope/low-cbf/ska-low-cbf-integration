# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 30 - PST Timestamp accuracy.
Summary:
    Show that PST beamformer packets have accurate timestamps
    - send SPS packets containing pulsed noise into the PST beamformer
    - form a single beam
    - capture SPS input and PST output
    - Test pass:
        A. show that first SPS pulse into PST results in matching PST beam pulse output
        B. fold SPS input and PST output on pulse period, calculate timing difference
          - difference should be less than a few SPS sample periods (limited by test precision)
        C. show that PST timestamps increment as expected (by 32 samples for each group of 9 packets)

More information:
    - `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
    - `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
    - `PTC #30 Presentation <https://where.is.it>`_  # TODO
"""  # noqa  # To allow the URLs to stay in one line
# pylint: enable=line-too-long
import json
import os
import shutil
import time

import pytest

from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.pst_timestamps import analysis_main, check_first_pulses

from .latex_reporting import download_overleaf_repo, replace_word_in_report  # noqa
from .station_beam_ids import BeamId
from .test_processor import ProcessorVdStaticTrackingMulticastTestConfig, cnic_processor
from .test_ptc_39 import Ptc39Results

TEST_TIMEOUT_SEC = 900

SUBARRAY_ID = 1
STN_BEAM_ID = BeamId.PTC30
PST_BEAM_ID = 1

NUM_STATIONS = 4  # 4 is enough to demonstrate beamforming
NUM_CHANNELS = 1  # 1 is enough to calculate timing
STARTING_EPOCH = 131800  # seconds after SKA epoch
FRAMES_TO_CAPTURE = 300  # 53msec per frame (300 = 16s)
DATA_DIR = "/test-data/PTC/ptc30"
CAPTURE_FILENAME = os.path.join(DATA_DIR, "PTC30_tmp.pcap")
RX_WAIT_SECS = 60  # 16s plus 8s start would be enough
BUILD_DIR = "/app/build/ptc30"

source_cfg = {
    "subarray_id": SUBARRAY_ID,
    "stn_beam_id": STN_BEAM_ID,
    "direction": [
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
        {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
    ],
}
station_beam_cfg = {
    "subarray_id": SUBARRAY_ID,
    "stn_beam_id": STN_BEAM_ID,
    "direction": {"ra": "00h00m00.0s", "dec": "-30d00m00.0s"},
}
pst_beam_cfg = {}


@pytest.mark.timeout(TEST_TIMEOUT_SEC)
@pytest.mark.hardware_present
def test_ptc30_pst_timestamp_accuracy(cnic_and_processor, ready_subarray):
    """Implement PTC 30."""
    # pylint: disable=too-many-locals
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(BUILD_DIR, exist_ok=True)
    time_start = time.time()

    my_cfg = {
        "subarray_id": SUBARRAY_ID,
        "stn_beam_id": STN_BEAM_ID,
        "stn_ids": [i + 1 for i in range(0, NUM_STATIONS)],
        "chans": [64 + i for i in range(0, NUM_CHANNELS)],
        "pst_beam_ids": [PST_BEAM_ID],
    }
    spead_multicast_routings = [
        {
            "src": {
                "frequency": channel,
                "beam": STN_BEAM_ID,
                "sub_array": SUBARRAY_ID,
            },
            # port numbers are inserted in cnic_processor function
            "dst": {"port_bf": "", "port_corr": ""},
        }
        for channel in my_cfg["chans"]
    ]
    # === Set up delay polynomials ====
    fixed_dlys = [{"stn": stn, "nsec": 0.0} for stn in my_cfg["stn_ids"]]
    beamdir = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": fixed_dlys,
    }
    srcdir = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": [fixed_dlys] * 4,
    }
    # we use a PST polynomial for PSS - its all the same really
    srcdirpst = {
        "subarray_id": my_cfg["subarray_id"],
        "beam_id": my_cfg["stn_beam_id"],
        "delay": [fixed_dlys] * 4,
    }

    # CNIC transmit configuration
    cnic_config = cnic_configure(
        my_cfg["subarray_id"],
        my_cfg["stn_beam_id"],
        my_cfg["stn_ids"],
        my_cfg["chans"],
    )
    # Note long start delay so we capture first pulse
    pulse_start = 9000480  # delay so we capture first pulse
    pulse_on = 96 - 1  # 0.5 SPS samples on
    pulse_off = 96 * 9  # 4.5 SPS samples off
    # 72 PST pkts/frame/chanl + 24SPSpkt/frame/ch
    cnic_capture_packets = (72 + NUM_STATIONS * 24) * NUM_CHANNELS * FRAMES_TO_CAPTURE
    scan_config = subarray_pst_config(
        my_cfg["subarray_id"],
        my_cfg["stn_beam_id"],
        my_cfg["stn_ids"],
        my_cfg["chans"],
        my_cfg["pst_beam_ids"],
    )
    test_config = ProcessorVdStaticTrackingMulticastTestConfig(
        cnic_capture_min_size=200,
        cnic_capture_packets=cnic_capture_packets,
        scan_config=scan_config,
        firmware_type=Personality.PST,
        input_packet_count=None,
        start_scan_packet_count=None,
        before_end_scan_packet_count=None,
        output_packet_count=None,
        vd_config=cnic_config,
        during_scan_callback=None,
        pulsar_config=(pulse_start, pulse_on, pulse_off),
        spead_multicast_routing=spead_multicast_routings,
        source_delay_config=[srcdir],
        beam_delay_config=[beamdir],
        pst_delay_config=srcdirpst,
    )
    pst_sps_output = cnic_processor(
        cnic_and_processor, ready_subarray, test_config, CAPTURE_FILENAME
    )
    time_k8s_end = time.time()

    # Analyse results
    print("Analyse results")
    result = Ptc30Results()
    # First check: Peak of folded PST-beam pulse should align with
    # the middle of the station-beam pulse if timestamps are accurate
    result.t_folded_s, result.timestamps_n_bad = analysis_main(
        pst_sps_output.name, plot_dir=BUILD_DIR
    )
    # Second check: Timestamp of first pulse out of FPGA matches
    # timestamp of first pulse from CNIC into FPGA
    result.t_first_s, result.latency_s = check_first_pulses(
        pst_sps_output.name, plot_dir=BUILD_DIR
    )
    shutil.copytree(BUILD_DIR, DATA_DIR, dirs_exist_ok=True)  # put graphs with PCAPs
    ptc30_report(test_config, result)

    # 1. Folded PST output should match SPS pulse middle within ~3 SPS samples
    # (3 samples pulled out of the air, roughly matches test accuracy)
    assert result.t_folded_ok, f"Timing error ({result.t_folded_s * 1e6} us) too large"
    # 2. There should be no FPGA timestamp errors
    assert result.timestamps_ok, f"{result.timestamps_n_bad} Bad Timestamps"
    # 3. Timestamp of first PST pulse out should match first SPS pulse in
    # to within ~1/4 pulse period (1/4 of 959SPS samples = 240SPS samples)
    assert result.t_first_ok, "First PST pulse not time-aligned with SPS pulse"
    # 4. Latency through PST should be no more than 1 second (really only
    # a PSS requirement)
    assert result.latency_ok, "PST latency more than 1.0 second"

    time_analysis_end = time.time()
    print(f"Test duration      : {time_analysis_end - time_start:02f} seconds")
    print(f" - data capture    : {time_k8s_end - time_start:02f} seconds")
    print(f" - capture analysis: {time_k8s_end - time_start:02f} seconds")


class Ptc30Results(Ptc39Results):
    """
    Collate PTC#30 Results and assess pass/fail for each parameter.

    The only difference between PTC#39 and PTC#30 is the "first pulse OK" threshold.
    """

    @property
    def t_first_ok(self) -> bool:
        """First PST pulse times within 1/4 pulse period of SPS pulse time."""
        # (pulse_on + pulse_off) = 959, 959/4 = 240
        return abs(self.t_first_s) < 240 * 1080e-9


def cnic_configure(sub_id, beam_id, stn_ids, beam_freq_ids, scale=4000) -> dict:
    """Create CNIC configuration."""
    config = {
        "sps_packet_version": 3,
        "stream_configs": [],  # config is a list of dicts - one per stn-chan
    }

    # Add configs into the stream_configs list
    sc_list = []
    for stn_id in stn_ids:
        for freq_id in beam_freq_ids:
            stream_cfg = {
                "scan": 0,  # this is in packet but unused, dropped by FPGA
                "subarray": sub_id,
                "station": stn_id,
                "substation": 1,
                "frequency": freq_id,
                "beam": beam_id,
                "sources": {},  # initially empty, configured later
            }
            sc_list.append(stream_cfg)
    config["stream_configs"] = sc_list

    # Configure all sources. Noise, Chans have same seed for all stns.
    for stream in config["stream_configs"]:
        beam_id = stream["beam"]
        chan = stream["frequency"]

        stream["sources"] = {
            "x": [
                {"tone": False, "seed": 1000 + chan, "scale": scale},
            ],
            "y": [
                {"tone": False, "seed": 2000 + chan, "scale": scale},
            ],
        }

    return config


def subarray_pst_config(sub_id, stn_bm_id, stn_ids, freq_ids, pst_beams_list):
    """PST subarray configuration."""
    wts = [1.0]  # all stations used
    pstbeam_list = []
    for pst_bm_id in pst_beams_list:
        beam = {
            "pst_beam_id": pst_bm_id,
            "stn_beam_id": stn_bm_id,
            "delay_poly": f"low-cbf/delaypoly/0/pst_s{sub_id:02d}_b{stn_bm_id:02d}_1",
            "jones": "tbd",
            "destinations": [  # TODO distinguish each destination
                {
                    "data_host": "192.168.0.1",
                    "data_port": 11001,
                    "start_channel": 0,
                    "num_channels": 144,
                },
            ],
            "stn_weights": wts,
        }
        pstbeam_list.append(beam)

    pst_cfg = {
        "id": 123,
        "lowcbf": {
            "stations": {
                "stns": [[s, 1] for s in stn_ids],
                "stn_beams": [
                    {
                        "beam_id": stn_bm_id,
                        "freq_ids": freq_ids,
                        "delay_poly": "low-cbf/delaypoly/0/delay_s"
                        + f"{sub_id:02d}_b{stn_bm_id:02d}",
                    },
                ],
            },
            "timing_beams": {
                "beams": pstbeam_list,
            },
        },
    }
    return pst_cfg


def ptc30_report(test_config, result: Ptc30Results) -> None:
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "66f4f246df246c608722046a"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config.scan_config),
    )

    replace_word_in_report(
        file_result_to_replace,
        "T_ERR",
        f"{result.t_folded_s * 1e6:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_TERR_OK",
        f"{result.t_folded_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "BAD_TS",
        f"{result.timestamps_n_bad}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_BADTS_OK",
        f"{result.timestamps_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "T_OFFS",
        f"{result.t_first_s * 1e6:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_TOFFS_OK",
        f"{result.t_first_ok}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "PST_LATENCY",
        f"{result.latency_s:.3f}",
    )
    replace_word_in_report(
        file_result_to_replace,
        "REQ_LATENCY_OK",
        f"{result.latency_ok}",
    )
    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        os.path.join(BUILD_DIR, "ptc30_folded_pulses.png"),
        local_directory + new_repo_name + "/ptc30_folded_pulses.png",
    )
    shutil.copy(
        os.path.join(BUILD_DIR, "ptc30_first_pulses.png"),
        local_directory + new_repo_name + "/ptc30_first_pulses.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/ptc30_folded_pulses.png")
    repo.git.add(new_repo_name + "/ptc30_first_pulses.png")
    # TODO to put the CI_COMMIT_SHORT_SHA in the report
    # repo.git.add(new_repo_name+"/appendix.tex")
    repo.index.commit("Test with python")
    repo.git.push()
