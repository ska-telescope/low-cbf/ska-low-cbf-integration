# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=line-too-long
"""
Perentie Test Case 47 - Multiple Correlator Subarrays.

- `Low CBF Requirements <https://docs.google.com/spreadsheets/d/1aPrUFcwlzPNIyesG-eEmbGiKZZ61PW3-9lHCBupHh8o>`_
- `Perentie Test Cases <https://docs.google.com/spreadsheets/d/1IpSJn0mVvee0YVvP8lKPsyCbWfyhBhUrUYx3520zR_E>`_
"""  # noqa
# pylint: enable=line-too-long
import json
import math
import os
import shutil
import warnings
from collections import defaultdict
from dataclasses import dataclass
from time import perf_counter, sleep
from typing import Any, Callable, Iterable, Optional, TypeAlias

import matplotlib.colors as mpl_colors
import matplotlib.pyplot as plt
import pytest
from typing_extensions import BinaryIO

from ska_low_cbf_integration.allocator import allocator_proxy
from ska_low_cbf_integration.cnic import VD_NOISE_SCALE
from ska_low_cbf_integration.connector import assert_packet_counts_zero
from ska_low_cbf_integration.correlator import integration_periods
from ska_low_cbf_integration.firmware import Personality
from ska_low_cbf_integration.low_psi import device_ports, get_connector_proxy
from ska_low_cbf_integration.pcap import data_start_stop_per_dst
from ska_low_cbf_integration.spead import PacketType, packet_infos
from ska_low_cbf_integration.subarray import empty_subarray, get_ready_subarray_proxy
from ska_low_cbf_integration.tango import wait_for_attr
from ska_low_cbf_integration.visibility_analyser import VisibilityAnalyser
from tests.integration.test_processor import (
    CONN_TIMEOUT_MS,
    ProcessorTestConfig,
    check_processors_firmware,
    configure_scan_for_test,
    is_fpga_log_enabled,
    log_registers,
)

from .latex_reporting import download_overleaf_repo, replace_word_in_report  # noqa

SUBARRAYS = (1, 2, 3, 4)
PCAP_FILE_NAME = "/test-data/PTC/ptc47/{}_ptc47.pcap"  # file name template
PTC47_DIR = "build/ptc47"
"""Directory to write plots etc."""

VIS_CHANNEL_COUNT = 144

# 47.4 Scan each subarray with a different start time and duration
# (start 0/10/20/40 seconds once configured, duration 10/20/30/60 seconds)
scans_start_simultaneously = {
    1: {"start": 0, "duration": 10},
    2: {"start": 0, "duration": 20},
    3: {"start": 0, "duration": 30},
    4: {"start": 0, "duration": 60},
}

scans_end_simultaneously = {
    1: {"start": 50, "duration": 10},
    2: {"start": 40, "duration": 20},
    3: {"start": 30, "duration": 30},
    4: {"start": 00, "duration": 60},
}

scans_standard = {
    1: {"start": 0, "duration": 10},
    2: {"start": 10, "duration": 20},
    3: {"start": 20, "duration": 30},
    4: {"start": 40, "duration": 60},
}

for test in (scans_start_simultaneously, scans_end_simultaneously, scans_standard):
    assert all(
        (
            i % 10 == 0
            for i in set(t["start"] for t in test.values())
            | set(t["duration"] for t in test.values())
        )
    ), "PTC#47 start times & durations must be multiples of 10"


@dataclass
class PacketsSummary:
    """Summary of Packets Received."""

    start: float | None = None
    stop: float | None = None
    count: int = 0


SubarrayPacketSummaries: TypeAlias = dict[int, dict[PacketType, PacketsSummary]]
"""{subarray_id: {PacketType: PacketsSummary, ...}, ... }"""
ReqResults: TypeAlias = dict[int, dict[int, bool | None]]
"""{ requirement: {subarray id: passed?, ... }, ... }"""


def subarray_config(n: int) -> dict:
    """
    Create Subarray Configuration for PTC#47.

    47.1 Configure 4 subarrays that can all fit within 1 Correlator FPGA.
    47.2 Configure each subarray with 1 station beam of 1/2/4/8 channels.
    Use different station beam numbers and differing channel numbers in each subarray.

    :param n: Subarray number
    :return: dict for use with ConfigureScan
    """
    return {
        "id": 100 + n,
        "lowcbf": {
            "stations": {
                "stns": [[1, 1]],
                "stn_beams": [
                    {
                        "beam_id": n,
                        "freq_ids": list(_channels_subarray(n)),
                        "delay_poly": "some_url",
                    },
                ],
            },
            "vis": {
                "stn_beams": [
                    {
                        "stn_beam_id": n,
                        "host": [[0, f"192.168.{n}.{n}"]],
                        "mac": [[0, "0c-42-a1-9c-a2-1b"]],
                        "port": [[0, 20000, 1]],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


def _channels_subarray(n: int) -> list[int]:
    """Get the list of channels for a Subarray."""
    # 47.2 Configure each subarray with 1 station beam of 1/2/4/8 channels.
    n_channels = 2 ** (n - 1)
    first_channel = 100 + 10 * n
    channels = list(range(first_channel, first_channel + n_channels))
    return channels


# 47.3 Set up the CNIC to produce data for all 4 subarrays. The CNIC has to produce all
# the SPS data from the beginning, and not be incremental. CNIC amplitude is
# proportional to subarray * station beam * channel.
cnic_config = {
    "sps_packet_version": 3,
    "stream_configs": [
        {
            "scan": 100 + subarray,
            "subarray": subarray,
            "station": 1,
            "substation": 1,
            "frequency": channel,
            "beam": subarray,
            "sources": {
                "x": [
                    {
                        "tone": False,
                        "seed": 4747,
                        "scale": 1000 + channel * subarray**2,
                    },
                ],
                "y": [
                    {
                        "tone": False,
                        "seed": 4747,
                        "scale": 1000 + channel * subarray**2,
                    },
                ],
            },
        }
        for subarray in SUBARRAYS
        for channel in _channels_subarray(subarray)
    ],
}

tagged_test_conditions = [
    #  label        scan start/duration times
    # -----------+------------------------------
    ("SAME_START", scans_start_simultaneously),
    ("SAME_END", scans_end_simultaneously),
    ("STANDARD", scans_standard),
]


@pytest.mark.timeout(600)
@pytest.mark.parametrize("test_cond", tagged_test_conditions)
@pytest.mark.hardware_present
def test_ptc47_corr_multiple_subarrays(cnic_and_processor, test_cond):
    """Run PTC#47 - Multiple Correlator Subarrays."""
    # pylint: disable=too-many-locals
    label, scan_times = test_cond

    # calculate scan END times for later use
    for sub_id, entry in scan_times.items():
        scan_times[sub_id]["end"] = entry["start"] + entry["duration"]

    os.makedirs(PTC47_DIR, exist_ok=True)
    # TODO - use subarray fixtures so subarrays clean up more reliably?
    subarrays = {n: get_ready_subarray_proxy(n) for n in SUBARRAYS}

    # we can't use our standard test fixture here because we need to stagger scans :(
    cnics, processors = cnic_and_processor
    cnic = cnics[0]

    connector = get_connector_proxy()
    connector.set_timeout_millis(CONN_TIMEOUT_MS)

    test_config = ProcessorTestConfig(
        # We shouldn't be too precise in the Rx packet count in case of small timing
        # variations. Will stop Rx manually.
        cnic_capture_packets=1_000_000,
        cnic_capture_min_size=64,
        firmware_type=Personality.CORR,
        nb_processors=1,  # 47.1 ... all fit within 1 Correlator FPGA
        scan_config={},  # will be supplied in loop
    )
    processors = processors[: test_config.nb_processors]

    subarray_configs = {}
    first = True
    for n, subarray in subarrays.items():
        test_config.scan_config = subarray_config(n)
        subarray_configs[n] = subarray_config(n)
        configure_scan_for_test(subarray, test_config)

        if first and is_fpga_log_enabled():
            # We will miss the first subarray configuration, but that's hard to avoid...
            # Can't enable register logging unless FPGA is already programmed!
            log_registers(processors[0])
            first = False

    # only need to check FW once as we expect to fit in one FPGA
    check_processors_firmware(test_config, processors)
    print(f"{processors[0].stats_mode=}")
    n_processors_used = len(allocator_proxy().internal_alveo)

    assert_packet_counts_zero(connector, device_ports(cnics + processors))
    pcap_fname = PCAP_FILE_NAME.format(label)
    # CNIC Rx
    cnic.CallMethod(
        method="receive_pcap",
        arguments={
            "out_filename": pcap_fname,
            "packet_size": test_config.cnic_capture_min_size,
            "n_packets": test_config.cnic_capture_packets,
        },
    )
    # 47.3 The CNIC has to produce all the SPS data from the beginning, and not be
    # incremental.
    cnic.ConfigureVirtualDigitiser(cnic_config)
    cnic.enable_vd = True

    # 47.4 Scan each subarray with a different start time and duration
    t_first_scan = control_subarrays(
        subarrays, cnic, scan_times, active_subarrays=SUBARRAYS
    )

    # Stop CNIC Tx & Rx
    cnic.enable_vd = False
    cnic.CallMethod(method="stop_receive")

    # Clean up for politeness
    for subarray in subarrays.values():
        empty_subarray(subarray)

    wait_for_attr(cnic, "finished_receive", max_duration=600, sleep_per_loop=5)
    processors[0].StopRegisterLog()  # not always needed, but shouldn't hurt

    subarray_vis = subarray_visibility_analysers(pcap_fname)
    subarray_t = data_start_stop_per_dst(pcap_fname)
    subarray_t = {n: subarray_t.get(f"192.168.{n}.{n}") for n in SUBARRAYS}
    with open(pcap_fname, "rb") as pcap_file:
        packet_summaries = summarise_by_subarray(pcap_file)
    words_to_change = ptc47_log_results(
        subarray_t, subarray_vis, scan_times, packet_summaries, label, PTC47_DIR
    )
    plot_subarray_spead_packets(packet_summaries, label, PTC47_DIR)
    plot_subarray_data_times(t_first_scan, subarray_t, scan_times, PTC47_DIR, label)
    ptc47_plot_integrations(subarray_vis, label, PTC47_DIR)
    results = ptc47_check(t_first_scan, subarray_t, subarray_vis, scan_times, label)
    # Transform results for the report
    report_results = {
        f"SUB{n_sub}REQ{n_req}": passed
        for n_req, sub_passed in results.items()
        for n_sub, passed in sub_passed.items()
    }
    # Overall pass/fail result for report
    report_results.update({"OVERALL": all(report_results.values())})
    words_to_change.update(report_results)
    # generate a single report for "standard" test conditions
    if label == "STANDARD":
        _ptc47_report(subarray_configs, words_to_change, label)
    ptc47_assert(results, n_processors_used)


def control_subarrays(
    subarrays: dict,
    cnic,
    scan_times: dict[int, dict[str, int]],
    active_subarrays: Optional[Iterable[int]] = None,
    debug_callback: Optional[Callable[[int], Any]] = None,
):
    """
    Start & Stop the Subarrays at the desired times.

    Duration & start time values must be multiples of 10.
    Only one subarray can start or stop at a time.

    :param subarrays: {subarray_id: DeviceProxy}
    :param cnic: DeviceProxy
    :param scan_times: {subarray_id: {"start": 10, "duration": 20, "end": 30}}
    :param active_subarrays: If you want to use a subset of subarrays.
    :param debug_callback: Called per time loop, with time (0, 10, ...) as argument.
    :returns: Time at which the first Scan command was sent (per CNIC clock)
    """
    # pylint: disable=too-many-arguments
    if active_subarrays is None:
        active_subarrays = scan_times.keys()

    # the 'if' clauses here allow testing with a reduced set of subarrays
    sub_scan = {
        sub_id: t for sub_id, t in scan_times.items() if sub_id in active_subarrays
    }
    t_first_scan = None  # UNIX timestamp, from CNIC (for consistency with PCAP)
    all_end_times = [times["end"] for times in sub_scan.values()]
    upper_limit = max(all_end_times)
    step_sec = 10
    for now in range(0, upper_limit + 1, step_sec):
        t_loop_start = perf_counter()
        for sub_id, t in sub_scan.items():
            if now == t["start"]:
                subarray = subarrays[sub_id]
                print(f"t={now} Subarray {sub_id} Scan")
                subarray.Scan({"id": 100 + sub_id})
                if t_first_scan is None:
                    t_first_scan = cnic.timeslave__unix_timestamp
                # Can't wait for ObsState.SCANNING here because it will
                # mess up our timing.
            elif now == t["end"]:
                subarray = subarrays[sub_id]
                print(f"t={now} Subarray {sub_id} EndScan")
                subarray.EndScan()
        if debug_callback is not None:
            debug_callback(now)
        sleep(step_sec - (perf_counter() - t_loop_start))
    sleep(5)  # as per KB: allow END packets to crawl out
    return t_first_scan


def subarray_visibility_analysers(filename="./ptc47.pcap"):
    """
    Create a VisibilityAnalyser per subarray from a PTC#47 PCAP file.

    :param filename: PCAP file name
    :returns: {subarray: VisibilityAnalyser}
    """
    subarray_visibilities = {}
    for n in SUBARRAYS:
        ip_addr = f"192.168.{n}.{n}"
        subarray_visibilities[n] = VisibilityAnalyser(
            json.dumps({"coarse_channels": _channels_subarray(n), "stations": [1]})
        )
        subarray_visibilities[n].extract_spead_data(filename, filter_="host " + ip_addr)

    return subarray_visibilities


def ptc47_log_results(  # pylint: disable=too-many-arguments
    subarray_t_start_stop,
    subarray_visibilities,
    scan_times,
    summaries: SubarrayPacketSummaries,
    label: str,
    dirname=".",
) -> dict:
    """
    Write a log file with some data/results.

    :param subarray_t_start_stop: {subarray_id: (start, stop), ... }
    :param subarray_visibilities: {subarray_id: VisibilityAnaylser, ... }
    :param scan_times: {subarray_id: {"start": 10, "duration": 20, "end": 30}}
    :param summaries: {subarray_id: {PacketType: PacketsSummary, ...}, ... }
    :param label: used to distinguish between parameterised tests
    :param dirname: Directory to write results to
    :returns: things to go in the report
    """
    # pylint: disable=too-many-locals
    os.makedirs(dirname, exist_ok=True)
    fname = os.path.join(dirname, f"{label}_raw-analysis.txt")
    with open(fname, "w", encoding="utf-8") as raw:
        print("Raw print of data analysis results.", file=raw)
        print("subarray_t_start_stop", file=raw)
        print(subarray_t_start_stop, file=raw)
        # No point printing subarray_visibilities,
        # VisibilityAnalyser doesn't render any meaningful str
        print("", file=raw)
        print("summaries", file=raw)
        print(summaries, file=raw)

    words_to_change = {}
    fname = os.path.join(dirname, f"{label}_timestamps.txt")
    with open(fname, "w", encoding="utf-8") as log:
        print("Packet Flow per Subarray (Destination IP Address)", file=log)
        print("")
        for n, timestamps in subarray_t_start_stop.items():
            if timestamps is None:
                log.write(f"\nERROR\n  Subarray {n} invalid timestamps {timestamps}\n")
                continue
            start, stop = timestamps
            print(
                f"Subarray {n: 2d}: {start: 8.3f} s -> {stop: 8.3f} s",
                f" = {stop - start: 6.1f} s duration",
                file=log,
            )
        print("=" * 20, file=log)
        print("", file=log)
        print("SPEAD Packet Summary per Subarray (Destination IP Address)", file=log)
        print("", file=log)
        #               1         2         3         4         5
        #      12345678901234567890123456789012345678901234567890
        print("Subarray | SPEAD Type |  Count   | Start -> End time", file=log)
        print("---------+------------+----------+------------------", file=log)
        for subarray_id, spead_type_start_stop in summaries.items():
            for spead_type, summary in spead_type_start_stop.items():
                print(
                    f"{subarray_id:^8d}",
                    f"{spead_type.name:^10s}",
                    f"{summary.count:>8d}",
                    f"{summary.start:.2f} -> {summary.stop:.2f}",
                    sep=" | ",
                    file=log,
                )
        print("=" * 20, file=log)
        print("", file=log)
        print("Visibility Analysis", file=log)
        print("", file=log)
        for n, vis_an in subarray_visibilities.items():
            times = vis_an.time_sequence
            period = integration_periods[849]
            print(f"Subarray {n}: {len(times)} integrations", file=log)
            expected_duration = scan_times[n]["duration"]
            print(
                f"Expected {int(expected_duration / period) - 1} integrations",
                file=log,
            )
            print("Time sequence:", file=log)
            print(times, file=log)
            print("Differences:", file=log)
            differences = [times[n] - times[n - 1] for n in range(1, len(times))]
            print(differences, file=log)
            bad_differences = sum((i != period * 1_000_000_000 for i in differences))
            print(
                f"{bad_differences}",
                "bad time differences",
                file=log,
            )
            print("---", file=log)
            words_to_change[f"SUB{n}EXP"] = expected_duration
            words_to_change[f"SUB{n}OBS"] = len(times)
            words_to_change[f"SUB{n}DIFF"] = bad_differences
    return words_to_change


def ptc47_check(
    t_0, subarray_t_start_stop, subarray_visibilities, scan_times, label: str
) -> ReqResults:
    """
    Check if requirements are met.

    :param t_0: Start time (when first scan command was sent)
    :param subarray_t_start_stop: {subarray_id: (start, stop), ... }
    :param subarray_visibilities: {subarrayid: VisibilityAnalyser, ... }
    :param scan_times: {subarray_id: {"start": 10, "duration": 20, "end": 30}}
    :param label: used to distinguish between parameterised tests
    :returns: { requirement: {subarray id: passed?, ... }, ... }
      where requirement is 5, 6, 7, or 8, from PTC steps - see comments
      'passed?' is True for pass, False for fail, None if not checked
    """
    # pylint: disable=too-many-locals
    result: ReqResults = defaultdict(lambda: defaultdict(lambda: None))
    for n, timestamps in subarray_t_start_stop.items():
        if not isinstance(timestamps, tuple):
            return {}
        start, stop = timestamps
        # 47.7 Confirm using PTP the time when visibilities flowed
        # (we use CNIC clock without PTP, but that's close enough)
        duration = stop - start
        expected_duration = scan_times[n]["duration"]
        result[7][n] = duration == pytest.approx(expected_duration, abs=5)
        # 47.6 Confirm the timestamps on the visibilities matches the start times of the
        # subarrays
        result[6][n] = start == pytest.approx(scan_times[n]["start"] + t_0, abs=5)

    fname = PTC47_DIR + f"/{label}_auto.txt"
    with open(fname, "w", encoding="utf-8") as log:
        for sub_id, vis_an in subarray_visibilities.items():
            # 47.5 Confirm the number of integrations received is 10/20/30/60
            # seconds long for each subarray
            expected_duration = scan_times[sub_id]["duration"]
            result[5][sub_id] = len(vis_an.time_sequence) == pytest.approx(
                expected_duration / integration_periods[849], abs=6
            )
            print(f"Subarray {sub_id}", file=log)
            auto = None
            cnic_scales = get_cnic_scales(sub_id)
            n_channels = len(cnic_scales)
            log.write(f"{cnic_scales=}\n")
            try:
                auto = vis_an.average_correlation[0][0]  # 1X*1X
                print(f"{auto=}", file=log)
            except TypeError:
                print("TypeError reading correlation matrix", file=log)
                continue
            # 47.8 Confirm that the data produced by the CNIC is valid
            # Autocorrelation amplitudes as expected.
            # progressive error margin: 2.0 / n_channels; experimental values:
            #   1 channel  1.5 %
            #   2 channels 0.76 %
            #   4 channels 0.36 %
            #   8 channels 0.13 %
            calculated_autocorr = expected_autocorrelation(cnic_scales)
            log_lines = [f"Calculated auto-correlation: {calculated_autocorr}\n"]
            error_percent = 100 * abs(calculated_autocorr - auto) / calculated_autocorr
            log_lines.append(f"Error:  {error_percent:.2}%\n")
            passed = auto == pytest.approx(
                calculated_autocorr, rel=2 / n_channels / 100
            )
            log_lines.append(f"Passed: {passed}\n")
            log_lines.append("=" * 20 + "\n")
            log.writelines(log_lines)
            result[8][sub_id] = passed
    return result


def get_cnic_scales(subarray_id: int) -> list[int]:
    """
    Get CNIC scales for each subarray (from ``cnic_scales``).

    Pulls values from the global ``cnic_config`` dict.

    :param subarray_id: subarray number e.g. 1, 2, ...
    :return: a list of channel scale factors for the given subarray
    """
    cnic_scales = defaultdict(list)
    for entry in cnic_config["stream_configs"]:
        sub_id = entry["subarray"]
        scale = entry["sources"]["x"][0]["scale"]
        cnic_scales[sub_id].append(scale)
    return cnic_scales[subarray_id]


def expected_autocorrelation(cnic_scales: list[int]) -> float:
    """
    Calculate theoretical autocorrelation for given CNIC noise scale values.

    See DH slides https://is.gd/qs8Kny

    :param cnic_scales: a list of scale factors for each channel in subarray
    :return: autocorrelation value
    """
    cnic_scales_average = float(sum(cnic_scales)) / len(cnic_scales)
    noise_rms = cnic_scales_average * math.sqrt(2) / VD_NOISE_SCALE
    # next line: 192 time samples, 24 fine channels (see slide ref. above)
    autocorr = 192 * 24 * noise_rms**2 * VIS_CHANNEL_COUNT
    return autocorr


def ptc47_assert(results: ReqResults, n_processors: int):
    """
    Drive pytest Pass/Fail by asserting requirements are met.

    :param results: from ptc47_check
    :param n_processors: number of Processors used
    :raises AssertionError: If requirements are not met
    """
    fail_message = {
        5: "Wrong number of integrations received",
        6: "Wrong start time (per CNIC clock)",
        7: "Wrong duration (per CNIC clock)",
        8: "Amplitude not as expected",
    }
    for n_requirement, subarray_pass in results.items():
        for sub_id, passed in subarray_pass.items():
            assert passed, f"Subarray {sub_id}: " + fail_message[n_requirement]
    assert n_processors == 1, f"Didn't fit into one Processor, used {n_processors}"


def plot_subarray_data_times(
    t_0: float,
    subarray_t_start_stop,
    scan_times,
    dirname: str = ".",
    test_label: str = "",
) -> None:
    """
    Plot Subarray start/stop times.

    :param t_0: Start time (when first scan command was sent)
    :param subarray_t_start_stop: Measured start/stop of packet flow
      {subarray_id: (start, stop), missing_id: None, ... }
    :param scan_times: Test scenario (expected timings)
      {subarray_id: {"start": 10, "duration": 20, "end": 30}..}
    :param dirname: Directory to save to
    :param test_label: File prefix, used to distinguish between parameterised tests
    """
    # pylint: disable=too-many-locals
    colors = dict(enumerate(mpl_colors.TABLEAU_COLORS))
    fig, ax = plt.subplots()
    plt.yticks(list(subarray_t_start_stop.keys()))
    first = True
    first_data = True
    for n, times in subarray_t_start_stop.items():
        if times is not None:
            (t_start, t_stop) = times
            label = "data packets" if first_data else None
            ax.plot((t_start - t_0, t_stop - t_0), (n, n), color=colors[n], label=label)
            first_data = False
        label = "scheduled start/stop" if first else None
        start_time = scan_times[n]["start"]
        end_time = scan_times[n]["end"]
        ax.scatter(start_time, n, color=colors[n], label=label)
        ax.scatter(end_time, n, color=colors[n])
        first = False

    ax.set(
        xlabel="Time since first Scan command (s)",
        ylabel="Subarray",
        title="Subarray Activity",
    )
    fig.tight_layout()
    plt.legend(loc="lower right")
    if test_label and not test_label.endswith("_"):
        test_label += "_"
    fname = f"{test_label}subarray_activity.png"
    fig.savefig(os.path.join(dirname, fname))


def plot_subarray_spead_packets(
    summaries: SubarrayPacketSummaries, label: str, dirname="."
) -> dict:
    """
    Plot summaries of packets per subarray.

    :param summaries: from summarise_by_subarray()
    :param label: used to distinguish between parameterised tests
    :param dirname: where to write the plot
    """
    type_linestyle = {
        PacketType.INIT: "dashed",
        PacketType.DATA: "solid",
        PacketType.END: "dotted",
    }
    type_marker = {
        PacketType.INIT: "+",
        PacketType.DATA: "o",
        PacketType.END: "x",
    }
    type_facecolor = {
        PacketType.INIT: None,  # use default - if we use "none", +/x do not appear
        PacketType.DATA: "none",  # un-filled circles
        PacketType.END: None,
    }
    colors = dict(enumerate(mpl_colors.TABLEAU_COLORS))
    fig, ax = plt.subplots()
    plt.yticks(sorted(summaries.keys()))
    first = defaultdict(lambda: True)  # only show legends once per packet type
    for subarray_id, spead_type_start_stop in summaries.items():
        for spead_type, summary in spead_type_start_stop.items():
            ax.plot(
                (summary.start, summary.stop),
                (subarray_id, subarray_id),
                color=colors[subarray_id],
                linestyle=type_linestyle[spead_type],
            )
            ax.scatter(
                summary.start,
                subarray_id,
                color=colors[subarray_id],
                marker=type_marker[spead_type],
                facecolor=type_facecolor[spead_type],
                label=spead_type.name if first[spead_type] else None,
            )
            ax.scatter(
                summary.stop,
                subarray_id,
                color=colors[subarray_id],
                marker=type_marker[spead_type],
                facecolor=type_facecolor[spead_type],
            )
            first[spead_type] = False

    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Subarray")
    ax.set_title("SPEAD Packets by Subarray")
    ax.legend()
    fig.tight_layout()
    fig.savefig(os.path.join(dirname, f"{label}_spead_activity.png"))


def summarise_by_subarray(pcap_file: BinaryIO) -> SubarrayPacketSummaries:
    """
    Summarise packet flow per SPEAD packet type per subarray.

    Assumes a maximum of one scan per subarray.

    :param pcap_file: file object to process
    :returns: {subarray_id: {PacketType: PacketsSummary, ...}, ... }
    """
    ip_subarray = {f"192.168.{n}.{n}": n for n in SUBARRAYS}

    summaries = defaultdict(lambda: defaultdict(PacketsSummary))
    """{subarray_id: {PacketType: PacketsSummary, ...}, ... }"""

    bad_ip_warned = set()

    for info in packet_infos(pcap_file):
        subarray = ip_subarray.get(info.dst, None)
        if subarray is None:
            if info.dst not in bad_ip_warned:
                warnings.warn(f"Packet skipped {info.dst} not in Subarray/IP table.")
                bad_ip_warned.add(info.dst)
            continue

        summary = summaries[subarray][info.type]
        summary.count += 1
        if summary.start is None or info.time < summary.start:
            summary.start = info.time
        if summary.stop is None or info.time > summary.stop:
            summary.stop = info.time

    return summaries


def ptc47_plot_integrations(
    subarray_visibilities: dict[int, VisibilityAnalyser], label: str, dirname="."
) -> None:
    """
    Plot number of integrations per each subarray.

    :param subarray_visibilities: {subarray_id: VisibilityAnaylser, ... }
    :param label: used to distinguish between parameterised tests
    :param dirname: directory to save to
    """
    colors = dict(enumerate(mpl_colors.TABLEAU_COLORS))
    fig, ax = plt.subplots()
    plt.yticks(list(subarray_visibilities.keys()))
    for n, vis_an in subarray_visibilities.items():
        for i in vis_an.time_sequence:
            ax.scatter(i, n, color=colors[n])
        # shift last annotation under data so we don't hit the edge of the frame
        annotate_offset = (-8, -14) if n == SUBARRAYS[-1] else (10, -4)
        if vis_an.time_sequence:
            # sometimes we don't see any time sequence, which upsets 'max' here
            ax.annotate(
                str(len(vis_an.time_sequence)),
                xy=(max(vis_an.time_sequence), n),
                xytext=annotate_offset,
                textcoords="offset points",
            )

    ax.set(
        xlabel="Visibility Timestamp",
        ylabel="Subarray",
        title="PTC#47 - Integrations",
    )
    fig.tight_layout()
    fig.savefig(os.path.join(dirname, f"{label}_integrations.png"))


def _ptc47_report(test_config, words_to_change, label: str):
    """Generate Test Report."""
    local_directory, new_repo_name, repo = download_overleaf_repo(
        "671b1df02da2e85cf4b9a949"
    )

    extension_of_new_results = (
        r"""%NEW_RESULTS
        \input{"""
        + new_repo_name
        + """/report}"""
    )
    file_result_to_replace = local_directory + new_repo_name + "/report.tex"
    replace_word_in_report(
        file_result_to_replace,
        "TOPUTDATE",
        new_repo_name.split("_")[1],
    )
    replace_word_in_report(file_result_to_replace, "TOPUTDIRECTORY", new_repo_name)

    replace_word_in_report(
        file_result_to_replace,
        "CONFIGURE_SCAN",
        json.dumps(test_config),
    )
    for word, change in words_to_change.items():
        replace_word_in_report(
            file_result_to_replace,
            word,
            str(change),
        )

    replace_word_in_report(
        local_directory + "report2017.tex", "%NEW_RESULTS", extension_of_new_results
    )
    shutil.copy(
        f"/app/build/ptc47/{label}_integrations.png",
        local_directory + new_repo_name + "/integrations.png",
    )
    shutil.copy(
        f"/app/build/ptc47/{label}_subarray_activity.png",
        local_directory + new_repo_name + "/subarray_activity.png",
    )
    repo.git.add("report2017.tex")
    repo.git.add(new_repo_name + "/report.tex")
    repo.git.add(new_repo_name + "/integrations.png")
    repo.git.add(new_repo_name + "/subarray_activity.png")
    repo.index.commit("Test with python")
    repo.git.push()
