-- -----------------------------------------------------------------------------
-- Title      : Streaming Protocol for Exchanging Astronomical Data (SPEAD)
--            : Wireshark Dissector
-- Project    : SKA
-- -----------------------------------------------------------------------------
-- File       : spead_dissector.lua
-- Author     : William Kamp <william.kamp@aut.ac.nz>
-- Company    :
-- Created    : 2019-05-02
-- Last update: 2024-03-13
-- -----------------------------------------------------------------------------
-- Description: Wireshark Dissector for SPEAD Packets.
--              Compatible with revision 0 (2010-10-07) of the specification.
--              https://casper.ssl.berkeley.edu/astrobaki/images/9/93/SPEADsignedRelease.pdf
-- Usage      : To use this dissector put this file in your personal plugins directory.
--              To find where:
--                 1. Open Wireshark.
--                 2. Goto menu 'Help' -> 'About Wireshark'.
--                 3. Goto 'Folders' tab
--                 4. directory listed under 'Personal Plugins'.
--              On Linux this is usually /home/<user>/.wireshark/plugins/
--
--              The dissector will be called for UDP packets with 41000 as the
--              destination port.
--              You can also force decoding of a UDP packet by selecting SPEAD.
-- -----------------------------------------------------------------------------
-- Copyright (c) William Kamp 2019
-- Copyright (c) 2024 CSIRO Space and Astronomy.
--
-- Distributed under the terms of the CSIRO Open Source Software Licence
-- -----------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-05-02  1.0.0    wkamp   Created
-- 2024-03-13  2.0.0    gjourjon Compliant with latest CBF to SDP ICD
-- -----------------------------------------------------------------------------
-- Requires: Wireshark 2.0.0 or better.
-- -----------------------------------------------------------------------------

function spead_checker(buffer, pinfo, tree)
  if buffer:len() < 8 then return false end
  if buffer(0,1):uint() ~= 0x53 then -- Magic must be 0x53
    return false end
  if buffer(1,1):uint() ~= 4 then    -- Version must be 4
    return false end
  if buffer(2,1):uint() ~= 2 then    -- Pointer width must be 2
    return false end
  if buffer(3,1):uint() ~= 6 then    -- Heap address width must be 6
      return false end
  return true
end

-- ------------------------------------------------------------------------------
-- SPEAD HEADER FIELDS ----------------------------------------------------
-- ------------------------------------------------------------------------------
local f_magic = ProtoField.uint8("spead.header.magic", "Magic", base.HEX, {[0x53] = "Valid"}, 0xFF)
local f_version = ProtoField.uint8("spead.header.version", "Version", base.DEC)
local f_ptr_width = ProtoField.uint8("spead.header.item_pointer_width", "ItemPointerWidth", base.DEC)
local f_heap_width = ProtoField.uint8("spead.header.heap_addresses_width", "HeapAddressesWidth", base.DEC)
local f_num_items = ProtoField.uint16("spead.header.num_items", "NumberOfItems", base.DEC)
local p_header = Proto("spead.header", "SPEAD Header")
p_header.fields = {f_magic, f_version, f_ptr_width, f_heap_width, f_num_items}

-- ITEM FIELDS - SPEAD flavour 64-48
local f_item_addr_mode  = ProtoField.uint8("spead.item.addr_mode", "ItemAddressMode", base.DEC, {[0] = "Pointer", [1] = "Immediate"}, 0x80)
local identifiers = {
  -- Basic SPEAD
  [0x0000] = "NULL",
  [0x0001] = "Heap Counter",
  [0x0002] = "Heap Size",
  [0x0003] = "Heap Offset",
  [0x0004] = "Payload Length",
  [0x0005] = "Item Descriptor",
  [0x0006] = "Stream Control",
  -- Item Descriptor Fields
  [0x0010] = "Item Descriptor: Name",
  [0x0011] = "Item Descriptor: Description",
  [0x0012] = "Item Descriptor: Shape",
  [0x0013] = "Item Descriptor: Type",
  [0x0014] = "Item Descriptor: Identifier",
  [0x0015] = "Item Descriptor: Numpy dType",
  -- SPS to CBF identifiers
  [0x3000] = "Channel Info",
  [0x3001] = "Antenna Info",
  [0x3010] = "Scan ID",
  [0x3300] = "Sample Offset",
  -- SKA CSP to SDP Identifiers. See ICD
  [0x6000] = "Visibility Timestamp Count",
  [0x6001] = "Visibility Timestamp Fraction",
  [0x6002] = "Visibility Channel ID",
  [0x6003] = "Visibility Channel Count",
  [0x6004] = "Visibility Baseline Polarisation ID",
  [0x6005] = "Visibility Baseline Count",
  [0x6006] = "Phase Bin ID",
  [0x6007] = "Phase Bin Count",
  [0x6008] = "Scan ID",
  [0x6009] = "Visibility Hardware ID",
  [0x600A] = "Correlator Output Data",
  [0x600B] = "Visibility Beam ID",
  [0x600C] = "Subarray ID",
  [0x600D] = "Integration Period",
  [0x600E] = "Frequency Resolution",
  [0x600F] = "Ouput Resolution",
  [0x6010] = "Frequency in Hz",
  [0x6011] = "Zoom Window",
  [0x6012] = "Firmware version",
  [0x6013] = "Visibility SPS Epoch",
  [0x6014] = "Visibility SPS Epoch offset",
  [0x6015] = "CBF Source ID",
  [0x6016] = "Visibility Flags",
}
local buffer_size = {
  -- SPS to CBF identifiers
  [0x3000] = 32,
  [0x3001] = 32,
  [0x3010] = 32,
  [0x3300] = 32,
  -- SKA CSP to SDP Identifiers. See ICD
  [0xE000] = 32,
  [0xE001] = 32,
  [0xE002] = 32,
  [0xE003] = 32,
  [0xE004] = 32,
  [0xE005] = 32,
  [0xE006] = 32,
  [0xE007] = 32,
  [0xE008] = 32,
  [0xE009] = 32,
  [0xE00A] = 32,
  [0xE00B] = 16,
  [0xE00C] = 16,
  [0xE00D] = 325,
  [0xE00E] = 325,
  [0xE00F] = 8,
  [0xE010] = 32,
  [0xE011] = 8,
  [0xE012] = 32,
  [0xE013] = 32,
  [0xE014] = 32,
  [0xE015] = 8,
  [0xE016] = 8,
}
local f_item_identifier = ProtoField.uint16("spead.item.identifier", "ItemIdentifier", base.HEX, identifiers, 0x7FFF)
local f_item_immediate  = ProtoField.uint64("spead.item.immediate", "Immediate Value", base.DEC, nil, 0x0000FFFFFFFFFFFF)
local f_item_address    = ProtoField.uint64("spead.item.address", "Heap Address", base.HEX, nil, 0x0000FFFFFFFFFFFF)
local f_first_byte      = ProtoField.uint8("spead.item.first_byte", "First byte pointed to", base.HEX)
local p_item_ptr = Proto("spead.item", "Item Pointer")
p_item_ptr.fields = {f_item_addr_mode, f_item_identifier, f_item_immediate, f_item_address, f_first_byte}

function p_item_ptr.dissector(buffer, pinfo, subtree)
  subtree:add(f_item_addr_mode, buffer(0,1))
  subtree:add(f_item_identifier, buffer(0,2))
  local addr_mode = buffer(0,1):bitfield(0,1)
  if addr_mode >= 1 then
    subtree:add(f_item_immediate, buffer(2,6))
  else
    subtree:add(f_item_address, buffer(2,6))
  end
end

local f_header = ProtoField.protocol("spead.header", "SPEAD Header")
local f_item_ptr = ProtoField.protocol("spead.item", "Item Pointer")
local f_payload = ProtoField.bytes("spead.payload", "Heap Payload")
local p_spead = Proto("spead", "SPEAD Packet")
p_spead.fields = {f_header, f_item_ptr, f_payload}

function p_spead.dissector(buffer, pinfo, tree)
  if not spead_checker(buffer, pinfo, tree) then return 0 end
  pinfo.cols.protocol = "SPEAD 64-48"
  --pinfo.cols.info:append("SPEAD")
  local subtree = tree:add(p_spead, buffer())
  subtree:add(f_magic, buffer(0,1))
  subtree:add(f_version, buffer(1,1))
  subtree:add(f_ptr_width, buffer(2,1))
  subtree:add(f_heap_width, buffer(3,1))
  subtree:add(f_num_items, buffer(6,2))
  local num_items = buffer(6,2):uint()
  local pos = 8
  local payload_start = pos + num_items * 8
  local payload_len = 0
  local heap_offset = 0
  -- Assuming SPEAD flavour 64-48 below (with 32b immediates and addressing).
  for item_idx = 0, num_items-1 do
    local item_subtree = subtree:add(p_item_ptr, buffer(pos, 8))
    -- Add the Item name to the display.
    item_subtree:append_text(" - " .. identifiers[buffer(pos,2):bitfield(1,15)])
    dissect = DissectorTable.get("spead_sd"):get_dissector(1)
    dissect:call(buffer(pos,8):tvb(), pinfo, item_subtree)

    -- Grab some values from items when they are seen to help dissect the packet.
    local identifier = buffer(pos,2):uint()
    if identifier == 0x8001 then
      local heap_cntr = buffer(pos+4,4):uint()
      pinfo.cols.info:append(" Heap-" .. heap_cntr)
    end
    if identifier == 0x8003 then
      heap_offset = buffer(pos+4,4):uint() -- 32b!
    end
    if identifier == 0x8004 then
      payload_len = buffer(pos+4,4):uint() -- 32b!
    end
    if identifier < 0x8000 and identifier > 0 then
      -- Is a pointer (not immediate). Try to point to the data in the payload.
      local heap_addr = buffer(pos+4,4):uint() -- 32b! big-endian
      local payload_offset = heap_addr - heap_offset
      if 0 <= payload_offset and payload_offset < payload_len then
        -- Data is in this packet's payload.
        if identifier == 0x0005 then
          -- Is an item descriptor - looks like a SPEAD packet embedded in the heap.
          dissect = DissectorTable.get("spead_sd"):get_dissector(0)
          dissect:call(buffer(payload_start+payload_offset):tvb(), pinfo, item_subtree)
        else
          -- Point to the first byte of the data in the payload.
          item_subtree:add(f_first_byte, buffer(payload_start+payload_offset,1))
        end
      end
    else
      -- Is immediate value
      local immediate = 0
      if buffer_size[identifier] == 32 then
        immediate = buffer(pos+4,4):le_uint()
      elseif buffer_size[identifier] == 16 then
        immediate = buffer(pos+6,2):le_uint()
      elseif buffer_size[identifier] == 8 then
        immediate = buffer(pos+7,1):le_uint()
      elseif buffer_size[identifier] == 325 then
        immediate = buffer(pos+4,4):le_float()
      end
      item_subtree:append_text(" = " .. immediate)
    end
    pos = pos + 8 -- advance through the packet buffer 64 bits.
  end -- for loop
  if payload_len > 0 then
    subtree:add(f_payload, buffer(payload_start, payload_len))
    subtree:set_len(payload_start+payload_len)
  end
  return buffer:len()
end -- function p_spead

local spead_sub_dt = DissectorTable.new("spead_sd")
spead_sub_dt:add(0, p_spead)
spead_sub_dt:add(1, p_item_ptr)

-- Register SPEAD as disector for UDP.
p_spead:register_heuristic("udp", p_spead.dissector)
p_spead:register_heuristic("infiniband.payload", p_spead.dissector)
