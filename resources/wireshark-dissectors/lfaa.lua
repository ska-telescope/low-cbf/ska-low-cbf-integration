--[[
    Wireshark dissector for TPM packet format. See LFAA-CSP ICD document for
    details of the packet format

    CSIRO 15 Jan 2021
    v0.2
--]]

-- define debug levels
local debug_level = {
    DISABLED = 0,
    LEVEL_1  = 1,
    LEVEL_2  = 2
}

-- set this DEBUG to debug_level.LEVEL_1 to enable printing debug_level info
-- set it to debug_level.LEVEL_2 to enable really verbose printing
-- note: this will be overridden by user's preference settings
local DEBUG = debug_level.LEVEL_1

local default_settings =
{
    debug_level  = DEBUG,
}

-- for testing purposes, we want to be able to pass in changes to the defaults
-- from the command line; because you can't set lua preferences from the command
-- line using the '-o' switch (the preferences don't exist until this script is
-- loaded, so the command line thinks they're invalid preferences being set)
-- so we pass them in as command arguments insetad, and handle it here:
local args={...} -- get passed-in args
if args and #args > 0 then
    for _, arg in ipairs(args) do
        local name, value = arg:match("(.+)=(.+)")
        if name and value then
            if tonumber(value) then
                value = tonumber(value)
            elseif value == "true" or value == "TRUE" then
                value = true
            elseif value == "false" or value == "FALSE" then
                value = false
            elseif value == "DISABLED" then
                value = debug_level.DISABLED
            elseif value == "LEVEL_1" then
                value = debug_level.LEVEL_1
            elseif value == "LEVEL_2" then
                value = debug_level.LEVEL_2
            else
                error("invalid commandline argument value")
            end
        else
            error("invalid commandline argument syntax")
        end

        default_settings[name] = value
    end
end

local dprint = function() end
local dprint2 = function() end
local function reset_debug_level()
    if default_settings.debug_level > debug_level.DISABLED then
        dprint = function(...)
            print(table.concat({"Lua:", ...}," "))
        end

        if default_settings.debug_level > debug_level.LEVEL_1 then
            dprint2 = dprint
        end
    end
end
-- call it now
reset_debug_level()

dprint2("Wireshark version = ", get_version())
dprint2("Lua version = ", _VERSION)

----------------------------------------
-- check for older buggy versions
local major, minor, micro = get_version():match("(%d+)%.(%d+)%.(%d+)")
if major and tonumber(major) <= 1 and ((tonumber(minor) <= 10) or (tonumber(minor) == 11 and tonumber(micro) < 3)) then
        error(  "Wireshark ("..get_version()..") is old!\n"..
                "Requires Wireshark version 1.11.3 or higher.\n" )
end
assert(ProtoExpert.new, "ProtoExpert missing - get Wireshark 1.11.3 or higher")

----------------------------------------


----------------------------------------
-- create Proto object that will be registered after configuration
local dbg0 = Proto("LFAA","SPS")

-- provide less verbose access to the fields part of the protocol object
local f = dbg0.fields

-- Description of each of the possible fields in the Gemini Protocol
--f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version")
--f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version", base.HEX, nil, 0xFF)

f.pf_magicver = ProtoField.uint16("dbg0.magicver","MagicV ", base.HEX, nil, nil)
f.pf_logchan  = ProtoField.uint16("dbg0.logchan" ,"lChan  ", base.HEX, nil, nil)
f.pf_pktcnt =   ProtoField.uint32("dbg0.pktcnt"  ,"PktCnt ", base.HEX, nil, nil)
f.pf_tsnsec  =  ProtoField.uint64("dbg0.tsnsec",  "nSec   ", base.HEX, nil, nil)
f.pf_beamid  =  ProtoField.uint16("dbg0.beamID",  "BeamID ", base.HEX, nil, nil)
f.pf_freqid =   ProtoField.uint16("dbg0.freqID",  "FreqID ", base.HEX, nil, nil)
f.pf_substn =   ProtoField.uint8("dbg0.substn",   "SubStn ", base.HEX, nil, nil)
f.pf_subary =   ProtoField.uint8("dbg0.subary",   "SubArry", base.HEX, nil, nil)
f.pf_stnid =    ProtoField.uint16("dbg0.stnid",    "StnID  ", base.HEX, nil, nil)

--f.pf_dest =  ProtoField.uint16("dbg0.dest",     "Dest XYZ ",    base.HEX, nil, nil)
--f.pf_type =  ProtoField.uint8 ("dbg0.pkt_type", "Pkt Type ",    nil, dbg0_pkt_types, nil)




----------------------------------------
-- Expert info fields
local ef_query     = ProtoExpert.new("dbg0ini.query.expert", "DNS query message",
                                     expert.group.REQUEST_CODE, expert.severity.CHAT)
local ef_response  = ProtoExpert.new("dbg0ini.response.expert", "DNS response message",
                                     expert.group.RESPONSE_CODE, expert.severity.CHAT)
local ef_ultimate  = ProtoExpert.new("dbg0ini.response.ultimate.expert", "DNS answer to life, the universe, and everything",
                                     expert.group.COMMENTS_GROUP, expert.severity.NOTE)
-- some error expert info's
local ef_too_short = ProtoExpert.new("dbg0ini.too_short.expert", "PSR packet too short",
                                     expert.group.MALFORMED, expert.severity.ERROR)
local ef_bad_query = ProtoExpert.new("dbg0ini.query.missing.expert", "DNS query missing or malformed",
                                     expert.group.MALFORMED, expert.severity.WARN)
-- add expert info into the proto object
dbg0.experts = { ef_query, ef_too_short, ef_bad_query, ef_response, ef_ultimate }



----------------------------------------
--[[ This function is the callback that Wireshark will call when it finds 
     UDP packets to/from the PSR port that we registered. 
     The callback generates information about the PSR part
     of the UDP packet that will be displayed by Wireshark
        'tvbuf' is a Tvb buffer object containing packet bytes,
        'pktinfo' is a Pinfo object, and 
        'root' is a TreeItem object to which we append our dissection text.
--]]
--dbg0.dissector = function (tvbuf,pkt,root)
function dbg0.dissector(tvbuf,pktinfo,root)
    dprint2("dbg0.dissector called")

    -- set the protocol column to show our protocol name
    --pktinfo.cols.protocol:set("Gemini_v" .. tvbuf:range(0,1))
    pktinfo.cols.protocol:set("SPS")

    local pktlen = tvbuf:reported_length_remaining()

    local DBG0_MIN_LEN = 72
    -- Guard against random packets arriving at debug UDP ports
    if pktlen < DBG0_MIN_LEN then
        tree:add_proto_expert_info(ef_too_short)
        dprint("packet length",pktlen,"too short")
        return
    end

    --[[ Add PSR Metadata to the dissection display tree and get tree
         object so we can add subtree items under it
    --]]
    local tree = root:add(dbg0, tvbuf:range(0,DBG0_MIN_LEN))

    -- Add dissected values into subtree 
    local bytes_parsed = 0
    tree:add(f.pf_magicver,   tvbuf:range(0,2):uint())
    tree:add(f.pf_logchan ,   tvbuf:range(10,2):uint())
    tree:add(f.pf_pktcnt  ,   tvbuf:range(12,4):uint())
    tree:add(f.pf_tsnsec  ,   tvbuf:range(32,8):uint64())
    tree:add(f.pf_beamid  ,   tvbuf:range(52,2):uint())
    tree:add(f.pf_freqid  ,   tvbuf:range(54,2):uint())
    tree:add(f.pf_substn  ,   tvbuf:range(58,1):uint())
    tree:add(f.pf_subary  ,   tvbuf:range(59,1):uint())
    tree:add(f.pf_stnid   ,   tvbuf:range(60,2):uint())
--    tree:add(f.pf_atto,   tvbuf:range(8,8):le_uint64())
--    tree:add(f.pf_sep,    tvbuf:range(20,4):le_uint())
--    tree:add(f.pf_frq,    tvbuf:range(24,8):le_uint64())
--    tree:add(f.pf_sca1,   tvbuf:range(32,4):le_uint())
--    tree:add(f.pf_sca2,   tvbuf:range(36,4):le_uint())
--    tree:add(f.pf_sca3,   tvbuf:range(40,4):le_uint())
--    tree:add(f.pf_sca4,   tvbuf:range(44,4):le_uint())
--    tree:add(f.pf_chan,   tvbuf:range(48,4):le_uint())
--    tree:add(f.pf_chpkt,   tvbuf:range(52,2):le_uint())
--    tree:add(f.pf_valch,   tvbuf:range(54,2):le_uint())
--    tree:add(f.pf_tsch,   tvbuf:range(56,2):le_uint())
--    tree:add(f.pf_beam,   tvbuf:range(58,2):le_uint())
--    tree:add(f.pf_magic,   tvbuf:range(60,4):le_uint())
--    tree:add(f.pf_dest,   tvbuf:range(64,1):le_uint())
--    tree:add(f.pf_prec,   tvbuf:range(65,1):le_uint())
--    tree:add(f.pf_pavg,   tvbuf:range(66,1):le_uint())
--    tree:add(f.pf_tswt,   tvbuf:range(67,1):le_uint())
--    tree:add(f.pf_osnum,   tvbuf:range(68,1):le_uint())
--    tree:add(f.pf_osden,   tvbuf:range(69,1):le_uint())
--    tree:add(f.pf_bfver,   tvbuf:range(70,2):le_uint())
--   tree:add(f.pf_scanid,   tvbuf:range(72,8):le_uint64())
--    tree:add(f.pf_offs1,   tvbuf:range(80,4):le_uint())
--    tree:add(f.pf_offs2,   tvbuf:range(84,4):le_uint())
--    tree:add(f.pf_offs3,   tvbuf:range(88,4):le_uint())
--    tree:add(f.pf_offs4,   tvbuf:range(92,4):le_uint())

    bytes_parsed = DBG0_MIN_LEN

    -- tell wireshark how much of the buffer was parsed/dissected
    dprint2("dbg0.dissector returning",bytes_parsed)
    return bytes_parsed

end

----------------------------------------

-- Register protocol dissector based on UDP port
DissectorTable.get("udp.port"):add(4660, dbg0)

