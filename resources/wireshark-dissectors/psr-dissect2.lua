--[[
    Wireshark dissector for SKA PSR packet format. See documentation:
    https://docs.google.com/document/d/1MMu38QMe7gUuV_bCBYHYfI6VxVaJby7c/edit

    CSIRO - 15 Jan 2021
    Fourier Space - 30 Jan 2024
    v0.3
    v0.4 Updated 2024-05-03 with:
        new Timestamp replacing "timestamp attoseconds"
        new Sample period replacing "timestamp seconds"
        new Validity field replacing "oversampling numerator"
    v0.4.2 2024-11-18 - Fix & expand Validity bits decoding
--]]

-- define debug levels
local debug_level = {
    DISABLED = 0,
    LEVEL_1  = 1,
    LEVEL_2  = 2
}

-- set this DEBUG to debug_level.LEVEL_1 to enable printing debug_level info
-- set it to debug_level.LEVEL_2 to enable really verbose printing
-- note: this will be overridden by user's preference settings
local DEBUG = debug_level.LEVEL_1

local default_settings =
{
    debug_level  = DEBUG,
}

-- for testing purposes, we want to be able to pass in changes to the defaults
-- from the command line; because you can't set lua preferences from the command
-- line using the '-o' switch (the preferences don't exist until this script is
-- loaded, so the command line thinks they're invalid preferences being set)
-- so we pass them in as command arguments insetad, and handle it here:
local args={...} -- get passed-in args
if args and #args > 0 then
    for _, arg in ipairs(args) do
        local name, value = arg:match("(.+)=(.+)")
        if name and value then
            if tonumber(value) then
                value = tonumber(value)
            elseif value == "true" or value == "TRUE" then
                value = true
            elseif value == "false" or value == "FALSE" then
                value = false
            elseif value == "DISABLED" then
                value = debug_level.DISABLED
            elseif value == "LEVEL_1" then
                value = debug_level.LEVEL_1
            elseif value == "LEVEL_2" then
                value = debug_level.LEVEL_2
            else
                error("invalid commandline argument value")
            end
        else
            error("invalid commandline argument syntax")
        end

        default_settings[name] = value
    end
end

local dprint = function() end
local dprint2 = function() end
local function reset_debug_level()
    if default_settings.debug_level > debug_level.DISABLED then
        dprint = function(...)
            print(table.concat({"Lua:", ...}," "))
        end

        if default_settings.debug_level > debug_level.LEVEL_1 then
            dprint2 = dprint
        end
    end
end
-- call it now
reset_debug_level()

dprint2("Wireshark version = ", get_version())
dprint2("Lua version = ", _VERSION)

----------------------------------------
-- check for older buggy versions
local major, minor, micro = get_version():match("(%d+)%.(%d+)%.(%d+)")
if major and tonumber(major) <= 1 and ((tonumber(minor) <= 10) or (tonumber(minor) == 11 and tonumber(micro) < 3)) then
        error(  "Wireshark ("..get_version()..") is old!\n"..
                "Requires Wireshark version 1.11.3 or higher.\n" )
end
assert(ProtoExpert.new, "ProtoExpert missing - get Wireshark 1.11.3 or higher")

----------------------------------------


----------------------------------------
-- create Proto object that will be registered after configuration
local dbg0 = Proto("PSR","PSR Metadata")

-- provide less verbose access to the fields part of the protocol object
local f = dbg0.fields

-- Description of each of the possible fields in the Gemini Protocol
--f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version")
--f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version", base.HEX, nil, 0xFF)

-- Word 0
f.pf_seq =    ProtoField.uint64("dbg0.seq",     "SeqNo          ", base.DEC, nil, nil)
-- Word 1
f.pf_samp =   ProtoField.uint64("dbg0.samp",    "Sample Num     ", base.DEC, nil, nil)
-- Word 2
f.pf_perio =  ProtoField.uint32("dbg0.perio",   "Period uSec    ", base.HEX, nil, nil)
f.pf_sep  =   ProtoField.uint32("dbg0.sep",     "Chan Sep mHz   ", base.DEC, nil, nil)
-- Word 3
f.pf_frq  =   ProtoField.uint64("dbg0.freq",    "Chan Frq mHz   ", base.DEC, nil, nil)
-- Word 4
f.pf_sca1 =   ProtoField.float("dbg0.sca1",     "Scale1         ", base.DEC, nil, nil)
f.pf_sca2 =   ProtoField.float("dbg0.sca2",     "Scale2         ", base.DEC, nil, nil)
-- Word 5
f.pf_sca3 =   ProtoField.float("dbg0.sca3",     "Scale3         ", base.DEC, nil, nil)
f.pf_sca4 =   ProtoField.float("dbg0.sca4",     "Scale4         ", base.DEC, nil, nil)
-- Word 6
f.pf_chan =   ProtoField.uint32("dbg0.channel", "1st Channel    ", base.DEC, nil, nil)
f.pf_chpkt =  ProtoField.float("dbg0.chpkt",    "Chan/Pkt       ", base.DEC, nil, nil)
f.pf_chvld =  ProtoField.float("dbg0.chvld",    "Valid Chan/Pkt ", base.DEC, nil, nil)
-- Word 7
f.pf_nsamp =  ProtoField.float("dbg0.nsamp",    "Nsamp/Pkt      ", base.DEC, nil, nil)
f.pf_beam =   ProtoField.uint16("dbg0.beam",    "BeamID         ", base.DEC, nil, nil)
f.pf_magic =  ProtoField.uint32("dbg0.magic",   "MagicWord      ", base.HEX, nil, nil)
-- Word 8
f.pf_dest =   ProtoField.string("dbg0.dest",    "PacketDest     ", nil,      nil, nil)
f.pf_prec =   ProtoField.uint8("dbg0.prec",     "DataPrecision  ", base.DEC, nil, nil)
f.pf_pavg =   ProtoField.uint8("dbg0.pavg",     "NPowerSampAvg  ", base.DEC, nil, nil)
f.pf_tswt =   ProtoField.uint8("dbg0.tswt",     "SampPerRelWt   ", base.DEC, nil, nil)
f.pf_valid =  ProtoField.uint8("dbg0.valid",    "Validity       ", base.DEC, nil, nil)
f.pf_rsvd =   ProtoField.uint8("dbg0.rsvd",     "Reserved.      ", base.DEC, nil, nil)
f.pf_bfver = ProtoField.uint16("dbg0.bfvmaj",   "CBF version.   ", base.DEC, nil, nil)
-- Word 9
f.pf_scanid = ProtoField.uint64("dbg0.scanid",  "ScanID         ", base.DEC, nil, nil)
-- Word 10
f.pf_offs1 =  ProtoField.float("dbg0.offs1",    "Offset1        ", base.DEC, nil, nil)
f.pf_offs2 =  ProtoField.float("dbg0.offs2",    "Offset2        ", base.DEC, nil, nil)
-- Word 11
f.pf_offs3 =  ProtoField.float("dbg0.offs3",    "Offset3        ", base.DEC, nil, nil)
f.pf_offs4 =  ProtoField.float("dbg0.offs4",    "Offset4        ", base.DEC, nil, nil)
f.pf_relwts = ProtoField.bytes("dbg0.relwts",   "Rel. Weights   ", base.BYTES, nil, nil)
f.pf_data   = ProtoField.bytes("dbg0.data",     "Data           ", base.BYTES, nil, nil)

--f.pf_dest =  ProtoField.uint16("dbg0.dest",     "Dest XYZ ",    base.HEX, nil, nil)
--f.pf_type =  ProtoField.uint8 ("dbg0.pkt_type", "Pkt Type ",    nil, dbg0_pkt_types, nil)

-- decoding of microsecond sample period field
f.pf_pnum =   ProtoField.uint32("dbg0.pnum",    "Period Numer.  ", base.DEC, nil, nil, "uSec Numerator")
f.pf_pden =   ProtoField.uint32("dbg0.pden",    "Period Denom.  ", base.DEC, nil, nil, "uSec Denominator")

-- Decoding of validity field
f.pf_pss_valid_jones = ProtoField.bool("psr.valid.pss-valid-jones",    "PSS: Stn Jones",
        6, {"Valid", "Invalid"}, 0x20, "PSS Only: Was valid Station Jones used?")
f.pf_pss_default_jones = ProtoField.bool("psr.valid.pss-default-jones","PSS: Stn Jones",
        6, {"Unity Default", "SDP Calculated"}, 0x10, "PSS Only: Was default Station Jones used?")

f.pf_valid_jones = ProtoField.bool("psr.valid.valid-jones",            "Beam Jones    ",
        6, {"Valid", "Invalid"}, 0x08, "Was valid Beam Jones used?")
f.pf_default_jones = ProtoField.bool("psr.valid.default-jones",        "Beam Jones    ",
        6, {"Unity Default", "SDP Calculated"}, 0x04, "Was default Beam Jones used?")

f.pf_pst_delay = ProtoField.bool("psr.valid.pst-delay",                "Beam Delays   ",
        6, {"Valid", "Invalid"}, 0x02, "PSS/PST delays valid?")
f.pf_stn_delay = ProtoField.bool("psr.valid.stn-delay",                "Station Delays",
        6, {"Valid", "Invalid"}, 0x01, "Station beam delays valid?")


----------------------------------------
-- Expert info fields
local ef_query     = ProtoExpert.new("dbg0ini.query.expert", "DNS query message",
                                     expert.group.REQUEST_CODE, expert.severity.CHAT)
local ef_response  = ProtoExpert.new("dbg0ini.response.expert", "DNS response message",
                                     expert.group.RESPONSE_CODE, expert.severity.CHAT)
local ef_ultimate  = ProtoExpert.new("dbg0ini.response.ultimate.expert", "DNS answer to life, the universe, and everything",
                                     expert.group.COMMENTS_GROUP, expert.severity.NOTE)
-- some error expert info's
local ef_too_short = ProtoExpert.new("dbg0ini.too_short.expert", "PSR packet too short",
                                     expert.group.MALFORMED, expert.severity.ERROR)
local ef_bad_query = ProtoExpert.new("dbg0ini.query.missing.expert", "DNS query missing or malformed",
                                     expert.group.MALFORMED, expert.severity.WARN)
local ef_magic_invalid = ProtoExpert.new("dbg0ini.invalid_sync.export", "MagicWord must be 0xBEADFEED",
                                      expert.group.MALFORMED, expert.severity.WARN)
local ef_bad_dest = ProtoExpert.new("dbg0ini.bad_dest.export", "PacketDest not in [0,1,2,3]",
                                      expert.group.MALFORMED, expert.severity.WARN)
local ef_bad_prec = ProtoExpert.new("dbg0ini.bad_prec.export", "DataPrecision not in [8,16]",
                                      expert.group.MALFORMED, expert.severity.WARN)
-- add expert info into the proto object
dbg0.experts = { ef_query, ef_too_short, ef_bad_query, ef_response, ef_ultimate, ef_magic_invalid, ef_bad_dest, ef_bad_prec }



----------------------------------------
--[[ This function is the callback that Wireshark will call when it finds
     UDP packets to/from the PSR port that we registered.
     The callback generates information about the PSR part
     of the UDP packet that will be displayed by Wireshark
        'tvbuf' is a Tvb buffer object containing packet bytes,
        'pktinfo' is a Pinfo object, and
        'root' is a TreeItem object to which we append our dissection text.
--]]
--dbg0.dissector = function (tvbuf,pkt,root)

-- present PacketDest field as a string
local destination_type = {
   [0] = "Low PSS",
   [1] = "Mid PSS",
   [2] = "Low PST",
   [3] = "Mid PST"
}

function dbg0.dissector(tvbuf,pktinfo,root)
    dprint2("dbg0.dissector called")

    -- set the protocol column to show our protocol name
    --pktinfo.cols.protocol:set("Gemini_v" .. tvbuf:range(0,1))
    pktinfo.cols.protocol:set("PSR (Pulsar)")

    local pktlen = tvbuf:reported_length_remaining()

    local DBG0_MIN_LEN = 96
    -- Guard against random packets arriving at debug UDP ports
    if pktlen < DBG0_MIN_LEN then
        tree:add_proto_expert_info(ef_too_short)
        dprint("packet length",pktlen,"too short")
        return
    end

    --[[ Add PSR Metadata to the dissection display tree and get tree
         object so we can add subtree items under it
    --]]
    local tree = root:add(dbg0, tvbuf:range(0,DBG0_MIN_LEN))

    -- Add dissected values into subtree
    local bytes_parsed = 0
    -- Word 0
    tree:add(f.pf_seq ,   tvbuf:range(0,8):le_uint64())
    -- Word 1
    tree:add(f.pf_samp,   tvbuf:range(8,8):le_uint64())
    -- Word 2
        -- Sample microsecond period decoding
        local ratio_tree = tree:add(f.pf_perio,  tvbuf:range(16,4):le_uint())
        local numer = tvbuf:range(16,2)
        ratio_tree:add(f.pf_pnum,   numer:le_uint())
        local denom = tvbuf:range(18,2)
        ratio_tree:add(f.pf_pden,   denom:le_uint())
    tree:add(f.pf_sep,    tvbuf:range(20,4):le_uint())
    -- Word 3
    tree:add(f.pf_frq,    tvbuf:range(24,8):le_uint64())
    -- Word 4
    tree:add(f.pf_sca1,   tvbuf:range(32,4):le_float())
    tree:add(f.pf_sca2,   tvbuf:range(36,4):le_float())
    -- Word 5
    tree:add(f.pf_sca3,   tvbuf:range(40,4):le_float())
    tree:add(f.pf_sca4,   tvbuf:range(44,4):le_float())
    -- Word 6
    tree:add(f.pf_chan,   tvbuf:range(48,4):le_uint())
    tree:add(f.pf_chpkt,  tvbuf:range(52,2):le_uint())
    tree:add(f.pf_chvld,  tvbuf:range(54,2):le_uint())
    -- Word 7
    tree:add(f.pf_nsamp,  tvbuf:range(56,2):le_uint())
    tree:add(f.pf_beam,   tvbuf:range(58,2):le_uint())
    local field_magic = tvbuf:range(60,4):le_uint()
    tree:add(f.pf_magic,   tvbuf:range(60,4), field_magic)
    if field_magic ~= 0xBEADFEED then -- Should be constant
        tree:add_proto_expert_info(ef_magic_invalid)
    end
    -- Word 8
    local dest = tvbuf:range(64,1):le_uint()
    dest_str = destination_type[ dest ]
    tree:add(f.pf_dest, dest_str)
    if (dest ~= 0 and dest ~= 1 and dest ~= 2 and dest ~= 3) then
       tree:add_proto_expert_info(ef_bad_dest)
    end
    tree:add(f.pf_prec,   tvbuf:range(65,1):le_uint())
    local prec = tvbuf:range(65,1):le_uint()
    if (prec ~= 8 and prec ~= 16) then
       tree:add_proto_expert_info(ef_bad_prec)
    end
    tree:add(f.pf_pavg,   tvbuf:range(66,1):le_uint())
    tree:add(f.pf_tswt,   tvbuf:range(67,1):le_uint())
    -- decode valid bits
        local flagrange = tvbuf:range(68,1)
        local valid_tree = tree:add(f.pf_valid, flagrange)
        valid_tree:add(f.pf_pss_valid_jones, flagrange)
        valid_tree:add(f.pf_pss_default_jones, flagrange)
        valid_tree:add(f.pf_valid_jones, flagrange)
        valid_tree:add(f.pf_default_jones, flagrange)
        valid_tree:add(f.pf_pst_delay, flagrange)
        valid_tree:add(f.pf_stn_delay, flagrange)
    tree:add(f.pf_rsvd,   tvbuf:range(69,1):le_uint())
    tree:add(f.pf_bfver,  tvbuf:range(70,2):le_uint())
    -- Word 9
    tree:add(f.pf_scanid,   tvbuf:range(72,8):le_uint64())
    -- Word 10
    tree:add(f.pf_offs1,   tvbuf:range(80,4):le_float())
    tree:add(f.pf_offs2,   tvbuf:range(84,4):le_float())
    -- Word 11
    tree:add(f.pf_offs3,   tvbuf:range(88,4):le_float())
    tree:add(f.pf_offs4,   tvbuf:range(92,4):le_float())

    local nchan = tvbuf:range(52,2):le_uint()
    local nbit = tvbuf:range(65,1):le_uint()
    local nsamp = tvbuf:range(56,2):le_uint()
    local dest = tvbuf:range(64,1):le_uint()

    -- determine npol and ndim from the packet destination
    local npol = 2
    local ndim = 2
    if dest == 0 then
      -- Low.PSS
      nbit = 8
    elseif dest == 1 then
      -- Mid.PSS
      nbit = 8
      npol = 4
      ndim = 1
    else
      -- Low.PST & Mid.PST
      nbit = 16
    end

    local wts_nbit = 2
    local relwts_len = nchan * wts_nbit
    local relwts_padlen = math.ceil(relwts_len/16)*16
    tree:add(f.pf_relwts,  tvbuf:range(96, relwts_padlen))

    local data_len = (nbit * nchan * nsamp * npol * ndim) / 8
    dprint2("dbg0.dissector data_len",data_len)
    tree:add(f.pf_data,  tvbuf:range(96+relwts_padlen,data_len))
    bytes_parsed = 96+relwts_padlen+data_len

    --bytes_parsed = 96+relwts_padlen

    -- tell wireshark how much of the buffer was parsed/dissected
    dprint2("dbg0.dissector returning",bytes_parsed)
    return bytes_parsed

end

----------------------------------------

-- Register protocol dissector based on UDP port
DissectorTable.get("udp.port"):add(9510, dbg0)


-- ------------------------------------------------------------------
--  Information about this plugin
-- ------------------------------------------------------------------
local plugin_info =
{
    version     = "0.4.2",
    author      = "CSIRO, Fourier Space",
    description = "PSR protocol dissector",
    repository  = "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration/"
}

set_plugin_info( plugin_info )
