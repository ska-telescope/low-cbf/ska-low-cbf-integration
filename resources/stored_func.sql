-- Functions used by Grafana queries
--
-- you can insert these functions into ska_low_cbf database from psql client with:
--  => \i <this_file_name.sql>
--

-- ----------------------------------------------------------------------------------------
--  PROCESSOR related functions
-- ----------------------------------------------------------------------------------------

-- Get the PROCESSOR 'subarrayids' ID (Tango attribute: 'subarrayids') (from att_conf.att_conf_id column)
--  for the specified K8S namespace and processor name (e.g. '0.0.0')
CREATE OR REPLACE FUNCTION get_subarray_config_id( ns text, processor text ) RETURNS integer
STABLE
LANGUAGE plpgsql AS
$func$
  BEGIN
    RETURN att_conf_id FROM att_conf
        WHERE att_name LIKE concat( '%', ns, '%' )
        AND family = 'processor'
        AND name = 'subarrayids'
        AND member = processor;
  END;
$func$;


-- Get the 'subarrayids' ID (Tango attribute: 'subarrayids') (from att_conf.att_conf_id column)
--  for the specified K8S namespace and processor name (e.g. '0.0.0')
CREATE OR REPLACE FUNCTION get_beam_config_id( ns text, processor text ) RETURNS integer
STABLE
LANGUAGE plpgsql AS
$func$
  BEGIN
    RETURN att_conf_id FROM att_conf
        WHERE att_name LIKE concat( '%', ns, '%' )
        AND family = 'processor'
        AND member = processor
        AND name   = 'beamids';
  END;
$func$;
-- TESTS:  SELECT get_beam_config_id( 'bb-test', '0.0.0');
-- beams are stored in att_image_devlong64 table


-- Get a list of subarrays this processor belongs to
CREATE OR REPLACE FUNCTION get_subarrays( ns text, processor text ) RETURNS integer[]
STABLE
LANGUAGE plpgsql AS
$func$
  BEGIN
    RETURN value_r FROM att_array_devushort INNER JOIN att_conf
                   ON att_array_devushort.att_conf_id = get_subarray_config_id( ns, processor )
                   WHERE array_length( value_r, 1 ) > 0
                   ORDER BY data_time DESC LIMIT 1;
  END
$func$;
-- TEST:  SELECT get_subarrays( 'bb-test', '0.0.0');
--        SELECT 4 = ANY( get_subarrays('bb-test', '0.0.0') );


-- Get the list of beams this processor handles
CREATE OR REPLACE FUNCTION get_beams( subarrayid int, ns text, processor text ) RETURNS int[]
STABLE
LANGUAGE plpgsql AS
$func$
  BEGIN
    -- filter out beams with ID of 0
    RETURN array_remove( ARRAY( SELECT * FROM UNNEST(
                           ( SELECT value_r FROM att_image_devlong64 INNER JOIN att_conf
                             ON att_image_devlong64.att_conf_id = get_beam_config_id( ns, processor )
                             ORDER BY data_time DESC LIMIT 1 )[ subarrayid:subarrayid ] ) ),
                         0 );
  END;
$func$;
-- TEST:  SELECT get_beams( 4, 'bb-test', '0.0.0' );



-- Fetch the name of personality (CORR, PST) loaded into Alveo
CREATE OR REPLACE FUNCTION get_alveo_personality_name( ns text, processor text ) RETURNS text
STABLE
LANGUAGE plpgsql AS
$func$
  BEGIN
    RETURN value_r FROM att_scalar_devstring INNER JOIN att_conf ON att_conf.att_conf_id = att_scalar_devstring.att_conf_id
        WHERE att_conf.name   = 'fw_personality' AND
              att_conf.domain = 'low-cbf' AND
              att_conf.att_name LIKE concat( '%', ns, '%' ) AND
              att_conf.member = processor  AND
              att_scalar_devstring.quality = 0
        ORDER BY data_time DESC LIMIT 1;
  END
$func$;
-- TEST:  SELECT get_alveo_personality_name( 'bb-test', '0.0.0' );


-- ----------------------------------------------------------------------------------------
--  SUBARRAY related functions
-- ----------------------------------------------------------------------------------------

-- get subarray (Tango) attribute id for the specified subarray
-- example attribute: delaysvalid
CREATE OR REPLACE FUNCTION get_subarray_attr_id( ns text, subarray int, attr text ) RETURNS integer
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN att_conf_id FROM att_conf
        WHERE att_name LIKE concat( '%', ns, '%' )
        AND family = 'subarray'
        AND name   = attr
        AND member::int = subarray;
  END;
$$;
-- TEST: SELECT get_subarray_attr_id( 'bb-test', 4, 'stationdelays' );


-- check if all delay polynomials in the speciried subarray are valid
-- returns: 0: INVALID, 1: VALID, 2: UNUSED
CREATE OR REPLACE FUNCTION are_subarray_delay_poly_valid( ns text, subarray int ) RETURNS int
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN value_r FROM att_scalar_devushort
        WHERE att_conf_id = get_subarray_attr_id( ns, subarray, 'delaysvalid' )
        ORDER BY data_time DESC LIMIT 1;
  END;
$$;
-- TEST: SELECT are_subarray_delay_poly_valid( 'bb-test', 4 );

-- return ROWS of att_conf_id for each SUBARRAY's delaysValid attribute
CREATE OR REPLACE FUNCTION get_all_subarray_delay_valid_ids( ns text ) RETURNS SETOF int
-- NOTE  the difference compared to get_subarray_attr_id is that it returns IDs for all subarrays,
--       the other one only for a specific subarray
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT att_conf_id FROM att_conf
        WHERE family='subarray'
        AND   name='delaysvalid'
        AND   domain='low-cbf'
        AND   att_name LIKE concat( '%', ns, '%' );
  END;
$$;
-- TEST: SELECT get_all_subarray_delay_valid_ids( 'bb-test' );

-- return JSON string for (specified, id) PROCESSOR's stationDelayValid attribute
-- e.g.
--  {
--    "subarray_id": 4,
--    "beams": [
--      {
--        "beam_id": 1,
--        "valid_delay": true,
--        "subscription_valid": true,
--        "pst": [
--          {
--            "name": "low-cbf/delaypoly/0/pst_s04_b01_1",
--            "valid_delay": true
--            "subscription_valid": true
--          },
--        ]
--      }
--    ]
--  }
CREATE OR REPLACE FUNCTION get_proc_station_delay_valid_str( ns text, id int ) RETURNS text
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN value_r FROM att_scalar_devstring
        WHERE att_conf_id = id
        ORDER BY data_time DESC
        LIMIT 1;
  END;
$$;
-- TEST: SELECT get_proc_station_delay_valid_str( 'bb-test', 271 );


-- unpack AND return as ROWS of (otherwise array) entries:
--   { "subarray_id": 4, "beams": [{"beam_id": 1, "valid_delay": false}] }
-- depens on: get_proc_station_delay_valid_str
CREATE OR REPLACE FUNCTION get_proc_station_delays_as_rows( ns text, id int ) RETURNS SETOF jsonb
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT * FROM jsonb_array_elements( get_proc_station_delay_valid_str( ns, id )::jsonb );
  END;
$$;
-- TEST: SELECT get_proc_station_delays_as_rows( 'bb-test', 271 );

-- --------------------------------------------------------------------------------
-- return rows of  of beam_id / valid pairs for all beams in the given subarray
-- e.g.    beam_id | valid  | subscription ok
--        ---------+--------+-----------------
--         1       | false  |  true
--         2       | true   |  true
-- This is a "top level" function used by Grafana query that depends on several
-- "low level" functions:
--   - get_procedure_station_delays_as_rows()
--   - get_all_station_delay_attr_ids()
CREATE OR REPLACE FUNCTION get_proc_station_delays_for_subarr( ns text, subarray_id int )
       RETURNS TABLE ( beam_id text, valid text, subscription text )
STABLE
LANGUAGE plpgsql AS
$$
  DECLARE
    id int; -- att_conf_id for PROCESSOR stationDelayValid attribute
  BEGIN
    FOR id IN SELECT get_all_station_delay_attr_ids( ns )
    LOOP
      RETURN QUERY SELECT jj->>'beam_id', jj->>'valid_delay', jj->>'subscription_valid'
        FROM jsonb_array_elements(
        -- subquery: pick only 'beams' belonging to the specified subarray:
        ( SELECT j->'beams' FROM get_proc_station_delays_as_rows( ns, id ) AS j
          WHERE j->>'subarray_id' = subarray_id::text ) ) AS jj;
    END LOOP;
  END;
$$;
-- TEST: SELECT * FROM get_proc_station_delays_for_subarr( 'bb-test', 4 );


-- -----------------------------------------------------------------------------
-- Unpack PST beam delays for displaying on Grafana panel
--
-- The information is packed in JSON consisting of nested (multiple level)
-- arrays and objects and we need to iterate over arrays and filter on
-- subarray/beam IDs.
--
-- This block of functins is delimited by a line consisting of === characters
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_pst_delays_for_subarr_beam( ns text, subarray_id int, beam_id int )
       RETURNS TABLE ( name text, valid text, subscr_ok text )
STABLE -- FIXME review STABLE
LANGUAGE plpgsql AS
$$
  DECLARE
    id int; -- att_conf_id for PROCESSOR stationDelayValid Tango attribute
  BEGIN
    -- different processors will have different att_conf_id for their attributes
    FOR id IN SELECT get_all_station_delay_attr_ids( ns )
    LOOP
        RETURN QUERY SELECT jj->>'name', jj->>'valid_delay', jj->>'subscription_valid'
        FROM get_pst_delays_json( ns, id, subarray_id, beam_id )  AS jj;
    END LOOP;
  END;
$$;
-- TEST: SELECT name, valid, subscr_ok FROM get_pst_delays_for_subarr_beam( 'bb-test', 4, 1 );

-- return delays for ALL SUBARRAYS in the given namepace 'ns' (as rows)
CREATE OR REPLACE FUNCTION get_all_delays_json( ns text, att_id int ) RETURNS SETOF jsonb
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT jsonb_array_elements( get_proc_station_delay_valid_str( ns, att_id )::jsonb );
  END;
$$;
-- TEST: SELECT get_all_delays_json( 'bb-test', 293 );

-- return delays FILTERED for the specified SUBARRAY (i.e. all beams) (as rows)
CREATE OR REPLACE FUNCTION get_beam_delays_json( ns text, att_id int, sub_id int ) RETURNS SETOF jsonb
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT jsonb_array_elements(
             ( SELECT COL->'beams' FROM get_all_delays_json( ns, att_id ) AS COL
               WHERE  COL->>'subarray_id' = sub_id::text) );
  END;
$$;
-- TEST: SELECT get_beam_delays_json( 'bb-test', 293, 4 );
-- to get PST delays from the above:
--   SELECT COL->'pst' pst FROM get_beam_delays_json( 'bb-test', 293, 4 ) COL WHERE COL->'beam_id' = '1';


CREATE OR REPLACE FUNCTION get_pst_delays_json( ns text, att_id int, sub_id int, beam_id int ) RETURNS SETOF jsonb
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT jsonb_array_elements(
             ( SELECT COL->'pst' FROM get_beam_delays_json( ns, att_id, sub_id ) AS COL
               WHERE  COL->>'beam_id' = beam_id::text)::jsonb );
  END;
$$;
-- TEST: SELECT get_pst_delays_json( 'bb-test', 293, 4, 1 );
-- == PST specfic block END =====================================================================


-- get ROWS of att_conf_id for PROCESSOR stationDelayValid Tango attribute
CREATE OR REPLACE FUNCTION get_all_station_delay_attr_ids( ns text ) RETURNS SETOF int
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT att_conf_id FROM att_conf
        WHERE family='processor'
        AND   name='stationdelayvalid'
        AND   domain='low-cbf'
        AND   att_name LIKE concat( '%', ns, '%' );
  END;
$$;
-- TEST: SELECT get_all_station_delay_attr_ids( 'bb-test' );


-- check if OVERALL across all subarrays delay polynomials are valid
-- returns: 0: INVALID, 1: VALID, 2: UNUSED
-- USED BY dashboard: Station Delays / panel: "ALL polynomial delays valid"
CREATE OR REPLACE FUNCTION are_all_delay_poly_valid( ns text ) RETURNS int
STABLE
LANGUAGE plpgsql AS
$$
  DECLARE
    i   int;
    res int := 2; -- 2 == UNUSED
  BEGIN
    FOR i IN SELECT get_all_subarray_delay_valid_ids( ns )
    LOOP
      res := (select LEAST(res, value_r)  FROM att_scalar_devushort WHERE att_conf_id=i ORDER BY data_time DESC LIMIT 1);
    END LOOP;
    RETURN res;
  END;
$$;
-- TEST: SELECT are_all_delay_poly_valid( 'bb-test' );


-- get a single beam array entry as JSON
CREATE OR REPLACE FUNCTION get_beams_array_entry_json( ns text, subarray int, beam_id int ) RETURNS text
STABLE
LANGUAGE plpgsql AS
$$
  DECLARE
    beam_idx int := get_array_index( get_subarray_beams_array( ns, subarray ), beam_id );
    elem_path text[] := '{beams,' || (beam_id-1)::text || '}';
    --                               ^^^^^^^^^^ index of the beam in the array of beams
  BEGIN
    RETURN (SELECT get_station_delays_json( ns, subarray )::jsonb #> elem_path );
  END;
$$;
-- TEST: SELECT get_beams_array_entry_json( 'bb-test', 4, 1 );


-- return RECORDS of beam IDs
-- USED BY: dashboard Station Delays / beam_id variable
CREATE OR REPLACE FUNCTION get_subarray_beams( ns text, subarray int ) RETURNS SETOF int
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT beam_ids::int
    FROM jsonb_path_query( get_station_delays_json( ns, subarray )::jsonb, '$.beams[*].beam_id' ) AS beam_ids;
  END;
$$;
-- TEST: SELECT get_subarray_beams( 'bb-test', 4 );

-- return an ARRAY of beam IDs for the given subarray
CREATE OR REPLACE FUNCTION get_subarray_beams_array( ns text, subarray int ) RETURNS int[]
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN ARRAY( SELECT beam_ids::int
                  FROM jsonb_path_query( get_station_delays_json( ns, subarray )::jsonb, '$.beams[*].beam_id' ) AS beam_ids
                );
  END;
$$;
-- TEST: SELECT get_subarray_beams_array( 'bb-test', 4 );
--       SELECT array_position( get_subarray_beams_array( 'bb-test', 4), 1 ) - 1;

-- return 0 based index of 'element' in the array 'arr'
CREATE OR REPLACE FUNCTION get_array_index( arr int[], element int ) RETURNS int
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN (SELECT array_position( arr, element ) - 1);
  END;
$$;
-- TEST: SELECT get_array_index( get_subarray_beams_array( 'bb-test', 4), 1 );



-- return records of { "stn": 345, "ns": 3464.13 } format
CREATE OR REPLACE FUNCTION get_delays_full_table( ns text, subarray int, beam_id int ) RETURNS SETOF jsonb
STABLE
LANGUAGE plpgsql AS
$$
  DECLARE
    beam_idx int := get_array_index( get_subarray_beams_array( ns, subarray ), beam_id );
    elem_path text[] := '{beams,' || beam_idx::text || ',stn_delay_ns}';
    --                               ^^^^^^^^ index of the beam in the array of beams
  BEGIN
    RETURN QUERY SELECT *
    FROM jsonb_array_elements( get_station_delays_json( ns, subarray )::jsonb #> elem_path);
  END;
$$;
-- TEST: SELECT get_delays_full_table( 'bb-test', 4, 1 );


-- return records with two columns: station_id, delay_ns, e.g. 345 | -15982.67
CREATE OR REPLACE FUNCTION get_delay_values( ns text, subarray int, beam int )
       RETURNS TABLE (station text, delay text)
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT j->>'stn', j->>'ns'
           FROM get_delays_full_table( ns, subarray, beam ) AS jdata(j);
  END;
$$;
-- TEST: SELECT * FROM get_delay_values( 'bb-test', 4, 1 );

-- -----------------------------------------------------------------------------
-- Return station beams for the specified subarray (and namespace)
--
-- Used by Grafana to populate beam_id variable (drop-down list) in
-- "low-cbf / Station delays" panel
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_station_beams_in_subarray( ns text, subarray int )
       RETURNS TABLE (beam int )
STABLE
LANGUAGE plpgsql AS
$$
  BEGIN
    RETURN QUERY SELECT beam_id::int
           FROM get_proc_station_delays_for_subarr( ns, subarray );
  END;
$$;
-- TEST: SELECT beam FROM get_station_beams_in_subarray( 'bb-test', 4 );
